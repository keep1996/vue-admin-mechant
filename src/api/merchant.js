import request from '@/utils/request'
// 商户列表  分页查询
export function getMerchantClientQueryMerchantClientList(data) {
	return request({
		url: '/merchantClient/queryMerchantClientList',
		method: 'post',
		data
	})
}
// 商户列表 查询下拉商户列表
export function getLoadMerchantClientList(data) {
	return request({
		url: '/merchantClient/loadMerchantClientList',
		method: 'post',
		data
	})
}
// 商户列表 配置导入
export function getMerchantClientConfigImport(data) {
	return request({
		url: '/merchantClient/configImport',
		method: 'post',
		data
	})
}
// 商户列表 查询首页推荐模块
export function getMerchantQueryMerchantClientMoudleList(data) {
	return request({
		url: '/merchantClient/queryMerchantClientGameMoudleList',
		method: 'post',
		data
	})
}
// 商户列表 更新商户信息-编辑后保存
export function getMerchantClientUpdateMerchantClient(data) {
	return request({
		url: '/merchantClient/updateMerchantClient',
		method: 'post',
		data
	})
}
// 商户模块-操作记录
export function getMerchantClientOperatequeryMerchantClientOperateList(data) {
	return request({
		url: '/merchantClientOperate/queryMerchantClientOperateList',
		method: 'post',
		data
	})
}

// 商户模块-游戏分类管理-分页查询
export function getMerchantClientGameAssortList(data) {
	return request({
		url: '/merchantClientGameAssort/queryMerchantClientGameAssortList',
		method: 'post',
		data
	})
}

// 商户模块-游戏分类管理-删除分类
export function deleteMerchantClientGameAssort(data) {
	return request({
		url: '/merchantClientGameAssort/deleteMerchantClientGameAssort',
		method: 'post',
		data
	})
}
// 商户模块-游戏分类管理-创建游戏分类
export function addMerchantClientGameAssort(data) {
	return request({
		url: '/merchantClientGameAssort/addMerchantClientGameAssort',
		method: 'post',
		data
	})
}
// 商户模块-游戏分类管理-编辑游戏分类信息
export function updateMerchantClientGameInfo(data) {
	return request({
		url: '/merchantClientGameAssort/updateMerchantClientGameAssort',
		method: 'post',
		data
	})
}
// 商户模块-游戏分类管理-开启/禁用
export function updateMerchantClientGameAssortStatus(data) {
	return request({
		url: '/merchantClientGameAssort/updateAssortStatus',
		method: 'post',
		data
	})
}
// 商户模块-游戏分类管理-分页记录点击子游戏时加载子游戏列表
export function getMerchantClientSubGameList(data) {
	return request({
		url: '/merchantClientGameAssort/queryMerchantClientSubGameList',
		method: 'post',
		data
	})
}
// 商户模块-游戏分类管理-分页记录拖动排序
export function merchantClientGameAssortSort(data) {
	return request({
		url: '/merchantClientGameAssort/sort',
		method: 'post',
		data
	})
}
// 商户- banner排序
export function getOperateconfigBannerqueryCSortedBannerArea(data) {
	return request({
		url: '/operate/configBanner/queryCSortedBannerArea',
		method: 'post',
		data
	})
}
// 商户- 操作记录商户名称
export function getmerchantClientloadNoOBMerchantClientList(data) {
	return request({
		url: '/merchantClient/loadNoOBMerchantClientList',
		method: 'post',
		data
	})
}
export default {
	getMerchantClientQueryMerchantClientList,
	getLoadMerchantClientList,
	getMerchantClientConfigImport,
	getMerchantQueryMerchantClientMoudleList,
	getMerchantClientUpdateMerchantClient,
	getMerchantClientOperatequeryMerchantClientOperateList,
	getMerchantClientGameAssortList,
	deleteMerchantClientGameAssort,
	addMerchantClientGameAssort,
	updateMerchantClientGameInfo,
	updateMerchantClientGameAssortStatus,
	getOperateconfigBannerqueryCSortedBannerArea,
	getmerchantClientloadNoOBMerchantClientList,
	getMerchantClientSubGameList,
	merchantClientGameAssortSort
}
