import request from '@/utils/request'

// 红利管理=>红利发放主标题
export function getBonusActivityTitleAPI(data) {
	return request({
		// url: '/operate/ActivityBonusAudit/activityTitle',
		url: 'operate/ActivityBonusAudit/activityConfigTitle',
		method: 'post',
		data
	})
}
// 红利管理=>红利发放submit
export function getSubmitBonusAPI(data) {
	return request({
		url: '/operate/ActivityBonusAudit/submitBonus',
		method: 'post',
		data
	})
}
// 红利管理=>红利发放下载模版
export function getActiveDownloadAPI(data) {
	return request({
		url: '/operate/ActivityBonusAudit/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 红利管理=>红利发放文件上传
export function getBonusExcelAPI(data) {
	return request({
		url: '/operate/ActivityBonusAudit/checkSubmitBonusExcel',
		method: 'post',
		data
	})
}
export default {
	getBonusActivityTitleAPI,
	getSubmitBonusAPI,
	getActiveDownloadAPI,
	getBonusExcelAPI
}
