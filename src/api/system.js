import request from '@/utils/request'
// 系统===》参数字典
export function getkvconfigQueryList(data) {
	return request({
		url: '/person/getValue',
		method: 'post',
		data
	})
}
// 系统===》参数字典===>修改
export function updateDictConfig(data) {
	return request({
		url: '/system/kvConfig/update',
		method: 'post',
		data
	})
}
// 系统管理-职员登录日志
export function queryStaffLoginLog(data) {
	return request({
		url: '/system/userLoginLog/query4Page',
		method: 'post',
		data
	})
}
// 系统===》参数字典===>修改
export function getUserInfoUpdatePwdAdmin(data) {
	return request({
		url: '/userInfo/updatePwdAdmin',
		method: 'post',
		data
	})
}
// 系统===》系统配置===>查询
export function getPersonList(data) {
	return request({
		url: '/person/list',
		method: 'post',
		data
	})
}
// 系统===》系统配置===>查询模块列表
export function getPersonListModules(data) {
	return request({
		url: '/person/listModules',
		method: 'post',
		data
	})
}
// 系统===》系统配置===>编辑
export function setPersonUpdatet(data) {
	return request({
		url: '/person/update',
		method: 'post',
		data
	})
}
// 系统管理-账户设置获取页面
export function getuserInfolist(params) {
	return request({
		url: '/system/user/getUserById',
		method: 'get',
		params
	})
}
// 系统管理-查询参数字典修改记录列表
export function getQueryRecordList(data) {
	return request({
		url: '/kvconfig/queryRecordList',
		method: 'post',
		data
	})
}
// 系统管理-修改
export function setSystemUserUpdatePwdAdmin(data) {
	return request({
		url: '/system/user/updatePwdAdmin',
		method: 'post',
		data
	})
}
// 第三方回调IP白名单查询
export function getCallbackIpWhiteList(data) {
	return request({
		url: '/callbackIpWhite/select',
		method: 'post',
		data
	})
}
// 新增第三方回调IP白名单
export function addCallbackIpWhite(data) {
	return request({
		url: '/callbackIpWhite/insert',
		method: 'post',
		data
	})
}
// 删除第三方回调IP白名单管理
export function deleteCallbackIpWhite(data) {
	return request({
		url: '/callbackIpWhite/delete',
		method: 'post',
		data
	})
}
// 修改第三方回调IP白名单管理
export function updateCallbackIpWhite(data) {
	return request({
		url: '/callbackIpWhite/update',
		method: 'post',
		data
	})
}
// 修改第三方回调IP白名单状态
export function updateStatusCallbackIpWhite(data) {
	return request({
		url: '/callbackIpWhite/updateStatus',
		method: 'post',
		data
	})
}
// ip白名单列表分页查询
export function queryBackEndIpConfigList(data) {
	return request({
		url: '/ipWhiteInfo/listPage',
		method: 'post',
		data
	})
}
// 添加ip白名单
export function addBackEndIpConfig(data) {
	return request({
		url: '/ipWhiteInfo/add',
		method: 'post',
		data
	})
}
// 删除ip白名单
export function deleteBackEndIpConfig(data) {
	return request({
		url: '/ipWhiteInfo/del',
		method: 'post',
		data
	})
}
// 编辑ip白名单
export function updateBackEndIpConfig(data) {
	return request({
		url: '/ipWhiteInfo/edit',
		method: 'post',
		data
	})
}

// 域名管理==>域名分页查询
export function getDomainList(data) {
	return request({
		url: '/system/configDomain/query4Page',
		method: 'post',
		data
	})
}

// 域名管理==>下拉框列表
export function getDomainTypeList(data) {
	return request({
		url: '/system/configDomain/domainTypeList',
		method: 'post',
		data
	})
}

// 域名管理==>删除域名
export function deleteDomain(data) {
	return request({
		url: '/system/configDomain/delete',
		method: 'post',
		data
	})
}

// 域名管理==>编辑APP域名
export function updateAppDomain(data) {
	return request({
		url: '/system/configDomain/update',
		method: 'post',
		data
	})
}
// 域名管理==>新增APP域名
export function addAppDomain(data) {
	return request({
		url: '/system/configDomain/add',
		method: 'post',
		data
	})
}

// 域名管理==>批量导入域名
export function importDomainExcel(data) {
	return request({
		url: '/system/configDomain/batchAdd',
		method: 'post',
		data
	})
}
// 域名管理==>下载域名模板
export function downLoadDomainTemplate(data) {
	return request({
		url: '/system/configDomain/downLoadTemplate',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 域名管理==>上传文件
export function upLoadDomainFile(data) {
	return request({
		url: '/system/configDomain/upLoad',
		method: 'post',
		data
	})
}
// 版本号配置==>分页查询App版本号配置
export function getappVersionConfigpageList(data) {
	return request({
		url: '/system/appVersionConfig/page4Page',
		method: 'post',
		data
	})
}
// 版本号配置==>App版本号启用禁用
export function updateVersionConfigStatus(data) {
	return request({
		url: '/system/appVersionConfig/onOff',
		method: 'post',
		data
	})
}
// 版本号配置==>App版本号配置创建
export function createAppVersionConfig(data) {
	return request({
		url: '/system/appVersionConfig/add',
		method: 'post',
		data
	})
}
// 版本号配置==>App版本号配置修改
export function updateAppVersionConfig(data) {
	return request({
		url: '/system/appVersionConfig/update',
		method: 'post',
		data
	})
}

// 海月盾验证接口
export function seaMoonValidate(data) {
	return request({
		url: '/seaMoonShield/seaMoonShieldVer',
		method: 'post',
		data
	})
}

export function queryDictModuleList(data) {
	return request({
		url: '/system/kvConfig/loadModuleList',
		method: 'post',
		data
	})
}
export function queryUserTypeListApi(data) {
	return request({
		url: '/system/user/loadUserType',
		method: 'post',
		data
	})
}
// 系统===》根据key获取参数字典===>{k:''}
export function getSystemKvconfigValue(data) {
	return request({
		url: '/system/kvConfig/getKvconfigValue',
		method: 'post',
		data
	})
}
// 系统===》根据tag获取参数字典===>{tag:''}
export function getSystemKvconfigByTag(data) {
	return request({
		url: '/system/kvConfig/getKvconfigByTag',
		method: 'post',
		data
	})
}
// 系统-获取密钥验证方式
export function getSystemValidateKeyType(data) {
	return request({
		url: '/dict/getVerifyCodeType',
		method: 'post',
		data
	})
}

// 运营===》信息配置管理===>系统维护白名单列表
export function sysMaintenanceWhiteList(data) {
	return request({
		url: '/sysMaintenanceWhite/list',
		method: 'post',
		data
	})
}
// 运营===》信息配置管理===>系统维护白名单新增
export function sysMaintenanceWhiteAdd(data) {
	return request({
		url: '/sysMaintenanceWhite/add',
		method: 'post',
		data
	})
}
// 运营===》信息配置管理===>系统维护白名单删除
export function sysMaintenanceWhiteDelete(data) {
	return request({
		url: '/sysMaintenanceWhite/delete',
		method: 'post',
		data
	})
}
// 系统===>操作记录===>获取操作类型
export function getSystemOperationType(params) {
	return request({
		url: '/system/audit/operation/record/operationType',
		method: 'get',
		params
	})
}

// 系统===>操作记录===>查询操作记录
export function getSystemOperationRecords(data) {
	return request({
		url: '/system/audit/operation/records',
		method: 'post',
		data
	})
}
// 系统===>操作记录===>获取操作类型
export function getSystemRecordById(params) {
	return request({
		url: `/system/audit/operation/record/${params.id}`,
		method: 'get',
		params
	})
}

export default {
	getkvconfigQueryList,
	updateDictConfig,
	queryStaffLoginLog,
	getPersonList,
	getPersonListModules,
	setPersonUpdatet,
	getuserInfolist,
	getQueryRecordList,
	getCallbackIpWhiteList,
	addCallbackIpWhite,
	deleteCallbackIpWhite,
	updateCallbackIpWhite,
	updateStatusCallbackIpWhite,
	queryBackEndIpConfigList,
	addBackEndIpConfig,
	deleteBackEndIpConfig,
	updateBackEndIpConfig,
	getDomainList,
	getDomainTypeList,
	deleteDomain,
	importDomainExcel,
	downLoadDomainTemplate,
	upLoadDomainFile,
	getappVersionConfigpageList,
	updateVersionConfigStatus,
	createAppVersionConfig,
	updateAppVersionConfig,
	seaMoonValidate,
	updateAppDomain,
	addAppDomain,
	queryDictModuleList,
	queryUserTypeListApi,
	setSystemUserUpdatePwdAdmin,
	getSystemKvconfigValue,
	getSystemKvconfigByTag,
	sysMaintenanceWhiteList,
	sysMaintenanceWhiteAdd,
	sysMaintenanceWhiteDelete,
	getSystemOperationType,
	getSystemOperationRecords,
	getSystemRecordById,
	getSystemValidateKeyType
}
