import request from '@/utils/request'

// 查询菜单列表
export function queryMenuListApi(data) {
	return request({
		url: '/system/menu/quer4Page',
		method: 'post',
		data
	})
}

// 新增菜单
export function addMenuApi(data) {
	return request({
		url: '/system/menu/addSubMenu',
		method: 'post',
		data: data
	})
}

// 修改菜单
export function updateMenuApi(data) {
	return request({
		url: '/system/menu/update',
		method: 'post',
		data: data
	})
}

// 删除菜单
export function delMenuApi(data) {
	return request({
		url: '/system/menu/delete',
		method: 'post',
		data
	})
}
