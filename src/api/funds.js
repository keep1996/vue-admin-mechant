import request from '@/utils/request'

export default {
		// dkdk
		getAuditDetailQTList(data) {
			return request({
				url: '/report/bet-record/member/venue/summary',
				method: 'post',
				data
			})
		},
		// 代理德州场馆返水明细弹窗 ==> 列表查询
		getDZMemberRebateDetailList(data) {
			return request({
				url: '/report/order-record/member/list',
				method: 'post',
				data
			})
		},
		// 代理德州场馆返水明细弹窗 ==> 列表查询
		getDZMemberRebateDetailSummary(data) {
			return request({
				url: '/report/order-record/member/summary',
				method: 'post',
				data
			})
		},
		// 会员其他场馆注单明细弹窗 ==> 列表查询
		getQTMemberRebateDetailList(data) {
			return request({
				url: '/report/bet-record/member/list',
				method: 'post',
				data
			})
		},
		// 会员其他场馆注单明细弹窗 ==> 汇总查询
		getQTMemberRebateDetailSummary(data) {
			return request({
				url: '/report/bet-record/member/summary',
				method: 'post',
				data
			})
		},
		// 代理德州场馆返水明细弹窗 ==> 列表查询
	getDZRebateDetailList(data) {
		return request({
			url: '/report/order-record/list',
			method: 'post',
			data
		})
	},
	// 代理德州场馆返水明细弹窗 ==> 汇总查询
	getDZRebateDetailSummary(data) {
		return request({
			url: '/report/order-record/summary',
			method: 'post',
			data
		})
	},
	// 代理其他场馆注单明细弹窗 ==> 列表查询
	getQTRebateDetailList(data) {
		return request({
			url: '/report/bet-record/list',
			method: 'post',
			data
		})
	},
	// 代理其他场馆注单明细弹窗 ==> 汇总查询
	getQTRebateDetailSummary(data) {
		return request({
			url: '/report/bet-record/summary',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员账变记录 ==> 列表查询
	getMemberWalletChangeList(data) {
		return request({
			url: '/member/wallet/change/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员账变记录 ==> 账变类型下拉框
	getMemberWalletChangeDic(params) {
		return request({
			url: '/member/wallet/change/walletChangeDic',
			method: 'get',
			params
		})
	},
	// 会员资金记录 ==> 会员账变记录 ==> 导出
	getMemberWalletChangeExport(data) {
		return request({
			url: '/member/wallet/change/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getTopupManagementList(data) {
		return request({
			url: '/manualTopUpListManagement/getPageList',
			method: 'post',
			data
		})
	},
	getTopupManagementById(data) {
		return request({
			url: '/manualTopUpListManagement/getConfigById',
			method: 'post',
			data
		})
	},
	getAdjustTypes(data) {
		return request({
			url: '/funds/amountDetail/getAdjustTypes',
			method: 'get',
			data
		})
	},
	manualQuotaControlAdjustment(data) {
		return request({
			url: '/manualTopUpListManagement/manualQuotaControlAdjustment',
			method: 'post',
			data
		})
	},
	checkSysUserExist(data) {
		return request({
			url: '/manualTopUpListManagement/getCheckSysUserInfoExist',
			method: 'post',
			data
		})
	},
	addTopupConfig(data) {
		return request({
			url: '/manualTopUpListManagement/addConfig',
			method: 'post',
			data
		})
	},
	deleteTopupConfig(data) {
		return request({
			url: '/manualTopUpListManagement/delConfigById',
			method: 'post',
			data
		})
	},
	topupAdjustList(data) {
		return request({
			url: '/funds/amountDetail/adjust/list',
			method: 'post',
			data
		})
	},
	topupUsedList(data) {
		return request({
			url: '/funds/amountDetail/consume/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员存款记录 ==> 列表查询
	getMemberDepositRecordList(data) {
		return request({
			url: '/member/deposit/record/list',
			method: 'post',
			data
		})
	},
	getOnlinePayWithdrawList(data) {
		return request({
			url: '/finance/query/onlinePay/withdraw/list',
			method: 'post',
			data
		})
	},

	getPlatformRecordList(data) {
		return request({
			url: '/platform/deposit/record/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员存款记录 ==> 导出
	getMemberDepositRecordExport(data) {
		return request({
			// url: '/member/deposit/list/export',
			url: '/member/deposit/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员提现下分 ==> 列表查询
	getMemberWithdrawRecordList(data) {
		return request({
			url: '/member/withdraw/record/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员提款记录 ==> 列表查询
	getMemberWithdrawOnlinePayRecordList(data) {
		return request({
			url: '/memberFundsRecords/withdraw/onlinePay/record/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员提款记录 ==> 导出
	getMemberWithdrawRecordExport(data) {
		return request({
			// url: '/member/withdraw/list/export',
			url: '/memberFundsRecords/withdraw/onlinePay/record/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getFinanceWithdrawRecordExport(data) {
		return request({
			// url: '/member/withdraw/list/export',
			url: '/finance/query/onlinePay/withdraw/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getPlatformExport(data) {
		return request({
			// url: '/member/withdraw/list/export',
			url: '/platform/deposit/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员转账记录 ==> 列表查询
	getMemberTransferRecordsList(data) {
		return request({
			url: '/memberTransferRecords/listPage',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员转账记录 ==> 列表查询
	getMemberTransferRecordsExport(data) {
		return request({
			url: '/memberTransferRecords/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员活动记录 ==> 列表查询
	getMemberActivityRecordList(data) {
		return request({
			url: '/member/activity/discount/record/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员活动记录 ==> 导出
	getMemberActivityRecordExport(data) {
		return request({
			url: '/member/activity/discount/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员充值优惠记录 ==> 列表查询
	getMemberDepositDiscountRecordList(data) {
		return request({
			url: '/member/deposit/discount/record/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员充值优惠记录 ==> 导出
	getMemberDepositDiscountRecordExport(data) {
		return request({
			url: '/member/deposit/discount/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员返水记录 ==> 列表查询
	getMemberRebateReportList(data) {
		return request({
			url: '/report/memberRebate/list',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员返水记录 ==> 总计
	getMemberRebateReportSummary(data) {
		return request({
			url: '/memberFundsRecords/rebateReport/summary',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员返水记录   详情 ==> 列表查询
	getMemberRebateReportDetailList(data) {
		return request({
			url: '/report/memberRebate/detailList',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员返水记录   详情 ==> 总计
	getMemberRebateReportDetailSummary(data) {
		return request({
			url: '/report/memberRebate/detailSummary',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员返水记录 ==> 导出
	getMemberRebateReportExport(data) {
		return request({
			url: '/report/memberRebate/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},

	// 会员资金记录 ==> 会员人工增加记录 ==> 列表查询
	getMemberAddRecordList(data) {
		return request({
			url: '/memberFundsRecords/memberArtificialAddRecord',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员人工增加记录 ==> 总计
	getMemberAddRecordSummary(data) {
		return request({
			url: '/memberFundsRecords/memberArtificialAddRecordTotal',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员人工增加记录 ==> 导出
	getMemberAddRecordExport(data) {
		return request({
			url: '/memberFundsRecords/memberArtificialAddRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员资金记录 ==> 会员人工扣除记录 ==> 列表查询
	getMemberAccountSubList(data) {
		return request({
			url: '/memberFundsRecords/artificialAccountSub',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员人工扣除记录 ==> 总计
	getMemberAccountSubSummary(data) {
		return request({
			url: '/memberFundsRecords/memberArtificialSubTotal',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员人工扣除记录 ==> 导出
	getMemberAccountSubExport(data) {
		return request({
			url: '/memberFundsRecords/artificialAccountSub/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},

	// 会员资金记录 ==> 会员福利记录 ==> 列表查询
	getMemberVipRewardList(data) {
		return request({
			url: '/memberFundsRecords/vipReward/listPage',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员福利记录 ==> 总计
	getMemberVipRewardSummary(data) {
		return request({
			url: '/memberFundsRecords/vipReward/summary',
			method: 'post',
			data
		})
	},
	// 会员资金记录 ==> 会员福利记录 ==> 导出
	getMemberVipRewardExport(data) {
		return request({
			url: '/memberFundsRecords/vipReward/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 佣金收付记录 ==> 列表查询
	getProxyCommissionPaymentList(data) {
		return request({
			url: '/proxyCommissionPayment/selectRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 佣金收付记录 ==> 导出
	getProxyCommissionPaymentExport(data) {
		return request({
			url: '/proxyCommissionPayment/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理账变记录 ==> 列表查询
	getProxyWalletChangeList(data) {
		return request({
			url: '/proxyFundsRecords/proxy/wallet/change/list',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理账变记录 ==> 导出
	getProxyWalletChangeExport(data) {
		return request({
			url: '/proxyFundsRecords/proxy/wallet/change/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理存款记录 ==> 列表查询
	getProxyDepositRecordList(data) {
		return request({
			url: '/proxyFundsRecords/proxy/deposit/record/list',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理存款记录 ==> 导出
	getProxyDepositRecordExport(data) {
		return request({
			// url: '/proxyFundsRecords/proxy/deposit/record/list/export',
			url: '/proxyFundsRecords/proxy/deposit/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理提款记录 ==> 列表查询
	getProxyWithdrawRecordList(data) {
		return request({
			url: '/proxyFundsRecords/withdraw/record/list',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理提款记录 ==> 导出
	getProxyWithdrawRecordExport(data) {
		return request({
			url: '/proxyFundsRecords/withdraw/record/list/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理代存记录 ==> 列表查询
	getProxyFundsRecordsAssistDepositList(data) {
		return request({
			url: '/proxyFundsRecords/assistDeposit',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理代存记录 ==> 导出
	getProxyFundsRecordsAssistDepositExport(data) {
		return request({
			url: '/proxyFundsRecords/assistDeposit/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 转回中心钱包记录 ==> 列表查询
	getProxyFundsRecordsTransferList(data) {
		return request({
			url: '/proxyFundsRecords/transfer',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 转回中心钱包记录 ==> 导出
	getProxyFundsRecordsTransferExport(data) {
		return request({
			url: '/proxyFundsRecords/transfer/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理转账记录 ==> 列表查询
	getProxyFundsRecordsProxyTransferList(data) {
		return request({
			url: '/proxyFundsRecords/proxyTransferRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理转账记录 ==> 导出
	getProxyFundsRecordsProxyTransferExport(data) {
		return request({
			url: '/proxyFundsRecords/proxyTransferRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 总代佣金记录 ==> 列表查询
	getGeneralProxyCommissionList(data) {
		return request({
			url: '/generalProxyCommission/selectRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 总代佣金记录 ==> 总计
	getGeneralProxyCommissionSummary(data) {
		return request({
			url: '/generalProxyCommission/summary',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 总代佣金记录 ==> 导出
	getGeneralProxyCommissionExport(data) {
		return request({
			url: '/generalProxyCommission/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 非总代佣金记录 ==> 列表查询
	getNoGeneralProxyCommissionList(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralselectRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 非总代佣金记录 ==> 总计
	getNoGeneralProxyCommissionSummary(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralSummary',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 非总代佣金记录 ==> 导出
	getNoGeneralProxyCommissionExport(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralProxCommissionExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 列表查询
	getProxyReBateRecordList(data) {
		return request({
			url: '/proxyRebate/selectReBateRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 总计
	getProxyReBateRecordSummary(data) {
		return request({
			url: '/proxyRebate/reBateRecordSummary',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 返点审核详情==>分页查询==>活跃人数
	getProxyPageSumActivityMember(data) {
		return request({
			url: '/proxyRebate/pageSumActivityMember',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 返点审核详情==>分页查询==>有效活跃下级
	getProxyPageSumValidActivityMember(data) {
		return request({
			url: '/proxyRebate/pageSumValidActivityMember',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 导出
	getProxyReBateRecordExport(data) {
		return request({
			url: '/proxyRebate/proxyReBateRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理返点记录 ==> 代理账号信息
	getProxyAccountInfo(data) {
		return request({
			url: '/proxyDetail/queryDetail',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 收益审核 ==> 代理账号信息
	queryAgentDetailNew(data) {
		return request({
			url: '/proxyDetail/queryDetailNew',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理人工加额度记录 ==> 列表查询
	getProxyArtificialAddRecordList(data) {
		return request({
			url: '/proxyFundsRecords/proxyArtificialAddRecord',
			method: 'post',
			data
		})
	},
	getArtificialDepositList(data) {
		return request({
			url: '/artificial/deposit/withdraw/list',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理人工加额度记录 ==> 总计
	getProxyArtificialAddRecordSummary(data) {
		return request({
			url: '/proxyFundsRecords/proxyArtificialAddRecordTotal',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理人工加额度记录 ==> 导出
	getProxyArtificialAddRecordExport(data) {
		return request({
			url: '/proxyFundsRecords/proxyArtificialAddRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getArtificialDownloadExport(data) {
		return request({
			url: '/artificial/deposit/withdraw/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理人工减额记录 ==> 列表查询
	getProxyArtificialAccountSubList(data) {
		return request({
			url: '/proxyFundsRecords/artificialAccountSub',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理人工减额记录 ==> 总计
	getProxyArtificialAccountSubSummary(data) {
		return request({
			url: '/proxyFundsRecords/proxyArtificialSubTotal',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理人工减额记录 ==> 导出
	getProxyArtificialAccountSubExport(data) {
		return request({
			url: '/proxyFundsRecords/artificialAccountSub/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理佣金操作记录 ==> 列表查询
	getSelectCommissionOperateRecordList(data) {
		return request({
			url: '/generalProxyCommission/selectCommissionOperateRecord',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理佣金操作记录 ==> 总计
	getSelectCommissionOperateRecordTotal(data) {
		return request({
			url: '/generalProxyCommission/selectCommissionOperateRecordTotal',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返佣记录列表 - 总代
	getTopProxyRebateCommission(data) {
		return request({
			// url: '/proxySettlementCommission/selectReBateRecord',
			url:
				'/settlement/commission/proxy/listTopProxyCommissionRecordPage',
			method: 'post',
			data
		})
	},
	// 代理资金记录 ==> 代理返佣记录列表 - 非总代
	getNotTopProxyRebateCommission(data) {
		return request({
			url:
				'/settlement/commission/proxy/listNotTopProxyCommissionRecordPage',
			method: 'post',
			data
		})
	},

	// 代理资金记录 ==> 代理返佣记录列表 - 总代导出
	exportTopProxyRebateCommission(data) {
		return request({
			url:
				'/settlement/commission/proxy/listTopProxyCommissionRecordPageExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录 ==> 代理返佣记录列表 - 非总代导出
	exportNotTopProxyRebateCommission(data) {
		return request({
			url:
				'/settlement/commission/proxy/listNotTopProxyCommissionRecordPageExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},

	// 佣金记录调整日志
	getLogListPage: (data) => {
		return request({
			url: '/settlement/commission/proxy/log/ListPage',
			method: 'post',
			responseType: 'blob',
			data
		})
	},

	// 资金调整 ==> 会员人工增加、会员人工扣除查询余额
	getMemberBalance(data) {
		return request({
			url: '/artificialPatch/getMemberBalance',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 会员人工增加、会员人工扣除查询余额
	getMemberApply(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/apply',
			method: 'get',
			data
		})
	},
	// 资金调整 ==> 会员人工增加、会员人工扣除查询余额
	getConfigMultiple(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/getConfigMultiple?' + data,
			method: 'get'
		})
	},
	// 资金调整 ==> 会员人工增加保存
	setMemberArtificialPatchAccountAdd(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/apply',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 会员人工扣除保存
	setMemberArtificialPatchAccountSub(data) {
		return request({
			url: '/artificialPatch/member/lessMoney',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 会员红利发放活动标题
	getMemberBonusAuditActivityConfigTitle(data) {
		return request({
			url: '/activity/bonusAudit/activityConfigTitle',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 会员红利发放模板下载
	getMemberBonusAuditDownload(data) {
		return request({
			url: '/activity/bonusAudit/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金调整 ==> 会员红利发放模板上传
	setMemberBonusAuditCheckSubmitBonusExcel(data) {
		return request({
			url: '/activity/bonusAudit/checkSubmitBonusExcel',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 会员红利发放申请
	setMemberBonusAuditSubmitBonus(data) {
		return request({
			url: '/activity/bonusAudit/submitBonus',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 查询加额钱包加额类型
	getProxyGetWalletAdjustType(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/getWalletAdjustType',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 查询减额钱包加额类型
	getProxyGetProxySubAdjustType(data) {
		return request({
			url: '/artificialPatch/getProxySubAdjustType',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 代理人工添加额度申请
	setProxyArtificialPatchAccountAdd(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/apply',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 代理人工扣钱申请
	setProxyProxyLessMoney(data) {
		return request({
			url: '/artificialPatch/proxy/proxyLessMoney',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 代理资金调整-非总代返佣管理
	getNotTopProxyFundAdjustmentCommissionList(data) {
		return request({
			url:
				'/settlement/commission/proxy/listNotTopProxyFundAdjustmentCommissionRecordPage',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 代理资金调整-代发佣金
	setNotTopProxyReplaceIssueCommission(data) {
		return request({
			url: '/settlement/commission/proxy/replaceIssueCommission',
			method: 'post',
			data
		})
	},
	// 资金调整 ==> 代理资金调整-佣金取消
	cancelProxySettlementCommission(data) {
		return request({
			url: '/proxySettlementCommission/cancelAmount',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>查询开启状态的商户
	queryPaymentMerchantConfigEnabled(data) {
		return request({
			url: '/paymentMerchantConfig/query/enabled',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=》查询支付商户列表
	queryPaymentMerchantConfigList(data) {
		return request({
			url: '/paymentMerchantConfig/query',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=》查询支付三商列表
	queryPaymentMerchantList(data) {
		return request({
			url: '/paymentMerchant/list',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=》查询银行下拉
	querySelectBankDropDownList(data) {
		return request({
			url: '/bankManage/selectBankDropDown',
			method: 'post',
			data
		})
	},
	// // 出入款设置 =>三方入款=》新增
	paymentMerchantConfigAdd(data) {
		return request({
			url: '/paymentMerchantConfig/add',
			method: 'post',
			data
		})
	},
	// // 出入款设置 =>三方入款=》启用或者停用
	paymentMerchantConfigStartOrStop(data) {
		return request({
			url: '/paymentMerchantConfig/startOrStop',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=编辑
	paymentMerchantConfigUpdate(data) {
		return request({
			url: '/paymentMerchantConfig/update',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=》支付类型支付通道联动
	triggerDepositChannel(data) {
		return request({
			url: '/paymentMerchantConfig/triggerDepositChannel',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>三方入款=》支付参数联动
	triggerPaymentChannelParam(data) {
		return request({
			url: '/paymentMerchantConfig/paymentChannelParam',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>虚拟币汇率设置=》虚拟币汇率设置
	getVirtualRateRecords(data) {
		return request({
			url: '/virtualRateConfig/query',
			method: 'post',
			data
		})
	},
	// 出入款设置 =>虚拟币汇率设置=》虚拟币汇率设置
	updateVirtualRate(data) {
		return request({
			url: '/virtualRateConfig/update',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>提现下分一审
	selectMemberWithDrawAuthEsPageOne(data) {
		return request({
			url: '/memberWithDrawUser/selectMemberWithDrawAudit1Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>提现下分二审
	selectMemberWithDrawAuthEsPageTwo(data) {
		return request({
			url: '/memberWithDrawUser/selectMemberWithDrawAudit2Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>提现下分审核详情
	selectMemberWithdrawUser(data) {
		return request({
			url: '/memberWithDrawUser/memberWithdrawAuditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>一审待审核
	selectMemberOnlinePayWithDrawAuthEsPageOne(data) {
		return request({
			url: '/memberWithDrawUser/onlinePay/selectMemberWithDrawAudit1Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>二审待审核
	selectMemberOnlinePayWithDrawAuthEsPageTwo(data) {
		return request({
			url: '/memberWithDrawUser/onlinePay/selectMemberWithDrawAudit2Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>三审待审核
	selectMemberOnlinePayWithDrawAuthEsPageThree(data) {
		return request({
			url: '/memberWithDrawUser/onlinePay/selectMemberWithDrawAudit3Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>待出款
	selectMemberOnlinePayWithDrawAuthEsPagePay(data) {
		return request({
			url: '/memberWithDrawUser/onlinePay/selectMemberWithDrawAudit4Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>审核详情
	selectOnlinePayMemberWithdrawUser(data) {
		return request({
			url: '/memberWithDrawUser/onlinePay/memberWithdrawAuditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>锁单
	memberWithDrawUserupdateWithdrawLock(data) {
		return request({
			url: '/memberWithDrawUser/updateWithdrawLock',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>会员取款审核记录=>列表
	memberWithdrawRecordAPI(data) {
		return request({
			// url: '/memberWithDrawUser/audit/record/list',
			url: '/member/withdraw/onlinePay/record/list',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>会员取款审核记录=>详情
	memberWithdrawDetailsAPI(data) {
		return request({
			// url: '/memberWithDrawUser/memberWithdrawAuditDetail',
			url: '/member/withdraw/onlinePay/detail',
			method: 'post',
			data
		})
	},

	// 资金审核=>会员提款管理=>详情=》显示同ip和设备取款记录
	getSelectMemberIPOrDeviceNo(data) {
		return request({
			// url: '/memberWithDrawUser/selectMemberIPOrDeviceNo',
			url: '/memberWithDrawUser/selectIPOrDeviceNo',
			method: 'post',
			data
		})
	},

	// 资金审核=>会员提款管理=>详情=>审核通过
	memberWithDrawUserupdateWithdraw(data) {
		return request({
			url: '/memberWithDrawUser/updateWithdraw',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>会员提款确认-取消出款
	memberWithDrawUserconfirmOrCancelWithdraw(data) {
		return request({
			url: '/memberWithDrawUser/confirmOrCancelWithdraw',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>会员提款确认-刷新会员取款订单出款状态
	memberWithDrawUserRefreshOrderStatus(data) {
		return request({
			url: '/memberWithDrawUser/refreshOrderStatus',
			method: 'post',
			data
		})
	},
	// 充提管理=>查询会员-代理，充值-提款通道配置
	getPaymentChannelConfigMemberDeposit(data) {
		return request({
			url: '/paymentChannelConfig/query',
			method: 'post',
			data
		})
	},
	getCreditLineConfigList(data) {
		return request({
			url: '/funds/creditLineConfig/list',
			method: 'post',
			data
		})
	},
	getDownloadConfigList(data) {
		return request({
			url: '/DownloadConfig/list',
			method: 'post',
			data
		})
	},
	getJoinConfigList(data) {
		return request({
			url: '/joinConfig/list',
			method: 'post',
			data
		})
	},
	getCreditLineConfigGetProxy(data) {
		return request({
			url: '/funds/creditLineConfig/getProxy',
			method: 'post',
			data
		})
	},
	// 充提管理=>查询会员-编辑会员-代理，充值-提款充值通道配
	updatePaymentChannelConfig(data) {
		return request({
			url: '/paymentChannelConfig/update',
			method: 'post',
			data
		})
	},
	addPaymentChannelConfig(data) {
		return request({
			url: '/funds/creditLineConfig/add',
			method: 'post',
			data
		})
	},
	addDownloadConfigList(data) {
		return request({
			url: '/DownloadConfig/add',
			method: 'post',
			data
		})
	},
	addJoinConfigList(data) {
		return request({
			url: '/joinConfig/add',
			method: 'post',
			data
		})
	},
	dePaymentChannelConfig(data) {
		return request({
			url: '/funds/creditLineConfig/delete',
			method: 'post',
			data
		})
	},
	deAppChannelConfig(data) {
		return request({
			url: '/DownloadConfig/delete',
			method: 'post',
			data
		})
	},
	deJoinConfigChannelConfig(data) {
		return request({
			url: '/joinConfig/delete',
			method: 'post',
			data
		})
	},
	updateCreditLineConfigUpdate(data) {
		return request({
			url: '/funds/creditLineConfig/update',
			method: 'post',
			data
		})
	},
	updateDefaultConfig(data) {
		return request({
			url: '/DownloadConfig/updateDefaultConfig',
			method: 'post',
			data
		})
	},
	joinConfigUpdateDefaultConfig(data) {
		return request({
			url: '/joinConfig/updateDefaultConfig',
			method: 'post',
			data
		})
	},
	updateDownloadConfigUpdate(data) {
		return request({
			url: '/DownloadConfig/update',
			method: 'post',
			data
		})
	},
	updateJoinConfigUpdaAte(data) {
		return request({
			url: '/joinConfig/update',
			method: 'post',
			data
		})
	},
	getCreditLineConfig(data) {
		return request({
			url: '/funds/creditLineConfig/get',
			method: 'post',
			data
		})
	},
	getdefaultConfig(data) {
		return request({
			url: '/DownloadConfig/defaultConfig',
			method: 'post',
			data
		})
	},
	getJoinConfigdefaultConfig(data) {
		return request({
			url: '/joinConfig/defaultConfig',
			method: 'post',
			data
		})
	},
	// 充提管理=>查询会员-代理提款配置
	getPaymentChannelConfigWithdraw(data) {
		return request({
			url: '/paymentChannelConfig/list/proxy/withdraw',
			method: 'post',
			data
		})
	},
	// 充提管理=>代理提款配置-新增代理提款配置
	addProxyWithdraw(data) {
		return request({
			url: '/paymentChannelConfig/addProxy/withdraw',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款管理=>审核详情
	proxyWithDrawUserMemberWithdrawAuditDetail(data) {
		return request({
			// url: '/proxyWithDrawUser/memberWithdrawAuditDetail',
			url: '/proxyWithDrawUser/onlinePay/proxyWithdrawAuditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款管理=>刷新代理取款订单出款状态
	proxyWithDrawUserRefreshOrderStatus(data) {
		return request({
			url: '/proxyWithDrawUser/refreshOrderStatus',
			method: 'post',
			data
		})
	},
	// 充提管理=>编辑会员-代理提款配置
	updateConfigWithdraw(data) {
		return request({
			url: '/paymentChannelConfig/update/withdraw',
			method: 'post',
			data
		})
	},
		// 资金审核=>会员审核加额-=>待一审列表
	firstAuditAddAudit(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/firstAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员审核加额-=>待二审列表
	secondAddAudit(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/secondAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员审核加额-=>锁单
	memberArtificialPatchAccountAddAuditLockRecord(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/lockRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员审核加额-=>记录列表
	memberIncreaseQuotaRecordAPI(data) {
		return request({
			url: '/memberFundsRecords/memberArtificialAddAuditRecord',
			method: 'post',
			data
		})
	},

	// 资金审核=>会员审核加额-=>详情
	memberArtificialPatchAccountAddAuditAuditDetail(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/auditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员审核加额-=>详情
	proxyArtificialPatchAccountAddAuditDetail(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/auditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员审核加额-=>审核
	memberArtificialPatchAccountAddAuditauditRecord(data) {
		return request({
			url: '/memberArtificialPatchAccountAdd/auditRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>一审查询
	getProxyCommissionAuditFirstAuditList(data) {
		return request({
			url: '/proxyCommissionAudit/selectFirstAudit',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>二审查询
	getProxyCommissionAuditSecondAuditList(data) {
		return request({
			url: '/proxyCommissionAudit/selectSecondAudit',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>代理佣金审核记录
	getProxyCommissionSelectAuditRecordList(data) {
		return request({
			url: '/proxySettlementCommission/selectAuditRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>一审查询和二审查询
	getProxyCommissionWaitAuditList(data) {
		return request({
			url: '/proxySettlementCommission/waitAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核=>审核详情
	getProxySettlementCommissionDetail(data) {
		return request({
			url: '/settlement/commission/proxy/detail',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核=>审核信息
	getProxySettlementCommissionAudit(data) {
		return request({
			url: '/proxySettlementCommission/queryProxyRebateAuditDetailHeader',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>锁单
	proxyCommissionAuditLockRecord(data) {
		return request({
			url: '/proxyCommissionAudit/lock',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>审核通过和拒绝
	proxyCommissionAuditRecord(data) {
		return request({
			url: '/proxySettlementCommission/auditRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核=>返佣金回查
	getProxyCommissionAdjustAmount(data) {
		return request({
			url: '/settlement/commission/proxy/log/getLastLogRecordByProxyId',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核=>返佣加减额
	setProxyCommissionAdjustAmount(data) {
		return request({
			url: '/proxySettlementCommission/adjustAmount',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>审核记录
	proxyCommissionAuditBatchLock(data) {
		return request({
			url: '/proxyCommissionAudit/batchLock',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=> 单个锁单和批量锁单
	proxySettlementCommissionRecord(data) {
		return request({
			url: '/proxySettlementCommission/lockRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>审核详情
	geCommissionAuditDetails(data) {
		return request({
			url: '/proxyCommissionAudit/commissionAuditDetails',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理佣金审核-=>审核详情 查看详情
	getChildProxyCommissionDetail(data) {
		return request({
			url: '/proxyCommissionAudit/childProxyCommissionDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>审核查询
	getProxyRebateWaitAuditList(data) {
		return request({
			url: '/proxyRebate/waitAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>审核记录查询
	getProxyRebateSelectAuditRecord(data) {
		return request({
			url: '/proxyRebate/selectAuditRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>锁单
	proxyRebateLockRecord(data) {
		return request({
			url: '/proxyRebate/lockRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>审核详情
	getProxyRebateAuditDetail(data) {
		return request({
			url: '/proxyRebate/queryProxyRebateAuditDetailHeader',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>审核详情=》返点账目审核
	getProxyRateDetailPageAPI(data) {
		return request({
			url: '/proxyRebate/getProxyRateDetailPage',
			method: 'post',
			data
		})
	},
	// 资金审核=>返点审核-=>审核
	proxyRebateAuditAPI(data) {
		return request({
			url: '/proxyRebate/auditRecord',
			method: 'post',
			data
		})
	},

	// 资金=>代理收益记录，审核列表， 审核记录
	proxyIncomeList(data) {
		return request({
			url: '/report/proxy/income/audit/list',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益记录总计
	getProxyIncomListSummary(data) {
		return request({
			url: '/report/proxy/income/audit/summary',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益记录，审核详情加减金额查询
	getAdjustAmount(data) {
		return request({
			url: '/report/revenue/queryPopup',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益记录，审核详情加减金额
	revenueAdjustAmount(data) {
		return request({
			url: '/report/revenue/adjustAmount',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益记录，审核详情加减金额
	queryServiceRateAmount(data) {
		return request({
			url: '/report/revenue/queryServiceRateAmount',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益 审核详情
	proxyIncomeAuditDetail(data) {
		return request({
			url: '/report/proxy/income/audit/detail',
			method: 'post',
			data
		})
	},

	// 资金=>代理收益 锁单 解锁
	proxyIncomeAuditLock(data) {
		return request({
			url: '/report/proxy/income/auditLock',
			method: 'post',
			data
		})
	},

	// 资金=>代理收益 批量审核
	proxyIncomeAuditRecord(data) {
		return request({
			url: '/report/proxy/income/auditRecord',
			method: 'post',
			data
		})
	},

	// 资金=>代理收益  收益比例弹窗
	proxyIncomeIncomeDetail(data) {
		return request({
			url: '/report/proxy/income/income/detail',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益  收益比例弹窗
	proxyIncomeEffectsDetail(data) {
		return request({
			url: '/rebateRate/effectives',
			method: 'post',
			data
		})
	},
	// 资金=>代理收益 记录下载
	proxyIncomeExport(data) {
		return request({
			url: '/report/proxy/income/audit/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金审核=>佣金审核-=>审核详情调整佣金
	adjustCommissionAPI(data) {
		return request({
			url: '/proxyCommissionAudit/adjustCommission',
			method: 'post',
			data
		})
	},
	// 资金审核=>佣金审核-=>审核
	proxyCommissionAuditAPI(data) {
		return request({
			url: '/proxyCommissionAudit/audit',
			method: 'post',
			data
		})
	},
	// 资金审核=>佣金审核-=>审核详情=>场馆费用
	getProxyVenueFeeAPI(data) {
		return request({
			url: '/proxyCommissionAudit/proxyVenueFee',
			method: 'post',
			data
		})
	},

	// 资金审核=>代理提款审核=>代理提款审核列表待一审
	proxyselectMemberWithDrawAuthEsPageOne(data) {
		return request({
			// url: '/proxyWithDrawUser/selectProxyWithDrawAudit1Page',
			url: '/proxyWithDrawUser/onlinePay/selectProxyWithDrawAudit1Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>代理提款审核列表待二审
	proxyselectMemberWithDrawAuthEsPageTwo(data) {
		return request({
			// url: '/proxyWithDrawUser/selectProxyWithDrawAudit2Page',
			url: '/proxyWithDrawUser/onlinePay/selectProxyWithDrawAudit2Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>代理提款审核列表待三审
	proxyselectMemberWithDrawAuthEsPageThree(data) {
		return request({
			// url: '/proxyWithDrawUser/selectProxyWithDrawAudit3Page',
			url: '/proxyWithDrawUser/onlinePay/selectProxyWithDrawAudit3Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>代理提款审核列表待出款
	proxySelectMemberWithDrawAuthEsPagePay(data) {
		return request({
			// url: '/proxyWithDrawUser/selectProxyWithDrawAudit4Page',
			url: '/proxyWithDrawUser/onlinePay/selectProxyWithDrawAudit4Page',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>详情=》显示同ip和设备取款记录
	proxyWithDrawUserSelectMemberIPOrDeviceNo(data) {
		return request({
			// url: '/proxyWithDrawUser/selectMemberIPOrDeviceNo',
			url: '/proxyWithDrawUser/selectIPOrDeviceNo',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>锁单
	memberWithDrawProxyUpdateWithdrawLock(data) {
		return request({
			url: '/proxyWithDrawUser/updateWithdrawLock',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>审核
	memberWithDrawProxyUpdateWithdraw(data) {
		return request({
			url: '/proxyWithDrawUser/updateWithdraw',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理提款审核=>审核
	proxyWithDrawUserConfirmOrCancelWithdraw(data) {
		return request({
			url: '/proxyWithDrawUser/confirmOrCancelWithdraw',
			method: 'post',
			data
		})
	},

	// 资金审核=>代理提款审核=>审核详情
	memberWithDrawProxySelectMemberWithdrawProxy(data) {
		return request({
			url: '/proxyWithDrawUser/memberWithdrawAuditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>代理提款审核记录=>审核记录列表
	agentWithdrawRecordAPI(data) {
		return request({
			// url: '/proxyWithDrawUser/audit/record/list',
			url: '/proxy/withdraw/onlinePay/record/list',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>代理提款审核记录=>审核记录详情
	agentWithdrawAuditDetail(data) {
		return request({
			url: '/proxy/withdraw/onlinePay/detail',
			method: 'post',
			data
		})
	},
	// 资金审核=>会员提款管理=>审核详情
	selectAgentWithdrawUser(data) {
		return request({
			url: '/memberWithDrawUser/memberWithdrawAuditDetail',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理人工加额=>一审列表

	firstAuditAddAuditProxy(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/firstAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理人工加额=>一审列表
	secondAddAuditProxy(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/secondAudit/page',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理人工加额=>锁单
	proxyArtificialPatchAccountAddAuditlockRecord(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/lockRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理人工加额=>审核
	proxyArtificialPatchAccountAddAuditauditDetail(data) {
		return request({
			url: '/proxyArtificialPatchAccountAdd/auditRecord',
			method: 'post',
			data
		})
	},
	// 资金审核=>代理人工加额审核记录=>记录列表
	fundsAuthRecordsProxyAddAudit(data) {
		return request({
			url: '/proxyFundsRecords/proxyArtificialAddAuditRecord',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>获取初始账单相关配置
	getInitBillConfig() {
		return request({
			url: '/report/proxyLoanBill/conf',
			method: 'post'
		})
	},
	// 结算账单管理=>账单相关设置
	setBillRelated(data) {
		return request({
			url: '/report/proxyLoanBill/conf/set',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>未出账单列表
	getNoOutBillList(data) {
		return request({
			url: '/report/proxyLoanBill/unPub/listPage',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>未出账单列表=>导出
	getNoOutBillListExport(data) {
		return request({
			url: '/report/proxyLoanBill/unPub/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 结算账单管理=>未出账单列表
	getSettementDetailsMemberRewardList(data) {
		return request({
			url: '/report/memberLiveRewardRecord/list',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>未出账单列表
	getSettementDetailsMemberRewardListExport(data) {
		return request({
			url: '/report/memberLiveRewardRecord/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 结算账单管理=>历史结算账单账单周期时间列表
	getHistorySettlementBillTime(data) {
		return request({
			url: '/report/proxyLoanBill/period/listPage',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>历史结算账单列表
	getHistorySettlementBillList(data) {
		return request({
			url: '/report/proxyLoanBill/hist/listPage',
			method: 'post',
			data
		})
	},
	// 结算账单管理=>历史结算账单列表=>导出
	getHistorySettlementBillListExport(data) {
		return request({
			url: '/report/proxyLoanBill/hist/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录=>代理返点记录=>代理返点详情（团队返点明细）（直属会员贡献明细）
	getTeamRebateDetailApi(data) {
		return request({
			url: '/proxyRebate/getProxyTeamRebate',
			method: 'post',
			data
		})
	},
	// 资金审核记录=>会员账单提现下分审核记录=>审核详情
	memberBillWithdrawalSubAuditDetail(data) {
		return request({
			url: '/member/withdraw/detail',
			method: 'post',
			data
		})
	},
	// 代理树结构
	proxyLevelLink(data) {
		return request({
			url: '/report/proxyTree/levelLink',
			method: 'post',
			data
		})
	},
	// 获取全部会员
	getAllMemberList(data) {
		return request({
			url: '/report/proxyTree/getMemberMainInfo',
			method: 'post',
			data
		})
	},

	// 获取代理下级
	proxyLevelLinkByUserName(data) {
		return request({
			url: '/proxyList/levelLinkByUserName',
			method: 'post',
			data
		})
	},
	// 会员详情-基本信息-概要信息以及个人资料
	getOutlineInfo(params) {
		return request({
			url: '/member/outlineInfo',
			method: 'get',
			params
		})
	},
	// 获取业务类型和账变类型下拉数据通用接口
	getBizAndChangeDic(data) {
		return request({
			url: '/selection/bizAndChangeTypes',
			method: 'post',
			data
		})
	},
	// 会员返水审核列表
	getMemberRebateWaitAuditList(data) {
		return request({
			url: '/memberRebate/waitAudit/page',
			method: 'post',
			data
		})
	},
	// 会员返水审核锁单
	memberRebateLockRecord(data) {
		return request({
			url: '/memberRebate/lockRecord',
			method: 'post',
			data
		})
	},
	// 会员返水审核锁单
	memberRebateAuditRecord(data) {
		return request({
			url: '/memberRebate/auditRecord',
			method: 'post',
			data
		})
	},
	// 会员返水审核详情
	getMemberRebateAuditDetailHeader(data) {
		return request({
			url: '/memberRebate/queryMemberRebateAuditDetailHeader',
			method: 'post',
			data
		})
	},
	// 会员返水审核详情流水
	getMemberRebateAuditDetailPage(data) {
		return request({
			url: '/memberRebate/getMemberRateDetailPage',
			method: 'post',
			data
		})
	},
	// 会员返水个人返水明细
	getMemberRebateTeamRebate(data) {
		return request({
			url: '/memberRebate/getMemberTeamRebate',
			method: 'post',
			data
		})
	},
	// 会员返水个人返水明细: 代理发放+平台发放
	getMemberRebateTeamRebateV2(data) {
		return request({
			url: '/memberRebate/getMemberTeamRebateV2',
			method: 'post',
			data
		})
	},
	// 会员返水审核记录列表
	getMemberRebateSelectAuditRecord(data) {
		return request({
			url: '/memberRebate/selectAuditRecord',
			method: 'post',
			data
		})
	},
	// 未出账单列表-历史结算账单列表->会员账单详情,代理账单详情
	getReportSettlementBillDetail(data) {
		return request({
			url: '/report/proxyLoanBill/unPub/details',
			method: 'post',
			data
		})
	},
	// 代理收益操作记录
	revenueRecordList(data) {
		return request({
			url: '/report/revenue/listPage',
			method: 'post',
			data
		})
	},
	// 会员流水配置分页查询
	getMemberBillConfig(data) {
		return request({
			url: '/paymentChannelConfig/list/member/billConfig',
			method: 'post',
			data
		})
	},
	// 新增会员流水配置
	addMemberBillConfig(data) {
		return request({
			url: '/paymentChannelConfig/add/member/billConfig',
			method: 'post',
			data
		})
	},
	// 更新流水配置
	updateMemberBillConfig(data) {
		return request({
			url: '/paymentChannelConfig/update/member/billConfig',
			method: 'post',
			data
		})
	},
	// 删除会员流水配置
	deleteMemberBillConfig(data) {
		return request({
			url: '/paymentChannelConfig/delete/member/billConfig',
			method: 'post',
			data
		})
	},
	// 流水配置详情
	queryMemberBillConfig(data) {
		return request({
			url: '/paymentChannelConfig/query/member/billConfig',
			method: 'post',
			data
		})
	},
	// 会员提款配置分页查询
	getMemberWithdrawList(data) {
		return request({
			url: '/paymentChannelConfig/list/member/withdraw',
			method: 'post',
			data
		})
	},
	// 新增会员提款配置
	addMemberWithdrawConfig(data) {
		return request({
			url: '/paymentChannelConfig/add/member/withdraw',
			method: 'post',
			data
		})
	},
	// 编辑会员-代理提款配置
	updateMemberWithdrawConfig(data) {
		return request({
			url: '/paymentChannelConfig/update/withdraw',
			method: 'post',
			data
		})
	},
	// 删除会员提款配置
	deleteMemberWithdrawConfig(data) {
		return request({
			url: '/paymentChannelConfig/delete/member/withdraw',
			method: 'post',
			data
		})
	},
	// 查询会员-代理提款配置
	queryMemberWithdrawConfig(data) {
		return request({
			url: '/paymentChannelConfig/query/withdraw',
			method: 'post',
			data
		})
	},
	// 代理团队账单列表
	proxyTeamBillList(data) {
		return request({
			url: '/report/proxyTeamBill/listPage',
			method: 'post',
			data
		})
	},
	// 代理团队账单列表
	proxyTeamBillPeriod(data) {
		return request({
			url: '/report/proxyTeamBill/period/list',
			method: 'post',
			data
		})
	},
	// 代理团队账单总计
	proxyTeamBillTotal(data) {
		return request({
			url: '/report/proxyTeamBill/summary',
			method: 'post',
			data
		})
	},
	// 代理团队账单周期
	proxyTeamBillExport(data) {
		return request({
			url: '/report/proxyTeamBill/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理团队账单详情
	proxyTeamBillDetail(data) {
		return request({
			url: '/report/proxyTeamBill/detail',
			method: 'post',
			data
		})
	},
	// 会员账单列表
	memberTeamBillList(data) {
		return request({
			url: '/report/memberTeamBill/listPage',
			method: 'post',
			data
		})
	},
	// 会员团队账单总计
	memberTeamBillTotal(data) {
		return request({
			url: '/report/memberTeamBill/summary',
			method: 'post',
			data
		})
	},
	// 会员团队账单=>导出
	memberTeamBillExport(data) {
		return request({
			url: '/report/memberTeamBill/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员团队账单=>详情
	memberTeamBillDetail(data) {
		return request({
			url: '/report/memberTeamBill/detail',
			method: 'post',
			data
		})
	},
	// 资金操作记录-查询操作栏目操作页面
	getFundsTypeList() {
		return request({
			url: '/funds/accountOptLog/getAllOptPages',
			method: 'get'
		})
	},
	// 资金操作记录
	getFundsActionLogList(data) {
		return request({
			url: '/funds/accountOptLog/list',
			method: 'post',
			data
		})
	},
	// 会员返水重算调整记录=>列表
	memberRebateRecallList(data) {
		return request({
			url: '/report/memberRebate/recall/list',
			method: 'post',
			data
		})
	},
	// 资金审核-佣金审核-查询列表
	getProxyCommissionAuditList(data) {
		return request({
			url: '/report/proxy/commission/audit/list',
			method: 'post',
			data
		})
	},
	// 会员返水详情合计
	memberRebateRecallSummary(data) {
		return request({
			url: '/report/memberRebate/recall/summary',
			method: 'post',
			data
		})
	},
	// 会员重算返水调整导出
	memberRebateRecallExportExcel(data) {
		return request({
			url: '/report/memberRebate/recall/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金审核-佣金审核-导出
	getProxyCommissionAuditExport(data) {
		return request({
			url: '/report/proxy/commission/audit/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金审核-佣金审核-导出
	getProxyCommissionAuditExport(data) {
		return request({
			url: '/report/proxy/commission/audit/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--会员返水重算调整--查看详情--重算场馆返水信息
	reportRecalMemberVenueRecalDetailList(data) {
		return request({
			url: '/reportRecal/memberVenueRecalDetailList',
			method: 'post',
			data
		})
	},
	// 资金审核-佣金审核记录-查询列表
	getProxyCommissionRecordList(data) {
		return request({
			url: '/report/proxy/commission/audit/record/list',
			method: 'post',
			data
		})
	},
	// 资金--会员返水重算调整--查看详情--会员订单重算列表
	reportRecalMemberOrderRecalList(data) {
		return request({
			url: '/reportRecal/memberOrderRecalList',
			method: 'post',
			data
		})
	},
	// 代理返点重算调整记录=>列表
	proxyRebateRecallSelectReBateRecord(data) {
		return request({
			url: '/proxyRebate/recall/selectReBateRecord',
			method: 'post',
			data
		})
	},
	// 代理返点重算调整汇总
	proxyRebateRecallReBateRecordSummary(data) {
		return request({
			url: '/proxyRebate/recall/reBateRecordSummary',
			method: 'post',
			data
		})
	},
	// 代理返点记录-导出
	proxyRebateRecallProxyReBateRecordDownload(data) {
		return request({
			url: '/proxyRebate/recall/proxyReBateRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金审核-佣金审核记录-导出
	getProxyCommissionRecordExport(data) {
		return request({
			url: '/report/proxy/commission/audit/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金审核-佣金审核记录-导出
	getProxyCommissionRecordExport(data) {
		return request({
			url: '/report/proxy/commission/audit/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--代理返水重算调整--查看详情--重算场馆返点信息
	reportRecalProxyVenueRecalDetailList(data) {
		return request({
			url: '/reportRecal/proxyVenueRecalDetailList',
			method: 'post',
			data
		})
	},
	// 代理资金记录-总代佣金记录-查询列表
	getProxyCommissionProxyRecordList(data) {
		return request({
			url: '/report/proxy/commission/proxy/record/list',
			method: 'post',
			data
		})
	},
	// 资金--代理返水重算调整--查看详情--重算注单会员列表
	reportRecalProxyRecalMemberList(data) {
		return request({
			url: '/reportRecal/proxyRecalMemberList',
			method: 'post',
			data
		})
	},
	// 代理资金记录-总代佣金记录-合计
	getProxyCommissionProxySummary(data) {
		return request({
			url: '/report/proxy/commission/proxy/summary',
			method: 'post',
			data
		})
	},
	// 资金--代理返水重算调整--查看详情--会员订单详情
	reportRecalProxyVenueRecalMemberOrderList(data) {
		return request({
			url: '/reportRecal/proxyVenueRecalMemberOrderList',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队帐变 列表----代理
	reportCreditBillTeamBillChangeQueryProxyBillListPage(data) {
		return request({
			url: '/report/creditBill/teamBillChange/queryProxyBillListPage',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队帐变 列表----会员
	reportCreditBillTeamBillChangeQueryMemberBillListPage(data) {
		return request({
			url: '/report/creditBill/teamBillChange/queryMemberBillListPage',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队帐变 导出----代理
	reportCreditBillTeamBillChangeQueryProxyBillListPageExport(data) {
		return request({
			url:
				'/report/creditBill/teamBillChange/queryProxyBillListPage/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理资金记录-总代佣金记录-导出
	getProxyCommissionProxyRecordExport(data) {
		return request({
			url: '/report/proxy/commission/proxy/record/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 现金占成账单周期下拉框
	proxyTeamRateBillPeriod(data) {
		return request({
			url: '/report/proportion/cycle',
			method: 'get',
			data
		})
	},
	// 现金占成账单列表
	proxyTeamRateBillList(data) {
		return request({
			url: '/report/proportion/list',
			method: 'post',
			data
		})
	},
	// 占成账单统计(总计)
	proxyTeamRateBillTotal(data) {
		return request({
			url: '/report/proportion/summary',
			method: 'post',
			data
		})
	},
	// 导出占成列表
	proxyTeamRateBillExport(data) {
		return request({
			url: '/report/proportion/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队帐变 导出----会员
	reportCreditBillTeamBillChangeQueryMemberBillListPageExport(data) {
		return request({
			url:
				'/report/creditBill/teamBillChange/queryMemberBillListPage/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 查询占成账单详情
	getTeamRateBillDetail(data) {
		return request({
			url: '/report/proportion/detail',
			method: 'get',
			params: data
		})
	},
	// 查询直属代理占成
	getChildTeamRateBillDetail(data) {
		return request({
			url: '/report/proportion/under',
			method: 'get',
			params: data
		})
	},
	// 资金--财务管理-信用账单对账--本期重算注单 列表
	reportCreditBillReConciliationQueryRecalRecordListPage(data) {
		return request({
			url: '/report/creditBill/reconciliation/queryRecalRecordListPage',
			method: 'post',
			data
		})
	},
	// 资金审核-一审/二审/列表数据弹窗
	getProxyCommissionCashDetail(data) {
		return request({
			url: '/report/proxy/commission/cash/detail',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账--本期重算注单 导出
	reportCreditBillReConciliationQueryRecalRecordListPageExport(data) {
		return request({
			url:
				'/report/creditBill/reconciliation/queryRecalRecordListPage/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队收益 列表-- 代理
	reportCreditBillReconciliationReconciliationListPage(data) {
		return request({
			url: '/report/creditBill/reconciliation/reconciliationListPage',
			method: 'post',
			data
		})
	},
	// 佣金审核-勾选订单判断
	getProxyCommissionCashIsFirstRebateAuditOrder(data) {
		return request({
			url: '/report/proxy/commission/cash/isFirstCommissionAuditOrder',
			method: 'post',
			data
		})
	},
	// 资金审核-批量锁定/解锁
	getProxyCommissionCashAuditLock(data) {
		return request({
			url: '/report/proxy/commission/cash/auditLock',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队收益 导出-- 代理
	reportCreditBillReconciliationReconciliationExport(data) {
		return request({
			url: '/report/creditBill/reconciliation/reconciliationExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--财务管理-信用账单对账--本账期应发返点 弹窗
	reportProxyIncomeAuditDetail(data) {
		return request({
			url: '/report/proxy/income/audit/detail',
			method: 'post',
			data
		})
	},
	// 资金审核-批量审批通过/拒绝
	getProxyCommissionCashAuditRecord(data) {
		return request({
			url: '/report/proxy/commission/cash/auditRecord',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本期实发返点 弹窗列表
	reportCreditBillReconciliationRealListPage(data) {
		return request({
			url: '/report/creditBill/reconciliation/realListPage',
			method: 'post',
			data
		})
	},
	// 资金审核-会员返水列表
	getProxyCommissionCashMemberCommissionList(data) {
		return request({
			url: '/report/proxy/commission/cash/memberCommissionList',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本期实发返点 导出
	reportCreditBillReconciliationRealExport(data) {
		return request({
			url: '/report/creditBill/reconciliation/realExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getProxyDividendInfo(data) {
		return request({
			url: '/proxyDividend/proxy/dividendInfo',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本期实发返点 合计
	reportCreditBillReconciliationRealSummary(data) {
		return request({
			url: '/report/creditBill/reconciliation/realSummary',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本账期未审核拒绝返点 弹窗列表
	reportCreditBillReconciliationInvalidListPage(data) {
		return request({
			url: '/report/creditBill/reconciliation/invalidListPage',
			method: 'post',
			data
		})
	},
	getCalculateDividend(data) {
		return request({
			url: '/proxyDividend/proxy/calculateDividend',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本账期未审核拒绝返点 导出
	reportCreditBillReconciliationInvalidExport(data) {
		return request({
			url: '/report/creditBill/reconciliation/invalidExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	batchCalculateDividend(data) {
		return request({
			url: '/proxyDividend/proxy/batchCalculateDividend',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-团队本账期未审核拒绝返点-合计
	reportCreditBillReconciliationInvalidSummary(data) {
		return request({
			url: '/report/creditBill/reconciliation/invalidSummary',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-异常帐变列表分页查询
	venueAbnormalBillListPage(data) {
		return request({
			url: '/venue/abnormal/bill/listPage',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-获取异常注单详情
	venueAbnormalGetGameRecordDetails(data) {
		return request({
			url: '/venue/abnormal/getGameRecordDetails',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-获取异常重算注单详情
	venueAbnormalGetResettlementGameRecordDetails(data) {
		return request({
			url: '/venue/abnormal/getResettlementGameRecordDetails',
			method: 'post',
			data
		})
	},
	// 资金--财务管理-信用账单对账-异常注单列表分页查询
	venueAbnormalListPage(data) {
		return request({
			url: '/venue/abnormal/listPage',
			method: 'post',
			data
		})
	},

	// 资金--财务管理-信用账单对账-异常注单列表分页查询-导出
	venueAbnormaListExport(data) {
		return request({
			url: '/venue/abnormal/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--财务管理-信用账单对账--团队收益 列表-- 代理
	reconciliationAdsProxyReconciliationDetail(data) {
		return request({
			url:
				'/report/creditBill/reconciliation/adsProxyReconciliationDetail',
			method: 'post',
			data
		})
	},
	listProxyGroupByMerchantId(params) {
		return request({
			url: '/proxyGroup/listProxyGroupByMerchantId',
			method: 'get',
			params
		})
	},

	pageProxyByGroupId(data) {
		return request({
			url: '/proxyGroup/pageProxyByGroupId',
			method: 'post',
			data
		})
	},

	// 资金--异常帐变列表-导出excel(请求参数复用列表查询,只是无需传分页参数)
	venueAbnormalBillListExport(data) {
		return request({
			url: '/venue/abnormal/bill/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},

	// 资金--异常注单列表-导出excel(请求参数复用列表查询,只是无需传分页参数)
	venueAbnormalListExport(data) {
		return request({
			url: '/venue/abnormal/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 资金--充提设置--会员提款配置-流水设置
	paymentChannelConfigGetMember(data) {
		return request({
			url: '/paymentChannelConfig/getMember',
			method: 'post',
			data
		})
	},
	// 资金--资金管理--代理净资产查询
	reportNetAssertList(data) {
		return request({
			url: '/report/net-assert/list',
			method: 'post',
			data
		})
	},
	pageDividendRecordPage(data) {
		return request({
			url: '/proxy/dividendRecord/page',
			method: 'post',
			data
		})
	},
	pageAuditedRecord(data) {
		return request({
			url: '/proxy/dividendRecord/pageAuditedRecord',
			method: 'post',
			data
		})
	},
	saveAudit(data) {
		return request({
			url: '/proxy/dividendRecord/audit',
			method: 'post',
			data
		})
	},
	findDetailById(params) {
		return request({
			url: '/proxy/dividendRecord/findDetailById',
			method: 'get',
			params
		})
	},
	batchClock(data) {
		return request({
			url: '/proxy/dividendRecord/batchLock',
			method: 'post',
			data
		})
	},
	batchDividendAudit(data) {
		return request({
			url: '/proxy/dividendRecord/batchAudit',
			method: 'post',
			data
		})
	},
	batchDividendApply(data) {
		return request({
			url: '/proxyDividend/proxy/batchDividendApply',
			method: 'post',
			data
		})
	},
	dividendApply(data) {
		return request({
			url: '/proxyDividend/proxy/dividendApply',
			method: 'post',
			data
		})
	},
	// 资金--资金管理--代理净资产导出
	reportNetAssertExport(data) {
		return request({
			url: '/report/net-assert/export',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},
	// 资金--本期分红
	reportProxyTeamBillRebate(data) {
		return request({
			url: '/report/proxyTeamBill/rebate',
			method: 'post',
			data
		})
	},
	findCalculateResult(data) {
		return request({
			url: '/proxyDividend/proxy/findCalculateResult',
			method: 'post',
			data
		})
	},
	// 资金-资金审核-代理审核再次出款
	proxyAgainPayoutApi(data) {
		return request({
			url: '/proxyWithDrawUser/retry',
			method: 'post',
			data
		})
	},
	// 资金-资金审核-代理审核再次出款
memberAgainPayoutApi(data) {
		return request({
			url: '/memberWithDrawUser/retry',
			method: 'post',
			data
		})
	}
}
