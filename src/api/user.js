import request from '@/utils/request'

export function login(data) {
	return request({
		url: '/login',
		method: 'post',
		data: {
			...data,
			type: 1
		}
	})
}

export function getDics() {
	return request({
		url: '/dict/getAllDictList',
		method: 'post'
	})
}
export function logout() {
	return request({
		url: '/logout',
		method: 'post'
	})
}
export function updateXPSStatus(data) {
	return request({
		url: '/system/user/updateStatus',
		method: 'post',
		data
	})
}

export function googleAuth() {
	return request({
		url: '/system/user/getGoogleAuth',
		method: 'get'
	})
}

export function modifyPassword(data) {
	return request({
		url: '/system/user/updatePassword',
		method: 'post',
		data
	})
}

export function getUsers(data) {
	return request({
		url: '/system/user/query4Page',
		method: 'post',
		data
	})
}

export function unLockUser(data) {
	return request({
		url: '/system/user/unLockUser',
		method: 'post',
		data
	})
}
// 字典列表选择框
export function getSecurityDictList(params) {
	return request({
		url: '/securityDict/list',
		method: 'get',
		params
	})
}
// 设置用户字典
export function setUserSecurityDict(data) {
	return request({
		url: '/securityDict/setUserDicts',
		method: 'post',
		data
	})
}
// 角色列表
export function getRoleList(data) {
	return request({
		url: '/system/role/queryRoleList',
		method: 'post',
		data
	})
}
// 用户明细
export function getUserRoles(data) {
	return request({
		url: '/system/user/getUserDetailInfo',
		method: 'post',
		data
	})
}

// 会员管理==> 会员列表
export function memberListAPI(data) {
	return request({
		url: '/member/queryMemberLs',
		method: 'post',
		data
	})
}

// 会员管理==> 会员列表 ===> 风控层级
export function merchantDictAPI(params) {
	return request({
		url: '/member/merchantDict',
		method: 'get',
		params
	})
}

// 会员管理==> 会员列表 ==> 导出
export function exportExcelAPI(data) {
	return request({
		url: '/member/memberLsExport',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 会员管理==> 新增
export function addMemberAPI(data) {
	return request({
		url: '/member/addPlayer',
		method: 'post',
		data
	})
}
// 会员管理==> 电话
export function systemKvConfigLoadSupportPhoneCode(data) {
	return request({
		url: '/system/kvConfig/loadSupportPhoneCode',
		method: 'post',
		data
	})
}
// 会员管理==> 新增 ===> 获取会员等级
export function getMemberRiskAPI(data) {
	return request({
		url: '/memberVip/rebateRecord/getVipGrade',
		method: 'post',
		data
	})
}

// 会员管理==> 会员注册信息 ==> 列表
export function memberRegisterInfoListAPI(data) {
	return request({
		url: '/member/memberRegisterList',
		method: 'post',
		data
	})
}

// 会员管理==> 会员银行记录 ==> 列表
export function bankRecordListAPI(data) {
	return request({
		// url: '/bankOperateRecord/userBankCode/pageList',
		url: '/management/memberBankCardOperation',
		method: 'post',
		data
	})
}
// 会员管理==> 会员虚拟币 ==> 列表
export function virtualAccountRecordListAPI(data) {
	return request({
		// url: '/bankOperateRecord/virtualAccount/pageList',
		url: '/management/memberVirtualOperation',
		method: 'post',
		data
	})
}

// 编辑用户
export function editUser(data) {
	return request({
		url: '/system/user/updateUserInfo',
		method: 'post',
		data
	})
}
// 新增用户
export function addUser(data) {
	return request({
		url: '/system/user/add',
		method: 'post',
		data
	})
}
// 查询会员银行卡管理
export function getListUserBankListUserBank(data) {
	return request({
		url: '/management/listUserBank',
		method: 'post',
		data
	})
}
// 修改会员银行卡管理 解绑状态
export function setUpdateUserBankBindStatus(data) {
	return request({
		url: '/management/updateUserBankBindStatus',
		method: 'post',
		data
	})
}
// 修改会员银行卡管理  开启/禁用状态
export function setUpdateUserBankStatus(data) {
	return request({
		url: '/management/updateUserBankBlackStatus',
		method: 'post',
		data
	})
}
// 查询会员虚拟币管理
export function getListVirtualAccount(data) {
	return request({
		url: '/management/listVirtualAccount',
		method: 'post',
		data
	})
}
// 修改会员虚拟币管理 解绑状态
export function setUpdateVirtualBindStatus(data) {
	return request({
		url: '/management/updateVirtualBindStatus',
		method: 'post',
		data
	})
}
// 修改会员虚拟币管理  开启/禁用状态
export function setUpdateVirtualBlackStatuss(data) {
	return request({
		url: '/management/updateVirtualBlackStatus',
		method: 'post',
		data
	})
}
export function memberDataInfoChangeRecord(data) {
	return request({
		url: '/memberData/infoChangeRecord',
		method: 'post',
		data
	})
}
// 会员标签分页查询
export function getMemberPageLabel(data) {
	return request({
		url: 'userLabel/pageLabel',
		method: 'post',
		data
	})
}
// 会员标签-新增（编辑）
export function setMemberAddOrEditMemberLabel(data) {
	return request({
		url: '/userLabel/addOrEditMemberLabel',
		method: 'post',
		data
	})
}
// 会员标签-标签人数-会员信息
export function getMemberMemberInfoByLabelId(data) {
	return request({
		url: '/userLabel/memberInfoByLabelId',
		method: 'post',
		data
	})
}
// 会员标签-删除
export function setMemberDeleteLabel(data) {
	return request({
		url: '/userLabel/deleteLabel',
		method: 'post',
		data
	})
}
// 会员标签-变更记录分页查询
export function getMemberLabelChangeRecordPage(data) {
	return request({
		url: '/member/getMemberLabelChangeRecordPage',
		method: 'post',
		data
	})
}
// 审核代办数量
export function getUpcomingSelect(data) {
	return request({
		url: '/upcoming/selectUpcoming',
		method: 'post',
		data
	})
}

export function deleteUser(data) {
	return request({
		url: '/system/user/deleteUser',
		method: 'post',
		data
	})
}

export function updateGoogleAuthCode(data) {
	return request({
		url: '/system/user/updateGoogleAuthCode',
		method: 'post',
		data
	})
}

export function checkGoogleAuthCode(data) {
	return request({
		url: '/system/user/checkGoogleAuthCode',
		method: 'post',
		data
	})
}
export function queryCurrentAccountPeriod(data) {
	return request({
		url: '/report/proxyLoanBill/periodCur',
		method: 'post',
		data
	})
}
// 获取变更记录下拉数据
export function getChangeTypeList() {
	return request({
		url: '/member/changeType',
		method: 'post'
	})
}

// 获取会员信息变更记录
export function getMemberInfoChangeList(data) {
	return request({
		url: '/operatelog/queryMemberChangeList',
		method: 'post',
		data
	})
}

// 获取白马会会员列表
export function getBmhAccountList(data) {
	return request({
		url: '/management/bmh/listBmhAccount',
		method: 'post',
		data
	})
}

// 设置白马会账号黑名单状态
export function setBmhBlackStatus(data) {
	return request({
		url: '/management/bmh/updateBmhStatus',
		method: 'post',
		data
	})
}

// 设置白马会账号解绑状态
export function setUpdateBmhBindStatus(data) {
	return request({
		url: '/management/bmh/updateBmhBindStatus',
		method: 'post',
		data
	})
}

// 获取白马会账号记录列表
export function getBmhAccountRecordList(data) {
	return request({
		url: '/management/bmh/memberBmhOperation',
		method: 'post',
		data
	})
}

// 设置易换账号黑名单状态
export function setYhBlackStatus(data) {
	return request({
		url: '/management/yh/updateYhStatus',
		method: 'post',
		data
	})
}

// 设置易换账号解绑状态
export function setUpdateYhBindStatus(data) {
	return request({
		url: '/management/yh/updateYhBindStatus',
		method: 'post',
		data
	})
}

// 获取易换账号列表
export function getYhAccountList(data) {
	return request({
		url: '/management/yh/listYhAccount',
		method: 'post',
		data
	})
}

// 获取易换账号记录列表
export function getYhAccountRecordList(data) {
	return request({
		url: '/management/yh/memberYhOperation',
		method: 'post',
		data
	})
}

export default {
	login,
	logout,
	getUsers,
	addUser,
	editUser,
	getUserRoles,
	memberDataInfoChangeRecord,
	getRoleList,
	modifyPassword,
	getSecurityDictList,
	setUserSecurityDict,
	updateXPSStatus,
	memberListAPI,
	merchantDictAPI,
	exportExcelAPI,
	addMemberAPI,
	systemKvConfigLoadSupportPhoneCode,
	getMemberRiskAPI,
	memberRegisterInfoListAPI,
	bankRecordListAPI,
	virtualAccountRecordListAPI,
	getListUserBankListUserBank,
	setUpdateUserBankBindStatus,
	setUpdateUserBankStatus,
	getListVirtualAccount,
	setUpdateVirtualBindStatus,
	setUpdateVirtualBlackStatuss,
	getMemberPageLabel,
	setMemberAddOrEditMemberLabel,
	getMemberMemberInfoByLabelId,
	setMemberDeleteLabel,
	getMemberLabelChangeRecordPage,
	googleAuth,
	unLockUser,
	getUpcomingSelect,
	deleteUser,
	updateGoogleAuthCode,
	checkGoogleAuthCode,
	queryCurrentAccountPeriod,
	getChangeTypeList,
	getMemberInfoChangeList,
	getBmhAccountList,
	setBmhBlackStatus,
	setUpdateBmhBindStatus,
	getBmhAccountRecordList,
	getYhAccountList,
	setYhBlackStatus,
	setUpdateYhBindStatus,
	getYhAccountRecordList
}
