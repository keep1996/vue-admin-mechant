import request from '@/utils/request'

// 好友邀请 基础配置 ===> 活动开关
export function getSwitchControlApi(data) {
	return request({
		url: '/operate/activityInviteConfig/switchControl',
		method: 'post',
		data
	})
}
// 好友邀请 基础配置 ===> 查询单条
export function getActivityInviteConfigApi(data) {
	return request({
		url: '/operate/activityInviteConfig/queryOne',
		method: 'post',
		data
	})
}
// 好友邀请 基础配置 保存
export function getActivityInviteConfigSaveConfigApi(data) {
	return request({
		url: '/operate/activityInviteConfig/saveConfig',
		method: 'post',
		data
	})
}
// 好友邀请 基好友邀请活动：app主图、分享图上传接口
export function getActivityInviteUploadImageApi(data) {
	return request({
		url: '/operate/activityInviteConfig/inviteUploadImage',
		method: 'post',
		data
	})
}
// 好友邀请 导入名单模板下载
export function getActivityExcelDownloadApi(data) {
	return request({
		url: '/operate/activityInviteConfig/excelDownload',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 好友邀请 导入名单模板上传验收
export function getActivityExcelCheckApi(data) {
	return request({
		url: '/operate/activityInviteConfig/excelCheck',
		method: 'post',
		data
	})
}
// 好友邀请 友邀请-存款数据导出
export function getInviteTaskDownloadApi(data) {
	return request({
		url: '/operate/activityInviteTask/inviteTask/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 好友邀请 好友邀请-邀请数据分页查询
export function getInviteTaskPageApi(data) {
	return request({
		url: '/operate/activityInviteTask/inviteTaskPage',
		method: 'post',
		data
	})
}
// 好友邀请 友邀请-投注数据导出
export function getActivityInviteBetDownloadApi(data) {
	return request({
		url: '/activityInviteBet/export',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 好友邀请 好友邀请-投注数据-查询
export function getActivityInviteBetPayloadApi(data) {
	return request({
		url: '/activityInviteBet/select',
		method: 'post',
		data
	})
}
// 好友邀请 好友邀请-投注数据-详情
export function getActivityInviteBetDetailsPayloadApi(data) {
	return request({
		url: '/activityInviteBet/details',
		method: 'post',
		data
	})
}
// 好友邀请 好友邀请-存款数据 列表数据
export function geInviteDepositPageApi(data) {
	return request({
		url: '/operate/activityInviteDeposit/inviteDepositPage',
		method: 'post',
		data
	})
}

// 好友邀请 好友邀请-存款数据 列表数据 导出
export function getAccessAmountDaylitexportExcelApi(data) {
	return request({
		url: '/operate/activityInviteDeposit/inviteDepositPage/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 好友邀请 好友邀请-访问数据
export function activityInviteBindingSaveApi(data) {
	return request({
		url: '/operate/activityInviteBinding/save',
		method: 'post',
		data
	})
}

// 会员(代理)活动排除名单: 分页查询排除名单列表
export function queryActivityConfigMemberPage(data) {
	return request({
		url: '/activity/member/queryActivityConfigMemberPage',
		method: 'post',
		data
	})
}

// 会员(代理)活动排除名单: 添加排除名单
export function memberAdd(data) {
	return request({
		url: '/activity/member/add',
		method: 'post',
		data
	})
}

// 会员(代理)活动排除名单: 删除排除名单
export function memberDelete(data) {
	return request({
		url: '/activity/member/del',
		method: 'post',
		data
	})
}

// 会员(代理)活动可参与名单: 添加可参与名单
export function memberAvailableAdd(data) {
	return request({
		url: '/activity/memberAvailable/add',
		method: 'post',
		data
	})
}

// 会员(代理)活动可参与名单: 删除可参与名单
export function memberAvailableDelete(data) {
	return request({
		url: '/activity/memberAvailable/del',
		method: 'post',
		data
	})
}

// 会员(代理)活动可参与名单: 分页查询可参与名单列表
export function queryActivityConfigMemberAvailablePage(data) {
	return request({
		url: '/activity/memberAvailable/queryActivityConfigMemberPage',
		method: 'post',
		data
	})
}
export default {
	getSwitchControlApi,
	getActivityInviteConfigApi,
	getActivityInviteConfigSaveConfigApi,
	getActivityInviteUploadImageApi,
	getActivityExcelDownloadApi,
	getActivityExcelCheckApi,
	getInviteTaskDownloadApi,
	getInviteTaskPageApi,
	getActivityInviteBetDownloadApi,
	getActivityInviteBetPayloadApi,
	getActivityInviteBetDetailsPayloadApi,
	activityInviteBindingSaveApi,
	geInviteDepositPageApi,
	getAccessAmountDaylitexportExcelApi,
	queryActivityConfigMemberPage,
	memberAdd,
	memberDelete,
	memberAvailableAdd,
	memberAvailableDelete,
	queryActivityConfigMemberAvailablePage
}
