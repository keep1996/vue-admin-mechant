import user from './user'
import secret from './secret'
import bankController from './bankController'
import roleController from './roleController'
import riskManage from './riskManage'
import memberDetails from './memberDetails'
import game from './game'
import agent from './agent'
import control from './control'
import vipConfig from './vipConfig'
import memberFunds from './memberFunds'
import funds from './funds'
import operation from './operation'
import agencyfunds from './agencyfunds'
import merchant from './merchant'
import report from './report'
import system from './system'
import bonusBistribution from './bonusBistribution'
import activeList from './activeList'
import proxyFundsRecord from './proxyFundsRecord'
import agentCommissionCheck from './agentCommissionCheck'
import friendInvitation from './friendInvitation'
import gameManager from './gameManager'
import clubManager from './clubManager'
import betSlip from './betSlip'
import dxn from './dxn'
import home from './home'

export default {
	...bankController,
	...user,
	...secret,
	...roleController,
	...riskManage,
	...memberDetails,
	...game,
	...agent,
	...control,
	...vipConfig,
	...memberFunds,
	...funds,
	...operation,
	...agencyfunds,
	...report,
	...merchant,
	...system,
	...bonusBistribution,
	...activeList,
	...proxyFundsRecord,
	...agentCommissionCheck,
	...friendInvitation,
	...gameManager,
	...betSlip,
	...clubManager,
	...dxn,
	...home
}
