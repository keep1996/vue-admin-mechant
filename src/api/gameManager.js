import request from '@/utils/request'

// 德州==> 游戏管理 ===> 游戏配置查询
export function getDxgameConfigDdetail(params) {
	return request({
		url: '/dxgame/config/detail',
		method: 'get',
		params
	})
}

// 德州==> 游戏管理 ===> 游戏配置修改
export function dxgameConfigUpdate(data) {
	return request({
		url: '/dxgame/config/update',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 	牌桌列表
export function getDxtableList(data) {
	return request({
		url: '/dxtable/list',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 	手牌列表
export function getDxhandLlist(data) {
	return request({
		url: '/dxhand/list',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 牌桌详情 ===》 基础信息列表
export function getDxtableDetail(params) {
	return request({
		url: '/dxtable/detail',
		method: 'get',
		params
	})
}

// 德州==> 游戏管理 ===> 牌桌详情 ===》 结算详情列表
export function getGameOrderUserSettlementDetail(data) {
	return request({
		url: '/dxGameRecords/gameOrderUserSettlementDetail',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 牌桌详情 ===》 基础信息 ===》 牌桌收入列表
export function getCardTableBaseGameTableList(data) {
	return request({
		url: '/getCardTableBaseGameTableList',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 牌桌详情 ===》 手牌数据
export function getCardTableHandDataTableList(data) {
	return request({
		url: '/getCardTableHandDataTableList',
		method: 'post',
		data
	})
}

// 会员==> 会员管理 ===> 会员详情 ===》牌桌信息
export function getMemberDetailTableList(data) {
	return request({
		url: '/getMemberDetailTableList',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 手牌详情
export function getDxhandDetail(params) {
	return request({
		url: '/dxhand/detail',
		method: 'get',
		params
	})
}

// 德州===》 游戏管理 ===》 手牌详情 ===》 解散
export function dxtableGameDissolve(params) {
	return request({
		url: '/dxtable/game/dissolve',
		method: 'get',
		params
	})
}

// 德州===》 游戏管理 ===》 牌桌详情 ===》牌桌基础信息
export function getDxtableBaseDetail(params) {
	return request({
		url: '/dxtable/base/detail',
		method: 'get',
		params
	})
}

// 德州===》 游戏管理 ===》游戏列表
export function getDxnGameManagerList(params) {
	return request({
		url: '/game/list',
		method: 'get',
		params
	})
}

// 德州===》 游戏管理 ===》 牌桌管理 ===》 导出
export function getDxTableListExport(data) {
	return request({
		url: '/dxtable/listExport',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 德州===》 游戏管理 ===》 手牌管理 ===》 导出
export function getDxhandListExport(data) {
	return request({
		url: '/dxhand/listExport',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 德州===》 游戏管理 ===》 牌桌详情 ===》 更新机器人数量
export function updateRobotNum(data) {
	return request({
		url: '/dxtable/update/robotNum',
		method: 'post',
		data
	})
}

// 德州==> 游戏管理 ===> 游戏规则管理 ===》修改开关
export function dxgameConfigSwitch(data) {
	return request({
		url: '/dxgame/config/switch',
		method: 'post',
		data
	})
}

// 游戏==> 白名单配置
export function dxgameConfigListWhitelist(data) {
	return request({
		url: '/venue/listWhitelist',
		method: 'post',
		data
	})
}

// 游戏==> 白名单配置 ==> 编辑
export function dxgameConfigListWhitelistUpdate(data) {
	return request({
		url: 'venue/editWhitelist',
		method: 'post',
		data
	})
}

export default {
	dxgameConfigListWhitelistUpdate,
	dxgameConfigListWhitelist,
	getDxgameConfigDdetail,
	dxgameConfigUpdate,
	getDxtableList,
	getDxhandLlist,
	getDxtableDetail,
	getGameOrderUserSettlementDetail,
	getCardTableBaseGameTableList,
	getCardTableHandDataTableList,
	getMemberDetailTableList,
	getDxhandDetail,
	dxtableGameDissolve,
	getDxtableBaseDetail,
	getDxnGameManagerList,
	getDxTableListExport,
	getDxhandListExport,
	updateRobotNum,
	dxgameConfigSwitch
}
