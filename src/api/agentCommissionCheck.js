import request from '@/utils/request'

export default {
	// 非总代佣金管理==代发佣金
	nonGeneralSendCommissionAPI(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralSendCommission',
			method: 'post',
			data
		})
	},

	// 非总代佣金管理== 取消佣金
	// 佣金审核取消佣金共用
	nonGeneralCancelCommissionAPI(data) {
		return request({
			url: '/generalProxyCommission/cancelCommission',
			method: 'post',
			data
		})
	},

	// 非总代佣金管理==分页查询
	getNonGeneralSelectOperateList(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralselectOperate',
			method: 'post',
			data
		})
	},

	// 非总代佣金管理==>代理余额查询
	getProxyAccountBalance(data) {
		return request({
			url: '/proxyCommissionAudit/selectBalance',
			method: 'post',
			data
		})
	},
	// 非总代佣金管理==>总计查询
	getNonGeneralCommissionTotalSummary(data) {
		return request({
			url: '/generalProxyCommission/nonGeneralselectOperateSum',
			method: 'post',
			data
		})
	}
}
