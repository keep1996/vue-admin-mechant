import request from '@/utils/request'

// 商户层级配置列表
export function queryMerchantLevelListApi(data = {}) {
	return request({
		url: '/merchantLevel/list',
		method: 'post',
		data
	})
}

// 商户层级配置-编辑
export function updateMerchantLevelApi(data) {
	return request({
		url: '/merchantLevel/edit',
		method: 'post',
		data
	})
}
