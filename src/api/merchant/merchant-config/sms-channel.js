import request from '@/utils/request'

// 短信渠道-分页查询
export function querySmsChannelPageListApi(data) {
	return request({
		url: '/sms/merchant/channelList',
		method: 'post',
		data
	})
}

// 短信渠道-新增
export function addSmsChannelgApi(data) {
	return request({
		url: '/sms/channel/add',
		method: 'post',
		data
	})
}

// 短信渠道-编辑
export function updateSmsChannelApi(data) {
	return request({
		url: '/sms/channel/update',
		method: 'post',
		data
	})
}

// 短信渠道-停用&开启
export function updateSmsChannelStatusApi(data) {
	return request({
		url: '/sms/channel/enable',
		method: 'post',
		data
	})
}
