import request from '@/utils/request'

// 新增商户下载地址
export function addMerchantGameLinkApi(data) {
	return request({
		url: '/memberGameUrl/add',
		method: 'post',
		data
	})
}

// 查询商户下载地址分页数据
export function queryMerchantGameLinkListApi(data) {
	return request({
		url: '/memberGameUrl/list',
		method: 'post',
		data
	})
}

// 编辑商户下载地址
export function updateMerchantGameLinkApi(data) {
	return request({
		url: '/memberGameUrl/update',
		method: 'post',
		data
	})
}
