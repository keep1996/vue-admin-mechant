import request from '@/utils/request'

// 商户基本配置-列表查询
export function queryMerchantMemberConfigListApi(data) {
	return request({
		url: '/memberConfig/list',
		method: 'post',
		data
	})
}

// 商户基本配置-编辑
export function updateMerchantMemberConfigApi(data) {
	return request({
		url: '/memberConfig/update',
		method: 'post',
		data
	})
}

// 商户基本配置-新增
export function addMerchantMemberConfigApi(data) {
	return request({
		url: '/memberConfig/add',
		method: 'post',
		data
	})
}
