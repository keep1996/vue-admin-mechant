import request from '@/utils/request'

// 新增商户支付渠道
export function addMerchantCustomerApi(data) {
	return request({
		url: '/memberCustomer/add',
		method: 'post',
		data
	})
}

// 查询商户场馆分页数据
export function queryMerchantCustomerListApi(data) {
	return request({
		url: '/memberCustomer/list',
		method: 'post',
		data
	})
}

// 编辑商户支付渠道
export function updateMerchantCustomerApi(data) {
	return request({
		url: '/memberCustomer/update',
		method: 'post',
		data
	})
}
// 查询出入款厂商配置列表
export function getMerchantConfigQuery(data) {
	return request({
		url: '/depositWithdraw/merchantConfig/query',
		method: 'post',
		data
	})
}
// 查询出入款厂商配置-新增
export function getmerchantConfigAdd(data) {
	return request({
		url: '/depositWithdraw/merchantConfig/add',
		method: 'post',
		data
	})
}
// 查询出入款厂商配置-编辑
export function getMerchantConfigUpdate(data) {
	return request({
		url: '/depositWithdraw/merchantConfig/update',
		method: 'post',
		data
	})
}
// 查询支付厂商配置-列表
export function getPaymentMerchantquery(data) {
	return request({
		url: '/paymentMerchant/query',
		method: 'post',
		data
	})
}
// 查询支付厂商配置-新增
export function getpaymentMerchantadd(data) {
	return request({
		url: '/paymentMerchant/add',
		method: 'post',
		data
	})
}
// 查询支付厂商配置-编辑
export function getPaymentMerchantupdate(data) {
	return request({
		url: '/paymentMerchant/update',
		method: 'post',
		data
	})
}
// // 厂商枚举
// export function getPaymentMerchantquery(data) {
//     return request({
//         url: '/paymentMerchant/query',
//         method: 'post',
//         data
//     })
// }
// 厂商枚举
export function getPayMerchantName(data) {
	return request({
		url: '/paymentMerchant/getPayMerchantName',
		method: 'post',
		data
	})
}
