import request from '@/utils/request'

// 新增商户支付渠道
export function addMerchantChannelApi(data) {
	return request({
		url: '/memberChannel/add',
		method: 'post',
		data
	})
}

// 查询商户场馆分页数据
export function queryMerchantChannelListApi(data) {
	return request({
		url: '/memberChannel/list',
		method: 'post',
		data
	})
}

// 编辑商户支付渠道
export function updateMerchantChannelApi(data) {
	return request({
		url: '/memberChannel/update',
		method: 'post',
		data
	})
}

// 获取配置的货币符号
export function queryCurrencySymbolApi() {
	return request({
		url: '/common/getCurrency',
		method: 'get'
	})
}
