import request from '@/utils/request'

// 短信配置-分页查询
export function querySmsPageListApi(data) {
	return request({
		url: '/sms/merchant/channelMerchantList',
		method: 'post',
		data
	})
}

// 短信配置-新增
export function addSmsConfigApi(data) {
	return request({
		url: '/sms/merchant/channel/add',
		method: 'post',
		data
	})
}

// 短信配置-编辑
export function updateSmsConfigApi(data) {
	return request({
		url: '/sms/merchant/channel/update',
		method: 'post',
		data
	})
}

// 短信配置-查询验证码记录
export function querySmsRecordApi(data) {
	return request({
		url: '/sms/smsRecordsList',
		method: 'post',
		data
	})
}

// 短信配置-短信渠道下拉框列表
export function querySmsChannelList(data) {
	return request({
		url: '/sms/channel/ComboBox',
		method: 'post',
		data
	})
}
