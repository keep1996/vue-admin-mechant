import request from '@/utils/request'

// 新增商户场馆
export function addMerchantVenueApi(data) {
	return request({
		url: '/memberVenue/add',
		method: 'post',
		data
	})
}

// 查询商户场馆分页数据
export function queryMerchantVenueListApi(data) {
	return request({
		url: '/memberVenue/list',
		method: 'post',
		data
	})
}

// 编辑商户场馆配置
export function updateMerchantVenueApi(data) {
	return request({
		url: '/memberVenue/update',
		method: 'post',
		data
	})
}
