import request from '@/utils/request'

// 新增商户层级下拉接口（仅用于新增商户）
export function addMerchantOptionsApi(data) {
	return request({
		url: '/merchantLevel/dropDownBox',
		method: 'post',
		data
	})
}

// 查询当前商户下的所有商户列表，包含当前商户
export function queryMerchantListApi(data) {
	return request({
		url: '/merchantManage/list',
		method: 'post',
		data
	})
}

// 商户列表分页查询
export function queryMerchantPageListApi(data) {
	return request({
		url: '/merchantManage/listPage',
		method: 'post',
		data
	})
}

// 商户列表导出
export function updateMerchantInfoApi(data) {
	return request({
		url: '/merchantManage/listExport',
		responseType: 'blob',
		method: 'post',
		data
	})
}
