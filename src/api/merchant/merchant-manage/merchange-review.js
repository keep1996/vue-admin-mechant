import request from '@/utils/request'

// 新增商户
export function addMerchantApi(data) {
	return request({
		url: '/newMerchant/add',
		method: 'post',
		data
	})
}

// 新增商户审核-分页列表
export function merchantAuditListApi(data) {
	return request({
		url: '/newMerchant/auditPageList',
		method: 'post',
		data
	})
}

// 新增商户审核锁定&解锁接口
export function merchantAuditLockApi(data) {
	return request({
		url: '/newMerchant/lock',
		method: 'post',
		data
	})
}

// 新增商户审核通过&拒绝
export function merchantAuditApi(data) {
	return request({
		url: '/newMerchant/audit',
		method: 'post',
		data
	})
}
