import request from '@/utils/request'

// 查询商户详情
export function queryMerchantDetailApi(data) {
	return request({
		url: '/merchantDetail/infos',
		method: 'post',
		data
	})
}

// 商户信息编辑
export function updateMerchantInfoApi(data) {
	return request({
		url: '/merchantDetail/infosEdit',
		method: 'post',
		data
	})
}
