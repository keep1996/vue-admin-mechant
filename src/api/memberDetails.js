import request from '@/utils/request'
// 会员详情-基本信息-概要信息以及个人资料

export function checkInformation(data) {
	return request({
		url: '/SecurityChecks/checkInformation',
		method: 'post',
		data
	})
}

export function getOutlineInfo(params) {
	return request({
		url: '/member/outlineInfo',
		method: 'get',
		params
	})
}
// 风控层级
export function getMerchantDict(params) {
	return request({
		url: '/member/merchantDict',
		method: 'get',
		params
	})
}
// 会员标签
export function getMemberLabelDict(data) {
	return request({
		url: '/userLabel/labelDict',
		method: 'post',
		data
	})
}

// 会员详情-风控信息-风控备注
export function getRiskControlNote(data) {
	return request({
		url: '/risk-control-note/get-page-by-user-id',
		method: 'post',
		data
	})
}

// 会员详情-风控信息-显示所有信息
export function getRiskControlFullDetailInfo(data) {
	return request({
		url: '/member/detail/fullDetailInfo',
		method: 'post',
		data
	})
}

// 会员详情-基本信息-活动流水限制
export function getActivityTaskLimitList(data) {
	return request({
		url: '/member/activityTaskLimitList',
		method: 'post',
		data
	})
}
// 会员详情-基本信息-信息编辑
export function setMemberInfoEdit(data) {
	return request({
		url: '/member/infoEdit',
		method: 'post',
		data
	})
}
// vip信息
export function getVipInfo(data) {
	return request({
		url: '/vipInfo/info',
		method: 'post',
		data
	})
}
// 会员详情==>会员充值限制
export function getThirdPayWhiteDepositLimit(data) {
	return request({
		url: '/thirdPayWhite/depositLimit',
		method: 'post',
		data
	})
}
// 查询备注信息
export function getMemberRemarkList(params) {
	return request({
		url: '/memberRemark/list',
		method: 'get',
		params
	})
}
// 查询中心钱包余额
export function getAccountCashAccount(data) {
	return request({
		url: '/account/cashAccount',
		method: 'post',
		data
	})
}
// 查询提现冻结余额
export function getWithdrawalFreeze(data) {
	return request({
		url: '/account/cashAccount',
		method: 'post',
		data
	})
}
// 一键查询所有场馆余额
export function getOneKeyBalance(data) {
	return request({
		url: '/member/oneKeyBalance',
		method: 'post',
		data
	})
}
// 查询中心钱包和信用钱包余额
export function getMemberDetailBalance(data) {
	return request({
		url: '/member/detail/balance',
		method: 'post',
		data
	})
}
// 提现信息人工调整
export function setMemberFlowAdjust(data) {
	return request({
		url: '/member/detail/flow/adjust',
		method: 'post',
		data
	})
}
// 提现信息人工调整列表
export function getMemberFlowAdjustList(data) {
	return request({
		url: '/member/detail/flow/adjust/list',
		method: 'post',
		data
	})
}
// 一键回收至中心钱包
export function getOneKeyWithdraw(data) {
	return request({
		url: '/member/oneKeyWithdraw',
		method: 'post',
		data
	})
}
// 提现流水查询
export function getWithdrawWater(data) {
	return request({
		url: '/account/withdrawWater',
		method: 'post',
		data
	})
}
// 添加会员备注
export function getMemberRemarkAdd(data) {
	return request({
		url: '/memberRemark/add',
		method: 'post',
		data
	})
}
// 会员充提信息查询
export function getPlayerOrderSumInfo(data) {
	return request({
		url: '/player/playerOrderSumInfo',
		method: 'post',
		data
	})
}
// 会员投注信息查询
export function getPlayerBetHistorySum(data) {
	return request({
		url: '/member/playerBetHistorySum',
		method: 'post',
		data
	})
}
// top3平台统计
export function getPlayerTop3(params) {
	return request({
		url: '/player/top3',
		method: 'get',
		params
	})
}
// 会员登录日志查询
export function getLogMemberLoginLog(data) {
	return request({
		url: '/log/memberLoginLog',
		method: 'post',
		data
	})
}

// 会员详情获取场馆限红模板列表
export function getMemberVenueLimitRed(data) {
	return request({
		url: '/venue/redLimit/getMemberVenueLimitRed',
		method: 'post',
		data
	})
}

// 批量设置会员体育限红模板
export function batchTyUpdateLimitRed(data) {
	return request({
		url: '/venue/redLimit/batchTyUpdateLimitRed',
		method: 'post',
		data
	})
}

// 根据会员id批量获取体育限红模板列表
export function batchQueryTyMemberLimitRed(data) {
	return request({
		url: '/venue/redLimit/batchQueryTyMemberLimitRed',
		method: 'post',
		data
	})
}

// 查询体育限红模板详情
export function getTyLimitRedDetails(data) {
	return request({
		url: '/venue/redLimit/getTyLimitRedDetails',
		method: 'post',
		data
	})
}

// 银行卡/虚拟币账号信息
export function getBankCardBank(data) {
	return request({
		// url: '/member/bank',
		url: '/member/detail/getUserCardInfo',
		method: 'post',
		data
	})
}
// 会员详情的解除充值限制
export function getThirdPayWhiteCheckDepositLimit(data) {
	return request({
		url: '/thirdPayWhite/checkDepositLimit',
		method: 'post',
		data
	})
}
// 俱乐部管理-俱乐部查询
export function getClubQuery(data) {
	return request({
		url: '/club/query',
		method: 'post',
		data
	})
}

// 代理-批量设置信用等级和账号状态
export function setProxyBatchInfoEdit(data) {
	return request({
		url: '/proxyData/batchInfoEdit',
		method: 'post',
		data
	})
}
// 会员-批量设置信用等级和账号状态
export function setMemberBatchInfoEdit(data) {
	return request({
		url: '/member/batchInfoEdit',
		method: 'post',
		data
	})
}

// 获取体育下拉设置限红模板列表和赔率下拉框
export function proxyTyLimitRedList(data) {
	return request({
		url: '/venue/redLimit/proxyTyLimitRedList',
		method: 'post',
		data
	})
}

// 真人会员限红配置修改-批量或所有-或单个
export function templateUpdateBatch(data) {
	return request({
		url: '/venue/redLimit/templateUpdateBatch',
		method: 'post',
		data
	})
}

// 获取会员体育模板修改记录
export function templateList(data) {
	return request({
		url: '/venue/redLimit/templateList',
		method: 'post',
		data
	})
}

// 获取真人详情
export function batchQueryMemberLimitRed(data) {
	return request({
		url: '/venue/redLimit/batchQueryMemberLimitRed',
		method: 'post',
		data
	})
}

// 会员-操作记录
export function getMemberActionLogList(data) {
	return request({
		url: '/operatelog/queryMemberOperateList',
		method: 'post',
		data
	})
}

// 会员详情-基本信息-白马会账号解绑
export function setBmhMemberInfoEdit(data) {
	return request({
		url: '/member/unbindBmh',
		method: 'post',
		data
	})
}

// 会员详情-基本信息-易换账号解绑
export function setYhMemberInfoEdit(data) {
	return request({
		url: '/member/unbindYihuan',
		method: 'post',
		data
	})
}

export async function getCryptoWalletAddressPage(data) {
	return request({
		url: '/member/getCryptoWalletAddressPage',
		method: 'post',
		data
	})
}
export async function getLatestCryptoWalletAddress(data) {
	return request({
		url: '/member/getLatestCryptoWalletAddress',
		method: 'post',
		data
	})
}

export default {
	checkInformation,
	templateList,
	batchQueryMemberLimitRed,
	getOutlineInfo,
	batchTyUpdateLimitRed,
	getMerchantDict,
	getMemberLabelDict,
	setMemberInfoEdit,
	getActivityTaskLimitList,
	batchQueryTyMemberLimitRed,
	getVipInfo,
	getMemberRemarkList,
	getAccountCashAccount,
	templateUpdateBatch,
	getWithdrawalFreeze,
	getOneKeyBalance,
	getMemberDetailBalance,
	setMemberFlowAdjust,
	getMemberFlowAdjustList,
	getMemberVenueLimitRed,
	getOneKeyWithdraw,
	getWithdrawWater,
	getMemberRemarkAdd,
	proxyTyLimitRedList,
	getPlayerOrderSumInfo,
	getTyLimitRedDetails,
	getPlayerBetHistorySum,
	getPlayerTop3,
	getLogMemberLoginLog,
	getBankCardBank,
	getThirdPayWhiteDepositLimit,
	getThirdPayWhiteCheckDepositLimit,
	getClubQuery,
	setProxyBatchInfoEdit,
	setMemberBatchInfoEdit,
	getMemberActionLogList,
	setBmhMemberInfoEdit,
	getRiskControlNote,
	setYhMemberInfoEdit,
	getRiskControlFullDetailInfo,
	getCryptoWalletAddressPage,
	getLatestCryptoWalletAddress
}
