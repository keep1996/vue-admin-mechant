// 德州/注单管理
import request from '@/utils/request'

// 列表
export const betSlipQueryList = (data) => request({
	url: 'dxGameRecords/listPage',
	method: 'post',
	data
})

// 导出
export const betSlipListExport = (data) => request({
	url: 'dxGameRecords/listExport',
	method: 'post',
	data,
	responseType: 'blob'
})

export default {
	betSlipQueryList,
	betSlipListExport
}
