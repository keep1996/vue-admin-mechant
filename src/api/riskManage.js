import request from '@/utils/request'

// 风控==> 终端登录限制管理列表
export function ipBlackList(params) {
	return request({
		url: '/ipBlack/list',
		method: 'get',
		params
	})
}

// 风控==> 查询单个黑名单IP
export function ipBlackSingle(params) {
	return request({
		url: '/ipBlack/list',
		method: 'post',
		params
	})
}

// 风控==> 添加黑名单IP
export function ipBlackAdd(data) {
	return request({
		url: '/ipBlack/add',
		method: 'post',
		data
	})
}

// 风控==> 编辑黑名单IP
export function ipBlackEdit(data) {
	return request({
		url: '/ipBlack/edit',
		method: 'post',
		data
	})
}

// 风控==> 删除黑名单IP
export function ipBlackDelete(id) {
	return request({
		url: `/ipBlack/del/${id}`,
		method: 'post'
	})
}
// 风控==> 注册IP黑名单==> 分页
export function getWindControlBlacklistquery(data) {
	return request({
		url: `/windControlBlacklist/query`,
		method: 'post',
		data
	})
}
// 风控==> 注册IP黑名单==》新增
export function getWindControlBlacklistadd(data) {
	return request({
		url: `/windControlBlacklist/add`,
		method: 'post',
		data
	})
}
// 风控==> 注册IP黑名单==》删除
export function getWindControlBlacklistdel(data) {
	return request({
		url: `/windControlBlacklist/del`,
		method: 'post',
		data
	})
}
// 风控==> 注册IP黑名单==》编辑
export function getWindControlBlacklistupdate(data) {
	return request({
		url: `/windControlBlacklist/update`,
		method: 'post',
		data
	})
}
// 风控==> 登录IP白名单==》查询
export function getSelectLoginIpWhitelistPage(data) {
	return request({
		url: '/windControl/selectLoginIpWhitelistPage',
		method: 'post',
		data
	})
}
// 风控==> 登录IP白名单==》开关状态
export function getLoginIpWhitelistConfig(data) {
	return request({
		url: '/windControl/getLoginIpWhitelistConfig',
		method: 'post',
		data
	})
}
// 风控==> 登录IP白名单==》新增
export function setSaveIpWhitelist(data) {
	return request({
		url: '/windControl/saveIpWhitelist',
		method: 'post',
		data
	})
}
// 风控==> 登录IP白名单==》删除
export function setDelIpWhitelist(data) {
	return request({
		url: '/windControl/delIpWhitelist',
		method: 'post',
		data
	})
}
// 风控==> 会员登录白名单==》列表
export function userWhitelistPage(data) {
	return request({
		url: '/userWhitelist/page',
		method: 'post',
		data
	})
}
// 风控==> 会员登录白名单==》新增
export function userWhitelistSave(data) {
	return request({
		url: '/userWhitelist/save',
		method: 'post',
		data
	})
}
// 风控==> 会员登录白名单==》编辑
export function userWhitelistUpdate(data) {
	return request({
		url: '/userWhitelist/update',
		method: 'post',
		data
	})
}
// 风控==> 会员登录白名单==》删除
export function userWhitelistDelete(data) {
	return request({
		url: '/userWhitelist/delete',
		method: 'post',
		data
	})
}

// 风控==> 风控对象管理==> 获取风控层级描述
export function getWindControlDescrption(data) {
	return request({
		url: '/userWhitelist/delete',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-列表
export function windControllerCreditProxyList(data) {
	return request({
		url: '/windControllerCreditProxy/List',
		method: 'post',
		data
	})
}

// 获取绑定德信账号历史记录
export function getBindDxAccountRecord(data) {
	return request({
		url: '/userWhitelist/delete',
		method: 'post',
		data
	})
}

// 风控==> 会员返水状态管里==》会员返水状态管理
export function rebateStatusFreezeRecordSaveOrUpdate(data) {
	return request({
		url: '/rebateStatusFreezeRecord/saveOrUpdate',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-编辑
export function windControllerCreditProxyEdit(data) {
	return request({
		url: '/windControllerCreditProxy/edit',
		method: 'post',
		data
	})
}
export function riskControlNoteSave(data) {
	return request({
		url: '/risk-control-note/save',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-修改记录
export function windControllerCreditProxyEditList(data) {
	return request({
		url: '/windControllerCreditProxy/editList',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-风控记录
export function windControllerCreditProxyRecordList(data) {
	return request({
		url: '/windControllerCreditProxy/recordList',
		method: 'post',
		data
	})
}

// 会员返水冻结记录->返水状态管理分页查询
export function rebateStatusFreezeRecordPage(data) {
	return request({
		url: '/rebateStatusFreezeRecord/page',
		method: 'post',
		data
	})
}

// 会员冻结返水记录
export function memberFrozenRebateRecord(data) {
	return request({
		url: '/report/memberRebateStatus/list',
		method: 'post',
		data
	})
}

// 会员冻结返水记录详情
export function getMemberFrozenRebateReportDetailList(data) {
	return request({
		url: '/report/memberRebateStatus/list-by-user',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-风控记录-导出
export function windControllerCreditProxyExport(data) {
	return request({
		url: '/windControllerCreditProxy/export',
		method: 'post',
		data,
		responseType: 'blob'
	})
}

// 风控==> 风控管理==> 信用总代风控设置-解除锁定
export function windControllerCreditProxyUnlock(data) {
	return request({
		url: '/windControllerCreditProxy/unlock',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 白名单管理-净资产锁定白名单-新增白名单
export function windControllerLevelAddNetAssetLockingList(data) {
	return request({
		url: '/windControllerLevel/addNetAssetLockingList',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 白名单管理-净资产锁定白名单列表
export function windControllerLevelQueryAssetLockingList(data) {
	return request({
		url: '/windControllerLevel/queryAssetLockingList',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 风控-白名单管理-删除白名单
export function windControllerLevelDeleteLockingList(data) {
	return request({
		url: '/windControllerLevel/deleteLockingList',
		method: 'post',
		data
	})
}
// 会员返水冻结记录->返水状态 Excel
export function rebateStatusFreezeRecordExport(data) {
	return request({
		url: '/rebateStatusFreezeRecord/export',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 风控管理 ==> 会员冻结返水记录 ==> 导出
export function getMemberRebateRecordReportExport(data) {
	return request({
		url: '/report/memberRebateStatus/exportExcel',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-手动锁定
export function windControllerCreditProxyHandLock(data) {
	return request({
		url: '/windControllerCreditProxy/handLock',
		method: 'post',
		data
	})
}

// 风控==> 风控管理==> 信用总代风控设置-手动解锁
export function windControllerCreditProxyHandUnLock(data) {
	return request({
		url: '/windControllerCreditProxy/handUnLock',
		method: 'post',
		data
	})
}

export default {
	windControllerCreditProxyHandLock,
	windControllerCreditProxyHandUnLock,
	windControllerCreditProxyList,
	windControllerCreditProxyEdit,
	windControllerCreditProxyEditList,
	windControllerCreditProxyRecordList,
	windControllerCreditProxyExport,
	windControllerCreditProxyUnlock,
	windControllerLevelAddNetAssetLockingList,
	windControllerLevelQueryAssetLockingList,
	windControllerLevelDeleteLockingList,
	ipBlackList,
	ipBlackSingle,
	ipBlackAdd,
	ipBlackEdit,
	ipBlackDelete,
	getWindControlBlacklistquery,
	getWindControlBlacklistadd,
	getWindControlBlacklistdel,
	getWindControlBlacklistupdate,
	getSelectLoginIpWhitelistPage,
	getLoginIpWhitelistConfig,
	setSaveIpWhitelist,
	setDelIpWhitelist,
	userWhitelistPage,
	userWhitelistSave,
	userWhitelistUpdate,
	userWhitelistDelete,
	rebateStatusFreezeRecordSaveOrUpdate,
	getWindControlDescrption,
	getBindDxAccountRecord,
	rebateStatusFreezeRecordPage,
	rebateStatusFreezeRecordExport,
	riskControlNoteSave,
	memberFrozenRebateRecord,
	getMemberFrozenRebateReportDetailList,
	getMemberRebateRecordReportExport
}
