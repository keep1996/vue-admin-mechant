import request from '@/utils/request'

// 运营 ===> 公共资源配置 ==> 提交
export function clientCommonUpdateAPI(data) {
	return request({
		url: '/operate/publicResources/update',
		method: 'post',
		data
	})
}

// 运营 ===> 公共资源配置 ==> 分页查询
export function clientCommonQueryByType(data) {
	return request({
		url: '/operate/publicResources/domainList',
		method: 'post',
		data
	})
}

// 运营 ===> 公共资源配置 ==> 资源配置列表
export function clientCommonTypeList() {
	return request({
		url: '/operate/publicResources/queryPublicResources',
		method: 'get'
	})
}

// 运营 ===> 公共资源配置 ==> 新增/编辑公共资源配置
export function editClientCommonList(data) {
	return request({
		url: '/operate/publicResources/update',
		method: 'post',
		data
	})
}

// 运营 ===> 公共资源配置 ==> 删除公共资源
export function deleteClientCommonList(data) {
	return request({
		url: '/operate/publicResources/delPublicResources',
		method: 'post',
		data
	})
}

// 运营 ===> 公共资源配置 ==> 启用、禁用公共资源
export function useClientCommonList(data) {
	return request({
		url: '/operate/publicResources/use',
		method: 'post',
		data
	})
}

// 运营 ===> 公共资源配置 ==> 切换查询详情信息
export function clientCommonQueryPublicResourcesAPI(params) {
	return request({
		url: '/operate/publicResources/queryPublicResources',
		method: 'get',
		params
	})
}

// 运营 ===> 启动页配置 ==> 列表
export function clientStartListAPI(data) {
	return request({
		url: '/operate/startPage/queryList',
		method: 'post',
		data
	})
}

// 运营 ===> 启动页配置 ==> 新增
export function clientStartAddAPI(data) {
	return request({
		url: '/operate/startPage/add',
		method: 'post',
		data
	})
}

// 运营 ===> 启动页配置 ==> 修改
export function clientStartUpdateAPI(data) {
	return request({
		url: '/operate/startPage/update',
		method: 'post',
		data
	})
}

// 运营 ===> 启动页配置 ==> 删除
export function clientStartDeleteAPI(data) {
	return request({
		url: '/operate/startPage/delete',
		method: 'post',
		data
	})
}

// 运营 ===> 启动页配置 ==> 启用禁用
export function clientStartUseAPI(data) {
	return request({
		url: '/operate/startPage/use',
		method: 'post',
		data
	})
}

// 运营 ===> 启动页配置 ==> 文件上传
export function clientStartUploadAPI(data, cb) {
	return request({
		url: '/uploadFile/image',
		method: 'post',
		data,
		cb
	})
}

// 运营 ===> 场馆费率 ==> 配置查询
export function platformSelectAPI(data) {
	return request({
		url: '/operate/obMerchantGame/select',
		method: 'post',
		data
	})
}

// 运营 ===> 场馆费率 ==> 更新配置
export function platformUpdateAPI(data) {
	return request({
		url: '/operate/obMerchantGame/update',
		method: 'post',
		data
	})
}

// 运营 ===> 公告配置 ==> 操作记录枚举
export function activityInfoLogAPI(params) {
	return request({
		url: '/operate/obConfigAnnouncementRecord/queryEnums',
		method: 'get',
		params
	})
}

// 运营 ===> 公告配置 ==> 操作记录列表
export function activityInfoLogListAPI(data) {
	return request({
		url: '/operate/obConfigAnnouncementRecord/select',
		method: 'post',
		data
	})
}

// 运营 ===> 活动类型 ==> 详情
export function activityTypeDetailAPI(params) {
	return request({
		url: '/operate/configActivityType/queryDetail',
		method: 'get',
		params
	})
}

// 运营 ===> 活动类型 ==> 弹框列表
export function activityQueryTypeListAPI(params) {
	return request({
		url: '/operate/configActivityType/queryTypeList',
		method: 'get',
		params
	})
}

// 运营 ===> 活动类型 ==> 排序
export function activitySortAPI(data) {
	return request({
		url: '/operate/configActivityType/sort',
		method: 'post',
		data
	})
}

// 运营 ===> 活动类型 ==> 更新
export function activityUpdateAPI(data) {
	return request({
		url: '/operate/configActivityType/update',
		method: 'post',
		data
	})
}

export function configDiscountTagQueryList(data) {
	return request({
		url: '/operate/configDiscountTag/queryList',
		method: 'post',
		data
	})
}

export function configDiscountTagUse(data) {
	return request({
		url: '/operate/configDiscountTag/use',
		method: 'post',
		data
	})
}

export function configDiscountTagDelete(data) {
	return request({
		url: '/operate/configDiscountTag/delete',
		method: 'post',
		data
	})
}

export function configDiscountTagAdd(data) {
	return request({
		url: '/operate/configDiscountTag/add',
		method: 'post',
		data
	})
}

export function configDiscountTagEdit(data) {
	return request({
		url: '/operate/configDiscountTag/update',
		method: 'post',
		data
	})
}

// 运营==>公告配置==>查询所有公告配置
export function getOperateConfigAnnouncementSelectAll(data) {
	return request({
		url: '/operate/obConfigAnnouncement/selectAll',
		method: 'post',
		data
	})
}

// 运营==>公告配置==>查询单个公告配置
export function getOperateConfigAnnouncementSelect(data) {
	return request({
		url: '/operate/obConfigAnnouncement/select',
		method: 'post',
		data
	})
}

// 运营==>公告配置==>保存公告配置
export function getOperateConfigAnnouncementSave(data) {
	return request({
		url: '/operate/obConfigAnnouncement/add',
		method: 'post',
		data
	})
}
// 运营==>公告配置==>保存公告配置
export function getOperateConfigAnnouncementUpdate(data) {
	return request({
		url: '/operate/obConfigAnnouncement/updateById',
		method: 'post',
		data
	})
}

// 运营==>公告配置==>删除公告配置
export function getOperateConfigAnnouncementDelete(data) {
	return request({
		url: '/operate/obConfigAnnouncement/delete',
		method: 'post',
		data
	})
}

// 运营==>公告配置==>修改公告状态
export function getOperateConfigAnnouncementStatus(data) {
	return request({
		url: '/operate/obConfigAnnouncement/status',
		method: 'post',
		data
	})
}

// 运营==>活动，通知消息配置==>查询全部活动，通知配置
export function getOperateConfigNoticeSelectAll(data) {
	return request({
		url: '/operate/obConfigNotice/selectAll',
		method: 'post',
		data
	})
}

// 运营==>活动，通知消息配置==>查询单个活动，通知配置
export function getOperateConfigNoticeSelectDetail(data) {
	return request({
		url: '/operate/obConfigNoticeDetail/selectDetail',
		method: 'post',
		data
	})
}

// 运营==>活动，通知消息配置==>保存，修改活动，通知配置
export function getOperateConfigNoticeSave(data) {
	return request({
		url: '/operate/bwConfigNotice/add',
		method: 'post',
		data
	})
}

// 运营==>活动，通知消息配置==>活动，通知  撤回
export function getOperateConfigNoticeRetract(data) {
	return request({
		url: '/operate/bwConfigNotice/retract',
		method: 'post',
		data
	})
}

export function configDiscountTagQueryNames(params) {
	return request({
		url: '/operate/configDiscountTag/queryNames',
		method: 'get',
		params
	})
}

// 活动类型名称配置操作记录
export function queryActivityTypeList(data) {
	return request({
		url: '/operate/ActivityConfigRecord/queryActivityTypeList',
		method: 'post',
		data
	})
}

// 优惠类型页签操作记录
export function queryDiscountTagList(data) {
	return request({
		url: '/operate/ActivityConfigRecord/queryDiscountTagList',
		method: 'post',
		data
	})
}

export function queryDiscountActivityList(data) {
	return request({
		url: '/operate/ActivityConfigRecord/queryDiscountActivityList',
		method: 'post',
		data
	})
}

export function queryVipActivityList(data) {
	return request({
		url: '/operate/ActivityConfigRecord/queryVipActivityList',
		method: 'post',
		data
	})
}

// 运营 ===>场馆配置管理 ==>操作记录
export function getOperateObMerchantGameRecordSelect(data) {
	return request({
		url: '/operate/obMerchantGameRecord/select',
		method: 'post',
		data
	})
}

// 运营 ===>客户端配置管理 ==>查询banner列表
export function getQperateConfigBannerQueryBannerList(data) {
	return request({
		url: '/operate/configBanner/queryBannerList',
		method: 'post',
		data
	})
}

export function getOperateConfigBannerUse(data) {
	return request({
		url: '/operate/configBanner/use',
		method: 'post',
		data
	})
}

export function getOperateConfigBannerDelete(data) {
	return request({
		url: '/operate/configBanner/delete',
		method: 'post',
		data
	})
}

// 修改banner
export function getPperateConfigBannerUpdate(data) {
	return request({
		url: '/operate/configBanner/update',
		method: 'post',
		data
	})
}

// 新增banner
export function getOperateConfigBannerAdd(data) {
	return request({
		url: '/operate/configBanner/add',
		method: 'post',
		data
	})
}

// 查询banner区域
export function operateConfigBannerQueryBannerAreaAPI(params) {
	return request({
		url: '/operate/configBanner/queryBannerArea',
		method: 'get',
		params
	})
}

// 按banner区域排序
export function setoperateConfigBannerSort(data) {
	return request({
		url: '/operate/configBanner/sort',
		method: 'post',
		data
	})
}

// 客户端配置管理操作记录=》查询
export function operateConfigClientRecordQueryEnumsAPI(params) {
	return request({
		url: '/operate/ConfigClientRecord/queryEnums',
		method: 'get',
		params
	})
}

// 客户端配置管理操作记录==>查询操作记录列表
export function getOperateConfigClientRecordQueryRecordList(data) {
	return request({
		url: '/operate/ConfigClientRecord/queryRecordList',
		method: 'post',
		data
	})
}

// 活动消息配置
export function getOperateObConfigNoticeSelectAll(data) {
	return request({
		url: '/operate/bwConfigNotice/list',
		method: 'post',
		data
	})
}

// 系统维护配置查询
export function getsysMaintenanceList(data) {
	return request({
		url: '/sysMaintenance/list',
		method: 'post',
		data
	})
}
// 系统维护配置新增
export function getsysMaintenanceAdd(data) {
	return request({
		url: '/sysMaintenance/add',
		method: 'post',
		data
	})
}
// 系统维护配置编辑
export function getsysMaintenanceUpdate(data) {
	return request({
		url: '/sysMaintenance/updateInfo',
		method: 'post',
		data
	})
}
// 系统维护配置状态
export function getsysMaintenanceUpdateStatus(data) {
	return request({
		url: '/sysMaintenance/updateStatus',
		method: 'post',
		data
	})
}
// 系统维护配置删除
export function getsysMaintenanceDelete(data) {
	return request({
		url: '/sysMaintenance/delete',
		method: 'post',
		data
	})
}
// 活动消息、通知配置查询明细
export function getBwConfigNoticeDetails(data) {
	return request({
		url: '/operate/bwConfigNotice/getDetails',
		method: 'post',
		data
	})
}

// 系统消息配置
export function getOperateSysMsgList(data) {
	return request({
		url: '/sysMsg/list',
		method: 'post',
		data
	})
}

// 编辑消息配置
export function setOperateSysMsgList(data) {
	return request({
		url: '/sysMsg/update',
		method: 'post',
		data
	})
}

// 编辑系统消息状态
export function setStatusOperateSysMsg(data) {
	return request({
		url: '/sysMsg/updateStatus',
		method: 'post',
		data
	})
}

// 会员意见反馈
export function getFeedBackPageFeedBack(data) {
	return request({
		url: '/feedBack/pageFeedBack',
		method: 'post',
		data
	})
}

// 会员意见反馈===>反馈类型
export function OperateObConfigAnnouncementRecordQueryFeedBackEnums(params) {
	return request({
		url: '/operate/obConfigAnnouncementRecord/queryFeedBackEnums',
		method: 'get',
		params
	})
}

// vip活动配置/赞助活动配置 ==> 查询分页列表
export function getOperateActivityVipQueryList(data) {
	return request({
		url: '/operate/activityVip/queryList',
		method: 'post',
		data
	})
}

// vip活动配置/赞助活动配置 ==> 获取已创建的活动
export function getOperateActivityVipQueryActivityNameList(params) {
	return request({
		url: '/operate/activityVip/queryActivityNameList',
		method: 'get',
		params
	})
}

// vip活动配置/赞助活动配置 ==> 新增
export function setOperateActivityVipAdd(data) {
	return request({
		url: '/operate/activityVip/add',
		method: 'post',
		data
	})
}

// vip活动配置/赞助活动配置 ==> 删除
export function setOperateActivityVipDelete(data) {
	return request({
		url: '/operate/activityVip/delete',
		method: 'post',
		data
	})
}

// vip活动配置/赞助活动配置 ==> 修改
export function setOperateActivityVipUpdate(data) {
	return request({
		url: '/operate/activityVip/update',
		method: 'post',
		data
	})
}

// vip活动配置/赞助活动配置 ==> 启用,禁用
export function setOperateActivityVipUse(data) {
	return request({
		url: '/operate/activityVip/use',
		method: 'post',
		data
	})
}

// vip活动配置/赞助活动配置 ==> 排序
export function setOperateActivityVipSort(data) {
	return request({
		url: '/operate/activityVip/sort',
		method: 'post',
		data
	})
}

// 查询banner区域
export function operateConfigBannerQueryGameList(params) {
	return request({
		url: '/operate/configBanner/queryGameList',
		method: 'get',
		params
	})
}

// vip活动配置/赞助活动配置 ==> 排序
export function setUserInfoupdatePwdAdmin(data) {
	return request({
		url: '/userInfo/updatePwdAdmin',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 查询列表
export function getOperateDiscountActivityQueryList(data) {
	return request({
		url: '/operate/discountActivity/queryList',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 获取已创建的活动id，名称
export function getOperateDiscountActivityQueryActivityNameList(params) {
	return request({
		url: '/operate/discountActivity/queryActivityNameList',
		method: 'get',
		params
	})
}

// 优惠活动配置 ==> 获取页签下的所有活动
export function getOperateDiscountActivityQueryActivityNameListByTag(params) {
	return request({
		url: '/operate/discountActivity/queryActivityNameListByTag',
		method: 'get',
		params
	})
}

// 优惠活动配置 ==> 新增
export function setOperateDiscountActivityAdd(data) {
	return request({
		url: '/operate/discountActivity/add',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 删除
export function setOperateDiscountActivityDelete(data) {
	return request({
		url: '/operate/discountActivity/delete',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 排序
export function setOperateDiscountActivitySort(data) {
	return request({
		url: '/operate/discountActivity/sort',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 修改
export function setOperateDiscountActivityUpdate(data) {
	return request({
		url: '/operate/discountActivity/update',
		method: 'post',
		data
	})
}

// 优惠活动配置 ==> 启用、禁用
export function setOperateDiscountActivityUse(data) {
	return request({
		url: '/operate/discountActivity/use',
		method: 'post',
		data
	})
}

// 排序
export function setoperateConfigDiscountTagSort(data) {
	return request({
		url: '/operate/configDiscountTag/sort',
		method: 'post',
		data
	})
}

// 查询排序后banner区域
export function operatecCnfigBannerQuerySortedBannerArea(data) {
	return request({
		url: '/operate/configBanner/querySortedBannerArea',
		method: 'post',
		data
	})
}

// 创建页签
export function operateConfigDiscountTagQuerySortedNames(params) {
	return request({
		url: '/operate/configDiscountTag/querySortedNames',
		method: 'get',
		params
	})
}

// 分页查询代理操作记录
export function proxyOperate(data) {
	return request({
		url: '/proxyOperate/select',
		method: 'post',
		data
	})
}

// 教程名称配置-查询详情
export function configTutorNameQueryDetail(data) {
	return request({
		url: '/operate/configTutorName/queryDetail',
		method: 'post',
		data
	})
}

// 教程名称配置-查询所有教程名称
export function configTutorNameQueryTutorList(data) {
	return request({
		url: '/operate/configTutorName/queryTutorList',
		method: 'post',
		data
	})
}

// 教程名称配置-查询排序后类型
export function configTutorNameQueryTypeList(params) {
	return request({
		url: '/operate/configTutorName/queryTypeList',
		method: 'get',
		params
	})
}

// 教程名称下拉框
export function operateConfigTutorNameQueryTypeList(params) {
	return request({
		url: '/operate/configTutorName/queryTypeList',
		method: 'get',
		params
	})
}

// 教程名称配置-删除
export function configTutorNameDelete(data) {
	return request({
		url: '/operate/configTutorName/delete',
		method: 'post',
		data
	})
}

// 教程名称配置-保存
export function configTutorNameSave(data) {
	return request({
		url: '/operate/configTutorName/save',
		method: 'post',
		data
	})
}

// 教程内容状态下拉框
export function configTutorContentQuerySortedNames(data) {
	return request({
		url: '/configTutorContent/querySortedNames',
		method: 'post',
		data
	})
}

// 教程名称配置-排序
export function configTutorNameSort(data) {
	return request({
		url: '/operate/configTutorName/sort',
		method: 'post',
		data
	})
}

// 教程内容配置分页查询
export function getConfigTutorContentQueryList(data) {
	return request({
		url: '/configTutorContent/queryList',
		method: 'post',
		data
	})
}

// 教程名称配置-启用、禁用
export function configTutorNameUse(data) {
	return request({
		url: '/operate/configTutorName/use',
		method: 'post',
		data
	})
}

// 教程内容配置启用，禁用
export function getConfigTutorContentUse(data) {
	return request({
		url: '/configTutorContent/use',
		method: 'post',
		data
	})
}

// 教程页签配置-删除页签配置
export function bookmarkDeleteBookmark(data) {
	return request({
		url: '/bookmark/deleteBookmark',
		method: 'post',
		data
	})
}

// 教程内容配置删除
export function getConfigTutorContentDelete(data) {
	return request({
		url: '/configTutorContent/delete',
		method: 'post',
		data
	})
}

// 教程页签配置-新增页签配置
export function bookmarkInsertBookmark(data) {
	return request({
		url: '/bookmark/insertBookmark',
		method: 'post',
		data
	})
}

// 教程内容配置新增
export function getConfigTutorContentInsert(data) {
	return request({
		url: '/configTutorContent/insert',
		method: 'post',
		data
	})
}

// 教程页签配置-查询分页列表
export function bookmarkQueryList(data) {
	return request({
		url: '/bookmark/queryList',
		method: 'post',
		data
	})
}

// 教程内容配置修改
export function getConfigTutorContentUpdate(data) {
	return request({
		url: '/configTutorContent/update',
		method: 'post',
		data
	})
}

// 教程页签配置-获取已创建的页签+状态--排序展示
export function bookmarkQuerySortedNames(data) {
	return request({
		url: '/bookmark/querySortedNames',
		method: 'post',
		data
	})
}

// 教程内容配置排序
export function getConfigTutorContentSort(data) {
	return request({
		url: '/configTutorContent/sort',
		method: 'post',
		data
	})
}

// 教程页签配置-排序
export function bookmarkSort(data) {
	return request({
		url: '/bookmark/sort',
		method: 'post',
		data
	})
}

// 教程页签配置-页签配置修改
export function updateBookmark(data) {
	return request({
		url: '/bookmark/updateBookmark',
		method: 'post',
		data
	})
}

// 教程页签配置-启用、禁用
export function bookmarkUse(data) {
	return request({
		url: '/bookmark/use',
		method: 'post',
		data
	})
}

// 教程变更记录==>变更目录下拉框
export function getOperateObConfigTutorRecordQueryTutorEnums(params) {
	return request({
		url: '/operate/obConfigTutorRecord/queryTutorEnums',
		method: 'get',
		params
	})
}

// 教程变更记录==>变更类型下拉框
export function getOperateObConfigTutorRecordQueryAllEnums(params) {
	return request({
		url: '/operate/obConfigTutorRecord/queryAllEnums',
		method: 'get',
		params
	})
}

// 教程变更记录分页查询
export function getOperateObConfigTutorRecordSelect(data) {
	return request({
		url: '/operate/obConfigTutorRecord/select',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利一审查询
export function getActivityBonusFirstAuditList(data) {
	return request({
		url: '/activity/bonusAudit/firstAudit/page',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利二审查询
export function getActivityBonusSecondAuditList(data) {
	return request({
		url: '/activity/bonusAudit/secondAudit/page',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利审核一审详情
export function getActivityBonusFirstAuditDetail(data) {
	return request({
		url: '/activity/bonusAudit/oneDetail',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利审核二审详情
export function getActivityBonusSecondAuditDetail(data) {
	return request({
		url: '/activity/bonusAudit/twoDetail',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利审核锁单&解锁
export function activityBonusLockRecord(data) {
	return request({
		url: '/activity/bonusAudit/lockRecord',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利一二审审核操作
export function activityBonusAuditAPI(data) {
	return request({
		url: '/activity/bonusAudit/bonusAudit',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利审核记录
export function getActivityBonusAudiAuditRecordPage(data) {
	return request({
		url: '/activity/bonusAudit/auditRecordPage',
		method: 'post',
		data
	})
}

// 红利管理 ===> 红利审核记录==》查看
export function getoperateActivityBonusAuditdetail(data) {
	return request({
		url: '/activity/bonusAudit/detail',
		method: 'post',
		data
	})
}

// vip介绍页面设置
export function getclientConfigvipActivityqueryList(data) {
	return request({
		url: '/clientConfig/vipActivity/queryList',
		method: 'post',
		data
	})
}

// vip介绍页面设置===》新增
export function getclientConfigvipActivityadd(data) {
	return request({
		url: '/clientConfig/vipActivity/add',
		method: 'post',
		data
	})
}

// vip介绍页面设置===》开启，禁用
export function getclientConfigvipActivityuse(data) {
	return request({
		url: '/clientConfig/vipActivity/use',
		method: 'post',
		data
	})
}

// vip介绍页面设置===》编辑
export function getclientConfigvipActivityupdate(data) {
	return request({
		url: '/clientConfig/vipActivity/update',
		method: 'post',
		data
	})
}

// vip介绍页面设置===》编辑
export function getclientConfigvipActivitydeletee(data) {
	return request({
		url: '/clientConfig/vipActivity/delete',
		method: 'post',
		data
	})
}

// vip介绍页面设置===》排序
export function getclientConfigvipActivitysort(data) {
	return request({
		url: '/clientConfig/vipActivity/sort',
		method: 'post',
		data
	})
}

// 分享管理==》操作记录
export function getmemberDomaindomainRecordList(data) {
	return request({
		url: '/memberDomain/domainRecordList',
		method: 'post',
		data
	})
}

// 赞助页面设置==》新增
export function getclientConfigsponsoradd(data) {
	return request({
		url: '/clientConfig/sponsor/add',
		method: 'post',
		data
	})
}

// 赞助页面设置==》修改
export function getclientConfigsponsorupdate(data) {
	return request({
		url: '/clientConfig/sponsor/update',
		method: 'post',
		data
	})
}

// 赞助页面设置==》查询
export function getclientConfigsponsorqueryList(data) {
	return request({
		url: '/clientConfig/sponsor/queryList',
		method: 'post',
		data
	})
}

// 赞助页面设置==》删除
export function getclientConfigsponsordelete(data) {
	return request({
		url: '/clientConfig/sponsor/delete',
		method: 'post',
		data
	})
}

// 赞助页面设置==开启，禁用
export function getclientConfigsponsoruse(data) {
	return request({
		url: '/clientConfig/sponsor/use',
		method: 'post',
		data
	})
}

// 赞助页面设置==排序
export function getclientConfigsponsorsort(data) {
	return request({
		url: '/clientConfig/sponsor/sort',
		method: 'post',
		data
	})
}

// 赞助页面设置 ==> 赞助获取已创建的活动
export function getclientConfigsponsorqueryActivityNameList(params) {
	return request({
		url: '/clientConfig/sponsor/queryActivityNameList',
		method: 'get',
		params
	})
}

// VIP活动页面设置 ==> 活动获取已创建的活动
export function getclientConfigvipActivityqueryActivityNameList(params) {
	return request({
		url: '/clientConfig/vipActivity/queryActivityNameList',
		method: 'get',
		params
	})
}

// 域名管理分页查询
export function getMemberDomainDomainList(data) {
	return request({
		url: '/memberDomain/domainList',
		method: 'post',
		data
	})
}

// 域名管理新增、编辑
export function getMemberDomainSaveDomain(data) {
	return request({
		url: '/memberDomain/saveDomain',
		method: 'post',
		data
	})
}

// 域名管理删除
export function getMemberDomainDeleteDomain(data) {
	return request({
		url: '/memberDomain/deleteDomain',
		method: 'post',
		data
	})
}

// 下载模板
export function getMemberDomainDownLoadTemplate(data) {
	return request({
		url: '/memberDomain/downLoadTemplate',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 上传文件
export function setMemberDomainUpLoadExcel(data) {
	return request({
		url: '/memberDomain/upLoadExcel',
		method: 'post',
		responseType: 'blob',
		data
	})
}

// 导入Excel
export function getMemberDomainImportExcel(data) {
	return request({
		url: '/memberDomain/importExcel',
		method: 'post',
		data
	})
}

// 设置
export function getMemberDomainSetting(data) {
	return request({
		url: '/memberDomain/setting',
		method: 'post',
		data
	})
}

// 公共配置===》开关
export function getOperatactivityConfigCommonupdateStatus(data) {
	return request({
		url: '/operate/activityConfigCommon/updateStatus',
		method: 'post',
		data
	})
}

// 公共配置===》查询
export function getOperateactivityConfigCommonselect(data) {
	return request({
		url: '/operate/activityConfigCommon/select',
		method: 'post',
		data
	})
}

// 公共配置===》查询
export function getOperateactivityConfigCommonupdate(data) {
	return request({
		url: '/operate/activityConfigCommon/update',
		method: 'post',
		data
	})
}

// 充值优惠配置===> 列表数据查询接口
export function getOperateActivityConfigQuery(data) {
	return request({
		url: '/operate/activityConfigDeposit/query',
		method: 'post',
		data
	})
}

// 充值优惠配置===> 编辑功能
export function getOperateActivityConfigEdit(data) {
	return request({
		url: '/operate/activityConfigDeposit/edit',
		method: 'post',
		data
	})
}

// 充值优惠配置===> 列表查询：启用, 停用功能
export function getOperateActivityConfigUpdateStatus(data) {
	return request({
		url: '/operate/activityConfigDeposit/updateStatus',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 列表查询
export function getOperateActivityConfiTabSelect(data) {
	return request({
		url: '/operate/activityConfigTab/select',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 新增
export function getOperateActivityConfiTabInsert(data) {
	return request({
		url: '/operate/activityConfigTab/insert',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 删除
export function getOperateActivityConfiTabDelete(data) {
	return request({
		url: '/operate/activityConfigTab/delete',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 修改
export function getOperateActivityConfiTabUpdate(data) {
	return request({
		url: '/operate/activityConfigTab/update',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 拖动排序
export function getOperateActivityConfiTabUpdateSort(data) {
	return request({
		url: '/operate/activityConfigTab/sort',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 修改状态
export function getOperateActivityConfiTabUpdateStatus(data) {
	return request({
		url: '/operate/activityConfigTab/updateStatus',
		method: 'post',
		data
	})
}

// 优惠页面设置===> 修改状态
export function getOperateActivityConfiTabSelectStatus(data) {
	return request({
		url: '/operate/activityConfigTab/selectStatus'
	})
}

// 查询政策类型
export function getProxyMinContractPolicyTypeList(data) {
	return request({
		url: '/proxy/MinContract/policyTypeList',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》首页banner配置==>分页查询
export function getBannerList(data) {
	return request({
		url: '/operate/configBanner/queryBannerList',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》首页banner配置==> 新增
export function bannerAdd(data) {
	return request({
		url: '/operate/configBanner/add',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》首页banner配置==> 编辑
export function bannerEdit(data) {
	return request({
		url: '/operate/configBanner/update',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》首页banner配置==> 删除
export function bannerDel(data) {
	return request({
		url: '/operate/configBanner/delete',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》首页banner配置==> 状态修改
export function bannerUpdataStatus(data) {
	return request({
		url: '/operate/configBanner/use',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》首页场馆/APP闪频页分页查询  场馆/APP闪频页一个接口 类型区分
export function homeVenueList(data) {
	return request({
		url: '/operate/startPage/queryList',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》首页场馆/APP闪频页修改  场馆/APP闪频页一个接口 类型区分
export function homeVenueUpdate(data) {
	return request({
		url: '/operate/startPage/update',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》App闪频页添加
export function startPageAdd(data) {
	return request({
		url: '/operate/startPage/add',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》App闪频页编辑
export function startPageEdit(data) {
	return request({
		url: '/operate/startPage/update',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》App闪频页-状态修改
export function startPageUpdataStatus(data) {
	return request({
		url: '/operate/startPage/use',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》优惠页面页签配置-分页查询
export function promotionList(data) {
	return request({
		url: '/operate/activityConfigTab/queryActivityTypeList',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》优惠页面页签配置-新增
export function promotionPageAdd(data) {
	return request({
		url: '/operate/activityConfigTab/add',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》优惠页面页签配置-编辑
export function promotionPageEdit(data) {
	return request({
		url: '/operate/activityConfigTab/update',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》优惠页面页签配置-状态修改
export function promotionUpdataStatus(data) {
	return request({
		url: '/operate/activityConfigTab/usequeryProxyApplyEdit',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》优惠页面页签配置-删除
export function promotionDel(data) {
	return request({
		url: '/operate/activityConfigTab/delete',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》代理申请图配置-分页查询
export function queryProxyApplyList(data) {
	return request({
		url: '/operate/configProxyApply/queryProxyApplyList',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》代理申请图配置-编辑
export function queryProxyApplyEdit(data) {
	return request({
		url: '/operate/configProxyApply/update',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》代理申请图配置-新增
export function queryProxyApplyAdd(data) {
	return request({
		url: '/operate/configProxyApply/add',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》代理申请图配置-状态修改
export function queryProxyApplyUpStatus(data) {
	return request({
		url: '/operate/configProxyApply/use',
		method: 'post',
		data
	})
}

// 运营==>客户端管理配置==》代理申请图配置-删除
export function queryProxyApplyDel(data) {
	return request({
		url: '/operate/configProxyApply/delete',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》公共规则配置-查询
export function activityConfigCommon(data) {
	return request({
		url: '/operate/activityConfigCommon/select',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》公共规则配置-修改状态
export function activityConfigCommonuUpdataStatus(data) {
	return request({
		url: '/operate/activityConfigCommon/updateStatus',
		method: 'post',
		data
	})
}
// 运营==>客户端管理配置==》公共规则配置-修改状态
export function activityConfigCommonuUpdata(data) {
	return request({
		url: '/operate/activityConfigCommon/update',
		method: 'post',
		data
	})
}

export function shutdownMaintenanceList(data) {
	return request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/shutdownMaintenance/info/list',
		method: 'post',
		data
	})
}

export function shutdownMaintenanceAdd(data) {
	return request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/shutdownMaintenance/add',
		method: 'post',
		data
	})
}

export function shutdownMaintenanceUpdate(data) {
	return request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/shutdownMaintenance/update',
		method: 'post',
		data
	})
}

// 运营==》》代理APP常用功能默认配置-App默认列表查询
export function getAppDefaultConfigList(data) {
	return request({
		url: '/appDefaultConfig/list',
		method: 'post',
		data
	})
}

// 运营==》代理APP常用功能默认配置-App默认明细查询
export function getAppDefaultConfigDetail(data) {
	return request({
		url: '/appDefaultConfig/detail',
		method: 'post',
		data
	})
}

// 运营==》代理APP常用功能默认配置-App默认编辑
export function updateAppDefaultConfig(data) {
	return request({
		url: '/appDefaultConfig/update',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-分页查询-客服入口开关配置列表
export function customerConfigList(data) {
	return request({
		url: '/customerConfig/list',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-新增-客服入口开关配置
export function customerConfigAdd(data) {
	return request({
		url: '/customerConfig/add',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-查询信用线-现金线默认配置
export function customerConfigDefaultConfig(data) {
	return request({
		url: '/customerConfig/defaultConfig',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-删除-客服入口开关配置
export function customerConfigDelete(data) {
	return request({
		url: '/customerConfig/delete',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-校验代理是否存在
export function customerConfigProxyVerify(data) {
	return request({
		url: '/customerConfig/proxyVerify',
		method: 'post',
		data
	})
}

export function downloadConfigProxyVerify(data) {
	return request({
		url: '/DownloadConfig/proxyVerify',
		method: 'post',
		data
	})
}

export function joinConfigConfigProxyVerify(data) {
	return request({
		url: '/joinConfig/proxyVerify',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-编辑-客服入口开关配置
export function customerConfigUpdate(data) {
	return request({
		url: '/customerConfig/update',
		method: 'post',
		data
	})
}

// 运营==》运营配置管理-客服入口配置-编辑信用线-现金线默认配置
export function customerConfigUpdateDefaultConfig(data) {
	return request({
		url: '/customerConfig/updateDefaultConfig',
		method: 'post',
		data
	})
}

//获取代理登出配置列表
export function getProxyLoginOutConfigList(data) {
	return request({
		url: '/proxyLoginOutCfg/page',
		method: 'post',
		data
	})
}

//代理登出配置编辑
export function proxyLoginOutEditAPI(data) {
	return request({
		url: '/proxyLoginOutCfg/update',
		method: 'post',
		data
	})
}

//代理登出配置新增
export function proxyLoginOutAddAPI(data) {
	return request({
		url: '/proxyLoginOutCfg/add',
		method: 'post',
		data
	})
}

//代理登出配置删除
export function proxyLoginOutDeleteAPI(data) {
	return request({
		url: '/proxyLoginOutCfg/remove',
		method: 'post',
		data
	})
}


export default {
	customerConfigList,
	customerConfigAdd,
	customerConfigDefaultConfig,
	customerConfigDelete,
	customerConfigProxyVerify,
	customerConfigUpdate,
	customerConfigUpdateDefaultConfig,
	updateAppDefaultConfig,
	getAppDefaultConfigDetail,
	getAppDefaultConfigList,
	shutdownMaintenanceList,
	shutdownMaintenanceAdd,
	shutdownMaintenanceUpdate,
	queryDiscountTagList,
	queryVipActivityList,
	queryDiscountActivityList,
	queryActivityTypeList,
	configDiscountTagQueryNames,
	configDiscountTagEdit,
	configDiscountTagAdd,
	configDiscountTagQueryList,
	configDiscountTagDelete,
	configDiscountTagUse,
	clientCommonUpdateAPI,
	deleteClientCommonList,
	useClientCommonList,
	clientCommonQueryByType,
	clientCommonQueryPublicResourcesAPI,
	clientStartListAPI,
	getOperateConfigAnnouncementSelectAll,
	getOperateConfigAnnouncementSelect,
	getOperateConfigAnnouncementSave,
	getOperateConfigAnnouncementDelete,
	getOperateConfigAnnouncementStatus,
	getOperateConfigNoticeSelectAll,
	getOperateConfigNoticeSelectDetail,
	getOperateConfigNoticeSave,
	getOperateConfigNoticeRetract,
	clientStartAddAPI,
	clientStartUpdateAPI,
	clientStartDeleteAPI,
	editClientCommonList,
	clientStartUseAPI,
	clientStartUploadAPI,
	platformSelectAPI,
	platformUpdateAPI,
	activityInfoLogAPI,
	activityInfoLogListAPI,
	activityTypeDetailAPI,
	activityQueryTypeListAPI,
	activitySortAPI,
	activityUpdateAPI,
	getOperateObMerchantGameRecordSelect,
	getQperateConfigBannerQueryBannerList,
	getOperateConfigBannerUse,
	getOperateConfigBannerDelete,
	getPperateConfigBannerUpdate,
	getOperateConfigBannerAdd,
	operateConfigBannerQueryBannerAreaAPI,
	setoperateConfigBannerSort,
	operateConfigClientRecordQueryEnumsAPI,
	getOperateConfigClientRecordQueryRecordList,
	getOperateObConfigNoticeSelectAll,
	getsysMaintenanceList,
	getsysMaintenanceAdd,
	getsysMaintenanceUpdate,
	getsysMaintenanceUpdateStatus,
	getsysMaintenanceDelete,
	getOperateSysMsgList,
	setOperateSysMsgList,
	setStatusOperateSysMsg,
	getFeedBackPageFeedBack,
	OperateObConfigAnnouncementRecordQueryFeedBackEnums,
	getOperateActivityVipQueryList,
	getOperateActivityVipQueryActivityNameList,
	setOperateActivityVipAdd,
	setOperateActivityVipDelete,
	setOperateActivityVipUpdate,
	setOperateActivityVipUse,
	setOperateActivityVipSort,
	operateConfigBannerQueryGameList,
	setUserInfoupdatePwdAdmin,
	getOperateDiscountActivityQueryList,
	getOperateDiscountActivityQueryActivityNameList,
	getOperateDiscountActivityQueryActivityNameListByTag,
	setOperateDiscountActivityAdd,
	setOperateDiscountActivityDelete,
	setOperateDiscountActivitySort,
	setOperateDiscountActivityUpdate,
	setOperateDiscountActivityUse,
	setoperateConfigDiscountTagSort,
	operatecCnfigBannerQuerySortedBannerArea,
	operateConfigDiscountTagQuerySortedNames,
	proxyOperate,
	configTutorNameQueryDetail,
	configTutorNameQueryTutorList,
	configTutorNameQueryTypeList,
	configTutorNameDelete,
	configTutorNameSave,
	configTutorNameSort,
	configTutorNameUse,
	bookmarkDeleteBookmark,
	bookmarkInsertBookmark,
	bookmarkQueryList,
	downloadConfigProxyVerify,
	joinConfigConfigProxyVerify,
	bookmarkQuerySortedNames,
	bookmarkSort,
	updateBookmark,
	bookmarkUse,
	operateConfigTutorNameQueryTypeList,
	configTutorContentQuerySortedNames,
	getConfigTutorContentQueryList,
	getConfigTutorContentUse,
	getConfigTutorContentDelete,
	getConfigTutorContentInsert,
	getConfigTutorContentUpdate,
	getConfigTutorContentSort,
	clientCommonTypeList,
	getOperateObConfigTutorRecordQueryTutorEnums,
	getOperateObConfigTutorRecordQueryAllEnums,
	getOperateObConfigTutorRecordSelect,
	getActivityBonusFirstAuditList,
	getActivityBonusSecondAuditList,
	getActivityBonusFirstAuditDetail,
	getActivityBonusSecondAuditDetail,
	activityBonusLockRecord,
	activityBonusAuditAPI,
	getActivityBonusAudiAuditRecordPage,
	getoperateActivityBonusAuditdetail,
	getclientConfigvipActivityqueryList,
	getclientConfigvipActivityadd,
	getclientConfigvipActivityuse,
	getclientConfigvipActivityupdate,
	getclientConfigvipActivitydeletee,
	getmemberDomaindomainRecordList,
	getclientConfigvipActivitysort,
	getclientConfigsponsoradd,
	getclientConfigsponsorupdate,
	getclientConfigsponsorqueryList,
	getclientConfigsponsordelete,
	getclientConfigsponsoruse,
	getclientConfigsponsorsort,
	getclientConfigsponsorqueryActivityNameList,
	getclientConfigvipActivityqueryActivityNameList,
	getMemberDomainDomainList,
	getMemberDomainSaveDomain,
	getMemberDomainDeleteDomain,
	getMemberDomainDownLoadTemplate,
	getMemberDomainImportExcel,
	setMemberDomainUpLoadExcel,
	getMemberDomainSetting,
	getOperatactivityConfigCommonupdateStatus,
	getOperateactivityConfigCommonselect,
	getOperateactivityConfigCommonupdate,
	getOperateActivityConfigQuery,
	getOperateActivityConfigEdit,
	getOperateActivityConfigUpdateStatus,
	getOperateActivityConfiTabSelect,
	getOperateActivityConfiTabInsert,
	getOperateActivityConfiTabDelete,
	getOperateActivityConfiTabUpdate,
	getOperateActivityConfiTabUpdateSort,
	getOperateActivityConfiTabUpdateStatus,
	getOperateActivityConfiTabSelectStatus,
	getProxyMinContractPolicyTypeList,
	getBannerList,
	activityConfigCommon,
	queryProxyApplyAdd,
	queryProxyApplyEdit,
	queryProxyApplyUpStatus,
	getOperateConfigAnnouncementUpdate,
	getBwConfigNoticeDetails,
	getProxyLoginOutConfigList,
	proxyLoginOutEditAPI,
	proxyLoginOutAddAPI,
	proxyLoginOutDeleteAPI
}
