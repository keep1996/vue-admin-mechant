// 德州=>俱乐部管理
import request from '@/utils/request'

export default {
	// 德州==> 俱乐部管理 ===> 俱乐部列表 ==》 查看
	clubManagerListBend: (data) => request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/club/list',
		method: 'post',
		data
	}),

	// 俱乐部申请列表
	clubManageClubAuditList: (data) => request({
		url: 'clubAudit/clubAuditList',
		method: 'post',
		data
	}),

	// 更改俱乐部审批状态
	clubManageAudit: (data) => request({
		url: 'clubAudit/audit',
		method: 'post',
		data
	}),

	// 锁单、解锁
	clubManageClubAuditLock: (data) => request({
		url: 'clubAudit/clubAuditLock',
		method: 'post',
		data
	}),

	// 俱乐部锁单加审核
	clubManageLockAndAudit: (data) => request({
		url: 'clubAudit/clubAuditLockAndAudit',
		method: 'post',
		data
	}),

	// 德州==> 俱乐部管理 ===> 俱乐部列表 ==》 导出
	clubManageExportBend: (data) => request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/club/clubLsExport',
		method: 'post',
		data,
		responseType: 'blob'
	}),

	// 德州==> 俱乐部管理 ===> 通用俱乐部管理 ==》 更新
	clubManageAddUpdateClubParamBend: (data) => request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/club/addOrUpdateClubParam',
		method: 'post',
		data
	}),

	// 更新通用设置
	clubManageUpdateClubParam: (data) => request({
		url: 'club/updateClubParam',
		method: 'post',
		data
	}),

	// 德州==> 俱乐部管理 ===> 通用俱乐部管理 ==》 查询
	clubManageQueryClubParamBend: () => request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/club/queryClubParam',
		method: 'post',
		data: {}
	}),

	// 俱乐部管理-俱乐部会员列表
	getClubMemberList: (data) => {
		return request({
			url: 'club/memberList',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部会员列表-导出
	exportClubMemberList: (data) => {
		return request({
			url: 'club/memberLsExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 俱乐部管理-俱乐部会员列表-启用/禁用
	setClubMemberStartOrStop: (data) => {
		return request({
			url: 'club/incomingSwitch',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部会员列表-获取操作记录
	getClubMemberOperationRecords: (data) => {
		return request({
			url: 'club/clubMemberOperatelist',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部联盟-列表-查询
	getClubLeaguesListBend: (data) => {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubLeagues/List',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部联盟-列表-导出
	exportClubLeaguesListBend: (data) => {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubLeagues/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 俱乐部管理-俱乐部联盟-新增-编辑
	addOrEditClubLeaguesBend: (data) => {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubLeagues/addOrEdit',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部联盟-查询所有俱乐部
	getClubLeaguesQueryAllClubBend: (data) => {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubLeagues/queryAllClub',
			method: 'post',
			data
		})
	},
	// 俱乐部管理-俱乐部联盟-校验俱乐部信息
	checkClubLeaguesClubIsReapetBend: (data) => {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubLeagues/checkClub',
			method: 'post',
			data
		})
	}

}
