import request from '@/utils/request'

// 游戏配置==> 游戏分类管理 ===> 分页列表拖拽排序
export function gameAssortSortAPI(data) {
	return request({
		url: '/gameAssort/sort',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏分类管理 ===> 子游戏分页查询
export function queryChildGamePageAPI(data) {
	return request({
		url: '/gameAssort/queryChildrenGamePage',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===> 子游戏查询
export function queryChildGameAPI(data) {
	return request({
		url: '/gameAssort/queryChildrenGame',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===> 子游戏配置
export function queryChildGameConfigAPI(data) {
	return request({
		url: '/gameAssort/selectById',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏分类管理 ===> 删除
export function gameDeleteAPI(data) {
	return request({
		url: '/gameAssort/delete',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===> 编辑保存
export function gameUpdateAPI(data) {
	return request({
		url: '/gameAssort/update',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===> 编辑
export function gameUpdateStatusAPI(data) {
	return request({
		url: '/gameAssort/updateStatus',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===> 创建
export function gameCreateAPI(data) {
	return request({
		url: '/gameAssort/create',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏分类管理 ===>游戏平台查询
export function queryGameAPI(data) {
	return request({
		url: '/gameAssort/queryGame',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏平台管理 ===> 分页列表
export function gamePlatformListAPI(data) {
	return request({
		url: '/gamePlatform/platformList',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏平台管理 ===> 分页列表
export function gamePlatformEditAPI(data) {
	return request({
		url: '/gamePlatform/edit',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏平台管理 ===> 分页列表
export function gamePlatformStatusAPI(data) {
	return request({
		url: '/gamePlatform/editStatus',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏推荐管理 ===> 推荐游戏查询
export function getRecommendGameList(data) {
	return request({
		url: '/gameRecommend/queryRecommend',
		method: 'post',
		data
	})
}
// 游戏配置==> 游戏推荐管理 ===> 保存
export function saveGameRecommendInfo(data) {
	return request({
		url: '/gameRecommend/saveRecommend',
		method: 'post',
		data
	})
}

//  游戏标签管理 ===> 表格查询
export function getTabelData(data) {
	return request({
		url: '/obGameLabel/select',
		method: 'post',
		data
	})
}
//  游戏标签管理 ===> 新增游戏标签管理
export function addObGameLabel(data) {
	return request({
		url: '/obGameLabel/insert',
		method: 'post',
		data
	})
}
//  游戏标签管理 ===> 已标签游戏查询
export function getGameLabelRelation(data) {
	return request({
		url: '/obGameLabel/getGameLabelRelation',
		method: 'post',
		data
	})
}
//  游戏标签管理 ===> 修改游戏标签 状态
export function setUpdateStatus(data) {
	return request({
		url: '/obGameLabel/updateStatus',
		method: 'post',
		data
	})
}
//  游戏标签管理 ===> 修改游戏标签管理
export function setUpdateLabel(data) {
	return request({
		url: '/obGameLabel/update',
		method: 'post',
		data
	})
}
//  游戏标签管理 ===> 删除游戏标签管理
export function setUpdateDelete(data) {
	return request({
		url: '/obGameLabel/delete',
		method: 'post',
		data
	})
}
//  游戏注单 ===> 游戏平台
export function getMerchantGameGamePlant(data) {
	return request({
		url: '/merchantGame/gamePlant',
		method: 'post',
		data
	})
}
//  游戏注单 ===> 关联推荐游戏
export function gameManageList(data) {
	return request({
		url: '/gameManager/gameManageList',
		method: 'post',
		data
	})
}
//  游戏注单 ===> 游戏注单列表导出
export function getGameRecordDownload(data) {
	return request({
		url: '/gameRecord/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
//  游戏注单 ===> 获取注单明细
export function getGameRecordDetail(data) {
	return request({
		url: '/gameRecords/getGameRecordDetails',
		method: 'post',
		data
	})
}
//  异常注单 ===> 获取异常注单分页
export function getExceptionGameRecord(data) {
	return request({
		url: '/gameRecord/exceptionGameRecord',
		method: 'post',
		data
	})
}
//  异常注单 ===> 获取异常注单详情
export function getExceptionDetail(data) {
	return request({
		url: '/gameRecord/exceptionDetail',
		method: 'post',
		data
	})
}
//  异常注单 ===> 异常注单列表导出
export function exceptionDownloadAPI(data) {
	return request({
		url: '/gameRecord/exceptionDownload',
		method: 'post',
		responseType: 'blob',
		data
	})
}
//  游戏搜索管理 ===> 游戏搜索日志
export function getGameSearchLog(data) {
	return request({
		url: '/gameSearchLog/selectLog',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏搜索管理 ===> 列表
export function gameSearchListAPI(data) {
	return request({
		url: '/obSearchConfig/select',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏搜索管理 ===> 新增
export function gameSearchCreateAPI(data) {
	return request({
		url: '/obSearchConfig/insert',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏搜索管理 ===> 修改
export function gameSearchUpdateAPI(data) {
	return request({
		url: '/obSearchConfig/save',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏搜索管理 ===> 删除
export function gameSearchDeleteAPI(data) {
	return request({
		url: '/obSearchConfig/delete',
		method: 'post',
		data
	})
}
export function gameLabelList(data) {
	return request({
		url: '/gameManager/gameLabelList',
		method: 'post',
		data
	})
}
export function gameModuleNameList(data) {
	return request({
		url: '/gameManager/gameModuleNameList',
		method: 'post',
		data
	})
}
export function addGame(data) {
	return request({
		url: '/gameManager/addGame',
		method: 'post',
		data
	})
}

// 图片上传用这个
export function imageUpload(data, cb) {
	return request({
		url: '/uploadFile/image',
		method: 'post',
		data,
		cb
	})
}

export function imageUploadAPI(data, cb) {
	return request({
		url: '/uploadFile/image',
		method: 'post',
		data,
		cb
	})
}
export function editGameStatus(data) {
	return request({
		url: '/gameManager/editStatus',
		method: 'post',
		data
	})
}
export function gameManageDetail(data) {
	return request({
		url: '/gameManager/gameManageDetail',
		method: 'post',
		data
	})
}
export function gamePlant(data) {
	return request({
		url: '/merchantGame/gamePlant',
		method: 'post',
		data
	})
}

// 游戏配置==> 游戏搜索管理 ===> 删除
export function gameList(data) {
	return request({
		url: '/gameManager/gameList',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 列表
export function gameHomeRecommendListAPI(data) {
	return request({
		url: '/gameCommonModule/gameModuleList',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 列表拖动
export function gameHomeRecommendListSortAPI(data) {
	return request({
		url: '/gameCommonModule/gameModuleSort',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 列表状态变更
export function recommendStatusChangeAPI(data) {
	return request({
		url: '/gameCommonModule/editModuleStatus',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 列表 ===> 详情
export function gameHomeRecommendDetailsAPI(data) {
	return request({
		url: '/gameCommonModule/gameModuleDetail',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 游戏专题 ===> 详情
export function gameSpecialDetailsAPI(params) {
	return request({
		url: '/gameCommonModule/gameTopicModuleDetail',
		method: 'get',
		params
	})
}

// 游戏==> 首页推荐位 ==> 游戏专题 ===> 详情分类名称下拉
export function gameAssortDicAPI(params) {
	return request({
		url: '/gameCommonModule/gameAssortDic',
		method: 'get',
		params
	})
}

// 游戏==> 首页推荐位 ==> 游戏专题 ===> 开启禁用
export function gameEitTopicModuleAPI(data) {
	return request({
		url: '/gameCommonModule/editTopicModuleStatus',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 游戏专题 ===> 删除
export function gameDelTopicModuleAPI(data) {
	return request({
		url: '/gameCommonModule/delTopicModule',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 游戏专题 ===> 保存
export function editGameTopicModuleAPI(data) {
	return request({
		url: '/gameCommonModule/editGameTopicModule',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 最新游戏 ===> 回显
export function gameLatestModuleDetailAPI(params) {
	return request({
		url: '/gameCommonModule/gameLatestModuleDetail',
		method: 'get',
		params
	})
}

// 游戏==> 首页推荐位 ==> 最新游戏 ===> 回显
export function gameModuleSortAPI(data) {
	return request({
		url: '/gameCommonModule/gameModuleSort',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 最新游戏 ===> 保存
export function editGameLatestModuleAPI(data) {
	return request({
		url: '/gameCommonModule/editGameLatestModule',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 列表 ===> 子模块编辑
export function gameHomeRecommendDetailsEditAPI(data) {
	return request({
		url: '/gameCommonModule/editModule',
		method: 'post',
		data
	})
}

// 游戏==> 首页推荐位 ==> 电子 ===> 查看更多跳转地址
export function gameHomeRecommendGameAssortDicAPI(params) {
	return request({
		url: '/gameCommonModule/gameAssortDic',
		method: 'get',
		params
	})
}

// 游戏==> 首页推荐位 ==> 直播 ===> 直播编辑数据获取
export function gameHomeRecommendGameLiveDetailAPI(params) {
	return request({
		url: '/gameCommonModule/gameLive',
		method: 'get',
		params
	})
}
// 游戏==> 首页推荐位 ==> 直播 ===> 直播编辑数据获取
export function gameHomeRecommendGameLiveEditAPI(data) {
	return request({
		url: '/gameCommonModule/gameLiveEdit',
		method: 'post',
		data
	})
}

// 游戏推荐==> 操作记录
export function GameCommonModuleOperateRecord(data) {
	return request({
		url: '/gameCommonModule/operateRecord',
		method: 'post',
		data
	})
}
// 游戏==> 分页查询游戏搜索管理操作记录
export function getSearchConfigOperateSelect(data) {
	return request({
		url: '/searchConfigOperate/select',
		method: 'post',
		data
	})
}
// 游戏==> 客户端配置管理==> 操作记录===>枚举
export function OperateGameConfigOperateRecordQueryEnumsAPI(params) {
	return request({
		url: '/operate/gameConfigOperateRecord/queryEnums',
		method: 'get',
		params
	})
}
// 游戏==> 客户端配置管理==> 操作记录===>分页
export function getOperateGameConfigOperateRecordQueryRecordList(data) {
	return request({
		url: '/operate/gameConfigOperateRecord/queryRecordList',
		method: 'post',
		data
	})
}
// 游戏==> 游戏管理==> 新增
export function gameManageAdd(data) {
	return request({
		url: '/game/add',
		method: 'post',
		data
	})
}

// 游戏==> 游戏管理==> 列表
export function getGameList(data) {
	return request({
		url: '/game/listPage',
		method: 'post',
		data
	})
}
// 游戏==> 游戏管理==> 禁用/启用/维护
export function setGameStatus(data) {
	return request({
		url: '/game/setStatus',
		method: 'post',
		data
	})
}
// 游戏==> 游戏管理==> 编辑
export function editGame(data) {
	return request({
		url: '/game/edit',
		method: 'post',
		data
	})
}

// 游戏==> 场馆管理==> 列表
export function getVenueList(data) {
	return request({
		url: '/venue/listPage',
		method: 'post',
		data
	})
}
// 游戏==> 场馆管理==> 设置场馆状态
export function setVenueStatus(data) {
	return request({
		url: '/venue/setStatus',
		method: 'post',
		data
	})
}
// 游戏==> 场馆管理==> 编辑
export function editVenue(data) {
	return request({
		url: '/venue/edit',
		method: 'post',
		data
	})
}
// 游戏==> 场馆接入配置==> 列表
export function venueAccessList(data) {
	return request({
		url: '/venueConnect/listPage',
		method: 'post',
		data
	})
}
// 游戏==> 场馆接入配置==> 新增
export function venueAccessEdit(data) {
	return request({
		url: '/venueConnect/edit',
		method: 'post',
		data
	})
}
//  游戏注单 ===> 游戏注单分页
export function getGameRecordNotes(data) {
	return request({
		url: '/gameRecords/listPage',
		method: 'post',
		data
	})
}
//  游戏注单 ===> 列表-导出excel(请求参数复用列表查询,只是无需传分页参数)
export function gameRecordsListExport(data) {
	return request({
		url: '/gameRecords/listExport',
		method: 'post',
		responseType: 'blob',
		data
	})
}
//  获取游戏场馆列表,用于场馆下拉和场馆匹配，没有分页
export function getVenueListWithoutPage(data) {
	return request({
		url: '/venue/getVenues',
		method: 'post',
		data
	})
}

// 重算注单详情
export function getResettlementGameRecordDetails(data) {
	return request({
		url: '/gameRecords/getResettlementGameRecordDetails',
		method: 'post',
		data
	})
}

// 游戏 操作记录
export function getPageVenueOptLog(data) {
	return request({
		url: '/venue/pageVenueOptLog',
		method: 'post',
		data
	})
}

// 列表
export const getGameRecoedTypeSection = (data) =>
	request({
		baseURL: process.env.VUE_APP_B_END_BASE_API,
		url: '/dx-game-data-manager/dxGameRecords/getGameRecoedTypeSection',
		method: 'post',
		data
	})

// 重结算列表 - 分页查询
export const getGameRecordResettlement = (data) =>
	request({
		url: '/gameRecordResettlement/listPage',
		method: 'post',
		data
	})

//  重结算列表 - 导出 excel(请求参数复用列表查询，只是无需传分页参数)
export function gameRecordResettlementListExport(data) {
	return request({
		url: '/gameRecordResettlement/listExport',
		method: 'post',
		responseType: 'blob',
		data
	})
}

//  重结算列表 - 导出 excel(请求参数复用列表查询，只是无需传分页参数)
export function gameRecordResettlementDetails(data) {
	return request({
		url: '/gameRecordResettlement/getGameRecordDetails',
		method: 'post',
		data
	})
}

// 游戏配置管理-新增
export function venueGameConfigAdd(data) {
	return request({
		url: '/venueGameConfig/add',
		method: 'post',
		data
	})
}

// 游戏配置管理-编辑信息
export function venueGameConfigEdit(data) {
	return request({
		url: '/venueGameConfig/edit',
		method: 'post',
		data
	})
}

// 游戏配置管理-列表查询
export function venueGameConfigListPage(data) {
	return request({
		url: '/venueGameConfig/listPage',
		method: 'post',
		data
	})
}
// 场馆管理 - 模式切换
export const setVenueModeSwitch = (data) =>
	request({
		url: '/venue/editSourceType',
		method: 'post',
		data
	})

// 游戏配置管理-启用禁用维护
export function venueGameConfigSetStatus(data) {
	return request({
		url: '/venueGameConfig/setStatus',
		method: 'post',
		data
	})
}

// 联赛-赛事名称下拉框
export function gameRecordsGameLeagueList(data) {
	return request({
		url: '/gameRecords/gameLeagueList',
		method: 'post',
		data
	})
}

export function dzGameOrderList(data) {
	return request({
		url: '/gameOrder/dzGameOrderList',
		method: 'post',
		data
	})
}

export function popDetails(data) {
	return request({
		url: '/gameRecords/allocation/popDetails',
		method: 'post',
		data
	})
}

export function dzMemberRebateDetails(data) {
	return request({
		url: '/gameOrder/dzMemberRebateDetails',
		method: 'post',
		data
	})
}

export function dzGameOrderDetails(data) {
	return request({
		url: '/gameOrder/dzGameOrderDetails',
		method: 'post',
		data
	})
}

export function listExport(data) {
	return request({
		url: '/gameOrder/listExport',
		method: 'post',
		data
	})
}

export function dzGameOrderListSummeryAll(data) {
	return request({
		url: '/gameOrder/dzGameOrderListSummeryAll',
		method: 'post',
		data
	})
}

export default {
	gameRecordsGameLeagueList,
	venueGameConfigSetStatus,
	venueGameConfigListPage,
	venueGameConfigAdd,
	venueGameConfigEdit,
	getGameRecordResettlement,
	gameRecordResettlementDetails,
	gameRecordResettlementListExport,
	getGameRecoedTypeSection,
	getResettlementGameRecordDetails,
	gameList,
	editGame,
	addGame,
	editGameStatus,
	imageUpload,
	imageUploadAPI,
	gameManageList,
	gameModuleNameList,
	gamePlant,
	gameManageDetail,
	gameLabelList,
	queryChildGamePageAPI,
	queryChildGameAPI,
	queryChildGameConfigAPI,
	gameDeleteAPI,
	gameUpdateAPI,
	gameCreateAPI,
	queryGameAPI,
	gameUpdateStatusAPI,
	getTabelData,
	addObGameLabel,
	getGameLabelRelation,
	setUpdateStatus,
	setUpdateLabel,
	setUpdateDelete,
	getMerchantGameGamePlant,
	getGameRecordNotes,
	gameRecordsListExport,
	getGameRecordDownload,
	getGameRecordDetail,
	getGameSearchLog,
	gameSearchListAPI,
	gameSearchCreateAPI,
	gameSearchUpdateAPI,
	gameSearchDeleteAPI,
	gameHomeRecommendListAPI,
	gameSpecialDetailsAPI,
	gameAssortDicAPI,
	gameEitTopicModuleAPI,
	gameDelTopicModuleAPI,
	gameLatestModuleDetailAPI,
	editGameLatestModuleAPI,
	gameModuleSortAPI,
	editGameTopicModuleAPI,
	gameHomeRecommendListSortAPI,
	recommendStatusChangeAPI,
	gameHomeRecommendDetailsAPI,
	gameHomeRecommendDetailsEditAPI,
	GameCommonModuleOperateRecord,
	getSearchConfigOperateSelect,
	OperateGameConfigOperateRecordQueryEnumsAPI,
	getOperateGameConfigOperateRecordQueryRecordList,
	gameHomeRecommendGameAssortDicAPI,
	gameHomeRecommendGameLiveEditAPI,
	gameHomeRecommendGameLiveDetailAPI,
	gameAssortSortAPI,
	gamePlatformListAPI,
	gamePlatformStatusAPI,
	gamePlatformEditAPI,
	getExceptionGameRecord,
	getExceptionDetail,
	exceptionDownloadAPI,
	getRecommendGameList,
	saveGameRecommendInfo,
	gameManageAdd,
	getGameList,
	getVenueList,
	getVenueListWithoutPage,
	getPageVenueOptLog,
	setVenueModeSwitch,
	dzGameOrderList,
	popDetails,
	dzMemberRebateDetails,
	dzGameOrderDetails,
	listExport,
	dzGameOrderListSummeryAll
}
