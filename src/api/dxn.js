import request from '@/utils/request'

export default {
	// 德州==> 游戏管理 ===> 游戏配置查询
	getDxgameConfigDdetailBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxgame/config/detail',
			method: 'get',
			params
		})
	},
	// 德州==> 游戏管理 ===> 游戏配置修改
	dxgameConfigUpdateBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxgame/config/update',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部开桌规则-德州长牌修改
	dxgameConfigUpdateTexasBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxgame/config/updateTexas',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部开桌规则-短牌修改
	dxgameConfigUpdateShortBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxgame/config/updateShort',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 	牌桌列表
	getDxtableListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/list',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 	手牌列表
	getDxhandLlistBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/list',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 牌桌详情 ===》 基础信息列表
	getDxtableDetailBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/detail',
			method: 'get',
			params
		})
	},
	// 德州==> 游戏管理 ===> 牌桌详情 ===》 结算详情列表
	getGameOrderUserSettlementDetailBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/gameOrderUserSettlementDetail',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 牌桌详情 ===》 聊天记录
	getTableChatRecordListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/queryTableChatRecords',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 敏感词管理 ===》 上传列表
	getSensitiveRecordBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/sensitive/record',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 敏感词管理 ===》上传
	uploadSensitiveBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/sensitive/upload',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 敏感词管理 ===》下载
	downloadSensitiveBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/sensitive/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 游戏管理 ===> 手牌详情
	getDxhandDetailBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/detail',
			method: 'get',
			params
		})
	},
	// 德州===》 游戏管理 ===》 手牌详情 ===》 解散
	dxtableGameDissolveBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/game/dissolve',
			method: 'get',
			params
		})
	},
	// 德州===》 游戏管理 ===》 牌桌详情 ===》牌桌基础信息
	getDxtableBaseDetailBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/base/detail',
			method: 'get',
			params
		})
	},
	// 德州===》 游戏管理 ===》游戏列表
	getDxnGameManagerListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/list',
			method: 'get',
			params
		})
	},
	// 德州===》 游戏管理 ===》 牌桌管理 ===》 导出
	getDxTableListExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州===》 游戏管理 ===》 手牌管理 ===》 导出
	getDxhandListExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州===》 游戏管理 ===》 牌桌详情 ===》 更新机器人数量
	updateRobotNumBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/update/robotNum',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 游戏规则管理 ===》修改开关
	dxgameConfigSwitchBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxgame/config/switch',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 手牌详情 ===》牌局回放 ===》展示牌局回顾牌谱列表
	showGameReviewCardListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/showGameReviewCardList',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 手牌详情 ===》牌局回放 ===》查看牌局回顾详细过程明细
	getGameReviewDeatailListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/game/getGameReviewDeatailList',
			method: 'post',
			data
		})
	},
	// 注单管理列表
	betSlipQueryListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/listPage',
			method: 'post',
			data
		})
	},
	// 注单详情
	betSlipQueryDetailBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/getGameRecordDetails',
			method: 'post',
			data
		})
	},
	// 注单管理列表导出
	betSlipListExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/listExport',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},
	// 会员===》 会员管理 ===》 会员详情 ===》 导出
	getMemberDxTableListExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/member/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州===》 游戏规则管理 ===》 保险赔率配置 ===》 列表
	getInsuranceOddsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/odds/list',
			method: 'post',
			data
		})
	},
	// 德州===》 游戏规则管理 ===》 保险赔率配置 ===》 修改
	updateInsuranceOddsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/odds/update',
			method: 'post',
			data
		})
	},
	// 德州===》 游戏规则管理 ===》 保险赔率配置 ===》 德州长牌修改
	updateInsuranceTexasOddsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/texasOdds/update',
			method: 'post',
			data
		})
	},
	// 德州===》 游戏规则管理 ===》 保险赔率配置 ===》 短牌修改
	updateInsuranceShortDeckOddsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/shortDeckOdds/update',
			method: 'post',
			data
		})
	},
	// 德州===》 保险管理 ===》 保险列表
	getInsuranceInfoListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/info/list',
			method: 'post',
			data
		})
	},
	// 德州===》 保险管理 ===》 保险列表导出
	getInsuranceInfoListExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/info/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州===》 保险管理 ===》 保险列表 ===》 购买详情
	getInsuranceDetailBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/detail',
			method: 'post',
			data
		})
	},
	// 德州===》 报表管理 ===》 盈亏表
	getReportProfitAndLossBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/report/getProfitAndLossStatement',
			method: 'post',
			data
		})
	},
	// 德州===》 报表管理 ===》 盈亏表
	getReportProfitAndLossExportBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/report/profitAndLossStatementExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 俱乐部开桌规则->自定义快捷加注配置->查询
	getRaiseConfigListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/raiseConfigList',
			method: 'get',
			params
		})
	},
	// 俱乐部开桌规则->自定义快捷加注配置->编辑
	setRaiseConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/setRaiseConfig',
			method: 'post',
			data
		})
	},
	// 增值服务管理->开关接口->修改
	updateConfigToggleBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/toggleUpdate',
			method: 'post',
			data
		})
	},
	// 增值服务管理->聊天弹幕->查询
	getChatPhrasesListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/chatPhrasesList',
			method: 'get',
			data
		})
	},
	// 增值服务管理->聊天弹幕->分页查询
	getChatPhrasesPageListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/chatPhrasesPageList',
			method: 'post',
			data
		})
	},
	// 增值服务管理->聊天弹幕->新增
	addChatPhrasesBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/addPhrases',
			method: 'post',
			data
		})
	},
	// 增值服务管理->聊天弹幕->修改,启用,禁用
	updateChatPhrasesBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/updatePhrases',
			method: 'post',
			data
		})
	},
	// 增值服务管理->查询所有配置
	getValueAddedConfigListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/valueAddedConfigList',
			method: 'get',
			params
		})
	},
	// 增值服务管理->查询配置->根据type标识(1,2,3,4,5)
	getValueAddedConfigByTypeBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/valueAddedConfigByType',
			method: 'get',
			params
		})
	},
	// 增值服务管理->修改配置
	updateValueAddedConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/updateValueAddedConfig',
			method: 'post',
			data
		})
	},
	// 增值服务管理->批量查询配置
	getValueAddedConfigByTypesBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/commonValueAddedConfigByTypes',
			method: 'post',
			data
		})
	},
	// 操作日志->获取操作类型
	getActionLogDropdownConfig(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxSysOperateLog/enums',
			method: 'get',
			params
		})
	},
	// 操作日志->获取操作记录
	getDXNActionLogData(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxSysOperateLog/list',
			method: 'post',
			data
		})
	},
	// 牌局回顾->获取手牌列表分页信息
	getHandPageListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/handPageList',
			method: 'post',
			data
		})
	},
	// 订单列表->订单列表
	getOrderListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/order/getOrderSummaryList',
			method: 'post',
			data
		})
	},
	// 订单列表->订单详情
	getOrderDetailBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/order/orderDetail',
			method: 'get',
			params
		})
	},
	// 订单列表->导出
	exportOrderListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/order/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 订单列表->获取订单类型和子类型
	getOrderTypeSectionBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/order/getOrderTypeSection',
			method: 'post',
			data
		})
	},
	// 注单列表->获取注单类型和子类型
	getGameRecordTypeSectionBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/getGameRecoedTypeSection',
			method: 'post',
			data
		})
	},
	// 会员详情->俱乐部贡献
	getClubContributeListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/clubContributeList',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询
	getPartnerQueryListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/deskmateQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询-单挑手牌信息
	getPartnerQueryOneChoiceBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/oneOnOneMatchQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询-同桌手牌信息
	getPartnerQuerySameTableBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameTableQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询-相同设备信息
	getPartnerQuerySameDeviceBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameDeviceQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询-相同IP信息
	getPartnerQuerySameIpBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameIpQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-伙牌查询-单挑手牌信息-导出
	exportPartnerQueryOneChoiceBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/oneOnOneMatchExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 风控管理-伙牌查询-同桌手牌信息-导出
	exportPartnerQuerySameTableBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameTableExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 风控管理-伙牌查询-相同设备信息-导出
	exportPartnerQuerySameDeviceBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameDeviceExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 风控管理-伙牌查询-相同IP信息-导出
	exportPartnerQuerySameIpBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/sameIpExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 风控管理-大数据伙牌筛查-伙牌初筛
	getPartnerCardBeginSiftBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/initial/query',
			method: 'post',
			data
		})
	},
	// 风控管理-大数据伙牌筛查-伙牌初筛-导出
	exportPartnerCardBeginSiftBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/deskmate/initial/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 风控管理-大数据伙牌筛查-伙牌精筛
	getPartnerCardPickSiftBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/fineScreen/fineScreenAggQuery',
			method: 'post',
			data
		})
	},
	// 风控管理-大数据伙牌筛查-伙牌精筛-导出
	exportPartnerCardPickSiftBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/fineScreen/fineScreenAggExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 增值服务管理-增值收费配置-查询
	getValueAddedFeeConfigListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/commonValueAddedFeeConfigList',
			method: 'get',
			params
		})
	},
	// 增值服务管理-增值收费配置-新增和修改
	updateValueAddedFeeConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/saveOrUpdateValueAddedFeeConfig',
			method: 'post',
			data
		})
	},
	// 增值服务管理-增值收费配置-删除
	deleteValueAddedFeeConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/delConfig',
			method: 'post',
			data
		})
	},
	// 德州==> 财务管理 ===> 每手核账
	queryVerifyHand(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/hand/query',
			method: 'post',
			data
		})
	},
	// 德州-财务管理-每手核账-导出
	exportVerifyHand(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/hand/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州-财务管理-每手核账-手动核账
	manualCheckVerifyHand(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/hand/manual/check',
			method: 'post',
			data
		})
	},
	// 德州==> 财务管理 ===> 每桌核账
	queryVerifyTable(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/table/query',
			method: 'post',
			data
		})
	},
	// 德州-财务管理-每桌核账-导出
	exportVerifyTable(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/table/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州-财务管理-每桌核账-手动核账
	manualCheckVerifyTable(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/verify/table/manual/check',
			method: 'post',
			data
		})
	},
	// 保险赔率配置-导出
	exportInsureLossRatioConfig(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/insurance/odds/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 牌桌详情-基础信息-导出
	exportCardTableBasicinfo(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/base/detailExport',
			method: 'get',
			responseType: 'blob',
			params
		})
	},
	// 牌桌详情-结算详情-导出
	exportCardTableSettlementDetail(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxGameRecords/gameOrderUserSettlementDetailExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 牌桌详情-手牌数据-导出
	exportCardTableHandData(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/talbeInfo/handListExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 手牌详情-导出
	exportHandCardDetail(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxhand/detailExport',
			method: 'get',
			responseType: 'blob',
			params
		})
	},
	// 增值服务-聊天弹幕-导出
	exportChatBarrage(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/chatPhrasesPageListExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 会员详情 ===> 鱿鱼信息
	getMemberSquidInfoBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidRoundData/getRoundPage',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼列表
	getSquidListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidRoundData/getRoundPage',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼列表 ===》 导出
	exportSquidListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidRoundData/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼详情 ===》 基本信息
	getSquidDetailInfoBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidRoundData/getInfoByTableCode',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼详情 ===》 鱿鱼结算(当前轮)
	getSquidDetailSettleListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidSettleRecord/getSettleList',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼详情 ===》 鱿鱼获得过程
	getSquidDetailProcessListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squidDetailData/getDetailPage',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼钱包账变
	getSquidWalletRecordBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squid/accountChangeDetail',
			method: 'post',
			data
		})
	},
	// 德州==> 鱿鱼管理 ===> 鱿鱼账变记录 ===》 导出
	exportSquidWalletRecordBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/squid/accountChangeDetailExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 牌桌详情 ===> 解散牌桌 ==> 牌桌手牌进行状态
	getCardTableDissolveJudgeBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/game/dissolve/judge',
			method: 'get',
			params
		})
	},
	// 德州==> 游戏管理 ===> 会员牌桌列表 ==> 查询
	getMemberCardTableListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxUsertable/list',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 会员牌桌列表 ==> 导出
	exportMemberCardTableListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxUsertable/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 游戏管理 ===> 会员手牌列表 ==> 查询
	getMemberHandCardListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxUserHand/list',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 会员手牌列表 ==> 导出
	exportMemberHandCardListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxUserHand/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 增值服务管理 ===> 增值收费配置 ==> 导出
	getLookHandAddedFeeConfigListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/lookHandAddedFeeConfigList',
			method: 'get',
			params
		})
	},
	// 德州==> 商城管理 ===> 商品基础配置
	getMallProductListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/getProductPage',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品基础配置 ==》不带分页
	getMallProductNoPageListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/getProductList',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品基础配置 ==> 编辑
	updateMallProducBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/product/edit',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品基础配置 ==> 商品名称检查
	getMallProductIfExistBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/mallProductIfExist',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品基础配置 ==> 导出
	exportMallProductListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 价格配置
	getMallGoodsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallGoods/getGoodsPage',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 商品检查
	getMallGoodsIfExistBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallGoods/mallGoodsIfExist',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 价格配置新增
	addMallGoodsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallGoods/addSave',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 价格配置修改
	updateMallGoodsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallGoods/updateSave',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 价格配置导出
	exportMallGoodsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallGoods/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 商城管理 ===> 商城商品配置 ==> 排序
	setMallProductSortBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallProduct/product/sort',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 会员商品列表 ==> 查询
	getUserGoodsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallUserGoods/getUserGoodsPage',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 会员商品列表 ==> 导出
	exportUserGoodsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallUserGoods/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品订单记录列表 ==> 查询
	getMallOrderListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallOrder/getOrderPage',
			method: 'post',
			data
		})
	},
	// 德州==> 商城管理 ===> 商品订单记录列表 ==> 导出
	exportMallOrderListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/mallOrder/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 游戏管理 ===> app资源包管理 ==》 获取资源包类型列表
	getResourcePackageListBend(params) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/resource-package/list',
			method: 'get',
			params
		})
	},
	// 德州==> 游戏管理 ===> app资源包管理 ==》 获取版本号上传记录
	getResourcePackageVersionListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/resource-package/version/list',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> app资源包管理 ==》 上传
	uploadResourcePackageBend(data, cb) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			uploadFile: true,
			url: '/dx-game-data-manager/resource-package/upload',
			method: 'post',
			data,
			cb
		})
	},
	// 德州==> 游戏管理 ===> app资源包管理 ==》 更新
	updateResourcePackageBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/resource-package/resource/update',
			method: 'post',
			data
		})
	},
	// 系统==> 特殊操作 ===> 指定用户上传日志
	saveUserUploadLogBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/user/upload/Logs/save',
			method: 'post',
			data
		})
	},
	// 系统==> 特殊操作 ===> 指定用户上传日志 ==> 用户是否存在
	checkUserIsExistBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/user/upload/Logs/check',
			method: 'post',
			data
		})
	},
	// 系统==> 特殊操作 ===> 指定用户上传日志 ==》 用户是否在线
	checkUserIsOnlineBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/user/upload/Logs/online',
			method: 'post',
			data
		})
	},
	// 系统==> 特殊操作 ===> 上传日志记录 ==》 查询
	getUserUploadLogsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/user/upload/Logs/list',
			method: 'post',
			data
		})
	},
	// 系统==> 特殊操作 ===> 上传日志记录 ==》 导出
	exportUserUploadLogsListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/user/upload/Logs/listExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 通用数据参数字典 ==》单选 ret [{code:1, description:'desc'}]
	getBackendEnumListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/enum/list',
			method: 'post',
			data
		})
	},
	// 德州==> 通用数据参数字典 ==》多选 ret: [{code:,list:[]}]
	getBackendEnumListsBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/enum/lists',
			method: 'post',
			data
		})
	},
	// 德州==> 增值收费类型 ==》互动道具 ==》 列表
	getPropsConfigListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/propsList',
			method: 'post',
			data
		})
	},
	// 德州==> 增值收费类型 ==》互动道具 ==》 导出
	exportPropsConfigListBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/exportPropsList',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 德州==> 增值收费类型 ===> 互动道具 ==》 更新
	updatePropsConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/updateProps',
			method: 'post',
			data
		})
	},
	// 德州==> 增值收费类型 ===> 互动道具 ==》 校验
	checkPropsConfigBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxglobal/config/updatePropsCheck',
			method: 'post',
			data
		})
	},
	// 德州==> 牌桌详情 ===> 解散牌桌 ==> 查看牌桌状态返回码
	getTableDissolveJudgeBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/dissolve/judge',
			method: 'post',
			data
		})
	},
	// 德州==> 牌桌详情 ===> 解散牌桌 ===》 解散牌桌
	setTableDissolveBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/dxtable/tableDissolve',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部白名单 ==> 查看
	getClubTableWhitePageBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubTableWhite/getClubTableWhitePage',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部白名单 ==> 新增
	addClubTableWhiteBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubTableWhite/addClubTableWhite',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部白名单 ==> 修改
	updateClubTableWhiteBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubTableWhite/updateClubTableWhite',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部白名单 ==> 启用禁用
	updateClubWhiteStatusBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubTableWhite/updateStatus',
			method: 'post',
			data
		})
	},
	// 德州==> 游戏管理 ===> 俱乐部白名单 ==> 变更记录
	getClubTableWhiteLogBend(data) {
		return request({
			baseURL: process.env.VUE_APP_B_END_BASE_API,
			url: '/dx-game-data-manager/clubTableWhiteLog/getClubTableWhiteLogList',
			method: 'post',
			data
		})
	}
}

