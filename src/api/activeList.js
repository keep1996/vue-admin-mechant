import request from '@/utils/request'
// 优惠页面设置-查询
export function activitySelect(data) {
	return request({
		url: '/operate/activityConfigTab/select',
		method: 'post',
		data
	})
}
// 列表数据查询接口(分页，排序)
export function activityConfigList(data) {
	return request({
		url: '/operate/activityConfigList/queryPage',
		method: 'post',
		data
	})
}
// 前端排序：查询
export function activityQuerySort(data) {
	return request({
		url: '/operate/activityConfigList/querySort',
		method: 'post',
		params: data
	})
}
// 编辑
export function activityEdit(data) {
	return request({
		url: '/operate/activityConfigList/edit',
		method: 'post',
		data
	})
}
// 查询活动详情
export function activityDetails(data) {
	return request({
		url: '/operate/activityConfigList/details',
		method: 'post',
		data
	})
}
// 删除
export function activityDelete(data) {
	return request({
		url: '/operate/activityConfigList/delete',
		method: 'post',
		data
	})
}
// 前端排序：拖动后保存
export function activitySaveSort(data) {
	return request({
		url: '/operate/activityConfigList/saveSort',
		method: 'post',
		data
	})
}
// 列表查询：启用, 停用功能
export function activityUpdateStatus(data) {
	return request({
		url: '/operate/activityConfigList/updateStatus',
		method: 'post',
		data
	})
}
// 首存活动申请记录分页查询-明细
export function activityApplyRecordSCPage(data) {
	return request({
		url: '/operate/activityStatisticsData/activityApplyRecordSCPage',
		method: 'post',
		data
	})
}
// 纯展示页活动申请记录分页查询-明细
export function activityApplyRecordCZSPage(data) {
	return request({
		url: '/operate/activityStatisticsData/activityApplyRecordCZSPage',
		method: 'post',
		data
	})
}
// 活动数据统计接口
export function getActivityVisitDataStatistic(data) {
	return request({
		url: '/operate/activityStatisticsData/getActivityVisitDataStatistic',
		method: 'post',
		data
	})
}
// 纯展示页模板：保存主数据及指定会员数据
export function saveDisplayPageConfig(data) {
	return request({
		url: '/operate/activityConfigTemplate/saveDisplayPageConfig',
		method: 'post',
		data
	})
}
// 活动模板：保存主数据及指定会员数据
export function saveFirstDepositConfig(data) {
	return request({
		url: '/operate/activityConfigTemplate/saveFirstDepositConfig',
		method: 'post',
		data
	})
}
// 活动模板：根据id查询列表数据
export function activityQueryById(data) {
	return request({
		url: '/operate/activityConfigList/queryById',
		method: 'post',
		data
	})
}
// 活动模板：导入文件
export function firstDepositCheck(data) {
	return request({
		url: '/operate/activityConfigTemplate/firstDepositCheck',
		method: 'post',
		data
	})
}
// 活动模板：导入文件
export function displayPageCheck(data) {
	return request({
		url: '/operate/activityConfigTemplate/displayPageCheck',
		method: 'post',
		data
	})
}
// 活动模板：下载文件
export function firstDepositDownload(data) {
	return request({
		url: '/operate/activityConfigTemplate/firstDepositDownload',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 展示页：下载文件
export function displayPageDownload(data) {
	return request({
		url: '/operate/activityConfigTemplate/displayPageDownload',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 首存活动申请记录分页查询-导出
export function activityApplyRecordSCdownload(data) {
	return request({
		url: '/operate/activityStatisticsData/activityApplyRecordSC/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 纯展示页活动申请记录分页查询-导出
export function activityApplyRecordCZSdownload(data) {
	return request({
		url: '/operate/activityStatisticsData/activityApplyRecordCZS/download',
		method: 'post',
		responseType: 'blob',
		data
	})
}
// 纯展示页模板：app主图、分享图及列表图上传接口
export function displayPageUploadImage(data) {
	return request({
		url: '/operate/activityConfigTemplate/displayPageUploadImage',
		method: 'post',
		data
	})
}
// 首存活动模板：app主图、分享图及列表图上传接口
export function firstDepositUploadImage(data) {
	return request({
		url: '/operate/activityConfigTemplate/firstDepositUploadImage',
		method: 'post',
		data
	})
}
// 首存活动模板：app主图、分享图及列表图上传接口
export function memberVenue(data) {
	return request({
		url: '/memberVenue/info',
		method: 'post',
		data
	})
}
export default {
	activityConfigList,
	activityQuerySort,
	activityEdit,
	activitySaveSort,
	activityUpdateStatus,
	activityDetails,
	activityDelete,
	activitySelect,
	activityApplyRecordSCPage,
	activityApplyRecordCZSPage,
	saveDisplayPageConfig,
	saveFirstDepositConfig,
	activityQueryById,
	getActivityVisitDataStatistic,
	firstDepositCheck,
	displayPageCheck,
	firstDepositDownload,
	displayPageDownload,
	activityApplyRecordSCdownload,
	activityApplyRecordCZSdownload,
	displayPageUploadImage,
	firstDepositUploadImage,
	memberVenue
}
