import request from '@/utils/request'

export default {
	// 中控后台==> 首页 ===> 数据日报
	getDxReportDaily: (data) => {
		return request({
			url: '/report/daily/dxReportD',
			method: 'post',
			data
		})
	},
	// 中控后台==> 首页 ===> 数据对比/日活跃/平台盈亏
	getDataCompareDayList: (data) => {
		return request({
			url: '/report/venueNetAmountDay/dataCompareDayList',
			method: 'post',
			data
		})
	},
	// 中控后台==> 首页 ===> 有效投注额/场馆盈亏
	getQueryList: (data) => {
		return request({
			url: '/report/venueNetAmountDay/queryList',
			method: 'post',
			data
		})
	},
	// 中控后台==> 首页 ===> 今日盈亏TOP3
	getQueryMembersProfitAndLossTop: (data) => {
		return request({
			url: '/report/dataReport/queryMembersProfitAndLossTop',
			method: 'post',
			data
		})
	},
	// 中控后台==> 首页 ===> 今日代理TOP3
	getQueryAgencyTeamTop: (data) => {
		return request({
			url: '/report/dataReport/queryAgencyTeamTop',
			method: 'post',
			data
		})
	},
	// 中控后台==> layout右上角 ===> 我的下载
	downloadListPage: (data) => {
		return request({
			url: '/download/listPage',
			method: 'post',
			data
		})
	}
}

