import request from '@/utils/request'

export function getSelectWindControlLevel(data) {
	return request({
		url: '/windControllerLevel/selectWindControlLevel',
		method: 'post',
		data
	})
}

export function getSelectWindControlLevelId(data) {
	return request({
		url: '/windControlRecord/selectBeforeAfterWindControlLevelId',
		method: 'post',
		data
	})
}
export function selectWindControlRecord(data) {
	return request({
		url: '/windControlRecord/selectWindControlRecord',
		method: 'post',
		data
	})
}
export function getWindControllerLevelDict(params) {
	return request({
		url: '/windControllerLevel/dict',
		method: 'get',
		params
	})
}

// 风控==> 创建风控层级 ===> 列表
export function riskRankListAPI(data) {
	return request({
		url: '/windControllerLevel/windControllerLevelList',
		method: 'post',
		data
	})
}

// 风控==> 创建风控层级 ===> 新增
export function createRiskRankAPI(data) {
	return request({
		url: '/windControllerLevel/windControllerLevelSave',
		method: 'post',
		data
	})
}

// 风控==> 创建风控层级 ===> 修改
export function updateRiskRankAPI(data) {
	return request({
		url: '/windControllerLevel/windControllerLevelUpdate',
		method: 'post',
		data
	})
}

// 风控==> 创建风控层级 ===> 删除
export function deleteRiskRankAPI(data) {
	return request({
		url: '/windControllerLevel/windControllerLevelDelete',
		method: 'post',
		data
	})
}

// 风控==> 编辑风控层级 ===> 列表
export function riskDictAPI(params) {
	return request({
		url: '/windControllerLevel/dict',
		method: 'get',
		params
	})
}

// 风控==>风控对象管理 ===>列表
export function riskObjManageAPI(data) {
	return request({
		url: '/windControllerUser/windControllerList',
		method: 'post',
		data
	})
}

// 风控==>风控对象管理 ===>新增
export function riskObjManageAddAPI(data) {
	return request({
		url: '/windControllerUser/windControllerList/save',
		method: 'post',
		data
	})
}

// 风控==>风控对象管理 ===>删除
export function riskObjManageDelAPI(data) {
	return request({
		url: `/windControllerUser/windControllerList/delete/${data}`,
		method: 'post',
		data
	})
}

// 风控==>风控对象管理 ===>编辑
export function riskObjManageEditAPI(data) {
	return request({
		url: '/windControllerUser/windControllerList/edit',
		method: 'post',
		data
	})
}

// 风控==>风控对象管理 ===>新增查询会员
export function searchMemberAPI(data) {
	return request({
		url: '/windControllerUser/checkUserName',
		method: 'post',
		data
	})
}

// 风控==> 编辑风控层级 ===> 信息列表
export function riskEditInfoAPI(data) {
	return request({
		url: '/windControllerLevel/windControllerLevelInfo',
		method: 'post',
		data
	})
}

// 风控==> 编辑风控层级 ===> 新增
export function riskEditAddAPI(data) {
	return request({
		url: '/windControllerLevel/windControlRecordSave',
		method: 'post',
		data
	})
}

export function getMemberFundsRecordsAccountChangeDic(params) {
	return request({
		url: '/memberFundsRecords/accountChangeDic',
		method: 'get',
		params
	})
}
export function getProxyFundsRecordsAccountBizDic(params) {
	return request({
		url: '/proxyFundsRecords/accountBizDic',
		method: 'get',
		params
	})
}
export function getProxyFundsRecordsAccountChangeDic(params) {
	return request({
		url: '/proxyFundsRecords/accountChangeDic',
		method: 'get',
		params
	})
}
// 风控==> 坐庄风控 ===> 操作记录
export function getBankerRiskControlLogQuery4Page(data) {
	return request({
		url: '/bankerRiskControlLog/query4Page',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 总代风控设置
export function getProxyWindControlQueryList(data) {
	return request({
		url: '/proxy/wind/control/queryList',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 总代风控设置批量编辑
export function getProxWindControlModify(data) {
	return request({
		url: '/proxy/wind/control/modify',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 赛事风控设置
export function getProxyMatchPageList(data) {
	return request({
		url: '/proxy/match/pageList',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 赛事风控设置编辑
export function getProxyMatchUpdateProxyMatch(data) {
	return request({
		url: '/proxy/match/updateProxyMatch',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 赛事风控设置批量编辑
export function getProxyMatchBatchUpdateProxyMatch(data) {
	return request({
		url: '/proxy/match/batchUpdateProxyMatch',
		method: 'post',
		data
	})
}
// 风控==> 坐庄风控 ===> 赛事风控设置小红点
export function getProxyMatchProxyMatchTagCount(data) {
	return request({
		url: '/proxy/match/proxyMatchTagCount',
		method: 'post',
		data
	})
}

export default {
	getSelectWindControlLevel,
	getSelectWindControlLevelId,
	selectWindControlRecord,
	getWindControllerLevelDict,
	riskRankListAPI,
	createRiskRankAPI,
	updateRiskRankAPI,
	deleteRiskRankAPI,
	riskDictAPI,
	riskEditInfoAPI,
	riskEditAddAPI,
	riskObjManageAPI,
	riskObjManageAddAPI,
	riskObjManageDelAPI,
	riskObjManageEditAPI,
	searchMemberAPI,
	getMemberFundsRecordsAccountChangeDic,
	getProxyFundsRecordsAccountBizDic,
	getProxyFundsRecordsAccountChangeDic,
	getBankerRiskControlLogQuery4Page,
	getProxyWindControlQueryList,
	getProxWindControlModify,
	getProxyMatchPageList,
	getProxyMatchUpdateProxyMatch,
	getProxyMatchBatchUpdateProxyMatch,
	getProxyMatchProxyMatchTagCount
}
