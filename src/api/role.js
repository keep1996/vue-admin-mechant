import request from '@/utils/request'

export function getUserPermissions(params) {
	return request({
		url: '/system/rolePermission/permissions',
		method: 'GET',
		params
	})
}
