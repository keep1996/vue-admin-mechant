import request from '@/utils/request'

export function blackList(params) {
	return request({
		url: '/bankBlack/list',
		method: 'get',
		params
	})
}
export function memberLoginLog(data) {
	return request({
		url: 'log/memberLoginLog',
		method: 'post',
		data
	})
}
export function memberLoginLogDetails(data) {
	return request({
		url: 'log/memberLoginLog/details',
		method: 'post',
		data
	})
}
export function recordInfo(data) {
	return request({
		url: '/updateMemberAudit/auditDetail',
		method: 'post',
		data
	})
}
export function audit(data) {
	return request({
		url: '/updateMemberAudit/audit',
		method: 'post',
		data
	})
}
export function memberAuditDetail(data) {
	return request({
		url: '/memberAudit/queryAuditDetail',
		method: 'post',
		data
	})
}
export function updateMemberAuditRecord(data) {
	return request({
		url: '/memberAudit/audit',
		method: 'post',
		data
	})
}
export function memberChange(data) {
	return request({
		url: '/updateMemberAudit/queryListPage',
		method: 'post',
		data
	})
}
export function playerAuditList(data) {
	return request({
		url: 'memberAudit/playerAuditList',
		method: 'post',
		data
	})
}
// 会员转代
export function memberTransferAudit(data) {
	return request({
		url: '/memberTransfer/audit',
		method: 'post',
		data
	})
}
export function memberTransferDetail(data) {
	return request({
		url: '/memberTransfer/detail',
		method: 'post',
		data
	})
}
export function memberTransferLockOrder(data) {
	return request({
		url: '/memberTransfer/lockOrder',
		method: 'post',
		data
	})
}
export function memberTransferSelectPage(data) {
	return request({
		url: '/memberTransfer/selectPage',
		method: 'post',
		data
	})
}
// 新增代理审核
export function lockProxyAuditRecord(data) {
	return request({
		url: '/newProxy/lock',
		method: 'post',
		data
	})
}
export function proxyDetail(data) {
	return request({
		url: '/proxy/proxyDetail',
		method: 'post',
		data
	})
}
export function updateProxyAuditRecord(data) {
	return request({
		url: '/newProxy/audit',
		method: 'post',
		data
	})
}
export function proxyCommissionRecordAudit(data) {
	return request({
		url: '/proxyCommissionRecord/audit',
		method: 'post',
		data
	})
}
export function proxyList(data) {
	return request({
		url: '/newProxy/auditPageList',
		method: 'post',
		data
	})
}
// 代理审核
export function proxyDataAudit(data) {
	return request({
		url: '/updateProxyAudit/audit',
		method: 'post',
		data
	})
}

export function proxyDataLock(data) {
	return request({
		url: '/updateProxyAudit/updateMemberAuditLock',
		method: 'post',
		data
	})
}
export function proxyDataRecordInfo(data) {
	return request({
		url: '/updateProxyAudit/auditDetail',
		method: 'post',
		data
	})
}

// 查询代理账户修改审核列表
export function proxyDataPage(data) {
	return request({
		url: '/updateProxyAudit/queryListPage',
		method: 'post',
		data
	})
}
export function lock(data) {
	return request({
		url: '/updateMemberAudit/updateMemberAuditLock',
		method: 'post',
		data
	})
}
export function lockMemberAuditRecord(data) {
	return request({
		url: '/memberAudit/memberAuditLock',
		method: 'post',
		data
	})
}
export function editBlackList(data) {
	return request({
		url: '/bankBlack/edit',
		method: 'post',
		data
	})
}
export function delBlackList(data) {
	return request({
		url: '/bankBlack/del',
		method: 'post',
		data
	})
}
export function addBlackList(data) {
	return request({
		url: '/bankBlack/add',
		method: 'post',
		data
	})
}
export function memberInComComback(data) {
	return request({
		url: '/memberInCom/queryLast',
		method: 'post',
		data
	})
}
export function memberInComQuery(data) {
	return request({
		url: '/memberInCom/query',
		method: 'post',
		data
	})
}
export function memberVipGradeSelectCurrency(data) {
	return request({
		url: '/memberVipGrade/selectCurrency',
		method: 'post',
		data
	})
}
export function memberInComSave(data) {
	return request({
		url: '/memberInCom/save',
		method: 'post',
		data
	})
}
export function bankManageQuery(data) {
	return request({
		url: '/bankManage/query',
		method: 'post',
		data
	})
}

// 银行管理-新增
export function setBankManageAdd(data) {
	return request({
		url: '/bankManage/add',
		method: 'post',
		data
	})
}
// 银行管理-修改
export function setBankManageUpdate(data) {
	return request({
		url: '/bankManage/update',
		method: 'post',
		data
	})
}
// 银行管理-删除
export function setBankManageelete(data) {
	return request({
		url: '/bankManage/delete',
		method: 'post',
		data
	})
}
// 银行管理-状态
export function setBankManageUpdateBankStatus(data) {
	return request({
		url: '/bankManage/updateBankStatus',
		method: 'post',
		data
	})
}
// 验证谷歌key结果
export function validateGoogleKeyApi(data) {
	return request({
		url: '/proxyCommissionAudit/add/proxyActiveNumber',
		method: 'post',
		data
	})
}
export default {
	memberInComComback,
	memberInComQuery,
	memberVipGradeSelectCurrency,
	memberInComSave,
	memberTransferAudit,
	memberTransferDetail,
	memberTransferLockOrder,
	memberTransferSelectPage,
	updateProxyAuditRecord,
	proxyCommissionRecordAudit,
	lockProxyAuditRecord,
	proxyDetail,
	proxyList,
	recordInfo,
	memberChange,
	proxyDataAudit,
	proxyDataLock,
	proxyDataRecordInfo,
	memberAuditDetail,
	proxyDataPage,
	audit,
	lockMemberAuditRecord,
	updateMemberAuditRecord,
	lock,
	playerAuditList,
	blackList,
	memberLoginLog,
	memberLoginLogDetails,
	editBlackList,
	delBlackList,
	addBlackList,
	bankManageQuery,
	setBankManageAdd,
	setBankManageUpdate,
	setBankManageelete,
	setBankManageUpdateBankStatus,
	validateGoogleKeyApi
}
