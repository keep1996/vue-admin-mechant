import request from '@/utils/request'

export function proxyDataResetProxyCheckType(data) {
	return request({
		url: '/proxyData/resetProxyCheckType',
		method: 'post',
		data
	})
}

export function proxyDataPageSubAccountPerm(data) {
	return request({
		url: '/proxyDetail/pageSubAccountPerm',
		method: 'post',
		data
	})
}

export function proxyDataSubAccountPermChange(data) {
	return request({
		url: '/proxyDetail/subAccountPermChange',
		method: 'post',
		data
	})
}


// 代理==> 代理管理 ===> 代理列表
export function AgentListAPI(data) {
	return request({
		url: '/proxyList/proxyRegisteredList',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理列表-树形结构/代理链路
export function AgentLevelLinkAPI(data) {
	return request({
		url: '/proxyList/levelLink',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理列表-树形结构
export function AgentLevelListAPI(data) {
	return request({
		url: '/proxyList/generalProxyList',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理列表-树形结构-搜索下级
export function AgentLevelLinkByUserNameAPI(data) {
	return request({
		url: '/proxyList/levelLinkByUserName',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理列表导出
export function agentListExportAPI(data) {
	return request({
		url: '/proxyList/download',
		method: 'post',
		data,
		responseType: 'blob'
	})
}

// 代理管理==> 代理列表 ===> 风控层级
export function agentDictAPI(params) {
	return request({
		url: '/proxy/labelDict',
		method: 'get',
		params
	})
}

// 代理==> 代理管理 ===> 新增总代理
export function addSuperAgentAPI(data) {
	return request({
		url: '/newProxy/addGeneralProxy',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 新增非总代
export function addAgentAPI(data) {
	return request({
		url: '/newProxy/addUnGeneralProxy',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 会员转代
export function memberTransAgentAPI(data) {
	return request({
		url: '/memberTransfer/apply',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理注册信息
export function agentRegisterInfoAPI(data) {
	return request({
		url: '/proxyInformationRegistered/proxyList',
		method: 'post',
		data
	})
}

// 溢出审核
export function listOverflowMember(data) {
	return request({
		url: '/memberOverflow/listOverflowMember',
		method: 'post',
		data
	})
}

export function lockMemberAuditRecordProxy(data) {
	return request({
		url: '/memberOverflow/lockMemberAuditRecord',
		method: 'post',
		data
	})
}

export function memberOverflowDetail(data) {
	return request({
		url: '/memberOverflow/memberOverflowDetail',
		method: 'post',
		data
	})
}

export function updateMemberOverflowRecord(data) {
	return request({
		url: '/memberOverflow/updateMemberOverflowRecord',
		method: 'post',
		data
	})
}

export function overflowMemberInfo(data) {
	return request({
		url: '/memberOverflow/overflowMemberInfo',
		method: 'post',
		data
	})
}

export function addOverflowMember(data) {
	return request({
		url: '/memberOverflow/addOverflowMember',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理银行卡记录
export function agentBankRecordAPI(data) {
	return request({
		url: '/bankProxyRecord/bankRecord',
		method: 'post',
		data
	})
}

// 代理==> 代理管理 ===> 代理虚拟币记录
export function agentVirtualRecordAPI(data) {
	return request({
		url: '/bankProxyRecord/virtualRecord',
		method: 'post',
		data
	})
}

// 代理==>  ===> 代理详情查询
export function getProxyDetailQueryDetail(data) {
	return request({
		url: '/proxyDetail/queryDetail',
		method: 'post',
		data
	})
}

export function getValueAddedDetail(data) {
	return request({
		url: '/report/order-proxy/detail',
		method: 'post',
		data
	})
}

//  代理==>代理详情==>佣金契约/返点契约
export function getProxyProxyDetailContract(data) {
	return request({
		url: '/proxyDetail/contract',
		method: 'post',
		data
	})
}

// 代理详情-基本信息-信息编辑
export function setProxyDataInfoEdit(data) {
	return request({
		url: '/proxyData/infoEdit',
		method: 'post',
		data
	})
}
// 代理详情-私庄信息-私庄概览
export function getProxyDetailBankerInfo(data) {
	return request({
		url: '/proxyDetail/proxyBankerInfo',
		method: 'post',
		data
	})
}
// 代理详情-私庄信息-资金使用率
export function getProxyProxyBillDetailInfo(data) {
	return request({
		url: '/proxyDetail/proxyBillDetailInfo',
		method: 'post',
		data
	})
}

// 代理详情-设置代理备注信息
export function setProxyDetailRemark(data) {
	return request({
		url: '/proxyRemark/add',
		method: 'post',
		data
	})
}
// 代理详情-代理备注信息分页
export function getProxyDetailRemark(params) {
	return request({
		url: '/proxyRemark/list',
		method: 'get',
		params
	})
}

// 代理详情-财务信息
export function getProxyDetailFinanceInfo(data) {
	return request({
		url: '/proxyDetail/financeInfo',
		method: 'post',
		data
	})
}
// 代理详情-私庄锁定金额
export function getProxyDetailBankerLockDetail(data) {
	return request({
		url: '/proxyDetail/proxyBankerLockDetail',
		method: 'post',
		data
	})
}

// 代理详情-余额查询接口
export function getProxyDataBalance(data) {
	return request({
		url: '/proxyDetail/balance',
		method: 'post',
		data
	})
}

// 代理详情-佣金信息
export function getProxyDataCommission(data) {
	return request({
		url: '/proxyDetail/commission',
		method: 'post',
		data
	})
}

// 代理详情-存提信息
export function getRechargeAndWithdrawInfo(data) {
	return request({
		url: '/proxyDetail/rechargeAndWithdrawInfo',
		method: 'post',
		data
	})
}

// 代理详情-代存信息
export function getProxyDetailProxyRechargeInfo(data) {
	return request({
		url: '/proxyDetail/ProxyRechargeInfo',
		method: 'post',
		data
	})
}

// 代理详情-团队成员概览
export function getProxyDetailTeamInfo(data) {
	return request({
		url: '/proxyDetail/teamInfo',
		method: 'post',
		data
	})
}

// 代理详情-团队成员投注信息
export function getProxyDetailTeamBet(data) {
	return request({
		url: '/proxyDetail/teamBet',
		method: 'post',
		data
	})
}

// 代理详情-top3投注，输赢
export function getProxyDetailTop3Bet(data) {
	return request({
		url: '/proxyDetail/top3Bet',
		method: 'post',
		data
	})
}

// 代理详情-登录日志，登录信息
export function getProxyDetailProxyLoginLog(data) {
	return request({
		url: '/proxyLog/proxyLoginLog',
		method: 'post',
		data
	})
}

// 助理账号列表
export function getProxySubAccount4Page(data) {
	return request({
		url: '/proxy/subAccount4Page',
		method: 'post',
		data
	})
}

// 助理角色列表
export function getProxySubRole4Page(data) {
	return request({
		url: '/proxy/subRole4Page',
		method: 'post',
		data
	})
}

// 代理详情-代理信息变更记录 分页查询
export function getProxyDataInfoChangeRecord(data) {
	return request({
		url: '/proxyData/infoChangeRecord',
		method: 'post',
		data
	})
}

// 代理==> 代理推广 ===> 代理图片列表
export function agentPictureListAPI(data) {
	return request({
		url: '/materialImage/select',
		method: 'post',
		data
	})
}

// 代理==> 代理推广 ===> 代理图片删除
export function agentPictureListDeleteAPI(data) {
	return request({
		url: '/materialImage/delete',
		method: 'post',
		data
	})
}

// 代理==> 代理推广 ===> 代理图片新增
export function agentPictureListCreateAPI(data) {
	return request({
		url: '/materialImage/insert',
		method: 'post',
		data
	})
}

// 代理==> 代理推广 ===> 代理图片修改
export function agentPictureListUpdateAPI(data) {
	return request({
		url: '/materialImage/update',
		method: 'post',
		data
	})
}

export function agentImageUploadAPI(data, cb) {
	return request({
		url: '/uploadFile/image',
		method: 'post',
		data,
		cb
	})
}

//  推广管理-推广域名管理-删除推广域名管理
export function setDomainDelete(data) {
	return request({
		url: '/promoteDomain/delete',
		method: 'post',
		data
	})
}

//  推广管理-推广域名管理-新增推广域名管理
export function addDomainInsert(data) {
	return request({
		url: '/promoteDomain/insert',
		method: 'post',
		data
	})
}

//  推广管理-推广域名管理-分页查询推广域名管理
export function getDomainSelect(data) {
	return request({
		url: '/promoteDomain/select',
		method: 'post',
		data
	})
}

//  推广管理-推广域名管理-修改推广域名管理
export function setDomainUpdate(data) {
	return request({
		url: '/promoteDomain/update',
		method: 'post',
		data
	})
}

//  等级福利配置-分页查询代理等级管理
export function getProxyGradesSelect(data) {
	return request({
		url: '/proxyGrade/select',
		method: 'post',
		data
	})
}

//  等级福利配置-删除代理等级管理
export function setProxyGradeDelete(data) {
	return request({
		url: '/proxyGrade/delete',
		method: 'post',
		data
	})
}

//  等级福利配置-新增代理等级管理
export function setProxyGradeInsert(data) {
	return request({
		url: '/proxyGrade/insert',
		method: 'post',
		data
	})
}

//  等级福利配置-修改代理等级管理
export function setProxyGradeUpdate(data) {
	return request({
		url: '/proxyGrade/update',
		method: 'post',
		data
	})
}

//  等级福利配置-分页查询代理返佣等级管理
export function getProxyCommissionSelect(data) {
	return request({
		url: '/proxyCommission/select',
		method: 'post',
		data
	})
}

//  等级福利配置-删除代理返佣等级管理
export function setProxyCommissionDelete(data) {
	return request({
		url: '/proxyCommission/delete',
		method: 'post',
		data
	})
}

//  等级福利配置-新增代理返佣等级管理
export function setProxyCommissionInsert(data) {
	return request({
		url: '/proxyCommission/insert',
		method: 'post',
		data
	})
}

//  等级福利配置-修改代理返佣等级管理
export function setProxyCommissionUpdate(data) {
	return request({
		url: '/proxyCommission/update',
		method: 'post',
		data
	})
}

//  代理标签变更记录
export function getProxyGetLabelChangeRecordPage(data) {
	return request({
		url: '/proxy/getLabelChangeRecordPage',
		method: 'post',
		data
	})
}

//  代理标签
export function getProxyPageLabel(data) {
	return request({
		url: '/proxy/pageLabel',
		method: 'post',
		data
	})
}

//  代理标签==>新增
export function getProxyAddLabel(data) {
	return request({
		url: '/proxy/addLabel',
		method: 'post',
		data
	})
}

//  代理标签==>删除
export function setProxyDeleteLabel(params) {
	return request({
		url: '/proxy/deleteLabel',
		method: 'get',
		params
	})
}

//  代理标签==>标签人数
export function getProxyProxyInfoByLabelId(data) {
	return request({
		url: '/proxy/proxyInfoByLabelId',
		method: 'post',
		data
	})
}

//  代理标签==>代理详情下拉框
export function getProxyLabelDict(data) {
	return request({
		url: '/userLabel/labelDict',
		method: 'post',
		data
	})
}

//  代理标签==>分页查询代理操作记录
export function getProxyOperateSelect(data) {
	return request({
		url: '/proxyOperate/select',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>契约条件配置
export function getProxyMinContractminContractList(data) {
	return request({
		url: '/proxy/MinContract/minContractList',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>新增契约条件配置
export function getProxyMinContractadd(data) {
	return request({
		url: '/proxy/MinContract/add',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>编辑契约条件配置
export function getProxyMinContractupdate(data) {
	return request({
		url: '/proxy/MinContract/update',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>删除契约条件配置
export function getProxyMinContractdelete(data) {
	return request({
		url: '/proxy/MinContract/delete',
		method: 'post',
		data
	})
}

//  代理==>契约管理变更记录==>查看
export function getProxycontractscaleList(data) {
	return request({
		url: '/proxy/contract/scaleList',
		method: 'post',
		data
	})
}

//  代理==>契约最低条件配置变更记录==>查询变更前详情
export function getProxyMinContractchangeHistoryDetails(data) {
	return request({
		url: '/proxy/MinContract/changeHistoryDetails',
		method: 'post',
		data
	})
}

//  代理==>契约最低条件配置变更记录==>查询变更后详情
export function getProxyMinContractchangeDetailsList(data) {
	return request({
		url: '/proxy/MinContract/changeDetailsList',
		method: 'post',
		data
	})
}

//  代理==>契约变更记录查询
export function getProxyContractContractRecordList(data) {
	return request({
		url: '/proxy/Contract/contractRecordList',
		method: 'post',
		data
	})
}

//  代理==>契约变更记录查询==》代理层级下拉框
export function getProxylevelnameList(data) {
	return request({
		url: '/proxy/level/nameList',
		method: 'post',
		data
	})
}

//  代理==>层级配置
export function getProxylevelproxyList(data) {
	return request({
		url: '/proxy/level/proxyList',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>新增代理层级
export function getProxyleveladd(data) {
	return request({
		url: '/proxy/level/add',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>最低条件变更记录
export function getProxyMinContractminContractRecordList(data) {
	return request({
		url: '/proxy/MinContract/minContractRecordList',
		method: 'post',
		data
	})
}

// 契约管理==>分页查询
export function getProxyContractList(data) {
	return request({
		url: '/proxy/Contract/proxyContractList',
		method: 'post',
		data
	})
}

// 契约管理==>代签
export function acceptContractAPI(data) {
	return request({
		url: '/proxy/Contract/acceptContract',
		method: 'post',
		data
	})
}

// 契约管理==>契约创建、重订
export function contractAddOrUpdateAPI(data) {
	return request({
		url: '/proxy/Contract/add',
		method: 'post',
		data
	})
}

// 契约管理==>代理校验、根据代理获取所属所有商户信息
export function checkProxyDetailAPI(data) {
	return request({
		url: '/proxyDetail/checkProxy',
		method: 'post',
		data
	})
}

// 契约管理==>代理校验、获取上级代理信息
export function contractCheckProxyAPI(data) {
	return request({
		url: '/proxy/Contract/checkProxy',
		method: 'post',
		data
	})
}

// 契约管理==>查询契约最低条件比例，用于获取所有比例的最低条件
// 契约模板共用
export function queryRebateRatioAPI(data) {
	return request({
		url: '/proxy/Contract/queryRebateRatio',
		method: 'post',
		data
	})
}

// 契约模板配置==>当前模板或对应的契约id所拥有的比例条件
// 契约管理相同功能共用此接口
export function getContractScaleListAPI(data) {
	return request({
		url: '/proxy/contract/scaleList',
		method: 'post',
		data
	})
}

// 契约模板配置==>保存契约模板
// 契约管理中的保存模板共用
export function saveContractTemplateAPI(data) {
	return request({
		url: '/proxy/contract/saveContractTemplate',
		method: 'post',
		data
	})
}

// 契约模板配置==>新增契约模板
export function addContractTemplateAPI(data) {
	return request({
		url: '/proxy/contract/add',
		method: 'post',
		data
	})
}

// 契约模板配置==>契约模板编辑
export function editContractTemplateAPI(data) {
	return request({
		url: '/proxy/contract/update',
		method: 'post',
		data
	})
}

// 契约模板配置==>契约模板分页查询
// 契约管理，导入模板的数据共用此接口
export function getContractTemplateListAPI(data) {
	return request({
		url: '/proxy/contract/contractTemplateList',
		method: 'post',
		data
	})
}

// 契约模板配置==>契约模板删除
export function delContractTemplateApi(data) {
	return request({
		url: '/proxy/contract/delete',
		method: 'post',
		data
	})
}

// 契约模板配置==>设置比例
export function setContractTemplateScale(data) {
	return request({
		url: '/proxy/contract/setScale',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>编辑代理层级
export function getProxylevelupdate(data) {
	return request({
		url: '/proxy/level/update',
		method: 'post',
		data
	})
}

//  代理==>代理人工增加额度下拉框
export function getProxyArtificialPatchAccountAddAuditgetAccountAdjustType(
	data
) {
	return request({
		url: 'proxyArtificialPatchAccountAddAudit/getAccountAdjustType',
		method: 'post',
		data
	})
}

//  代理==>代理人工减额度下拉框
export function getArtificialPatchgetProxySubAdjustType(data) {
	return request({
		url: 'artificialPatch/getProxySubAdjustType',
		method: 'post',
		data
	})
}

//  代理==>代理信用配置修改
export function getCreditLevelConfupdateCredit(data) {
	return request({
		url: '/CreditLevelConf/updateCredit',
		method: 'post',
		data
	})
}

//  会员&代理==>信用等级配置  会员  1   代理  2
export function getCreditLevelConfcreditList(data) {
	return request({
		url: '/CreditLevelConf/creditList',
		method: 'post',
		data
	})
}

//  会员&代理==>信用等级配置修改  会员  1   代理  2
export function getCreditLevelConfUpdateCredit(data) {
	return request({
		url: '/CreditLevelConf/updateCredit',
		method: 'post',
		data
	})
}

//  会员&代理==>充值设置查询  会员  0   代理  1
export function getCreditLevelConfQuery(data) {
	return request({
		url: '/depositSetting/query',
		method: 'post',
		data
	})
}

//  会员&代理==>修改充值设置  会员  0   代理  1
export function getCreditLevelConfUpdate(data) {
	return request({
		url: '/depositSetting/update',
		method: 'post',
		data
	})
}

//  会员&代理==>修改充值设置的状态  会员  0   代理  1
export function getCreditLevelConfUpdateStatus(data) {
	return request({
		url: '/depositSetting/updateStatus',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  域名配置删除
export function setExclusiveDomainDeleteDomain(data) {
	return request({
		url: '/exclusiveDomain/deleteDomain',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>新增政策类型
export function getproxyMinContractaddPolicyType(data) {
	return request({
		url: '/proxy/MinContract/addPolicyType',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  域名配置分页查询
export function getExclusiveDomainDomainList(data) {
	return request({
		url: '/exclusiveDomain/domainList',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>新增政策类型
export function getproxyMinContractupdatePolicyType(data) {
	return request({
		url: '/proxy/MinContract/updatePolicyType',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  下载模板
export function getExclusiveDomainDownLoad(data) {
	return request({
		url: '/exclusiveDomain/downLoad',
		responseType: 'blob',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  导入
export function getExclusiveDomainImportExcel(data) {
	return request({
		url: '/exclusiveDomain/importExcel',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  域名配置新增、编辑
export function setExclusiveDomainSaveDomain(data) {
	return request({
		url: '/exclusiveDomain/saveDomain',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  上传文件
export function setExclusiveDomainUpLoad(data) {
	return request({
		url: '/exclusiveDomain/upLoad',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  域名解绑
export function setExclusiveDomainUpdateBind(data) {
	return request({
		url: '/exclusiveDomain/updateBind',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  启用、禁用
export function setExclusiveDomainUse(data) {
	return request({
		url: '/exclusiveDomain/use',
		method: 'post',
		data
	})
}

//  专属域名配置  =>  启用、禁用
export function getdomainLogquery(data) {
	return request({
		url: '/domainLog/query',
		method: 'post',
		data
	})
}

//  域名类型下拉框
export function getdomainLogloadOperateList(data) {
	return request({
		url: '/domainLog/loadOperateList',
		method: 'post',
		data
	})
}

//  代理==>层级配置==>政策删除
export function getproxyMinContractdeletePolicyType(data) {
	return request({
		url: '/proxy/MinContract/deletePolicyType',
		method: 'post',
		data
	})
}

// 代理注单设置记录 => 代理注单设置记录分页查询
export function proxyConfigPageList(data) {
	return request({
		data,
		url: '/proxy/config/pageList',
		method: 'post'
	})
}

// 代理注单设置记录 => 代理注单设置记录会员名单详情查询
export function proxyConfigMemberDetailsList(data) {
	return request({
		url: '/proxy/config/memberDetailsList',
		method: 'post',
		data
	})
}

// 代理停盘设置记录列表
export function stopSettingStopSettingList(data) {
	return request({
		data,
		url: '/stop/setting/stopSettingList',
		method: 'post'
	})
}

// 坐庄政策配置==>坐庄政策配置明细
export function profitPolicyConfigDetailApi(data) {
	return request({
		data,
		url: '/profit/sharing/changeDetailedList',
		method: 'post'
	})
}
// 坐庄政策配置记录==>坐庄政策配置记录查询
export function profitSharingRecordListApi(data) {
	return request({
		data,
		url: '/profit/sharing/profitSharingRecordList',
		method: 'post'
	})
}
// 坐庄政策配置记录==>查询变更详情
export function profitPolicychangeDetailApi(data) {
	return request({
		data,
		url: '/profit/sharing/changeDetailsList',
		method: 'post'
	})
}

// 代理返点比例查询
export function rebateRateListApi(data) {
	return request({
		data,
		url: '/rebateRate/list',
		method: 'post'
	})
}
// 代理返点比例查询
export function memberRebateRatelistApi(data) {
	return request({
		data,
		url: '/rebateRate/memberRebateRatelist ',
		method: 'post'
	})
}
// 返点比例查询
export function rebateRateQueryApi(data) {
	return request({
		data,
		url: '/rebateRate/list',
		method: 'post'
	})
}

// 会员返点比例列表查询
export function rebateRateMemberList(data) {
	return request({
		data,
		url: '/rebateRate/memberList',
		method: 'post'
	})
}

// 更新返点比例
export function rebateRateUdate(data) {
	return request({
		data,
		url: '/rebateRate/update',
		method: 'post'
	})
}

// 更新返点比例
export function rebateRateQueryLimit(data) {
	return request({
		data,
		url: '/rebateRate/queryLimit',
		method: 'post'
	})
}

// 查询返点比例
export function rebateRateQuery(data) {
	return request({
		data,
		url: '/rebateRate/query',
		method: 'post'
	})
}

export function rebateQuerySubMax(data) {
	return request({
		data,
		url: '/rebateRate/querySubMax',
		method: 'post'
	})
}

// 查询返佣
export function commissionRateQuery(data) {
	return request({
		data,
		url: '/commissionRate/query',
		method: 'post'
	})
}

export function commissionRateList(data) {
	return request({
		data,
		url: '/commissionRate/list',
		method: 'post'
	})
}

export function commissionRateUpdate(data) {
	return request({
		data,
		url: '/commissionRate/update',
		method: 'post'
	})
}

export function getProxyCommissionSettleCycle(data) {
	return request({
		data,
		url: '/proxy/getProxyCommissionSettleCycle',
		method: 'post'
	})
}

export function updateProxyCommissionSettleCycle(data) {
	return request({
		data,
		url: '/proxy/updateProxyCommissionSettleCycle',
		method: 'post'
	})
}

export function proxyDataInfoEdit(data) {
	return request({
		data,
		url: 'proxyData/infoEdit',
		method: 'post'
	})
}

export function commissionRateQueryLimit(data) {
	return request({
		data,
		url: '/commissionRate/queryLimit',
		method: 'get'
	})
}

// 返点比例模板关联查询
export function rebateRateTemplateMappingQuery(data) {
	return request({
		data,
		url: '/RebateRateTemplateMapping/query'
	})
}
// 返点比例平台调整试算
export function platformAdjustmentTryCacl(data) {
	return request({
		data,
		url: '/rebateRate/platformAdjustmentTryCacl',
		method: 'post'
	})
}

// 返点比例平台调整试算
export function commissionRatePlatformAdjustmentTryCacl(data) {
	return request({
		data,
		url: '/commissionRate/platformAdjustmentTryCacl',
		method: 'post'
	})
}

// 新增返点比例模板
export function rebateRateTemplateAdd(data) {
	return request({
		data,
		url: '/rebateRateTemplate/add',
		method: 'post'
	})
}

// 返点比例模板查询
export function rebateRateTemplateDetail(data) {
	return request({
		data,
		url: '/rebateRateTemplate/detail',
		method: 'post'
	})
}

// 返点比例模板明细查询
export function getRebateRateTemplateDetail(data) {
	return request({
		data,
		url: '/rebateRateTemplate/getRebateRateTemplateDetail',
		method: 'post'
	})
}

// 返点比例模板分页查询
export function rebateRateTemplateList(data) {
	return request({
		data,
		url: '/rebateRateTemplate/list',
		method: 'post'
	})
}

// 返点比例模板查询
export function rebateRateTemplateQuery(data) {
	return request({
		data,
		url: '/rebateRateTemplate/query',
		method: 'post'
	})
}

// 更新返点比例模板
export function rebateRateTemplateUpdate(data) {
	return request({
		data,
		url: '/rebateRateTemplate/update',
		method: 'post'
	})
}

// 删除返点比例模板
export function rebateRateTemplateDelete(data) {
	return request({
		data,
		url: '/rebateRateTemplate/delete',
		method: 'post'
	})
}

// 返点比例模板 设置比例
export function rebateRateTemplateDetailAdd(data) {
	return request({
		data,
		url: '/rebateRateTemplateDetail/add',
		method: 'post'
	})
}

// 返点比例模板 查看比例
export function rebateRateTemplateDetailQuery(data) {
	return request({
		data,
		url: '/rebateRateTemplateDetail/query',
		method: 'post'
	})
}

// 返点比例模板 查看关联总代
export function rebateRateTemplateMappingTopProxyList(data) {
	return request({
		data,
		url: '/proxyList/rebateRateTemplateMappingTopProxyList',
		method: 'post'
	})
}

export function rebateRateTemplateQueryList(data) {
	return request({
		data,
		method: 'post',
		url: '/rebateRateTemplate/list'
	})
}

export function updateRebateAllotMode(data) {
	return request({
		data,
		method: 'post',
		url: '/rebateRate/updateRebateAllotMode'
	})
}

// 上月结余查询接口
export function queryLastMonthBalance(data) {
	return request({
		data,
		url: '/proxyDetail/lastMonthBalance',
		method: 'post'
	})
}

// 清除上月结余接口
export function clearedLastMonthBalance(data) {
	return request({
		data,
		url: '/proxyDetail/clearedLastMonthBalance',
		method: 'post'
	})
}

// 获取变更记录下拉数据
export function getProxyChangeTypeList() {
	return request({
		url: '/proxyList/changeType',
		method: 'post'
	})
}

// 获取会员信息变更记录
export function getProxyInfoChangeList(data) {
	return request({
		url: '/operatelog/queryProxyChangeList',
		method: 'post',
		data
	})
}

// 代理详情 - 返点比例查询
export function rebateRateQueryProxy(data) {
	return request({
		data,
		url: '/rebateRate/effectives',
		method: 'post'
	})
}

export function getTopProxyBillList(data) {
	return request({
		url: '/report/topProxyBill/list',
		method: 'post',
		data
	})
}

export function exportTopProxyBillList(data) {
	return request({
		url: '/report/topProxyBill/export',
		method: 'post',
		data,
		responseType: 'blob'
	})
}

export function getBillPeriodQueryBillPeriod(data) {
	return request({
		url: '/report/billPeriod/queryBillPeriod',
		method: 'post',
		data
	})
}
// 停用总代线，一键回收信用额度和可用额度
export function recoveringAvailableCreditLimitApi(data) {
	return request({
		data,
		url: '/proxyData/infoEdit',
		method: 'post'
	})
}

// 增值服务需求新增查询返点
export function getRebateRateQueryProxy(data) {
	return request({
		url: '/rebateRate/queryProxy',
		method: 'post',
		data
	})
}

// 增值服务需求新增查询返点
export function getRebateRateQueryMember(data) {
	return request({
		url: '/rebateRate/queryMember',
		method: 'post',
		data
	})
}

// 增值服务需求新增查询返点
export function getRebateRateQuery(data) {
	return request({
		url: '/rebateRate/query',
		method: 'post',
		data
	})
}

// 开通代理审核记录查询
export function getOpenProxyAuditAuditList(data) {
	return request({
		url: '/openProxyAudit/auditList',
		method: 'post',
		data
	})
}

// 开通代理审核详情
export function getOpenProxyDetail(data) {
	return request({
		url: '/openProxyAudit/queryAuditDetail',
		method: 'post',
		data
	})
}

// 开通代理审核 - 锁单操作
export function openProxyAuditLock(data) {
	return request({
		url: '/openProxyAudit/auditLock',
		method: 'post',
		data
	})
}

// 开通代理审核 - 锁单操作
export function openProxyAudit(data) {
	return request({
		url: '/openProxyAudit/audit',
		method: 'post',
		data
	})
}

export function openProxyAddOpenProxyAudit(data) {
	return request({
		url: '/openProxyAudit/addOpenProxyAudit',
		method: 'post',
		data
	})
}

// 获取操作栏目操作页面下拉值
export function getActionLogTypeList (data) {
	return request({
		url: '/operatelog/selectEnum',
		method: 'post',
		data
	})
}

// 代理-操作记录
export function getAgentActionLogList (data) {
	return request({
		url: '/operatelog/queryProxyOperateList',
		method: 'post',
		data
	})
}

// 升级总代审核
export function upTopProxyAuditAddUpTopProxyAudit(data) {
	return request({
		url: '/UpTopProxyAudit/addUpTopProxyAudit',
		method: 'post',
		data
	})
}

// 升级总代审核记录
export function upTopProxyAuditauditList(data) {
	return request({
		url: '/UpTopProxyAudit/auditList',
		method: 'post',
		data
	})
}

// 升级总代审核记录 - 锁单
export function upTopProxyAuditLock(data) {
	return request({
		url: 'UpTopProxyAudit/auditLock',
		method: 'post',
		data
	})
}

// 升级总代审核记录 - 详情
export function upTopProxyQueryAuditDetail(data) {
	return request({
		url: 'UpTopProxyAudit/queryAuditDetail',
		method: 'post',
		data
	})
}

// 升级总代审核记录 - 详情
export function upTopProxyAudit(data) {
	return request({
		url: 'UpTopProxyAudit/audit',
		method: 'post',
		data
	})
}

// 代理管理 - 代理详情 - 回收全部额度判断满不满足条件
export function recoverAllAmountApi(data) {
	return request({
		url: '/proxyData/checkOpenBets',
		method: 'post',
		data
	})
}

// 代理管理 - 代理清算账单
export function getProxyLiduidationList(data) {
	return request({
		url: '/report/proxyLiquidationBill/list/page',
		method: 'post',
		data
	})
}

// 代理管理 - 代理清算账单详情
export function getLiquidationListDetail(data) {
	return request({
		url: '/report/proxyLiquidationBill/detail/page',
		method: 'post',
		data
	})
}

// 代理管理 - 代理清算详情导出
export function getProxyLiquidationDetailExport(data) {
	return request({
		url: '/report/proxyLiquidationBill/detail/export',
		method: 'post',
		data,
		responseType: 'blob'
	})
}

// 升级总代审核记录 - 详情
export function commissionRatePage(data) {
	return request({
		data,
		url: '/commissionRate/queryPage',
		method: 'post'
	})
}

// 代理白马会账号记录
export function getProxyBmhAccountRecordList(data) {
	return request({
		url: '/bankProxyRecord/bmhRecord',
		method: 'post',
		data
	})
}

// 代理易换账号记录
export function getProxyYhAccountRecordList(data) {
	return request({
		url: '/bankProxyRecord/yhRecord',
		method: 'post',
		data
	})
}

// 总代分组
export function proxyGroupPageProxyGroup(data) {
	return request({
		url: '/proxyGroup/pageProxyGroup',
		method: 'post',
		data
	})
}

// 查询代理场馆配置分页查询
export function getSelectProxyStadiumConfigPage(data) {
	return request({
		url: '/proxy/selectProxyStadiumConfigPage',
		method: 'post',
		data
	})
}

// 总代分组-删除
export function proxyGroupDeleteById(data) {
	return request({
		url: '/proxyGroup/deleteById',
		method: 'post',
		data
	})
}

// 总代分组-新增
export function proxyGroupSaveProxyGroup(data) {
	return request({
		url: '/proxyGroup/saveProxyGroup',
		method: 'post',
		data
	})
}

// 总代分组-新增
export function proxyGroupPageProxyByGroupId(data) {
	return request({
		url: '/proxyGroup/pageProxyByGroupId',
		method: 'post',
		data
	})
}

// 查询代理场馆配置分页查询
export function updateProxyStadiumConfig(data) {
	return request({
		url: '/proxy/updateProxyStadiumConfig',
		method: 'post',
		data
	})
}


export  function batchUpdateSportsHandicapType(data) {
	return request({
		url: 'rebateRate/batchUpdateSportsHandicapType',
		method: 'post',
		data
	})
}

export  function retryUpdateSportsHandicapType(data) {
	return request({
		url: 'rebateRate/retryUpdateSportsHandicapType',
		method: 'post',
		data
	})
}

export  function getUpdateSportsHandicapTypeInfo(data) {
	return request({
		url: 'rebateRate/getUpdateSportsHandicapTypeInfo',
		method: 'post',
		data
	})
}

export default {
	getUpdateSportsHandicapTypeInfo,
	retryUpdateSportsHandicapType,
	batchUpdateSportsHandicapType,
	proxyDataPageSubAccountPerm,
	proxyDataSubAccountPermChange,
	proxyGroupPageProxyGroup,
	proxyGroupDeleteById,
	proxyGroupSaveProxyGroup,
	proxyGroupPageProxyByGroupId,
	updateProxyStadiumConfig,
	getSelectProxyStadiumConfigPage,
	commissionRatePage,
	proxyDataResetProxyCheckType,
	upTopProxyAudit,
	upTopProxyQueryAuditDetail,
	upTopProxyAuditLock,
	upTopProxyAuditauditList,
	upTopProxyAuditAddUpTopProxyAudit,
	openProxyAddOpenProxyAudit,
	getOpenProxyAuditAuditList,
	getOpenProxyDetail,
	openProxyAuditLock,
	openProxyAudit,
	getRebateRateQuery,
	getRebateRateQueryProxy,
	getRebateRateQueryMember,
	getBillPeriodQueryBillPeriod,
	exportTopProxyBillList,
	getTopProxyBillList,
	rebateRateQueryProxy,
	queryLastMonthBalance,
	clearedLastMonthBalance,
	updateRebateAllotMode,
	rebateRateTemplateQueryList,
	platformAdjustmentTryCacl,
	commissionRatePlatformAdjustmentTryCacl,
	getProxyCommissionSettleCycle,
	updateProxyCommissionSettleCycle,
	commissionRateUpdate,
	commissionRateList,
	commissionRateQueryLimit,
	rebateRateQueryLimit,
	rebateRateQuery,
	rebateRateListApi,
	memberRebateRatelistApi,
	commissionRateQuery,
	proxyConfigPageList,
	proxyConfigMemberDetailsList,
	stopSettingStopSettingList,
	updateMemberOverflowRecord,
	overflowMemberInfo,
	addOverflowMember,
	memberOverflowDetail,
	lockMemberAuditRecordProxy,
	listOverflowMember,
	AgentListAPI,
	AgentLevelLinkAPI,
	AgentLevelListAPI,
	AgentLevelLinkByUserNameAPI,
	agentListExportAPI,
	agentDictAPI,
	addSuperAgentAPI,
	addAgentAPI,
	memberTransAgentAPI,
	agentRegisterInfoAPI,
	agentBankRecordAPI,
	getProxySubAccount4Page,
	agentVirtualRecordAPI,
	getProxyDetailQueryDetail,
	getValueAddedDetail,
	agentPictureListAPI,
	agentImageUploadAPI,
	agentPictureListDeleteAPI,
	agentPictureListCreateAPI,
	agentPictureListUpdateAPI,
	getProxySubRole4Page,
	getProxyProxyDetailContract,
	setProxyDataInfoEdit,
	getProxyDetailBankerInfo,
	getProxyProxyBillDetailInfo,
	setProxyDetailRemark,
	getProxyDetailRemark,
	getProxyDetailFinanceInfo,
	getProxyDetailBankerLockDetail,
	getProxyDataBalance,
	getProxyDataCommission,
	getRechargeAndWithdrawInfo,
	getProxyDetailProxyRechargeInfo,
	getProxyDetailTeamInfo,
	getProxyDetailTeamBet,
	getProxyDetailTop3Bet,
	getProxyDetailProxyLoginLog,
	getProxyDataInfoChangeRecord,
	setDomainDelete,
	addDomainInsert,
	getDomainSelect,
	setDomainUpdate,
	getProxyGradesSelect,
	setProxyGradeDelete,
	setProxyGradeInsert,
	setProxyGradeUpdate,
	getProxyCommissionSelect,
	setProxyCommissionDelete,
	setProxyCommissionInsert,
	setProxyCommissionUpdate,
	getProxyGetLabelChangeRecordPage,
	getProxyPageLabel,
	getProxyAddLabel,
	setProxyDeleteLabel,
	getProxyProxyInfoByLabelId,
	getProxyLabelDict,
	getProxyOperateSelect,
	getProxyContractContractRecordList,
	getProxylevelnameList,
	getProxylevelproxyList,
	getProxyleveladd,
	getProxyMinContractminContractRecordList,
	getProxyContractList,
	acceptContractAPI,
	contractAddOrUpdateAPI,
	contractCheckProxyAPI,
	checkProxyDetailAPI,
	queryRebateRatioAPI,
	addContractTemplateAPI,
	editContractTemplateAPI,
	getContractTemplateListAPI,
	delContractTemplateApi,
	saveContractTemplateAPI,
	getContractScaleListAPI,
	setContractTemplateScale,
	getProxylevelupdate,
	getProxyMinContractminContractList,
	getProxyMinContractadd,
	getProxyMinContractupdate,
	getProxyMinContractdelete,
	getProxycontractscaleList,
	getProxyMinContractchangeHistoryDetails,
	getProxyArtificialPatchAccountAddAuditgetAccountAdjustType,
	getProxyMinContractchangeDetailsList,
	getArtificialPatchgetProxySubAdjustType,
	getCreditLevelConfcreditList,
	getCreditLevelConfUpdateCredit,
	getCreditLevelConfQuery,
	getCreditLevelConfUpdate,
	getCreditLevelConfUpdateStatus,
	getCreditLevelConfupdateCredit,
	setExclusiveDomainDeleteDomain,
	getExclusiveDomainDomainList,
	getExclusiveDomainDownLoad,
	getExclusiveDomainImportExcel,
	setExclusiveDomainSaveDomain,
	setExclusiveDomainUpLoad,
	setExclusiveDomainUpdateBind,
	setExclusiveDomainUse,
	getdomainLogquery,
	getdomainLogloadOperateList,
	getproxyMinContractaddPolicyType,
	getproxyMinContractupdatePolicyType,
	getproxyMinContractdeletePolicyType,
	profitPolicyConfigDetailApi,
	profitSharingRecordListApi,
	profitPolicychangeDetailApi,
	rebateRateQueryApi,
	rebateRateMemberList,
	rebateRateUdate,
	rebateQuerySubMax,
	rebateRateTemplateMappingQuery,
	rebateRateTemplateAdd,
	rebateRateTemplateDetail,
	getRebateRateTemplateDetail,
	rebateRateTemplateList,
	rebateRateTemplateQuery,
	rebateRateTemplateUpdate,
	rebateRateTemplateDelete,
	rebateRateTemplateDetailAdd,
	rebateRateTemplateDetailQuery,
	rebateRateTemplateMappingTopProxyList,
	recoveringAvailableCreditLimitApi,
	getActionLogTypeList,
	getAgentActionLogList,
	getProxyChangeTypeList,
	getProxyInfoChangeList,
	getProxyBmhAccountRecordList,
	recoverAllAmountApi,
	getProxyLiduidationList,
	getLiquidationListDetail,
	getProxyLiquidationDetailExport,
	getProxyYhAccountRecordList
}
