import request from '@/utils/request'

export default {
	proxyBetRecordExport: (data) => {
		return request({
			url: '/report/bet-record/proxy/export',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},
	memberBetRecordExport: (data) => {
		return request({
			url: '/report/bet-record/member/export',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},

	proxyOrderRecordExport: (data) => {
		return request({
			url: '/report/order-record/proxy/export',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},
	memberOrderRecordExport: (data) => {
		return request({
			url: '/report/order-record/member/export',
			method: 'post',
			data,
			responseType: 'blob'
		})
	},
	// 会员盈亏报表==>会员盈亏列表和小计(分页)
	getReportMembernetamountList: (data) => {
		return request({
			url: '/report/membernetamount/list',
			method: 'post',
			data
		})
	},
	// 重算会员盈亏分页查询
	getRecallListPage: (data) => {
		return request({
			url: '/report/membernetamount/recall/listPage',
			method: 'post',
			data
		})
	},
	getMemberListPage: (data) => {
		return request({
			url: '/report/membernetamount/member/listPage',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏总计
	getReportMembernetamountSummary: (data) => {
		return request({
			url: '/report/membernetamount/summary',
			method: 'post',
			data
		})
	},
	getMembernetamountRecallSummary: (data) => {
		return request({
			url: '/report/membernetamount/member/summary',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏详情
	getReportMembernetamountDetail: (data) => {
		return request({
			url: '/report/membernetamount/detail',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏详情新
	getReportMembernetamountDetailNew: (data) => {
		return request({
			url: '/report/membernetamount/list/venue/details',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏详情新2
	getReportMembernetamountDetailNew2: (data) => {
		return request({
			url: '/report/membernetamount/list/venue/details/new',
			method: 'post',
			data
		})
	},

	// 会员盈亏报表==>会员盈亏详情新2总计
	getReportMembernetamountTotal2: (data) => {
		return request({
			url: '/report/membernetamount/list/venue/summary/new',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏详情总计
	getReportMembernetamountTotal: (data) => {
		return request({
			url: '/report/membernetamount/list/venue/summary',
			method: 'post',
			data
		})
	},
	// 会员盈亏报表==>会员盈亏导出
	getReportMembernetamountExport: (data) => {
		return request({
			url: '/report/membernetamount/member/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	proxyRebateAmountMemberOrderList: (data) => {
		return request({
			url: '/reportRecal/proxyNetAmountMemberOrderList',
			method: 'post',
			data
		})
	},
	rebateAmountOrderList: (data) => {
		return request({
			url: '/reportRecal/rebateAmountOrderList',
			method: 'post',
			data
		})
	},

	reportRecalRetAmountOrderList: (data) => {
		return request({
			url: '/reportRecal/netAmountOrderList',
			method: 'post',
			data
		})
	},

	proxyTeamRebateAmountMemberOrderList: (data) => {
		return request({
			url: '/reportRecal/proxyTeamRebateAmountMemberOrderList',
			method: 'post',
			data
		})
	},
	saveValueAddedList: (data) => {
		return request({
			url: '/report/membernetamount/valueAdded/list',
			method: 'post',
			data
		})
	},
	queryNetAmountDay: (data) => {
		return request({
			url: 'report/membernetamount/member/queryNetAmountDay',
			method: 'post',
			data
		})
	},

	reportProxyNetAmountListPage: (data) => {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/total/listPage',
			method: 'post',
			data
		})
	},

	// 代理盈亏报表==>分页查询代理日盈亏列表
	getReportProxyNetAmountDayList(data) {
		return request({
			url: '/report/proxyNetAmount/day/listPage',
			method: 'post',
			data
		})
	},
	getReportDaySummary(data) {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/summary',
			method: 'post',
			data
		})
	},
	getReportDayDetailSummary(data) {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/detail/summary',
			method: 'post',
			data
		})
	},
	getReportNetAmountDaySummary(data) {
		return request({
			url: '/report/membernetamount/member/queryNetAmountDaySummary',
			method: 'post',
			data
		})
	},

	getReportRecalculateListPage(data) {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/listPage',
			method: 'post',
			data
		})
	},
	getReportRecalculateDetailListPage(data) {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/detail/listPage',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>代理日盈亏总计
	getReportProxyNetAmountDaySummary(data) {
		return request({
			url: '/report/proxyNetAmount/day/summary',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>查询日详情列表
	getReportProxyNetAmountDayDetail(data) {
		return request({
			url: '/report/proxyNetAmount/day/detail',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>代理日盈亏导出
	getReportProxyNetAmountDayExport(data) {
		return request({
			url: '/report/proxyNetAmount/recalculate/day/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理盈亏报表==>分页查询代理月盈亏列表
	getReportProxyNetAmountMonthList(data) {
		return request({
			url: '/report/proxyNetAmount/month/listPage',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>代理月盈亏总计
	getReportProxyNetAmountMonthSummary(data) {
		return request({
			url: '/report/proxyNetAmount/month/summary',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>查询月详情列表
	getReportProxyNetAmountMonthDetail(data) {
		return request({
			url: '/report/proxyNetAmount/month/detail',
			method: 'post',
			data
		})
	},
	// 代理盈亏报表==>代理月盈亏导出
	getReportProxyNetAmountMonthExport(data) {
		return request({
			url: '/report/proxyNetAmount/month/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 场馆盈亏报表==>场馆类型下拉列表
	getReportVenueTypeList(data) {
		return request({
			url: '/report/venueNetAmountDay/queryVenueTypeList',
			method: 'post',
			data
		})
	},
	getReportGetVenueGames(params) {
		return request({
			url: '/report/membernetamount/recall/getVenueGames',
			method: 'get',
			params
		})
	},
	// 场馆盈亏报表==>场馆名称下拉列表
	getReportVenueNameList(data) {
		return request({
			url: '/report/venueNetAmountDay/queryVenueNameList',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>分页查询场馆盈亏列表
	getReportVenueNetAmountDayList(data) {
		return request({
			url: '/report/venueNetAmountDay/listPage',
			method: 'post',
			data
		})
	},
	getReportProxyVenueList(data) {
		return request({
			url: '/report/proxy-venue/venue/list',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>场馆盈亏总计
	getReportVenueNetAmountDaySummary(data) {
		return request({
			url: '/report/venueNetAmountDay/summary',
			method: 'post',
			data
		})
	},
	getReportProxyVenueSummary(data) {
		return request({
			url: '/report/proxy-venue/venue/summary',
			method: 'post',
			data
		})
	},
	getReportVenueDetailSummary(data) {
		return request({
			url: '/report/proxy-venue/venue-detail/summary',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>分页查询场馆盈亏详情列表
	getReportVenueNetAmountDayDetail(data) {
		return request({
			url: '/report/venueNetAmountDay/detailListPage',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>分页查询场馆盈亏列表(场馆维度)
	getReportVenueNetAmountDayListNew(data) {
		return request({
			url: '/report/venueNetAmountDay/listPage/new',
			method: 'post',
			data
		})
	},
	getReportProxyVenueDayList(data) {
		return request({
			url: '/report/proxy-venue/day/list',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>场馆盈亏总计(场馆维度)
	getReportVenueNetAmountDaySummaryNew(data) {
		return request({
			url: '/report/venueNetAmountDay/summary/new',
			method: 'post',
			data
		})
	},
	getReportProxyVenueDaySummary(data) {
		return request({
			url: '/report/proxy-venue/day/summary',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>点击投注人数弹窗-场馆投注详情列表(场馆维度)
	getBetDetailList(data) {
		return request({
			url: '/report/venueNetAmountDay/betDetail/list',
			method: 'post',
			data
		})
	},
	getDetailMemberList(data) {
		return request({
			url: '/report/proxy-venue/venue-detail-member/list',
			method: 'post',
			data
		})
	},
	// 场馆盈亏报表==>点击投注人数弹窗-场馆投注详情合计(场馆维度)
	getBetDetailSummary(data) {
		return request({
			url: '/report/venueNetAmountDay/betDetail/summary',
			method: 'post',
			data
		})
	},
	getBetDetailMemberSummary(data) {
		return request({
			url: '/report/proxy-venue/venue-detail-member/summary',
			method: 'post',
			data
		})
	},
	getVenueMemberId(params) {
		return request({
			url: `/report/proxy-venue/venue-member/proxy`,
			method: 'get',
			params
		})
	},
	// 场馆盈亏报表==>场馆盈亏导出
	getReportVenueNetAmountDayExport(data) {
		return request({
			url: '/report/venueNetAmountDay/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getReportVenueDetailList(data) {
		return request({
			url: '/report/proxy-venue/venue-detail/list',
			method: 'post',
			data
		})
	},
	getReportProxyVenueExport(data) {
		return request({
			url: '/report/proxy-venue/venue/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 场馆盈亏报表==>场馆盈亏导出(场馆维度)
	getReportVenueNetAmountDayExportNew(data) {
		return request({
			url: '/report/venueNetAmountDay/export/new',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getReportProxyVenueDayExportNew(data) {
		return request({
			url: '/report/proxy-venue/day/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 游戏盈亏游戏获取
	getReportGameList(params) {
		return request({
			url: '/game/getVenueGames',
			method: 'get',
			params
		})
	},
	// 游戏盈亏报表==>分页查询游戏盈亏列表
	getReportGameProfitList(data) {
		return request({
			url: '/report/gameProfit/listPage',
			method: 'post',
			data
		})
	},
	// 游戏盈亏报表==>游戏盈亏总计
	getReportGameProfitSummary(data) {
		return request({
			url: '/report/gameProfit/summary',
			method: 'post',
			data
		})
	},
	// 游戏盈亏报表==>分页查询游戏盈亏详情列表
	getReportGameProfitDetail(data) {
		return request({
			url: '/report/gameProfit/detailListPage',
			method: 'post',
			data
		})
	},
	// 游戏盈亏报表==>游戏盈亏-列表Excel导出
	getReportGameProfitExport(data) {
		return request({
			url: '/report/gameProfit/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 每日盈亏报表==>每日盈亏列表和小计(分页)
	getReportDaynetamountList(data) {
		return request({
			url: '/report/daynetamount/list',
			method: 'post',
			data
		})
	},
	// 平台运营状况概述-分页列表
	getStatusOverViewList(data) {
		return request({
			url: '/report/platform/listPage',
			method: 'post',
			data
		})
	},
	// 平台运营状况概述-总计
	getStatusOverViewTotal(data) {
		return request({
			url: '/report/platform/summary',
			method: 'post',
			data
		})
	},
	// 平台运营状况概述-导出
	getStatusOverViewExport(data) {
		return request({
			url: '/report/platform/listPage',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 平台运营概况-三方场馆数据-分页列表
	getStatusOverViewTripartList(data) {
		return request({
			url: '/report/platform/third/list',
			method: 'post',
			data
		})
	},
	// 平台运营概况-三方场馆数据-总计
	getStatusOverViewTripartTotal(data) {
		return request({
			url: '/report/platform/third/summary',
			method: 'post',
			data
		})
	},
	// 台底管理 新增
	platformBottomRecordAdd(data) {
		return request({
			url: '/platformBottomRecord/add',
			method: 'post',
			data
		})
	},
	// 台底管理 修改
	platformBottomRecordUpdate(data) {
		return request({
			url: '/platformBottomRecord/update',
			method: 'post',
			data
		})
	},
	// 台底管理 列表
	platformBottomRecordPage(data) {
		return request({
			url: '/platformBottomRecord/page',
			method: 'post',
			data
		})
	},
	// 台底管理 删除
	platformBottomRecordDelete(data) {
		return request({
			url: '/platformBottomRecord/delete',
			method: 'post',
			data
		})
	},
	// 平台运营概况-三方场馆数据-导出
	getStatusOverViewTripartExport(data) {
		return request({
			url: '/report/platform/third/export',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 平台运营状况德州数据-分页
	getStatusPlatViewList(data) {
		return request({
			url: '/report/platformDz/listPage',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 平台运营状况德州数据-总计
	getStatusPlatViewTotal(data) {
		return request({
			url: '/report/platformDz/summary',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 平台运营状况德州数据-导出
	getStatusPlatViewExport(data) {
		return request({
			url: '/report/platformDz/export',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 代理预期返点报表(分页)
	selectProxyAnticipateRebate(data) {
		return request({
			url: '/proxyRebate/selectProxyAnticipateRebate',
			method: 'post',
			data
		})
	},
	// 代理预期返点报表(总计)
	selectProxyAnticipateRebateTotal(data) {
		return request({
			url: '/proxyRebate/selectProxyAnticipateRebateTotal',
			method: 'post',
			data
		})
	},
	// 代理预期返点报表(导出)
	selectProxyAnticipateRebateExport(data) {
		return request({
			url: '/proxyRebate/selectProxyAnticipateRebateExport',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 会员预期返水报表(分页)
	expectRebatePage(data) {
		return request({
			url: '/report/commission/member/expectRebatePage',
			method: 'post',
			data
		})
	},
	// 会员预期返水报表(总计)
	expectRebatePageSummary(data) {
		return request({
			url: '/report/commission/member/expectRebatePageSummary',
			method: 'post',
			data
		})
	},
	// 会员预期返水报表(导出)
	expectRebateExport(data) {
		return request({
			url: '/report/commission/member/expectRebateExport',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 每日盈亏报表==>每日盈亏总计
	getReportDaynetamountAggregation(data) {
		return request({
			url: '/report/daynetamount/aggregation',
			method: 'post',
			data
		})
	},
	// 每日盈亏报表==>每日盈亏列表导出
	getReportDaynetamountExportExcel(data) {
		return request({
			url: '/report/daynetamount/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员游戏盈亏==>列表和小计(分页)
	getMemberVenueTypeList(data) {
		return request({
			url: '/report/memberVenueType/list',
			method: 'post',
			data
		})
	},
	// 会员游戏盈亏==>总计
	getMemberVenueTypeSummary(data) {
		return request({
			url: '/report/memberVenueType/summary',
			method: 'post',
			data
		})
	},
	// 会员游戏盈亏==>列表导出
	getMemberVenueTypeExportExcel(data) {
		return request({
			url: '/report/memberVenueType/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员报表==>会员报表列表(分页) 全量统计
	getReportPlayerStaticInfoList(data) {
		return request({
			url: '/report/playerStaticInfo/listPage',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表总计 全量统计
	getReportPlayerStaticInfoSummary(data) {
		return request({
			url: '/report/playerStaticInfo/summary',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表列表excel导出 全量统计
	getReportPlayerStaticInfoExport(data) {
		return request({
			url: '/report/playerStaticInfo/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表列表(分页) 统计日期
	getReportPlayerReportList(data) {
		return request({
			url: '/report/playerReport/listPage',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表总计 统计日期
	getReportPlayerReportSummary(data) {
		return request({
			url: '/report/playerReport/summary',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表列表excel导出 统计日期
	getReportPlayerReportExport(data) {
		return request({
			url: '/report/playerReport/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 会员报表==>会员报表详情(分页)
	getReportPlayerStaticInfoDetail(data) {
		return request({
			url: '/report/playerReport/detailListPage',
			method: 'post',
			data
		})
	},
	// 会员报表==>查询用户余额
	getReportPlayerBalance(data) {
		return request({
			url: '/member/detail/balance',
			method: 'post',
			data
		})
	},
	// 代理报表==>分页查询
	getReportProxyReportList(data) {
		return request({
			url: '/report/ProxyReport/listPage',
			method: 'post',
			data
		})
	},
	// 代理报表==>总计
	getReportProxyReportSummary(data) {
		return request({
			url: '/report/ProxyReport/summary',
			method: 'post',
			data
		})
	},
	// 代理报表==>查询详情列表
	getReportProxyReportDetai(data) {
		return request({
			url: '/report/ProxyReport/detailListPage',
			method: 'post',
			data
		})
	},
	// 代理报表==>代理盈亏导出
	getReportProxyReportExport(data) {
		return request({
			url: '/report/ProxyReport/proxyDayReportExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员返水报表==>会员返水报表列表(分页)
	getReportRebateList(data) {
		return request({
			url: '/report/rebate/list',
			method: 'post',
			data
		})
	},
	// 会员返水报表==>会员返水报表总计
	getReportRebateSummary(data) {
		return request({
			url: '/report/rebate/aggregation',
			method: 'post',
			data
		})
	},
	// 会员返水报表==>会员返水报表列表excel导出
	getReportRebateExport(data) {
		return request({
			url: '/report/rebate/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 会员返水报表==>会员返水报表详情(分页)
	getRepoRebateDetail(data) {
		return request({
			url: '/report/rebate/detail',
			method: 'post',
			data
		})
	},
	// 会员优惠报表==>会员优惠报表列表(分页)
	getReportDiscountReportList(data) {
		return request({
			url: '/report/discountReport/listPage',
			method: 'post',
			data
		})
	},
	// 会员优惠报表==>会员优惠报表总计
	getReportDiscountReportSummary(data) {
		return request({
			url: '/report/discountReport/summary',
			method: 'post',
			data
		})
	},
	// 会员优惠报表==>会员优惠报表列表excel导出
	getReportDiscountReportExport(data) {
		return request({
			url: '/report/discountReport/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 会员优惠报表==>会员优惠报表详情(分页)
	getReportDiscountReportDetail(data) {
		return request({
			url: '/report/discountReport/detail',
			method: 'post',
			data
		})
	},
	// 平台出入款报表==>平台出入款报表列表(分页)
	getReportAccessAmountDayList(data) {
		return request({
			url: '/report/accessAmountDay/listPage',
			method: 'post',
			data
		})
	},
	// 平台出入款报表==>平台出入款报表总计
	getReportAccessAmountDaySummary(data) {
		return request({
			url: '/report/accessAmountDay/summary',
			method: 'post',
			data
		})
	},
	// 平台出入款报表==>平台出入款报表列表excel导出
	getReportAccessAmountDayExport(data) {
		return request({
			url: '/report/accessAmountDay/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 代理返点报表==>代理返点报表列表(分页)
	getReportPreviousRebateList(data) {
		return request({
			url: '/report/previous-rebate/queryRebateList',
			method: 'post',
			data
		})
	},
	// 代理返点报表==>代理返点报表详情(分页)
	getReportPreviousRebateDetailList(data) {
		return request({
			url: '/report/previous-rebate/queryRebateDetailList',
			method: 'post',
			data
		})
	},
	// 代理返点报表==>代理返点报表总计
	getReportPreviousRebateSummary(data) {
		return request({
			url: '/report/previous-rebate/queryRebateReportTotal',
			method: 'post',
			data
		})
	},
	// 代理返点报表==>代理返点报表列表excel导出
	getReportPreviousRebateExport(data) {
		return request({
			url: '/report/previous-rebate/exportExcel',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 佣金收付报表==>佣金收付报表列表(分页)
	getReportProxyCommissionCommissionList(data) {
		return request({
			url: '/report/proxy-commission-bill/queryCommissionList',
			method: 'post',
			data
		})
	},
	// 佣金收付报表==>佣金收付报表总计
	getReportProxyCommissionCommissionTotal(data) {
		return request({
			url: '/report/proxy-commission-bill/queryCommissionTotal',
			method: 'post',
			data
		})
	},
	// 佣金收付报表==>佣金收付报表列表excel导出
	getReportProxyCommissionExport(data) {
		return request({
			url: '/report/proxy-commission-bill/export',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 佣金收付报表==>未付清明细
	getReportProxyCommissionDebtsList(data) {
		return request({
			url: '/report/proxy-commission-bill/queryDebtsList',
			method: 'post',
			data
		})
	},
	// 佣金收付报表==>未收完明细
	getReportProxyCommissionReceivableList(data) {
		return request({
			url: '/report/proxy-commission-bill/queryReceivableList',
			method: 'post',
			data
		})
	},
	// 代理佣金报表==>代理佣金报表列表(分页)
	getReportCommissionRebateList(data) {
		return request({
			// url: '/report/commission/list',
			url: '/settlement/commission/proxy/listTopProxyListPage',
			method: 'post',
			data
		})
	},
	// 代理佣金报表==>代理佣金报表总计
	getReportCommissionRebateSummary(params) {
		return request({
			url: '/report/commission/aggregation',
			method: 'get',
			params
		})
	},
	// 代理佣金报表==>代理佣金报表列表excel导出
	getReportCommissionRebateExport(params) {
		return request({
			url: '/report/commission/commissionExport',
			responseType: 'blob',
			method: 'get',
			params
		})
	},
	// 代理返佣报表==>导出
	getListTopProxyListPageExport(data) {
		return request({
			url: '/settlement/commission/proxy/listTopProxyListPageExport',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 极验 => 二次校验
	geetestValidate(params) {
		return request({
			url: 'geetest/verify',
			method: 'get',
			params
		})
	},
	// 数据报表==>数据日报报表(分页)
	getDataDailyReportPage(data) {
		return request({
			url: '/report/daily/dxReportD',
			method: 'post',
			data
		})
	},
	// 数据报表==>数据日报报表(分页)
	getDataDailyReportSummary(data) {
		return request({
			url: '/report/daily/dxReportD/summary',
			method: 'post',
			data
		})
	},
	// 数据报表==>数据日报报表(导出)
	getDataDailyReportExport(data) {
		return request({
			url: '/report/daily/dxReportD/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 数据报表==>数据日报报表(详情)
	getDataDailyReportDetail(data) {
		return request({
			url: '/report/daily/financialData',
			method: 'post',
			data
		})
	},
	// 数据报表==>数据对比报表(分页)
	getDataComparisonReportPage(data) {
		return request({
			url: '/report/venueNetAmountDay/dataCompareDayList',
			method: 'post',
			data
		})
	},
	// 数据报表==>数据对比报表(总计)
	getDataComparisonReportTotal(data) {
		return request({
			url: '/report/venueNetAmountDay/dataCompareDaySummary',
			method: 'post',
			data
		})
	},
	// 数据报表==>数据对比报表(导出)
	getDataComparisonReportExport(data) {
		return request({
			url: '/report/venueNetAmountDay/dataCompareDayListExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 数据报表==>会员盈亏(分页)
	getMemberAndLossRankingPage(data) {
		return request({
			url: '/report/dataReport/queryMembersProfitAndLossTop',
			method: 'post',
			data
		})
	},
	// 数据报表==>会员盈亏(合计)
	getMemberAndLossRankingTotal(data) {
		return request({
			url: '/report/dataReport/queryMembersProfitAndLossTopTotal',
			method: 'post',
			data
		})
	},
	// 数据报表==>会员盈亏(导出)
	getMemberAndLossRankingExport(data) {
		return request({
			url: '/report/dataReport/membersProfitAndLossTopExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 数据报表==>代理团队排行(分页)
	getAgentAndLossRankingPage(data) {
		return request({
			url: '/report/dataReport/queryAgencyTeamTop',
			method: 'post',
			data
		})
	},
	// 数据报表==>代理团队排行(合计)
	getAgentAndLossRankingTotal(data) {
		return request({
			url: '/report/dataReport/queryAgencyTeamTopTotal',
			method: 'post',
			data
		})
	},
	// 数据报表==>代理团队排行(导出)
	getAgentAndLossRankingExport(data) {
		return request({
			url: '/report/dataReport/agencyTeamTopExport',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 首页==>场馆盈亏
	getVenueNetAmountDayPage(data) {
		return request({
			url: '/report/venueNetAmountDay/queryList',
			method: 'post',
			data
		})
	},
	// 会员返水对账表 - 列表
	getmemberRebateAccountPage(data) {
		return request({
			url: '/report/memberRebateAccount/list',
			method: 'post',
			data
		})
	},
	// 会员返水对账表 - 总计
	getmemberRebateAccountTotal(data) {
		return request({
			url: '/report/memberRebateAccount/summary',
			method: 'post',
			data
		})
	},
	// 会员返水对账表 - 导出
	getmemberRebateAccountExport(data) {
		return request({
			url: '/report/memberRebateAccount/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 代理返点数据报表列表
	proxyRebateDataApi(data) {
		return request({
			url: '/report/proxyRebateData/list',
			method: 'post',
			data
		})
	},
	// 代理返点数据报表列表总计
	proxyRebateDataApiTotal(data) {
		return request({
			url: '/report/proxyRebateData/summary',
			method: 'post',
			data
		})
	},
	// 代理返点数据报表导出
	proxyRebateDataExport(data) {
		return request({
			url: '/report/proxyRebateData/export',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	// 代理返点数据报表明细
	proxyRebateDataDetail(data) {
		return request({
			url: '/report/proxyRebateData/detail',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表列表
	memberRebateDataApi(data) {
		return request({
			url: '/report/memberRebateData/list',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表列表总计
	memberRebateDataApiTotal(data) {
		return request({
			url: '/report/memberRebateData/summary',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表列表明细
	memberRebateDataApiDetail(data) {
		return request({
			url: '/report/memberRebateData/personRebate',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表导出
	memberReturnDataExport(data) {
		return request({
			url: '/report/memberRebateData/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员返水数据报表列表明细
	incomeReportList(data) {
		return request({
			url: '/report/proxy/income/report/list',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表列表总计
	incomeReportTotal(data) {
		return request({
			url: '/report/proxy/income/report/summary',
			method: 'post',
			data
		})
	},
	// 会员返水数据报表列表总计
	incomeReportExport(data) {
		return request({
			url: '/report/proxy/income/report/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 会员限红模板修改记录
	getTemplatePage(data) {
		return request({
			url: '/venue/redLimit/templatePage',
			method: 'post',
			data
		})
	},
	// 会员限红模板修改记录
	redLimitSync(data) {
		return request({
			url: '/venue/redLimit/sync',
			method: 'post',
			data
		})
	},
	// 会员限红模板修改状态
	redLimitSetShieldc(data) {
		return request({
			url: '/venue/redLimit/setShield',
			method: 'post',
			data
		})
	},

	// api
	masterAgentReportList(data) {
		return request({
			url: '/report/masterAgent/list',
			method: 'post',
			data
		})
	},
	masterAgentReportSummary(data) {
		return request({
			url: '/report/masterAgent/summary',
			method: 'post',
			data
		})
	},
	masterAgentReportSummaryExport(data) {
		return request({
			url: '/report/masterAgent/export',
			responseType: 'blob',
			method: 'post',
			data
		})
	},
	getTopProxyGameList(data) {
		return request({
			url: '/report/topProxy/game/listPage',
			method: 'post',
			data
		})
	},
	getTopProxyGameExport(data) {
		return request({
			url: '/report/topProxy/game/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getTopFinanceListPage(data) {
		return request({
			url: '/report/topProxy/finance/listPage',
			method: 'post',
			data
		})
	},
	getTopProxyFinanceExport(data) {
		return request({
			url: '/report/topProxy/finance/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	getTopRecalculateListPage(data) {
		return request({
			url: '/report/topProxy/recalculate/listPage',
			method: 'post',
			data
		})
	},
	getTopProxyRecalculateExport(data) {
		return request({
			url: '/report/topProxy/recalculate/export',
			method: 'post',
			responseType: 'blob',
			data
		})
	}
}
