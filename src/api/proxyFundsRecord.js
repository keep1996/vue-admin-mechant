// 代理资金记录
import request from '@/utils/request'

export default {
	// 坐庄分润记录列表
	bankersProfitRecordList: (data) => {
		return request({
			url: '/proxyHolderFeeRecord/queryRecordPage',
			method: 'post',
			data
		})
	},
	// 坐庄分润记录列表总计
	bankersProfitRecordTotal: (data) => {
		return request({
			url: '/proxyHolderFeeRecord/queryRecordPageTotal',
			method: 'post',
			data
		})
	},
	// 坐庄分润记录导出
	bankersProfitRecordExportExcel: (data) => {
		return request({
			url: '/proxyHolderFeeRecord/exportExcel',
			method: 'post',
			responseType: 'blob',
			data
		})
	},
	// 吃单抽佣记录-列表
	eatTakeCommissionRecord: (data) => {
		return request({
			url: '/proxyFundsRecords/eatTakeCommissionRecord',
			method: 'post',
			data
		})
	},
	// 吃单抽佣记录-查看详情
	eatTakeCommissionRecordDetail: (data) => {
		return request({
			url: '/proxyFundsRecords/eatTakeCommissionRecordDetail',
			method: 'post',
			data
		})
	},
	// 吃单抽佣记录-导出
	eatTakeCommissionRecordExportExcel: (data) => {
		return request({
			url: '/proxyFundsRecords/eatTakeCommissionRecord/download',
			method: 'post',
			responseType: 'blob',
			data
		})
	}
}
