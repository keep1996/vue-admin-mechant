// 后端枚举接口返回的数据，用于下拉选项和表格枚举数据匹配
import i18n from '@/locales'

export default {
	virtualType: [
		// {
		// 	code: 'BTC',
		// 	description: i18n.t('dict.virtual_type.0.description')
		// },
		{
			code: 'USDT',
			description: i18n.t('dict.virtual_type.1.description')
		}
	],
	bindStatusType: [
		{
			code: '0',
			description: i18n.t('dict.bind_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.bind_status_type.1.description')
		}
	],
	virtualProtocolType: [
		// {
		// 	code: '1',
		// 	description: i18n.t('dict.virtual_protocol_type.0.description')
		// },
		{
			code: '2',
			description: i18n.t('dict.virtual_protocol_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.virtual_protocol_type.2.description')
		}
	],
	proxyPatchAddAdjustType: [
		{
			code: '28',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.14.description'
			)
		},
		{
			code: '29',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.15.description'
			)
		},
		{
			code: '30',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.16.description'
			)
		},
		{
			code: '32',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.17.description'
			)
		},
		{
			code: '7',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.0.description'
			)
		},
		{
			code: '19',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.1.description'
			)
		},
		{
			code: '1',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.2.description'
			)
		},
		{
			code: '11',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.3.description'
			)
		},
		{
			code: '13',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.4.description'
			)
		},
		{
			code: '15',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.5.description'
			)
		},
		{
			code: '17',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.6.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.7.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.8.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.9.description'
			)
		},
		{
			code: '6',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.10.description'
			)
		},
		{
			code: '9',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.11.description'
			)
		},
		{
			code: '21',
			description: i18n.t('官方帮代理信用还款')
		},
		{
			code: '23',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.12.description'
			)
		},
		{
			code: '24',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.13.description'
			)
		}
	],
	dxproxyPatchAddAdjustType: [
		{
			code: '7',
			description: i18n.t(
				'dict.dx_proxy_patch_add_adjust_type.0.description'
			)
		},
		{
			code: '8',
			description: i18n.t(
				'dict.dx_proxy_patch_add_adjust_type.1.description'
			)
		},
		{
			code: '11',
			description: i18n.t(
				'dict.proxy_patch_add_adjust_type.2.description'
			)
		}
	],
	patchAdjustStatus: [
		{
			code: '1',
			description: i18n.t('dict.patch_adjust_status.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.patch_adjust_status.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.patch_adjust_status.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.patch_adjust_status.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.patch_adjust_status.4.description')
		},
		{
			code: '6',
			description: i18n.t('dict.patch_adjust_status.5.description')
		},
		{
			code: '7',
			description: i18n.t('dict.patch_adjust_status.6.description')
		}
	],
	contractType: [
		{
			code: '1',
			description: i18n.t('dict.contract_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.contract_type.1.description')
		}
	],
	userLoginStatus: [
		{
			code: '0',
			description: i18n.t('dict.user_login_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.user_login_status.1.description')
		}
	],
	depositOrderStatus: [
		// {
		// 	code: '0',
		// 	description: i18n.t('dict.deposit_order_status.0.description')
		// },
		{
			code: '1',
			description: i18n.t('dict.deposit_order_status.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.deposit_order_status.2.description')
		},
		{
			code: '3',
			description: i18n.t('dict.deposit_order_status.3.description')
		},
		{
			code: '4',
			description: i18n.t('dict.deposit_order_status.4.description')
		},
		{
			code: '5',
			description: i18n.t('dict.deposit_order_status.5.description')
		}
	],
	entrAuthorityType: [
		{
			code: '0',
			description: i18n.t('dict.entr_authority_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.entr_authority_type.1.description')
		}
	],
	// virtualRateAdjustType: [
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.virtual_rate_adjust_type.0.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.virtual_rate_adjust_type.1.description')
	// 	}
	// ],
	vipChangeType: [
		{
			code: '0',
			description: i18n.t('dict.vip_change_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.vip_change_type.1.description')
		}
	],
	language: [
		{
			code: 'en_US',
			description: i18n.t('dict.language.0.description')
		},
		{
			code: 'zh_CN',
			description: i18n.t('dict.language.1.description')
		},
		{
			code: 'vi_VN',
			description: i18n.t('dict.language.2.description')
		},
		{
			code: 'th_TH',
			description: i18n.t('dict.language.3.description')
		}
	],
	updMemberAuditStatus: [
		{
			code: '0',
			description: i18n.t('dict.upd_member_audit_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.upd_member_audit_status.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.upd_member_audit_status.3.description')
		},
		{
			code: '3',
			description: i18n.t('dict.upd_member_audit_status.2.description')
		}
	],
	merchantAuditStatus: [
		{
			code: '0',
			description: i18n.t('dict.upd_member_audit_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.upd_member_audit_status.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.upd_member_audit_status.2.description')
		},
		{
			code: '3',
			description: i18n.t('dict.upd_member_audit_status.3.description')
		}
	],
	proxyPayoutStatus: [
		{
			code: '0',
			description: i18n.t('dict.proxy_payout_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.proxy_payout_status.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.proxy_payout_status.2.description')
		},
		{
			code: '3',
			description: i18n.t('dict.proxy_payout_status.3.description')
		},
		{
			code: '4',
			description: i18n.t('dict.proxy_payout_status.4.description')
		},
		{
			code: '5',
			description: i18n.t('dict.proxy_payout_status.5.description')
		},
		{
			code: '6',
			description: i18n.t('dict.proxy_payout_status.6.description')
		}
	],
	// depositOrderClientStatus: [
	// 	{
	// 		code: '0',
	// 		description: i18n.t('dict.deposit_order_client_status.0.description')
	// 	},
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.deposit_order_client_status.1.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.deposit_order_client_status.2.description')
	// 	}
	// ],
	depositPaymentType: [
		{
			code: '1',
			description: i18n.t('dict.deposit_payment_type.0.description')
		},
		// {
		// 	code: '2',
		// 	description: i18n.t('dict.deposit_payment_type.1.description')
		// },
		{
			code: '2',
			description: i18n.t('dict.deposit_payment_type.2.description')
		}
		// {
		// 	code: '4',
		// 	description: i18n.t('dict.deposit_payment_type.3.description')
		// },
		// {
		// 	code: '5',
		// 	description: i18n.t('dict.deposit_payment_type.4.description')
		// },
		// {
		// 	code: '6',
		// 	description: i18n.t('dict.deposit_payment_type.5.description')
		// }
	],
	depositProxyPaymentType: [
		{
			code: '1',
			description: i18n.t('dict.deposit_payment_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.deposit_payment_type.2.description')
		},
		{
			code: '8',
			description: i18n.t('dict.deposit_payment_type.6.description')
		},
		{
			code: '9',
			description: i18n.t('dict.deposit_payment_type.7.description')
		}
	],
	depositMemberPaymentType: [
		{
			code: '1',
			description: i18n.t('dict.deposit_payment_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.deposit_payment_type.2.description')
		},
		{
			code: '8',
			description: i18n.t('dict.deposit_payment_type.6.description')
		},
		{
			code: '9',
			description: i18n.t('dict.deposit_payment_type.7.description')
		}
	],
	enumProxyDomainTypeOperate: [
		{
			code: '0',
			description: i18n.t(
				'dict.enum_proxy_domain_type_operate.0.description'
			)
		},
		{
			code: '1',
			description: i18n.t(
				'dict.enum_proxy_domain_type_operate.1.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.enum_proxy_domain_type_operate.2.description'
			)
		}
	],
	updMemberApplyType: [
		{
			code: '13',
			description: '手机号'
		},
		{
			code: '14',
			description: '安全校验'
		},
		{
			code: '15',
			description: '邮箱'
		},
		{
			code: '16',
			description: '身份验证器'
		},
		{
			code: '1',
			description: i18n.t('dict.upd_member_apply_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.upd_member_apply_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.upd_member_apply_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.upd_member_apply_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.upd_member_apply_type.4.description')
		},
		{
			code: '6',
			description: i18n.t('dict.upd_member_apply_type.5.description')
		},
		{
			code: '7',
			description: i18n.t('dict.upd_member_apply_type.6.description')
		},
		{
			code: '8',
			description: i18n.t('dict.upd_member_apply_type.7.description')
		},
		{
			code: '10',
			description: i18n.t('dict.upd_member_apply_type.8.description')
		},
		{
			code: '11',
			description: i18n.t('dict.upd_member_apply_type.9.description')
		},
		{
			code: '12',
			description: i18n.t('dict.upd_member_apply_type.10.description')
		}
	],
	configdomainStatus: [
		{
			code: '0',
			description: i18n.t('dict.configdomain_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.configdomain_status.1.description')
		}
	],
	loginDeviceType: [
		{
			code: '1',
			description: i18n.t('dict.login_device_type.0.description')
		},
		// {
		// 	code: '2',
		// 	description: i18n.t('dict.login_device_type.1.description')
		// },
		// {
		// 	code: '3',
		// 	description: i18n.t('dict.login_device_type.2.description')
		// },
		{
			code: '2',
			description: i18n.t('dict.login_device_type.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.login_device_type.4.description')
		}
		// {
		// 	code: '4',
		// 	description: i18n.t('dict.login_device_type.4.description')
		// },
		// {
		// 	code: '6',
		// 	description: 'Robot'
		// }
	],
	loginDeviceType2: [
		{
			code: '1',
			description: i18n.t('dict.login_device_type.0.description')
		},
		// {
		// 	code: '2',
		// 	description: i18n.t('dict.login_device_type.1.description')
		// },
		{
			code: '3',
			description: i18n.t('dict.login_device_type.2.description')
		},
		{
			code: '2',
			description: i18n.t('dict.login_device_type.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.login_device_type.4.description')
		},
		{
			code: '4',
			description: i18n.t('dict.login_device_type.3.description')
		},
		// {
		// 	code: '6',
		// 	description: 'Robot'
		// }
	],
	login_device_type1: [
		{
			code: '1',
			description: i18n.t('dict.login_device_type1.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.login_device_type1.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.login_device_type1.2.description')
		}
	],
	bwVenueList: [
		{
			code: 'by',
			description: i18n.t('dict.bw_venue_list.0.description')
		},
		{
			code: 'agby',
			description: i18n.t('dict.bw_venue_list.1.description')
		},
		{
			code: 'cp',
			description: i18n.t('dict.bw_venue_list.2.description')
		},
		{
			code: 'dj',
			description: i18n.t('dict.bw_venue_list.3.description')
		},
		{
			code: 'imone-dj',
			description: i18n.t('dict.bw_venue_list.4.description')
		},
		{
			code: 'dy',
			description: i18n.t('dict.bw_venue_list.5.description')
		},
		{
			code: 'pg',
			description: i18n.t('dict.bw_venue_list.6.description')
		},
		{
			code: 'cq9',
			description: i18n.t('dict.bw_venue_list.7.description')
		},
		{
			code: 'fc',
			description: i18n.t('dict.bw_venue_list.8.description')
		},
		{
			code: 'evo',
			description: i18n.t('dict.bw_venue_list.9.description')
		},
		{
			code: 'jdb-lhj',
			description: i18n.t('dict.bw_venue_list.10.description')
		},
		{
			code: 'byqp',
			description: i18n.t('dict.bw_venue_list.11.description')
		},
		{
			code: 'ty',
			description: i18n.t('dict.bw_venue_list.12.description')
		},
		{
			code: 'imone-sb',
			description: i18n.t('dict.bw_venue_list.13.description')
		},
		{
			code: 'sbty',
			description: i18n.t('dict.bw_venue_list.14.description')
		},
		{
			code: 'zr',
			description: i18n.t('dict.bw_venue_list.15.description')
		},
		{
			code: 'ebet',
			description: i18n.t('dict.bw_venue_list.16.description')
		},
		{
			code: 'agzr',
			description: i18n.t('dict.bw_venue_list.17.description')
		},
		{
			code: 'evolution',
			description: i18n.t('dict.bw_venue_list.18.description')
		},
		{
			code: 'hash',
			description: i18n.t('dict.bw_venue_list.19.description')
		},
		{
			code: 'mg',
			description: i18n.t('dict.bw_venue_list.20.description')
		}
	],
	clientType: [
		{
			code: '1',
			description: i18n.t('dict.client_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.client_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.client_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.client_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.client_type.4.description')
		},
		{
			code: '6',
			description: 'Robot'
		},
		{
			code: '7',
			description: '代理后台'
		},
		{
			code: '8',
			description: 'Robot'
		},
		{
			code: '9',
			description: i18n.t('dict.client_type.5.description')
		}
	],
	proxyAssistDepositType: [
		{
			code: '1',
			description: i18n.t('dict.proxy_assist_deposit_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.proxy_assist_deposit_type.1.description')
		}
	],
	materialPictureType: [
		{
			code: '1',
			description: i18n.t('dict.material_picture_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.material_picture_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.material_picture_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.material_picture_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.material_picture_type.4.description')
		},
		{
			code: '6',
			description: i18n.t('dict.material_picture_type.5.description')
		},
		{
			code: '7',
			description: i18n.t('dict.material_picture_type.6.description')
		}
	],
	// contractChangeType: [
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.contract_change_type.0.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.contract_change_type.1.description')
	// 	},
	// 	{
	// 		code: '3',
	// 		description: i18n.t('dict.contract_change_type.2.description')
	// 	}
	// ],
	receiveType: [
		{
			code: '1',
			description: i18n.t('dict.receive_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.receive_type.1.description')
		}
	],
	domainNameType: [
		{
			code: '1',
			description: i18n.t('dict.domain_name_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.domain_name_type.1.description')
		}
	],
	genderType: [
		{
			code: '0',
			description: i18n.t('dict.gender_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.gender_type.1.description')
		}
	],
	deviceType: [
		{
			code: '1',
			description: i18n.t('dict.device_type.0.description')
		},
		{
			code: '2',
			description: 'IOS'
		},
		{
			code: '5',
			description: 'Android'
		},
		{
			code: '6',
			description: i18n.t('dict.device_type.5.description')
		},
		{
			code: '7',
			description: i18n.t('dict.device_type.6.description')
		}
	],
	appVersionUpdateType: [
		{
			code: '1',
			description: i18n.t('dict.app_version_update_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.app_version_update_type.1.description')
		}
	],
	proxyPatchSubAdjustType: [
		{
			code: '8',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.0.description'
			)
		},
		{
			code: '20',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.1.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.2.description'
			)
		},
		{
			code: '12',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.3.description'
			)
		},
		{
			code: '14',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.4.description'
			)
		},
		{
			code: '16',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.5.description'
			)
		},
		{
			code: '18',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.6.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.7.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.8.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.9.description'
			)
		},
		{
			code: '6',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.10.description'
			)
		},
		{
			code: '10',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.11.description'
			)
		},
		{
			code: '22',
			description: '代理占成亏损分摊'
		},
		{
			code: '25',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.12.description'
			)
		},
		{
			code: '26',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.13.description'
			)
		},
		{
			code: '27',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.14.description'
			)
		},
		{
			code: '28',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.15.description'
			)
		},
		{
			code: '29',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.16.description'
			)
		},
		{
			code: '31',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.17.description'
			)
		},
		{
			code: '33',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.18.description'
			)
		}
	],
	dxproxyPatchSubAdjustType: [
		{
			code: '9',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.0.description'
			)
		},
		{
			code: '10',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.1.description'
			)
		},
		{
			code: '12',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.2.description'
			)
		},
		{
			code: '13',
			description: i18n.t(
				'dict.proxy_patch_sub_adjust_type.3.description'
			)
		}
	],
	accountType: [
		{
			code: '1',
			description: i18n.t('dict.account_type.0.description')
		}
	],
	reportAccountType: [
		{
			code: '1',
			description: i18n.t('dict.account_type.0.description')
		}
	],
	proxyRebateAccountType: [
		{
			code: '1',
			description: i18n.t('dict.proxy_rebate_account_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.proxy_rebate_account_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.proxy_rebate_account_type.2.description')
		}
	],
	configdomainType: [
		{
			code: '1',
			description: i18n.t('dict.configdomain_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.configdomain_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.configdomain_type.2.description')
		},
		{
			code: '5',
			description: i18n.t('dict.configdomain_type.4.description')
		},
		{
			code: '4',
			description: i18n.t('dict.configdomain_type.3.description')
		}
	],
	memberPatchAddAdjustType: [
		{
			code: '1',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.0.description'
			)
		},
		// {
		// 	code: '7',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.1.description'
		// 	)
		// },
		{
			code: '10',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.2.description'
			)
		},
		{
			code: '12',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.3.description'
			)
		},
		{
			code: '14',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.4.description'
			)
		},
		{
			code: '19',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.11.description'
			)
		},
		{
			code: '20',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.12.description'
			)
		},
		// {
		// 	code: '8',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.5.description'
		// 	)
		// },
		// {
		// 	code: '3',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.6.description'
		// 	)
		// },
		// {
		// 	code: '4',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.7.description'
		// 	)
		// },
		// {
		// 	code: '5',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.8.description'
		// 	)
		// },
		// {
		// 	code: '6',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_add_adjust_type.9.description'
		// 	)
		// },
		{
			code: '21',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.13.description'
			)
		},
		{
			code: '23',
			description: i18n.t(
				'dict.dx_member_patch_add_adjust_type.14.description'
			)
		}
	],
	rebateType: [
		{
			code: '0',
			description: i18n.t('dict.rebate_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.rebate_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.rebate_type.2.description')
		}
	],
	exclusiveDomain: [
		{
			code: '1',
			description: i18n.t('dict.exclusive_domain.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.exclusive_domain.1.description')
		}
	],
	exclusiveBind: [
		{
			code: '1',
			description: i18n.t('dict.exclusive_bind.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.exclusive_bind.1.description')
		}
	],
	blackStatusType: [
		{
			code: '0',
			description: i18n.t('dict.black_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.black_status_type.1.description')
		}
	],
	activityConfigActivityStatus: [
		{
			code: '0',
			description: i18n.t(
				'dict.activity_config_activity_status.0.description'
			)
		},
		{
			code: '1',
			description: i18n.t(
				'dict.activity_config_activity_status.1.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.activity_config_activity_status.2.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.activity_config_activity_status.3.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.activity_config_activity_status.4.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.activity_config_activity_status.5.description'
			)
		},
		{
			code: '6',
			description: i18n.t(
				'dict.activity_config_activity_status.6.description'
			)
		}
	],
	// announcementAgingType: [
	// 	{
	// 		code: '0',
	// 		description: i18n.t('dict.announcement_aging_type.0.description')
	// 	},
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.announcement_aging_type.1.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.announcement_aging_type.2.description')
	// 	}
	// ],
	transType: [
		{
			code: '1',
			description: i18n.t('dict.trans_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.trans_type.1.description')
		}
	],
	// memberChangeType: [
	// 	{
	// 		code: '11',
	// 		description: i18n.t('dict.member_change_type.0.description')
	// 	},
	// 	{
	// 		code: '22',
	// 		description: i18n.t('dict.member_change_type.1.description')
	// 	},
	// 	{
	// 		code: '12',
	// 		description: i18n.t('dict.member_change_type.2.description')
	// 	},
	// 	{
	// 		code: '23',
	// 		description: i18n.t('dict.member_change_type.3.description')
	// 	},
	// 	{
	// 		code: '13',
	// 		description: i18n.t('dict.member_change_type.4.description')
	// 	},
	// 	{
	// 		code: '24',
	// 		description: i18n.t('dict.member_change_type.5.description')
	// 	},
	// 	{
	// 		code: '14',
	// 		description: i18n.t('dict.member_change_type.6.description')
	// 	},
	// 	{
	// 		code: '15',
	// 		description: i18n.t('dict.member_change_type.7.description')
	// 	},
	// 	{
	// 		code: '16',
	// 		description: i18n.t('dict.member_change_type.8.description')
	// 	},
	// 	{
	// 		code: '17',
	// 		description: i18n.t('dict.member_change_type.9.description')
	// 	},
	// 	{
	// 		code: '18',
	// 		description: i18n.t('dict.member_change_type.10.description')
	// 	},
	// 	{
	// 		code: '19',
	// 		description: i18n.t('dict.member_change_type.11.description')
	// 	},
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.member_change_type.12.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.member_change_type.13.description')
	// 	},
	// 	{
	// 		code: '4',
	// 		description: i18n.t('dict.member_change_type.14.description')
	// 	},
	// 	{
	// 		code: '5',
	// 		description: i18n.t('dict.member_change_type.15.description')
	// 	},
	// 	{
	// 		code: '6',
	// 		description: i18n.t('dict.member_change_type.16.description')
	// 	},
	// 	{
	// 		code: '7',
	// 		description: i18n.t('dict.member_change_type.17.description')
	// 	},
	// 	{
	// 		code: '8',
	// 		description: i18n.t('dict.member_change_type.18.description')
	// 	},
	// 	{
	// 		code: '9',
	// 		description: i18n.t('dict.member_change_type.19.description')
	// 	},
	// 	{
	// 		code: '20',
	// 		description: i18n.t('dict.member_change_type.20.description')
	// 	},
	// 	{
	// 		code: '10',
	// 		description: i18n.t('dict.member_change_type.21.description')
	// 	},
	// 	{
	// 		code: '21',
	// 		description: i18n.t('dict.member_change_type.22.description')
	// 	}
	// ],
	participateType: [
		{
			code: '0',
			description: i18n.t('dict.participate_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.participate_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.participate_type.2.description')
		}
	],
	activityBonusOrderStatus: [
		{
			code: '0',
			description: i18n.t(
				'dict.activity_bonus_order_status.0.description'
			)
		},
		{
			code: '1',
			description: i18n.t(
				'dict.activity_bonus_order_status.4.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.activity_bonus_order_status.2.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.activity_bonus_order_status.5.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.activity_bonus_order_status.6.description'
			)
		}
	],
	proxyWalletType: [
		{
			code: '17',
			description: i18n.t('dict.proxy_wallet_type.0.description')
		},
		{
			code: '18',
			description: i18n.t('dict.proxy_wallet_type.1.description')
		},
		{
			code: '21',
			description: i18n.t('dict.proxy_wallet_type.2.description')
		}
	],
	memberWalletType: [
		{
			code: '1',
			description: i18n.t('dict.proxy_wallet_type.0.description')
		},
		{
			code: '9',
			description: i18n.t('dict.proxy_wallet_type.1.description')
		}
	],
	// sendType: [
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.send_type.0.description')
	// 	}
	// ],
	withdrawThirdMessageStatus: [
		{
			code: '1',
			description: i18n.t(
				'dict.withdraw_third_message_status.0.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.withdraw_third_message_status.1.description'
			)
		}
	],
	proxyWaterRebateStatusNew: [
		{
			code: '2',
			description: i18n.t(
				'dict.proxy_water_rebate_status_new.0.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.proxy_water_rebate_status_new.1.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.proxy_water_rebate_status_new.2.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.proxy_water_rebate_status_new.3.description'
			)
		},
		{
			code: '6',
			description: i18n.t(
				'dict.proxy_water_rebate_status_new.4.description'
			)
		}
	],
	paymentCurrencyType: [
		{
			code: 'CNY',
			description: i18n.t('dict.payment_currency_type.0.description')
		},
		// {
		// 	code: 'VND',
		// 	description: i18n.t('dict.payment_currency_type.1.description')
		// },
		// {
		// 	code: 'THB',
		// 	description: i18n.t('dict.payment_currency_type.2.description')
		// },
		{
			code: 'USDT',
			description: i18n.t('dict.payment_currency_type.3.description')
		}
	],
	userType: [
		{
			code: '0',
			description: i18n.t('dict.user_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.user_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.user_type.2.description')
		},
		{
			code: '3',
			description: i18n.t('dict.user_type.3.description')
		}
	],
	loginStatusType: [
		{
			code: '0',
			description: i18n.t('dict.login_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.login_status_type.1.description')
		}
	],
	bwVenueTypeList: [
		{
			code: 'DZ_CARD',
			description: '德州'
		},
		{
			code: 'ty',
			description: i18n.t('dict.bw_venue_type_list.0.description')
		},
		{
			code: 'zr',
			description: i18n.t('dict.bw_venue_type_list.1.description')
		},
		{
			code: 'dj',
			description: i18n.t('dict.bw_venue_type_list.2.description')
		},
		{
			code: 'qp',
			description: i18n.t('dict.bw_venue_type_list.3.description')
		},
		{
			code: 'cp',
			description: i18n.t('dict.bw_venue_type_list.4.description')
		},
		{
			code: 'dy',
			description: i18n.t('dict.bw_venue_type_list.5.description')
		}
		// {
		// 	code: 'by',
		// 	description: i18n.t('dict.bw_venue_type_list.6.description')
		// },
		// {
		// 	code: 'qkl',
		// 	description: i18n.t('dict.bw_venue_type_list.7.description')
		// },
		// 二期不支持
		// {
		// 	code: 'byqp',
		// 	description: '博雅棋牌'
		// }
	],
	welfareType: [
		{
			code: '1',
			description: i18n.t('dict.welfare_type.0.description')
		}
	],
	awardType: [
		{
			code: '1',
			description: i18n.t('dict.award_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.award_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.award_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.award_type.3.description')
		}
	],
	windLevelType: [
		{
			code: '1',
			description: i18n.t('dict.wind_level_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.wind_level_type.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.wind_level_type.2.description')
		},
		{
			code: '6',
			description: i18n.t('dict.wind_level_type.3.description')
		},
		{
			code: '3',
			description: i18n.t('dict.wind_level_type.4.description')
		},
		{
			code: '4',
			description: i18n.t('dict.wind_level_type.5.description')
		},
		{
			code: '7',
			description: i18n.t('dict.wind_level_type.6.description')
		},
		{
			code: '8',
			description: i18n.t('dict.wind_level_type.7.description')
		}
		// {
		// 	code: '7',
		// 	description: i18n.t('dict.wind_level_type.6.description')
		// }
	],
	withdrawOrderClientStatus: [
		{
			code: '0',
			description: i18n.t(
				'dict.withdraw_order_client_status.0.description'
			)
		},
		{
			code: '1',
			description: i18n.t(
				'dict.withdraw_order_client_status.1.description'
			)
		},
		{
			code: '2',
			description: i18n.t(
				'dict.withdraw_order_client_status.2.description'
			)
		}
	],
	withdrawClientStatus: [
		{
			code: '0',
			description: i18n.t('dict.member_transfer_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.member_transfer_status.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.member_transfer_status.2.description')
		}
	],
	depositClientStatus: [
		{
			code: '2',
			description: i18n.t(
				'dict.withdraw_order_client_status.0.description'
			)
		},
		{
			code: '3',
			description: i18n.t(
				'dict.withdraw_order_client_status.2.description'
			)
		},
		{
			code: '4',
			description: i18n.t(
				'dict.withdraw_order_client_status.3.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.withdraw_order_client_status.4.description'
			)
		}
	],
	currencyType: [
		{
			code: 'CNY',
			description: i18n.t('dict.currencyType.0.description')
		},
		{
			code: 'USDT',
			description: i18n.t('dict.currencyType.1.description')
		}
	],
	withdrawTypeStr: [
		{
			code: '1',
			description: i18n.t('dict.withdrawTypeStr.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.withdrawTypeStr.1.description')
		},
		{
			code: '8',
			description: i18n.t('dict.withdrawTypeStr.2.description')
		},
		{
			code: '9',
			description: i18n.t('dict.withdrawTypeStr.3.description')
		}
	],
	appVersionStatus: [
		{
			code: '0',
			description: i18n.t('dict.app_version_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.app_version_status.1.description')
		}
	],
	betStatusType: [
		{
			code: '0',
			description: i18n.t('dict.bet_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.bet_status_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.bet_status_type.2.description')
		}
	],
	appVersionClientType: [
		{
			code: '1',
			description: i18n.t('dict.app_version_client_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.app_version_client_type.1.description')
		}
	],
	pictureSizeType: [
		{
			code: '1',
			description: i18n.t('dict.picture_size_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.picture_size_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.picture_size_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.picture_size_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.picture_size_type.4.description')
		}
	],
	effectStatusType: [
		{
			code: '0',
			description: i18n.t('dict.effect_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.effect_status_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.effect_status_type.2.description')
		}
	],
	memberPatchSubAdjustType: [
		// {
		// 	code: '9',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.0.description'
		// 	)
		// },
		{
			code: '2',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.1.description'
			)
		},
		{
			code: '11',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.2.description'
			)
		},
		{
			code: '13',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.3.description'
			)
		},
		{
			code: '15',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.4.description'
			)
		},
		{
			code: '16',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.10.description'
			)
		},
		// {
		// 	code: '17',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.11.description'
		// 	)
		// },
		// {
		// 	code: '18',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.12.description'
		// 	)
		// },
		{
			code: '22',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.13.description'
			)
		},
		{
			code: '24',
			description: i18n.t(
				'dict.dx_member_patch_sub_adjust_type.14.description'
			)
		}
		// {
		// 	code: '3',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.5.description'
		// 	)
		// },
		// {
		// 	code: '4',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.6.description'
		// 	)
		// },
		// {
		// 	code: '5',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.7.description'
		// 	)
		// },
		// {
		// 	code: '6',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.8.description'
		// 	)
		// },
		// {
		// 	code: '7',
		// 	description: i18n.t(
		// 		'dict.dx_member_patch_sub_adjust_type.9.description'
		// 	)
		// }
	],
	taskType: [
		{
			code: '1',
			description: i18n.t('dict.task_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.task_type.1.description')
		}
	],
	lockStatus: [
		{
			code: '0',
			description: i18n.t('dict.lock_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.lock_status.1.description')
		}
	],
	paymentChannelStatus: [
		{
			code: '0',
			description: i18n.t('dict.payment_channel_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.payment_channel_status.1.description')
		}
	],
	currency: [
		{
			code: 'CNY',
			description: i18n.t('dict.currency.0.description')
		},
		{
			code: 'VND',
			description: i18n.t('dict.currency.1.description')
		},
		{
			code: 'THB',
			description: i18n.t('dict.currency.2.description')
		}
	],
	contractModule: [
		{
			code: '1',
			description: i18n.t('dict.contract_module.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.contract_module.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.contract_module.2.description')
		}
	],
	// activityTypeTwo: [
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.activity_type_two.0.description')
	// 	},
	// 	{
	// 		code: '4',
	// 		description: i18n.t('dict.activity_type_two.1.description')
	// 	}
	// ],
	domainStatusType: [
		{
			code: '0',
			description: i18n.t('dict.domain_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.domain_status_type.1.description')
		}
	],
	patchAdjustStatusFinish: [
		{
			code: '5',
			description: i18n.t('dict.patch_adjust_status_finish.0.description')
		},
		{
			code: '6',
			description: i18n.t('dict.patch_adjust_status_finish.1.description')
		},
		{
			code: '7',
			description: i18n.t('dict.patch_adjust_status_finish.2.description')
		}
	],
	merchantStatus: [
		{
			code: '0',
			description: i18n.t('dict.merchant_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.merchant_status.1.description')
		}
	],
	gameType: [
		{
			code: 2001,
			description: i18n.t('dict.game_type.0.description')
		},
		{
			code: 2002,
			description: i18n.t('dict.game_type.1.description')
		},
		{
			code: 2003,
			description: 'AOF德州'
		},
		{
			code: 2004,
			description: 'AOF短牌'
		}
	],
	gameTypes: [
		{
			code: 1,
			description: i18n.t('dict.game_type.0.description')
		},
		{
			code: 2,
			description: i18n.t('dict.game_type.1.description')
		},
		{
			code: 3,
			description: 'AOF德州'
		},
		{
			code: 4,
			description: 'AOF短牌'
		}
	],
	gameDeviceType: [
		{
			code: '1',
			description: i18n.t('dict.game_device_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.game_device_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.game_device_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.game_device_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.game_device_type.4.description')
		},
		{
			code: '6',
			description: i18n.t('dict.game_device_type.5.description')
		}
	],
	memberFeedBackType: [
		{
			code: '0',
			description: i18n.t('dict.member_feed_back_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.member_feed_back_type.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.member_feed_back_type.2.description')
		},
		{
			code: '3',
			description: i18n.t('dict.member_feed_back_type.3.description')
		},
		{
			code: '4',
			description: i18n.t('dict.member_feed_back_type.4.description')
		},
		{
			code: '5',
			description: i18n.t('dict.member_feed_back_type.5.description')
		},
		{
			code: '6',
			description: i18n.t('dict.member_feed_back_type.6.description')
		},
		{
			code: '7',
			description: i18n.t('dict.member_feed_back_type.7.description')
		}
	],
	accountStatusType: [
		{
			code: '1',
			description: i18n.t('dict.account_status_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.account_status_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.account_status_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.account_status_type.3.description')
		}
	],
	accountStatusType2: [
		{
			code: '1',
			description: i18n.t('dict.account_status_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.account_status_type.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.account_status_type.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.account_status_type.3.description')
		},
		{
			code: '5',
			description: i18n.t('dict.account_status_type.4.description')
		}
	],
	currencySymbol: [
		{
			code: 'CNY',
			description: i18n.t('dict.currency_symbol.0.description')
		}
		// {
		// 	code: 'VND',
		// 	description: i18n.t('dict.currency_symbol.1.description')
		// },
		// {
		// 	code: 'THB',
		// 	description: i18n.t('dict.currency_symbol.2.description')
		// }
	],
	// activityConfigInTypeUser: [
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.activity_config_in_type_user.0.description')
	// 	},
	// 	{
	// 		code: '2',
	// 		description: i18n.t('dict.activity_config_in_type_user.1.description')
	// 	},
	// 	{
	// 		code: '3',
	// 		description: i18n.t('dict.activity_config_in_type_user.2.description')
	// 	}
	// ],
	auditStepType: [
		{
			code: '0',
			description: i18n.t('dict.audit_step_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.audit_step_type.1.description')
		}
	],
	sendPosition: [
		{
			code: '1',
			description: i18n.t('dict.send_position.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.send_position.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.send_position.2.description')
		}
	],
	userActive: [
		{
			code: '0',
			description: i18n.t('dict.user_active.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.user_active.1.description')
		}
	],
	proxySettlementStatus: [
		{
			code: '1',
			description: i18n.t('dict.proxy_settlement_status.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.proxy_settlement_status.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.proxy_settlement_status.2.description')
		}
	],
	auditStatus: [
		{
			code: '0',
			description: i18n.t('dict.audit_status.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.audit_status.1.description')
		},
		{
			code: '7',
			description: i18n.t('dict.audit_status.2.description')
		},
		{
			code: '14',
			description: i18n.t('dict.audit_status.1.description')
		},
		{
			code: '11',
			description: i18n.t('dict.audit_status.4.description')
		},
		{
			code: '12',
			description: i18n.t('dict.audit_status.5.description')
		},
		{
			code: '13',
			description: i18n.t('dict.audit_status.6.description')
		}
	],
	agentAuditStatus: [
		{ code: '0', description: '待处理' },
		{ code: '1', description: '处理中' },
		{ code: '2', description: '审核通过' },
		{ code: '3', description: '审核拒绝' }
	],
	memberWithdrawalAuditStatus: [
		{
			code: '1',
			description: i18n.t('dict.audit_status.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.audit_status.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.audit_status.2.description')
		},
		{
			code: '4',
			description: i18n.t('dict.audit_status.3.description')
		}
	],
	withdrawPaymentType: [
		{
			code: '1',
			description: i18n.t('dict.withdraw_payment_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.withdraw_payment_type.1.description')
		},
		{
			code: '8',
			description: i18n.t('dict.withdraw_payment_type.2.description')
		},
		{
			code: '9',
			description: i18n.t('dict.withdraw_payment_type.3.description')
		}
	],
	withdrawMemberPaymentType: [
		{
			code: '1',
			description: i18n.t('dict.withdraw_payment_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.withdraw_payment_type.1.description')
		},
		{
			code: '8',
			description: i18n.t('dict.withdraw_payment_type.2.description')
		},
		{
			code: '9',
			description: i18n.t('dict.withdraw_payment_type.3.description')
		}
	],
	withdrawPaymentTypeArr: [
		{
			code: '1',
			description: i18n.t('dict.withdraw_payment_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.withdraw_payment_type.1.description')
		},
		{
			code: '99',
			description: '后台取款'
		},
		{
			code: '8',
			description: i18n.t('dict.withdraw_payment_type.2.description')
		},
		{
			code: '9',
			description: i18n.t('dict.withdraw_payment_type.3.description')
		}
	],
	// memberBizType: [
	// 	{
	// 		code: '11',
	// 		description: i18n.t('dict.member_biz_type.0.description')
	// 	},
	// 	{
	// 		code: '1',
	// 		description: i18n.t('dict.member_biz_type.1.description')
	// 	},
	// 	{
	// 		code: '3',
	// 		description: i18n.t('dict.member_biz_type.2.description')
	// 	},
	// 	{
	// 		code: '4',
	// 		description: i18n.t('dict.member_biz_type.3.description')
	// 	},
	// 	{
	// 		code: '5',
	// 		description: i18n.t('dict.member_biz_type.4.description')
	// 	},
	// 	{
	// 		code: '6',
	// 		description: i18n.t('dict.member_biz_type.5.description')
	// 	},
	// 	{
	// 		code: '7',
	// 		description: i18n.t('dict.member_biz_type.6.description')
	// 	},
	// 	{
	// 		code: '8',
	// 		description: i18n.t('dict.member_biz_type.7.description')
	// 	},
	// 	{
	// 		code: '9',
	// 		description: i18n.t('dict.member_biz_type.8.description')
	// 	},
	// 	{
	// 		code: '10',
	// 		description: i18n.t('dict.member_biz_type.9.description')
	// 	}
	// ],
	memberBizType: [
		{
			code: '12',
			description: i18n.t('dict.dx_member_biz_type.0.description')
		},
		{
			code: '13',
			description: i18n.t('dict.dx_member_biz_type.1.description')
		},
		{
			code: '4',
			description: i18n.t('dict.dx_member_biz_type.2.description')
		},
		{
			code: '6',
			description: i18n.t('dict.dx_member_biz_type.3.description')
		},
		{
			code: '17',
			description: i18n.t('dict.dx_member_biz_type.4.description')
		},
		{
			code: '18',
			description: i18n.t('dict.dx_member_biz_type.5.description')
		},
		{
			code: '14',
			description: i18n.t('dict.dx_member_biz_type.6.description')
		},
		{
			code: '15',
			description: i18n.t('dict.dx_member_biz_type.7.description')
		},
		{
			code: '16',
			description: i18n.t('dict.dx_member_biz_type.8.description')
		}
	],
	memberTransferStatus: [
		{
			code: '1',
			description: i18n.t('dict.member_transfer_status.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.member_transfer_status.1.description')
		},
		{
			code: '3',
			description: i18n.t('dict.member_transfer_status.2.description')
		}
	],
	activityType: [
		{
			code: '1',
			description: i18n.t('dict.activity_type.0.description')
		},
		{
			code: '3',
			description: i18n.t('dict.activity_type.1.description')
		},
		{
			code: '4',
			description: i18n.t('dict.activity_type.2.description')
		}
	],
	contractStatusType: [
		{
			code: '0',
			description: i18n.t('dict.contract_status_type.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.contract_status_type.1.description')
		}
	],
	withdrawOrderStatus: [
		{
			code: '7',
			description: i18n.t('dict.withdraw_order_status.9.description')
		},
		{
			code: '14',
			description: i18n.t('dict.withdraw_order_status.13.description')
		},
		{
			code: '11',
			description: i18n.t('dict.withdraw_order_status.0.description')
		},
		{
			code: '12',
			description: i18n.t('dict.withdraw_order_status.1.description')
		},
		{
			code: '13',
			description: i18n.t('dict.withdraw_order_status.2.description')
		},
		{
			code: '1',
			description: i18n.t('dict.withdraw_order_status.3.description')
		},
		{
			code: '2',
			description: i18n.t('dict.withdraw_order_status.4.description')
		},
		{
			code: '8',
			description: i18n.t('dict.withdraw_order_status.10.description')
		},
		{
			code: '3',
			description: i18n.t('dict.withdraw_order_status.5.description')
		},
		{
			code: '4',
			description: i18n.t('dict.withdraw_order_status.6.description')
		},
		{
			code: '9',
			description: i18n.t('dict.withdraw_order_status.11.description')
		},
		{
			code: '5',
			description: i18n.t('dict.withdraw_order_status.7.description')
		},
		{
			code: '6',
			description: i18n.t('dict.withdraw_order_status.8.description')
		},
		{
			code: '10',
			description: i18n.t('dict.withdraw_order_status.12.description')
		}
	],
	contractTemplateType: [
		{
			code: '1',
			description: i18n.t('dict.contract_template_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.contract_template_type.1.description')
		}
	],
	updProxyApplyType: [
		{
			code: '1',
			description: i18n.t('dict.upd_proxy_apply_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.upd_proxy_apply_type.1.description')
		}
	],
	personConfigFoundModule: [
		{
			code: '11',
			description: i18n.t('dict.person_config_found_module.0.description')
		},
		// {
		// 	code: '22',
		// 	description: i18n.t('dict.person_config_found_module.1.description')
		// },
		{
			code: '23',
			description: i18n.t('dict.person_config_found_module.2.description')
		},
		// {
		// 	code: '2',
		// 	description: i18n.t('dict.person_config_found_module.3.description')
		// },
		{
			code: '3',
			description: i18n.t('dict.person_config_found_module.4.description')
		},
		// {
		// 	code: '4',
		// 	description: i18n.t('dict.person_config_found_module.5.description')
		// },
		{
			code: '5',
			description: i18n.t('dict.person_config_found_module.6.description')
		}
		// {
		// 	code: '18',
		// 	description: i18n.t('dict.person_config_found_module.7.description')
		// },
		// {
		// 	code: '19',
		// 	description: i18n.t('dict.person_config_found_module.8.description')
		// },
		// {
		// 	code: '21',
		// 	description: i18n.t('dict.person_config_found_module.9.description')
		// }
	],
	personConfigMemberModule: [
		{
			code: '3',
			description: i18n.t(
				'dict.person_config_member_module.0.description'
			)
		},
		{
			code: '5',
			description: i18n.t(
				'dict.person_config_member_module.1.description'
			)
		}
	],
	personConfigProxyModule: [
		// {
		// 	code: '13',
		// 	description: i18n.t('dict.person_config_proxy_module.0.description')
		// },
		// {
		// 	code: '14',
		// 	description: i18n.t('dict.person_config_proxy_module.1.description')
		// },
		// {
		// 	code: '15',
		// 	description: i18n.t('dict.person_config_proxy_module.2.description')
		// },
		// {
		// 	code: '16',
		// 	description: i18n.t('dict.person_config_proxy_module.3.description')
		// },
		// {
		// 	code: '6',
		// 	description: i18n.t('dict.person_config_proxy_module.4.description')
		// },
		// {
		// 	code: '7',
		// 	description: i18n.t('dict.person_config_proxy_module.5.description')
		// },
		// {
		// 	code: '8',
		// 	description: i18n.t('dict.person_config_proxy_module.6.description')
		// },
		{
			code: '9',
			description: i18n.t('dict.person_config_proxy_module.7.description')
		},
		{
			code: '10',
			description: i18n.t('dict.person_config_proxy_module.8.description')
		},
		{
			code: '24',
			description: '体育盘口返点限制:A盘'
		},
		{
			code: '25',
			description: '体育盘口返点限制:B盘'
		},
		{
			code: '26',
			description: '体育盘口返点限制:C盘'
		},
		{
			code: '27',
			description: '体育盘口返点限制:D盘'
		}
	],
	personConfigModule: [
		// {
		// 	code: '1',
		// 	description: i18n.t('dict.person_config_module.0.description')
		// },
		// {
		// 	code: '12',
		// 	description: i18n.t('dict.person_config_module.1.description')
		// },
		// {
		// 	code: '24',
		// 	description: i18n.t('dict.person_config_module.2.description')
		// },
		// {
		// 	code: '17',
		// 	description: i18n.t('dict.person_config_module.3.description')
		// }
	],
	sysMsgMemberModule: [
		{
			code: '1',
			description: i18n.t('dict.sys_msg_member_module.0.description')
		},
		{
			code: '4',
			description: i18n.t('dict.sys_msg_member_module.1.description')
		},
		{
			code: '15',
			description: i18n.t('dict.sys_msg_member_module.2.description')
		},
		{
			code: '16',
			description: '取款通知'
		},
		{
			code: '17',
			description: i18n.t('dict.sys_msg_member_module.4.description')
		},
		{
			code: '10',
			description: '存款通知'
		},
		{
			code: '27',
			description: '代理帮信用还款通知'
		}
	],
	sysMsgProxyModule: [
		{
			code: '12',
			description: i18n.t('dict.sys_msg_proxy_module.0.description')
		},
		{
			code: '14',
			description: i18n.t('dict.sys_msg_proxy_module.1.description')
		},
		{
			code: '13',
			description: i18n.t('dict.sys_msg_proxy_module.2.description')
		},
		{
			code: '8',
			description: i18n.t('dict.sys_msg_proxy_module.3.description')
		},
		{
			code: '11',
			description: i18n.t('dict.sys_msg_proxy_module.4.description')
		},
		{
			code: '18',
			description: i18n.t('dict.sys_msg_proxy_module.5.description')
		},
		{
			code: '19',
			description: i18n.t('dict.sys_msg_proxy_module.6.description')
		},
		{
			code: '20',
			description: i18n.t('dict.sys_msg_proxy_module.7.description')
		},
		{
			code: '21',
			description: i18n.t('dict.sys_msg_proxy_module.8.description')
		},

		{
			code: '22',
			description: '返佣比例已设置'
		},
		{
			code: '23',
			description: '返佣比例调整'
		},
		{
			code: '24',
			description: '返佣奖励'
		},
		{
			code: '25',
			description: '取款通知'
		},
		{
			code: '26',
			description: '存款通知'
		},
		{
			code: '30',
			description: '会员转出'
		},
		{
			code: '31',
			description: '会员转入'
		},
		{
			code: '32',
			description: '代理线转出'
		},
		{
			code: '33',
			description: '代理升级'
		},
		{
			code: '36',
			description: '净资产告警'
		},
		{
			code: '37',
			description: '净资产锁定'
		},
		{
			code: '38',
			description: '净资产解锁'
		},
		{
			code: '39',
			description: '新现金占成账单通知'
		}
	],
	paymentMerchantType: [
		{
			code: '1',
			description: i18n.t('dict.payment_merchant_type.0.description')
		},
		{
			code: '2',
			description: i18n.t('dict.payment_merchant_type.1.description')
		}
	],
	timeOptionList: [
		{
			code: '0',
			description: '0'
		},
		{
			code: '1',
			description: '1'
		},
		{
			code: '2',
			description: '2'
		},
		{
			code: '3',
			description: '3'
		},
		{
			code: '4',
			description: '4'
		},
		{
			code: '5',
			description: '5'
		},
		{
			code: '6',
			description: '6'
		},
		{
			code: '7',
			description: '7'
		},
		{
			code: '8',
			description: '8'
		},
		{
			code: '9',
			description: '9'
		},
		{
			code: '10',
			description: '10'
		},
		{
			code: '11',
			description: '11'
		},
		{
			code: '12',
			description: '12'
		},
		{
			code: '13',
			description: '13'
		},
		{
			code: '14',
			description: '14'
		},
		{
			code: '15',
			description: '15'
		},
		{
			code: '16',
			description: '16'
		},
		{
			code: '17',
			description: '17'
		},
		{
			code: '18',
			description: '18'
		},
		{
			code: '19',
			description: '19'
		},
		{
			code: '20',
			description: '20'
		},
		{
			code: '21',
			description: '21'
		},
		{
			code: '22',
			description: '22'
		},
		{
			code: '23',
			description: '23'
		}
	],
	proxyLevelList: [
		{
			code: '0',
			description: i18n.t('dict.proxy_level_list.0.description')
		},
		{
			code: '1',
			description: i18n.t('dict.proxy_level_list.1.description')
		},
		{
			code: '2',
			description: i18n.t('dict.proxy_level_list.2.description')
		},
		{
			code: '99',
			description: i18n.t('dict.proxy_level_list.3.description')
		}
	],
	rebateArrList: [
		{
			code: '1',
			description: i18n.t('dict.proxy_rebate_status.0.description')
		},
		{
			code: '3',
			description: i18n.t('dict.proxy_rebate_status.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.proxy_rebate_status.2.description')
		},
		{
			code: '6',
			description: i18n.t('dict.proxy_rebate_status.3.description')
		}
	],
	rebateClientStatusList: [
		{
			code: '3',
			description: i18n.t('dict.proxy_rebate_status.1.description')
		},
		{
			code: '5',
			description: i18n.t('dict.proxy_rebate_status.2.description')
		},
		{
			code: '6',
			description: i18n.t('dict.proxy_rebate_status.3.description')
		}
	],
	recycleModelTypeArr: [
		{
			code: 2,
			description: i18n.t('dict.recycle_model.1.description')
		}
	],
	recycleUserTypeList: [
		{
			code: 0,
			description: i18n.t('dict.recycle_user_type.1.description')
		},
		{
			code: 1,
			description: i18n.t('dict.recycle_user_type.2.description')
		}
	],
	businessModelTypeArr: [
		{
			code: 1,
			description: '现金业务'

		},
		{
			code: 2,
			description: '信用业务'
		}
	]
}
