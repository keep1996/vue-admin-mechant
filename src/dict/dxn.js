// 俱乐部开桌规则-保险模式
export const insuranceModeMap = {
	'0': '不开保险',
	'1': '经典保险',
	'2': 'EV保险'
}

// 牌桌列表-保险模式映射
export const insuranceModeArr = [
	{
		code: 1,
		description: '经典保险'
	},
	{
		code: 2,
		description: 'EV保险'
	}
]

// 俱乐部开桌规则-每手服务费-类型
export const serviceChargeTypeArr = [
	{
		code: '1',
		description: '按底池比例'
	},
	{
		code: '2',
		description: '按盈利比例'
	},
	{
		code: '3',
		description: '无'
	}
]

// 俱乐部开桌规则-每手服务费-类型映射
export const serviceChargeMap = {
	'0': [],
	'1': ['1'],
	'2': ['2'],
	'3': ['1', '2']
}

// 保险列表-购买详情-获取outs类型
export const defaultOutsType = '反超Outs'
export const outsTypeMap = new Map([
	[1, '反超Outs'],
	[2, '平分Outs']
])
export const getOutsTypeName = (val) => {
	return outsTypeMap.get(val) || defaultOutsType
}
// 俱乐部开桌规则-鱿鱼游戏-触发方式
export const squidTriggerModeArr = [
	{
		code: 0,
		description: '无限触发'
	},
	{
		code: 1,
		description: '手数触发'
	}
]
// 俱乐部开桌规则-鱿鱼游戏-奖励模式
export const squidRewardModelArr = [
	{
		code: 0,
		description: '普通鱿鱼'
	},
	{
		code: 1,
		description: '无限鱿鱼'
	},
	{
		code: 2,
		description: '疯狂鱿鱼'
	}
]

// 鱿鱼管理-鱿鱼列表-结算方式
export const squidSettleTypeArr = [
	{
		code: 1,
		description: '进行中'
	},
	{
		code: 2,
		description: '正常结算'
	},
	{
		code: 3,
		description: '认输结算'
	},
	{
		code: 4,
		description: '结算异常'
	}
]

// 鱿鱼管理-鱿鱼钱包账变-账变类型
export const squidWalletChangeTypes = [
	{
		code: 50,
		description: '带入鱿鱼钱包'
	},
	{
		code: 51,
		description: '带出鱿鱼钱包'
	},
	{
		code: 53,
		description: '鱿鱼钱包支付'
	},
	{
		code: 52,
		description: '牌桌划入'
	}
]

// 鱿鱼管理-鱿鱼钱包账变-收支类型
export const squidWalletChangeDirections = [
	{
		code: 1,
		description: '收入'
	},
	{
		code: 2,
		description: '支出'
	}
]

// 牌桌详情/手牌详情-特殊玩法
export const specialPlayTypeArr = [
	{
		code: 1,
		description: '鱿鱼游戏'
	}
]

// 牌桌列表-牌桌详情-牌桌状态
export const cardTableStatusArr = [
	{
		code: 0,
		description: '未开始'
	},
	{
		code: 1,
		description: '进行中'
	},
	{
		code: -1,
		description: '已结束'
	},
	{
		code: 2,
		description: '已结束'
	}
]

// 会员手牌列表-牌型
export const cardTypeArr = [
	{
		code: 'POKER10',
		description: '高牌'
	},
	{
		code: 'POKER9',
		description: '一对'
	},
	{
		code: 'POKER8',
		description: '两对'
	},
	{
		code: 'POKER7',
		description: '三条'
	},
	{
		code: 'POKER6',
		description: '顺子'
	},
	{
		code: 'POKER5',
		description: '同花'
	},
	{
		code: 'POKER4',
		description: '葫芦'
	},
	{
		code: 'POKER3',
		description: '四条'
	},
	{
		code: 'POKER2',
		description: '同花顺'
	},
	{
		code: 'POKER1',
		description: '皇家同花顺'
	}
]
