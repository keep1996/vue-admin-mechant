
// 会员返水-派发状态
export const bounsPayoutStatusMap = {
	'0': '待发放',
	'1': '待发放',
	'2': '待发放',
	'3': '已发放',
	'4': '已过期',
	'5': '无返水',
	'6': '已取消'
}

// 代理返佣-派发状态
export const commissionPayoutStatus = [
	{
		code: '0',
		description: '待发放'
	},
	{
		code: '1',
		description: '待发放'
	},
	{
		code: '2',
		description: '待发放'
	},
	{
		code: '3',
		description: '已发放'
	},
	{
		code: '4',
		description: '已过期'
	},
	{
		code: '6',
		description: '已取消'
	},
	{
		code: '5',
		description: '无返佣'
	}
]

// 代理返佣-派发状态(非总代搜索)
export const searchCommissionPayoutStatus = [
	{
		code: '1',
		description: '待发放'
	},
	{
		code: '3',
		description: '已发放'
	},
	{
		code: '6',
		description: '已取消'
	},
	{
		code: '5',
		description: '无返佣'
	}
]

// 代理返佣-返佣审核详情-团队返佣明细-项目
export const commissionSettlementName = [
	{
		code: '1',
		description: '德州-俱乐部'
	},
	{
		code: '2',
		description: '德州-保险'
	},
	{
		code: '3',
		description: '其他场馆'
	}
]
