/* eslint-disable no-unused-vars */
import dayjs from 'dayjs'
import i18n from '@/locales'
import store from '@/store'

export const rangeLastNMinutes = (n, t) => ({
	t: t,
	range: [dayjs().subtract(n, 'minute'), dayjs()]
})

export const rangeLastNHours = (n, t) => ({
	t: t,
	range: [dayjs().subtract(n, 'hour'), dayjs()]
})

export const rangeToday = () => ({
	t: i18n.t('common.utils.today'),
	range: [dayjs().startOf('day'), dayjs().endOf('day')]
})

export const rangeYesterday = () => ({
	t: i18n.t('common.utils.yesterday'),
	range: [
		dayjs()
			.startOf('day')
			.subtract(1, 'd'),
		dayjs()
			.endOf('day')
			.subtract(1, 'd')
	]
})
export const rangeCurrentWeek = () => ({
	t: i18n.t('common.utils.week'),
	range: [
		dayjs().day()
			? dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			: dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd'),
		dayjs().endOf('day')
	]
})

export const rangeLastWeek = () => ({
	t: i18n.t('common.utils.last_week'),
	range: dayjs().day()
		? [
				dayjs()
					.subtract(2, 'week')
					.startOf('week')
					.add(1, 'day'),
				dayjs()
					.subtract(2, 'week')
					.endOf('week')
					.add(1, 'day')
		  ]
		: [
				dayjs()
					.startOf('week')
					.subtract(1, 'week')
					.subtract(-1, 'day'),
				dayjs()
					.subtract(1, 'week')
					.endOf('week')
					.subtract(-1, 'day')
		  ]
})

export const rangeCurrentMonth = () => ({
	t: i18n.t('common.utils.month_now'),
	range: [dayjs().startOf('month'), dayjs().endOf('day')]
})

export const rangeLastMonth = () => ({
	t: i18n.t('common.utils.last_month'),
	range: [
		dayjs()
			.startOf('month')
			.subtract(1, 'month'),
		dayjs()
			.subtract(1, 'month')
			.endOf('month')
	]
})

export const rangePastNDays = (n, t) => ({
	t: t,
	range: [
		dayjs()
			.startOf('day')
			.subtract(n - 1, 'day'),
		dayjs().endOf('day')
	]
})

export const rangePastNMonth = (n, t) => ({
	t: t,
	range: [
		dayjs()
			.startOf('month')
			.subtract(n, 'month'),
		dayjs()
			.subtract(n, 'month')
			.endOf('month')
	]
})

export const rangeCurrentPeriod = () => ({
	t: i18n.t('common.utils.current_accounting_period'),
	range: [
		store.state.user.currentAccountPeriod.periodStDate,
		store.state.user.currentAccountPeriod.periodEnDate
	]
})

export const shortcut2 = [
	rangeToday(), // today
	rangeYesterday(), // yesterday
	rangeLastWeek(), // last week
	rangeCurrentWeek(), // current week
	rangeLastMonth(), // last month
	rangeCurrentMonth(), // current month
	rangePastNDays(7, i18n.t('common.utils.week1')), // past 7 days | past 1 week
	rangePastNDays(30, i18n.t('common.utils.month1_recent')), // past 30 days | 1 month
	rangePastNDays(90, i18n.t('common.utils.month_three')) // past 90 days | 3 month
]

export const shortcutsMonth = [
	rangeCurrentMonth(), // current month
	rangeLastMonth(), // last month
	rangePastNDays(30, i18n.t('common.utils.month1_recent')), // past 30 days | 1 month
	rangePastNDays(90, i18n.t('common.utils.month_three')) // past 90 days | 3 month
]

export const shortcutForButtons = [
	rangeCurrentPeriod(), // current period
	rangeToday(), // today
	rangeYesterday(), // yesterday
	rangePastNDays(7, i18n.t('common.utils.last_7days')), // past 7 days
	rangePastNDays(14, i18n.t('common.utils.last_14days')), // past 14 days
	rangePastNDays(30, i18n.t('common.utils.last_30days')), // past 30 days | 1 month
	rangeLastMonth(), // last month
	rangeCurrentMonth() // current month
]

export const shortcutForButtonsWOPeriod = [
	rangeToday(), // today
	rangeYesterday(), // yesterday
	rangePastNDays(7, i18n.t('common.utils.last_7days')), // past 7 days
	rangePastNDays(14, i18n.t('common.utils.last_14days')), // past 14 days
	rangePastNDays(30, i18n.t('common.utils.last_30days')), // past 30 days | 1 month
	rangeLastMonth(), // last month
	rangeCurrentMonth() // current month
]
