import dayjs from 'dayjs'
import i18n from '@/locales'
import store from '@/store'

// 路由的名称  一定要和 组件的name对应，缓存才起作用
export const routerNames = {
	customerConfig: 'customerConfig',
	editData: 'editData',
	friendInvitation: 'friendInvitation',
	memberMsgChange: 'memberMsgChange',
	memberLabelRecord: 'memberLabelRecord',
	memberChange: 'memberChange',
	addMember: 'addMember',
	memberOverflow: 'memberOverflow',
	bankRecord: 'bankRecord',
	memberDetails: 'memberDetails',
	memberList: 'memberList',
	memberLogin: 'memberLogin',
	memberInfoChangeRecord: 'memberInfoChangeRecord',
	registerInfo: 'registerInfo',
	virtualRecord: 'virtualRecord',
	addMemberReview: 'addMemberReview',
	memberChangeReview: 'memberChangeReview',
	memberBankManage: 'memberBankManage',
	memberVirtualManage: 'memberVirtualManage',
	gameClassify: 'gameClassify',
	gamePlatform: 'gamePlatform',
	addMemberCheck: 'addMemberCheck',
	gameLabel: 'gameLabel',
	gameHomeRecommendEdit: 'gameHomeRecommendEdit',
	gameManagement: 'gameManagement',
	gameHomeRecommend: 'gameHomeRecommend',
	gameClassification: 'gameClassification',
	addReview: 'addReview',
	agencyEdit: 'agencyEdit',
	transformationReview: 'transformationReview',
	gameSearchManage: 'gameSearchManage',
	gameBetslipDetails: 'gameBetslipDetails',
	agentList: 'agentList',
	addAgent: 'addAgent',
	agentBankCard: 'agentBankCard',
	agentDetails: 'agentDetails',
	agentInfoChangeRecord: 'agentInfoChangeRecord',
	agentTrans: 'agentTrans',
	pictureManagement: 'pictureManagement',
	domainCreateAndEidt: 'domainCreateAndEidt',
	levelWelfareConfig: 'levelWelfareConfig',
	createRiskRank: 'createRiskRank',
	editRisk: 'editRisk',
	memberLoginWhitelist: 'memberLoginWhitelist',
	riskObjectManage: 'riskObjectManage',
	memberRiskControlChange: 'memberRiskControlChange',
	proxyRiskControlChange: 'proxyRiskControlChange',
	bankRiskChangeInfo: 'bankRiskChangeInfo',
	virtualRiskChangeInfo: 'virtualRiskChangeInfo',
	ipRiskControlChange: 'ipRiskControlChange',
	terminalRiskControlChange: 'terminalRiskControlChange',
	vipLevelConfig: 'vipLevelConfig',
	vipRightConfig: 'vipRightConfig',
	vipDiscountConfig: 'vipDiscountConfig',
	vipRebateConfig: 'vipRebateConfig',
	vipOperationRecord: 'vipOperationRecord',
	vipRebateRecord: 'vipRebateRecord',
	vipChangeRecord: 'vipChangeRecord',
	memberLabelConfig: 'memberLabelConfig',
	memberLabelChangeRecord: 'memberLabelChangeRecord',
	agentShipIncrease: 'agentShipIncrease',
	agentDeduction: 'agentDeduction',
	roleManage: 'roleManage',
	agentReport: 'agentReport',
	gameConfigOperationRecord: 'gameConfigOperationRecord',
	gameRecommendOperationRecord: 'gameRecommendOperationRecord',
	gameBetslipTable: 'gameBetslipTable',
	dxGameOrders: 'dxGameOrders',
	abnormalGameBetslip: 'abnormalGameBetslip',
	searchOperationRecord: 'searchOperationRecord',
	courseContentConfig: 'courseContentConfig',
	courseConfigChange: 'courseConfigChange',
	courseNameConfig: 'courseNameConfig',
	agentBetConfigRecord: 'agentBetConfigRecord',
	agentStopHandicapConfigRecord: 'agentStopHandicapConfigRecord',
	eatOrderModeConfig: 'eatOrderModeConfig',
	transferOrderConfig: 'transferOrderConfig',
	eatOrderConfig: 'eatOrderConfig',
	courseLabelConfig: 'courseLabelConfig',
	gamePlatformManage: 'gamePlatformManage',
	activityOffersReport: 'activityOffersReport',
	commissionReport: 'commissionReport',
	comprehensiveReport: 'comprehensiveReport',
	platformDepositAndWithdraw: 'platformDepositAndWithdraw',
	memberReport: 'memberReport',
	newMemberReport: 'newMemberReport',
	proxyReport: 'proxyReport',
	rebateReport: 'rebateReport',
	proxyProfitAndLoss: 'proxyProfitAndLoss',
	rebatePeriodReport: 'rebatePeriodReport',
	transferReport: 'transferReport',
	clientProfitAndLoss: 'clientProfitAndLoss',
	dailyProfitAndLoss: 'dailyProfitAndLoss',
	gameProfitAndLoss: 'gameProfitAndLoss',
	memberProfitAndLoss: 'memberProfitAndLoss',
	venueProfitAndLoss: 'venueProfitAndLoss',
	agentRebateData: 'agentRebateData',
	memberReturnData: 'memberReturnData',
	memberWithdrawalSettings: 'memberWithdrawalSettings',
	proxyWithdrawalSettings: 'proxyWithdrawalSettings',
	memberAccountChangeRecord: 'memberAccountChangeRecord',
	proxyAccountChangeRecord: 'proxyAccountChangeRecord',
	memberDepositRecord: 'memberDepositRecord',
	memberWithdrawalRecord: 'memberWithdrawalRecord',
	proxyWithdrawalRecord: 'proxyWithdrawalRecord',
	agentRebateRecord: 'agentRebateRecord',
	memberTopupRecord: 'memberTopupRecord',
	memberDeductionRecord: 'memberDeductionRecord',
	memberRebateRecord: 'memberRebateRecord',
	noOutBillList: 'noOutBillList',
	historySettlementBillList: 'historySettlementBillList',
	billRelateSetting: 'billRelateSetting',
	memberBillWithdrawalSubReview: 'memberBillWithdrawalSubReview',
	memberBillWithdrawalSubAuditRecord: 'memberBillWithdrawalSubAuditRecord',
	JackpotRecord: 'JackpotRecord',
	IssueRecord: 'IssueRecord',
	proxyaccountChangeRecord: 'proxyaccountChangeRecord',
	proxyDepositRecord: 'proxyDepositRecord',
	proxyTransferRecord: 'proxyTransferRecord',
	substitutionRecord: 'substitutionRecord',
	proxyRebateRecord: 'proxyRebateRecord',
	agentCommissionRecord: 'agentCommissionRecord',
	superAgentCommissionRecord: 'superAgentCommissionRecord',
	proxyTopupRecord: 'proxyTopupRecord',
	proxyDeductionRecord: 'proxyDeductionRecord',
	commissionRecord: 'commissionRecord',
	memberWithdrawalReview: 'memberWithdrawalReview',
	memberDepositpreferenceRecord: 'memberDepositpreferenceRecord',
	agentWithdrawalReview: 'agentWithdrawalReview',
	agentTopupReview: 'agentTopupReview',
	bonusReview: 'bonusReview',
	agentLogin: 'agentLogin',
	agentRegisterInfo: 'agentRegisterInfo',
	agentInfoChange: 'agentInfoChange',
	overflowReview: 'overflowReview',
	domainNameManagement: 'domainNameManagement',
	pictureManage: 'pictureManage',
	domainChangeRecord: 'domainChangeRecord',
	pictureChangeRecord: 'pictureChangeRecord',
	levelWelfareCnfig: 'levelWelfareCnfig',
	agentLabelConfig: 'agentLabelConfig',
	agentLabelChangeRecord: 'agentLabelChangeRecord',
	agentLevelConfigRecord: 'agentLevelConfigRecord',
	rebateLevelRecord: 'rebateLevelRecord',
	clientBanner: 'clientBanner',
	clientStart: 'clientStart',
	venueImage: 'venueImage',
	publicRulesConfig: 'publicRulesConfig',
	memberDiscountConfig: 'memberDiscountConfig',
	createPage: 'createPage',
	promotionPageConfig: 'promotionPageConfig',
	memberShareDomainManage: 'memberShareDomainManage',
	depositWithdrawDiscountManage: 'depositWithdrawDiscountManage',
	clientLoading: 'clientLoading',
	clientLog: 'clientLog',
	platformLog: 'platformLog',
	announcement: 'announcement',
	activity: 'activity',
	notice: 'notice',
	feedBack: 'feedBack',
	infoLog: 'infoLog',
	activityType: 'activityType',
	activityConfig: 'activityConfig',
	vipConfig: 'vipConfig',
	activityConfigLog: 'activityConfigLog',
	memberRiskChangeInfo: 'memberRiskChangeInfo',
	proxyRiskChangeInfo: 'proxyRiskChangeInfo',
	ipRiskChange: 'ipRiskChange',
	terminalRiskChangeInfo: 'terminalRiskChangeInfo',
	gameManage: 'gameManage',
	gameSearchLog: 'gameSearchLog',
	gameSearchOperationRecord: 'gameSearchOperationRecord',
	memberTransferRecord: 'memberTransferRecord',
	transferRecord: 'transferRecord',
	rebateCheckRecord: 'rebateCheckRecord',
	contractTemplate: 'contractTemplate',
	contract: 'contract',
	commissionReview: 'commissionReview',
	agentCommissionCheck: 'agentCommissionCheck',
	commissionPayment: 'commissionPayment',
	commissionControlRecord: 'commissionControlRecord',
	membershipIncrease: 'membershipIncrease',
	memberDeduction: 'memberDeduction',
	bonusDistribution: 'bonusDistribution',
	agencyIncrease: 'agencyIncrease',
	agencyDeduction: 'agencyDeduction',
	memberTopupReview: 'memberTopupReview',
	memberWithdrawalReviewRecord: 'memberWithdrawalReviewRecord',
	memberTopupReviewRecord: 'memberTopupReviewRecord',
	bonusReviewRecord: 'bonusReviewRecord',
	agentWithdrawalReviewRecord: 'agentWithdrawalReviewRecord',
	agentTopupReviewRecord: 'agentTopupReviewRecord',
	commissionReviewRecord: 'commissionReviewRecord',
	memberTransfer: 'memberTransfer',
	contractChangeRecord: 'contractChangeRecord',
	agentVirtualRecord: 'agentVirtualRecord',
	contractMinConfigRecord: 'contractMinConfigRecord',
	clientCommon: 'clientCommon',
	platformRate: 'platformRate',
	editRiskRank: 'editRiskRank',
	commissionReceiptRaymentReport: 'commissionReceiptRaymentReport',
	valetRecharge: 'valetRecharge',
	valetRechargeRecord: 'valetRechargeRecord',
	merchantList: 'merchantList',
	domainNameConfiguration: 'domainNameConfiguration',
	loginIPBlackList: 'loginIPBlackList',
	abnormalBetslipRecord: 'abnormalBetslipRecord',
	pandanBetRecord: 'pandanBetRecord',
	pandanBetList: 'pandanBetList',
	pandanOperationRecord: 'pandanOperationRecord',
	bankersProfitPolicyConfig: 'bankersProfitPolicyConfig',
	bankersProfitPolicyConfigRecord: 'bankersProfitPolicyConfigRecord',
	bankersProfitRecord: 'bankersProfitRecord',
	eatOrderComissionRecord: 'eatOrderComissionRecord',
	generalAgentRiskControlSettings: 'generalAgentRiskControlSettings',
	eventRiskControlSettings: 'eventRiskControlSettings',
	operationRecords: 'operationRecords',
	eatOrderProfitAndLoss: 'eatOrderProfitAndLoss',
	bankersProfitAndLoss: 'bankersProfitAndLoss',
	topAgentBankerSettleReport: 'topAgentBankerSettleReport',
	profitAndLossOfFuyingbaoMembers: 'profitAndLossOfFuyingbaoMembers',
	loginIPBlacklist: 'loginIPBlacklist',
	registerIPBlacklist: 'registerIPBlacklist',
	blacklistRegisteredEquipment: 'blacklistRegisteredEquipment',
	logindeviceNumberBlacklist: 'logindeviceNumberBlacklist',
	gameActionLog: 'gameActionLog',
	agentBill: 'agentBill',
	agentDossiersCommissions: 'agentDossiersCommissions',
	// 商户
	addMerchant: 'addMerchant',
	levelConfig: 'levelConfig',
	paymentChannelConfig: 'paymentChannelConfig',
	addMerchantReview: 'addMerchantReview',
	customerServiceConfig: 'customerServiceConfig',
	venueConfig: 'venueConfig',
	messageConfig: 'messageConfig',
	gameLinkConfig: 'gameLinkConfig',
	merchantDetails: 'merchantDetails',
	initialAgentConfig: 'initialAgentConfig',
	paymentManufacturer: 'paymentManufacturer',
	bankManagement: 'bankManagement',
	cashinandout: 'cashinandout',
	// 资金三方出入款设置
	thirdDepositConfig: 'thirdDepositConfig',
	thirdWithdrawConfig: 'thirdWithdrawConfig',
	memberDepositChannelConfig: 'memberDepositChannelConfig',
	memberWithdrawChannelConfig: 'memberWithdrawChannelConfig',
	memberWithdrawConfig: 'memberWithdrawConfig',
	agentDepositChannelConfig: 'agentDepositChannelConfig',
	agentWithdrawChannelConfig: 'agentWithdrawChannelConfig',
	agentWithdrawConfig: 'agentWithdrawConfig',

	// 德州
	gameRuleManager: 'gameRuleManager',
	cardTablesList: 'cardTablesList',
	cardTablesBase: 'cardTablesBase',
	gameList: 'gameList',
	gameBase: 'gameBase',
	memberCardTableList: 'memberCardTableList',
	memberHandCardList: 'memberHandCardList',
	betSlip: 'betSlip',
	generalClubManage: 'generalClubManage',
	clubApplicationList: 'clubApplicationList',
	clubLogs: 'clubLogs',
	clubList: 'clubList',
	clubAllianceList: 'clubAllianceList',
	insureLossRatioConfig: 'insureLossRatioConfig',
	insureList: 'insureList',
	profitAndLossShow: 'profitAndLossShow',
	quickAddConfig: 'quickAddConfig',
	chatBarrage: 'chatBarrage',
	voicePhrases: 'voicePhrases',
	chatEmoji: 'chatEmoji',
	customVoice: 'customVoice',
	viewUnPublicCards: 'viewUnPublicCards',
	actionOvertime: 'actionOvertime',
	dxnActionLog: 'dxnActionLog',
	orderList: 'orderList',
	clubMemberList: 'clubMemberList',
	partnerCardQuery: 'partnerCardQuery',
	partnerCardSift: 'partnerCardSift',
	partnerCardRefined: 'partnerCardRefined',
	gameBasicManager: 'gameBasicManager',
	feeConfig: 'feeConfig',
	sensitiveManager: 'sensitiveManager',
	squidList: 'squidList',
	squidDetail: 'squidDetail',
	squidWalletRecord: 'squidWalletRecord',
	handAccount: 'handAccount',
	tableAccount: 'tableAccount',
	appResourceManager: 'appResourceManager',
	productBasicConfig: 'productBasicConfig',
	shopProductConfig: 'shopProductConfig',
	memberProductList: 'memberProductList',
	productBuyRecord: 'productBuyRecord',
	clubWhiteList: 'clubWhiteList',
	interactionProps: 'interactionProps',
	// 系统
	staffManage: 'staffManage',
	appEmbeddeddomainNameManage: 'appEmbeddeddomainNameManage',
	parameterDictionary: 'parameterDictionary',
	staffLoginLog: 'staffLoginLog',
	systemMaintainWhitelist: 'systemMaintainWhitelist',
	systemRecord: 'systemRecord',
	userUploadLog: 'userUploadLog',
	// 运营
	proxyAppDefaultConfig: 'proxyAppDefaultConfig',
	operationActionLog: 'operationActionLog',
	agentAutoLoginOutConfig: 'agentAutoLoginOutConfig',
	// 报表
	dataDaily: 'dataDaily',
	rebateExpectedReport: 'rebateExpectedReport',
	memberExpectedReport: 'memberExpectedReport',
	dataDailyReport: 'dataDailyReport',
	dataComparisonReport: 'dataComparisonReport',
	agentAndLossRanking: 'agentAndLossRanking',
	memberAndLossRanking: 'memberAndLossRanking',
	statusOverview: 'statusOverview',
	statusOverviewTripartite: 'statusOverviewTripartite',
	statusOverviewDx: 'statusOverviewDx',
	// 代理
	proxyActionLog: 'proxyActionLog',
	proxyBmhAccountRecord: 'proxyBmhAccountRecord',
	agentLineLiquidationBill: 'agentLineLiquidationBill',
	// 会员
	memberActionLog: 'memberActionLog',
	bmhAccountManage: 'bmhAccountManage',
	bmhAccountRecord: 'bmhAccountRecord',
	// 资金
	fundsActionLog: 'fundsActionLog',
	// 风控
	riskActionLog: 'riskActionLog',
	masterAgentReport: 'masterAgentReport'
}
const month = dayjs().month()
const year = dayjs().year()
const fromBegin = () => ({
	text: i18n.t('common.utils.from_beginning'),
	onClick(picker) {
		const end = dayjs().endOf('day')
		const start = dayjs('20231115', 'YYYYMMDD').startOf('day')
		picker.$emit('pick', [+start, +end])
	}
})
export const shortcutsM = [
	{
		text: i18n.t('common.utils.minite15'),
		onClick(picker) {
			const start = dayjs().subtract(15, 'minute')
			const end = dayjs()
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.hour1'),
		onClick(picker) {
			const start = dayjs().subtract(1, 'hour')
			const end = dayjs()
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当周
	{
		text: i18n.t('common.utils.week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当月
	{
		text: i18n.t('common.utils.month_now'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前2月
	{
		text:
			month - 1 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 1) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 1 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(2, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(2, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前3月
	{
		text:
			month - 2 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 2) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 2 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(3, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(3, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前4月
	{
		text:
			month - 3 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 3) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 3 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(4, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(4, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前5月
	{
		text:
			month - 4 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 4) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 4 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(5, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(5, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前6月
	{
		text:
			month - 5 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 5) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 5 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(6, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(6, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前7月
	{
		text:
			month - 6 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 6) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 6 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(7, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(7, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前8月
	{
		text:
			month - 7 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 7) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 7 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(8, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(8, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前9月
	{
		text:
			month - 8 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 8) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 8 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(9, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(9, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
]
export const reportDate = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当周
	{
		text: i18n.t('common.utils.week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当月
	{
		text: i18n.t('common.utils.month_now'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
]
export const shortcuts = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当周
	{
		text: i18n.t('common.utils.week_now'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当月
	{
		text: i18n.t('common.utils.month1'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 前3个月
	{
		text: i18n.t('common.utils.month_three'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			// const start = dayjs()
			// 	.startOf('month')
			// 	.subtract(3, 'month')
			// picker.$emit('pick', [+start, +end])
			// const end = new Date()
			// const start = new Date()
			// start.setTime(start.getTime() - 3600 * 1000 * 24 * 91)
			const start = dayjs().startOf('day') - 24 * 89 * 60 * 60 * 1000
			picker.$emit('pick', [start, end])
		}
	},
	fromBegin()
	// 往前2月
	// {
	// 	text:
	// 		month - 1 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 1) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 1 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(2, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(2, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前3月
	// {
	// 	text:
	// 		month - 2 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 2) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 2 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(3, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(3, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前4月
	// {
	// 	text:
	// 		month - 3 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 3) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 3 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(4, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(4, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前5月
	// {
	// 	text:
	// 		month - 4 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 4) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 4 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(5, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(5, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前6月
	// {
	// 	text:
	// 		month - 5 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 5) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 5 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(6, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(6, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前7月
	// {
	// 	text:
	// 		month - 6 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 6) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 6 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(7, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(7, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前8月
	// {
	// 	text:
	// 		month - 7 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 7) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 7 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(8, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(8, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前9月
	// {
	// 	text:
	// 		month - 8 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 8) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 8 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(9, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(9, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// }
]
export const shortcuts1 = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			// const start = dayjs().startOf('day')
			const start = new Date().getTime()
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当周
	{
		text: i18n.t('common.utils.week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当月
	{
		text: i18n.t('common.utils.month_now'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 前3个月
	{
		text: i18n.t('common.utils.month_three'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			// const start = dayjs()
			// 	.startOf('month')
			// 	.subtract(3, 'month')
			// picker.$emit('pick', [+start, +end])
			// const end = new Date()
			// const start = new Date()
			// start.setTime(start.getTime() - 3600 * 1000 * 24 * 91)
			const start = dayjs().startOf('day') - 24 * 89 * 60 * 60 * 1000
			picker.$emit('pick', [start, end])
		}
	},
	fromBegin()
	// 往前2月
	// {
	// 	text:
	// 		month - 1 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 1) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 1 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(2, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(2, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前3月
	// {
	// 	text:
	// 		month - 2 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 2) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 2 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(3, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(3, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前4月
	// {
	// 	text:
	// 		month - 3 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 3) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 3 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(4, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(4, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前5月
	// {
	// 	text:
	// 		month - 4 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 4) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 4 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(5, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(5, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前6月
	// {
	// 	text:
	// 		month - 5 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 5) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 5 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(6, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(6, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前7月
	// {
	// 	text:
	// 		month - 6 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 6) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 6 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(7, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(7, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前8月
	// {
	// 	text:
	// 		month - 7 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 7) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 7 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(8, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(8, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// },
	// 往前9月
	// {
	// 	text:
	// 		month - 8 > 0
	// 			? year + i18n.t('common.utils.day') + (month - 8) + i18n.t('common.utils.day')
	// 			: year - 1 + i18n.t('common.utils.day') + (month - 8 + 12) + i18n.t('common.utils.day'),
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.subtract(9, 'month')
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(9, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// }
]
export const shortcutsNoToday = [
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当周
	{
		text: i18n.t('common.utils.week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs()
					.endOf('day')
					.subtract(1, 'd')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs()
					.endOf('day')
					.subtract(1, 'd')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	// 当月
	{
		text: i18n.t('common.utils.month_now'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前2月
	{
		text:
			month - 1 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 1) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 1 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(2, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(2, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前3月
	{
		text:
			month - 2 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 2) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 2 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(3, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(3, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前4月
	{
		text:
			month - 3 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 3) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 3 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(4, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(4, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前5月
	{
		text:
			month - 4 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 4) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 4 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(5, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(5, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前6月
	{
		text:
			month - 5 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 5) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 5 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(6, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(6, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前7月
	{
		text:
			month - 6 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 6) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 6 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(7, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(7, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前8月
	{
		text:
			month - 7 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 7) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 7 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(8, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(8, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前9月
	{
		text:
			month - 8 > 0
				? year +
				i18n.t('common.utils.year') +
				(month - 8) +
				i18n.t('common.utils.month')
				: year -
				1 +
				i18n.t('common.utils.year') +
				(month - 8 + 12) +
				i18n.t('common.utils.month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(9, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(9, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
]
// 包含 今日、昨日、上周、本周、上月、本月、近一周（近7天）、最近一个月（近30天）、最近三个月（近90天）
export const shortcut2 = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.last_week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs()
					.subtract(2, 'week')
					.endOf('week')
					.add(1, 'day')
				start = dayjs()
					.subtract(2, 'week')
					.startOf('week')
					.add(1, 'day')
			} else {
				end = dayjs()
					.subtract(1, 'week')
					.endOf('week')
					.subtract(-1, 'day')
				start = dayjs()
					.startOf('week')
					.subtract(1, 'week')
					.subtract(-1, 'day')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.week_now'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.month1'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.week1'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(6, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.month1_recent'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(29, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.month3_recent'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(89, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
]

// 包含 今日、昨日、上周、本周、上月、本月、近一周（近7天）、最近一个月（近30天）
export const shortcut3 = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.last_week'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs()
					.subtract(2, 'week')
					.endOf('week')
					.add(1, 'day')
				start = dayjs()
					.subtract(2, 'week')
					.startOf('week')
					.add(1, 'day')
			} else {
				end = dayjs()
					.subtract(1, 'week')
					.endOf('week')
					.subtract(-1, 'day')
				start = dayjs()
					.startOf('week')
					.subtract(1, 'week')
					.subtract(-1, 'day')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.week_now'),
		onClick(picker) {
			let start, end
			const weekNum = dayjs().day()
			if (weekNum === 0) {
				end = dayjs().endOf('day')
				start = dayjs()
					.subtract(1, 'd')
					.startOf('week')
					.startOf('day')
					.add(1, 'd')
			} else {
				end = dayjs().endOf('day')
				start = dayjs()
					.startOf('day')
					.startOf('week')
					.add(1, 'd')
			}
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.month1'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.week1'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(6, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.month1_recent'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(29, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
]

export const shortcut4 = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const start = dayjs().startOf('day')
			picker.$emit('pick', +start)
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', +start)
		}
	},
	{
		text: '前日',
		onClick(picker) {
			const start = dayjs()
				.startOf('day')
				.subtract(2, 'd')
			picker.$emit('pick', +start)
		}
	}
]

// 包含 今日、昨日、上周、本周、上月、本月、近一周（近7天）、最近一个月（近30天）
export const shortcutcurr4 = [
	{
		text: i18n.t('common.utils.today'),
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.yesterday'),
		onClick(picker) {
			const end = dayjs()
				.endOf('day')
				.subtract(1, 'd')
			const start = dayjs()
				.startOf('day')
				.subtract(1, 'd')
			picker.$emit('pick', [+start, +end])
		}
	},

	{
		text: '近7天',
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(6, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: '近14天',
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(13, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: '近30天',
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs()
				.startOf('day')
				.subtract(29, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.subtract(1, 'month')
				.endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	{
		text: '当月',
		onClick(picker) {
			const end = dayjs().endOf('day')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	}
]

// 本月,上月,近三个月,近六个月
export const shortcutsMonth = [
	// 本月
	{
		text: i18n.t('common.utils.month1'),
		onClick(picker) {
			const end = dayjs().endOf('month')
			const start = dayjs().startOf('month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 上月
	{
		text: i18n.t('common.utils.last_month'),
		onClick(picker) {
			const end = dayjs()
				.endOf('month')
				.subtract(1, 'month')
			const start = dayjs()
				.startOf('month')
				.subtract(1, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 近一个月30天
	{
		text: i18n.t('common.utils.month1_recent'),
		onClick(picker) {
			const end = dayjs().endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(29, 'day')
			picker.$emit('pick', [+start, +end])
		}
	},
	// 往前3月
	{
		text: i18n.t('common.utils.month3_recent'),
		onClick(picker) {
			const end = dayjs().endOf('month')
			const start = dayjs()
				.startOf('month')
				.subtract(3, 'month')
			picker.$emit('pick', [+start, +end])
		}
	},
	fromBegin()
	// 往前6月
	// {
	// 	text: '近六个月',
	// 	onClick(picker) {
	// 		const end = dayjs()
	// 			.endOf('month')
	// 		const start = dayjs()
	// 			.startOf('month')
	// 			.subtract(6, 'month')
	// 		picker.$emit('pick', [+start, +end])
	// 	}
	// }
]

// Shortcut3 extend with 本账期
export const shortcut3WithCurrPeriod = [
	// 本账期
	{
		text: i18n.t('common.utils.current_accounting_period'),
		onClick(picker) {
			picker.$emit('pick', [
				+store.state.user.currentAccountPeriod.periodStDate,
				+store.state.user.currentAccountPeriod.periodEnDate
			])
		}
	},
	...shortcut3
]

export const shortcut4WithCurrPeriod = [
	// 本账期
	{
		text: i18n.t('common.utils.current_accounting_period'),
		onClick(picker) {
			picker.$emit('pick', [
				+store.state.user.currentAccountPeriod.periodStDate,
				+store.state.user.currentAccountPeriod.periodEnDate
			])
		}
	},
	...shortcutcurr4
]
