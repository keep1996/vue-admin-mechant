import fingerprintjs2 from 'fingerprintjs2'
import short from 'short-uuid'

import store from '@/store'
const fingerName = 'finger'

export default {
	load() {
		return new Promise((resolve, reject) => {
			fingerprintjs2.get(function(components) {
				const values = components.map(function(component) {
					return component.value
				})
				// const murmur = fingerprintjs2.x64hash128(values.join(''), 31)
				const murmur = short.generate()
				localStorage.setItem(fingerName, murmur)
				store.commit('app/SET_FINGER', murmur)
				resolve(murmur)
			})
		})
	},
	loadUuid() {
		return new Promise((resolve, reject) => {
			const murmur = localStorage.getItem('finger') || short.generate()
			localStorage.setItem(fingerName, murmur)
			store.commit('app/SET_FINGER', murmur)
			resolve(murmur)
		})
	},
	get() {
		return localStorage.getItem(fingerName)
	}
}
