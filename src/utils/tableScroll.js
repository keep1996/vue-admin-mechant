export default {
	data() {
		return {
			currentSelectRow: null,
			currentSelectRowScrollTop: null,
			currentSelectRowScrollLeft: null
		}
	},
	activated() {
		this.$refs?.refTable.$nextTick(() => {
			this.setSelectRowHighlight()
			this.setSelectRowScrollPosition()
		})
	},
	mounted() {
		this.registTableScrollEvent()
	},
	destroyed() {
		this.removeTableScrollEvent()
	},
	methods: {
		getTableBodyWrapper() {
			return document.querySelector('.el-table__body-wrapper')
		},
		registTableScrollEvent() {
			const tableBody = this.getTableBodyWrapper()
			if (tableBody) {
				tableBody.addEventListener('scroll', this.getSelectRowScrollPosition, { passive: false })
			}
		},
		removeTableScrollEvent() {
			const tableBody = this.getTableBodyWrapper()
			if (tableBody) {
				tableBody.removeEventListener('scroll', this.getSelectRowScrollPosition)
			}
		},
		getSelectRowScrollPosition() {
			const tableBody = this.getTableBodyWrapper()
			this.currentSelectRowScrollTop = tableBody.scrollTop
			this.currentSelectRowScrollLeft = tableBody.scrollLeft
		},
		setSelectRowScrollPosition() {
			const tableBody = this.getTableBodyWrapper()
			if (tableBody) {
				setTimeout(() => {
					tableBody.scroll(this.currentSelectRowScrollLeft || 0, this.currentSelectRowScrollTop || 0)
				}, 100)
			}
		},
		setSelectRowHighlight() {
			const refTable = this.$refs?.refTable
			if (refTable && this.currentSelectRow !== null) {
				refTable.setCurrentRow(this.currentSelectRow)
			}
		},
		handleSelectRowHighlight(currentRow) {
			this.currentSelectRow = currentRow
		}
	}
}
