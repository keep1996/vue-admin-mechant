import * as P from './pattern'
import i18n from '@/locales'

/**
 * 验证名称6-12位数字字母
 * @param {} value
 */
export const testName = (value) => {
	return P.USERNAME_NO_SPECIAL_PATTERN.test(value)
}

/**
 * 验证名称3-12位数字或者字母
 * @param {} value
 */
export const testXPName = (value) => {
	return P.USERNAME_3_12.test(value)
}

/**
 * 验证密码6-12位数字字母
 * @param {} value
 */
export const testPassword = (value) => {
	return P.PASSWORD_SIMPLE_PATTERN.test(value)
}

/**
 * 验证简单密码6-12位
 * @param {} value
 */
export const testSimplePassword = (value) => {
	return P.PASSWORD_MORE_SIMPLE_PATTERN.test(value)
}

/**
 * 验证密码3-10位
 * @param {} value
 */
export const testPassword310 = (value) => {
	return P.PASSWORD_3_10.test(value)
}

/**
 * 验证密码6-11位
 * @param {} value
 */
export const testPassword611 = (value) => {
	return P.PASSWORD_6_11.test(value)
}

/**
 * 验证是否有符号
 * @param {} str
 */
export function testSpecial(str) {
	var specialKey =
		"[`+-~!#$^&*()=|{}':;',\\[\\].<>/?~！#￥……&*（）——|{}【】‘；：”“'。，、？]‘'　@"
	for (var i = 0; i < str.length; i++) {
		if (specialKey.indexOf(str.substr(i, 1)) !== -1) {
			return false
		}
	}
	return true
}

/**
 * 验证表情
 * @param {} value
 */
export const testEmoji = (value) => {
	return P.EMOJI_PATTERN.test(value)
}

/**
 * 验证空白字符
 * @param {} value
 */
export const testSpace = (value) => {
	return P.SPACE_PATTERN.test(value)
}

/**
 * 验证字符串长度
 * @param {} value
 */
export const testStringLength = (value, range = { min: 6, max: 12 }) => {
	const str = String(value)
	if (str.length < range.min || str.length > range.max) {
		return false
	}
	return true
}

/**
 * 校验url地址
 */
export const validUrl = (rule, value, callback) => {
	if (value === '') {
		return callback(new Error(i18n.t('common.utils.input_address')))
	} else if (!P.URL_PATTERN.test(value)) {
		return callback(new Error(i18n.t('common.utils.input_right_address')))
	} else {
		callback()
	}
}

/**
 * 校验字母和字符串符号
 */
export function isNumberAndStr(rule, value, callback) {
	if (value === '') {
		return callback(new Error(i18n.t('common.utils.no_empty')))
	} else if (!P.NUMBER_AND_STR.test(value)) {
		return callback(new Error(i18n.t('common.utils.only_number')))
	} else {
		callback()
	}
}

/**
 * 校验0-100.00的数字
 * @param rule
 * @param value
 * @param callback
 * @returns {*}
 */
export const validINumber0To100 = (rule, value, callback) => {
	if (value === '') {
		return callback(i18n.t('common.please_enter'))
	} else if (!(parseFloat(value) <= 100 && parseFloat(value) >= 0)) {
		return callback(new Error(i18n.t('common.utils.limit')))
	} else if (!P.OUT_POS_INT_ZERO.test(value)) {
		return callback(new Error(i18n.t('common.utils.dot')))
	} else {
		callback()
	}
}
