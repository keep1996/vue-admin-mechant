
//
export function DB(warehouseName) {
 this.db = window.indexedDB.open(warehouseName)
 this.warehouseName = warehouseName
}

DB.prototype.getData = function () {
    const transaction = this.db.transaction([this.warehouseName])
	const objectStore = transaction.objectStore(this.warehouseName)
}

export default DB
