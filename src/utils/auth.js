import Cookies from 'js-cookie'
const TokenKey = 'token'
import dataHandle from '@/utils/encrypt'
import { Storage } from '@/utils/compose'
export function getToken() {
	return Cookies.get(TokenKey)
}
export function getRoles() {
	try {
		const roles = JSON.parse(localStorage.roles)
		return roles
	} catch (e) {
		return []
	}
}
export function setRoles(val) {
	localStorage.roles = JSON.stringify(val)
	return val
}
export function setGameTypes(val) {
	return Cookies.set('game_types', val)
}
export function getGameTypes() {
	return Cookies.get('game_types')
}

export function setToken(token) {
	return Cookies.set(TokenKey, token)
}

export function setUserInfo(userInfo) {
	return Storage.set('u1', dataHandle.encryptData2(userInfo))
}
export function getUserInfo() {
	const storageInfo = Storage.get('u1')
	if (storageInfo === null) {
		return {}
	}
	const info = dataHandle.decryptData(storageInfo) || ''
	return info ? JSON.parse(info) : {}
}

export function setUsername(token) {
	return Cookies.set('username', token)
}
export function getUsername() {
	return Cookies.get('username')
}

export function setNickName(token) {
	return Cookies.set('nickName', token)
}
export function getNickName() {
	return Cookies.get('nickName')
}

export function removeToken() {
	return Cookies.remove(TokenKey)
}
export function clearCookie() {
	const cookies = [
		'game_types',
		'id',
		'nickName',
		'sidebarStatus',
		'username'
	]
	cookies.forEach((i) => {
		Cookies.remove(i)
	})
}
