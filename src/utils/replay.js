// 获取牌局回放接口处理后的数据
// export const getGameReplayDetail = async (that) => {
// 	try {
// 		return await Promise.all([
// 			that.$api.showGameReviewCardListBend(that.replayQuery).then((res) => {
// 				const replayDetail = formatReplayDetailRes(res?.data)
// 				return Promise.resolve(replayDetail)
// 			}),
// 			that.$api.getGameReviewDeatailListBend(that.replayQuery).then((detailRes) => {
// 				const replayList = formatGameReplayListRes(detailRes?.data?.record)
// 				return Promise.resolve(replayList)
// 			})
// 		]).then((allRes) => {
// 			return Promise.resolve({ replayDetail: allRes[0], replayList: allRes[1] })
// 		})
// 	} catch (e) {
// 		// js代码出现异常或者后端接口出现异常
// 		return Promise.reject({
// 			message: e?.record || '未获取数据',
// 			error: e
// 		})
// 	}
// }

// 获取牌局回放接口处理后的数据
export const getGameReplayDetail = async (that) => {
	try {
		// 已结束状态下必须两个接口都返回数据,否则不返回
		return await Promise.all([
			that.$api.showGameReviewCardListBend(that.replayQuery).then((res) => {
				if (that.handStatus === 2 || res?.data?.totalRecord) {
					return Promise.resolve(formatReplayDetailRes(res?.data))
				} else {
					return Promise.reject({ record: '数据生成中' })
				}
			}),
			that.$api.getGameReviewDeatailListBend(that.replayQuery).then((detailRes) => {
				if (that.handStatus === 2 || detailRes?.data?.totalRecord) {
					return Promise.resolve(formatGameReplayListRes(detailRes?.data?.record))
				} else {
					return Promise.reject({ record: '数据生成中' })
				}
			})
		]).then((allRes) => {
			return Promise.resolve({ replayDetail: allRes[0], replayList: allRes[1] })
		})
	} catch (e) {
		// js代码出现异常或者后端接口出现异常
		return Promise.reject({
			message: e?.record || '未获取数据',
			error: e
		})
	}
}

// 步骤名称
const stepNameMap = new Map([
	['Detail', '牌谱'],
	['PRE', '翻前'],
	['FLOP', '翻牌圈'],
	['TURN', '转牌圈'],
	['RIVER', '河牌圈'],
	['SHOWDOWN', '摊牌']
])

// 保险code映射名称
const insureNameMap = new Map([
	['TB', '投保'],
	['PF', '赔付'],
	['BH', '背回']
])

// 鱿鱼获取标记映射：0正常，不展示
const squidMarkMap = new Map([
	[1, '首'],
	[2, '尾'],
	[3, '首&尾']
])

// 子组件步骤列表展示项
const stepItemMiddleArr = ['翻前', '翻牌圈', '转牌圈', '河牌圈']

// 处理牌局回放详情数据【牌谱数据】
export function formatReplayDetailRes(replayDetailRes) {
	if (!replayDetailRes?.record?.length || !replayDetailRes?.summary) {
		return {
			gameType: '',
			info: {},
			topInfo: {},
			bottomList: [],
			insurePoolDetail: {}
		}
	}
	// 牌局回放标题信息
	const gameType = replayDetailRes.summary.gameType
	const info = {
		gameType: gameType,
		handId: replayDetailRes.summary.id, // 手牌ID
		clubName: replayDetailRes.summary.clubName, // 俱乐部名称
		gameTypeName: getGameTypeName(gameType), // 游戏名称
		maxSeatCount: replayDetailRes.summary.maxSeatCount, // 最大人数
		joinCount: replayDetailRes.summary.joinCount, // 加入人数
		sbBlindScore: formatScoreNumber(replayDetailRes.summary.sbBlindScore), // 小盲注
		bbBlindScore: formatScoreNumber(replayDetailRes.summary.bbBlindScore), // 大盲注
		anteScore: formatScoreNumber(replayDetailRes.summary.anteScore), // 前注(大于0显示前注)
		straddleAmount: formatScoreNumber(replayDetailRes.summary.straddleAmount), // 强抓金额(大于0显示)
		isShowAnteScore: replayDetailRes.summary.anteScore > 0, // 是否显示前注
		isShowStraddleAmount: replayDetailRes.summary.straddleAmount > 0, // 是否显示强抓金额
		handStartDateStr: replayDetailRes.summary?.handStartDateStr || '' // 手牌开始时间
	}
	// 牌谱列表顶部信息
	const topInfo = {
		stepName: stepNameMap.get('Detail'),
		stepShowIndex: 'Detail',
		stepItemMiddle: false,
		joinCount: replayDetailRes.summary.joinCount,
		potAmount: replayDetailRes.summary.potAmount,
		// publicCardsImg: getTopInfoPublicCardsImg(replayDetailRes.summary.publicCards),
		publicCardsImg: [],
		squidMode: replayDetailRes.summary.squidMode // 鱿鱼模式
	}
	// 保险池
	const insurePoolDetail = {
		isInsured: 0,
		title: '保险池:',
		src: getOtherImg('insure'),
		insuredPool: replayDetailRes.summary.insuredPool || 0
	}
	// 牌谱列表信息
	const bottomList = []
	replayDetailRes.record.forEach((item) => {
		// 获取公牌和改牌标识
		if (topInfo.publicCardsImg.length === 0 && item.publicCards?.length > 0) {
			topInfo.publicCardsImg = getTopInfoPublicCardsImg(item.publicCards, item.publicCardExtList)
		}
		// 已亮牌玩家展示牌型，未亮牌或者弃牌玩家展示Fold
		// item.showStatus 底池竞争亮牌标识 1亮牌 2弃牌 resultInfo
		// item.betStatus === 6 标识牌谱置灰
		// item.showFlag === true 主动亮牌标识
		const bottomRow = {
			isShowCardEye: item?.showFlag === true, // 是否展示扑克眼睛图片
			fontGrayStyle: item.betStatus === 6 ? 'fontGrayStyle' : '', // 列表项文字置灰
			nickName: formatUserNickName(item.userName, item.nickName), // 账号+昵称
			// seatPosImg: item.seatCode ? getSeatPosImg(item.seatCode, gameType) : getImageUrl(undefined), // 玩家位置
			seatCodeName: item.seatCodeName, // 玩家位置
			resultInfo: item.betStatus === 6 ? 'Fold' : item.resultInfo, // 牌型
			publicCardsImg: [], // 公共牌信息
			handCardsImg: [], // 手牌信息
			netAmount: item?.netAmount || 0, // 打牌盈亏,盈亏筹码
			isInsured: item?.isInsured, // 是否投保:0否,1是
			insuredNetAmount: item?.insuredNetAmount || 0, // 保险池金额
			totalNetAmount: item?.totalNetAmount || 0, // 总盈亏
			squidNumber: item?.squidNumber || 0 // 鱿鱼数量累计值
		}

		// 列表中只要有一项购买了保险,则底部需要展示保险
		if (item?.isInsured === 1) {
			insurePoolDetail.isInsured = 1
		}
		// 获取置灰的图片
		const grayCardsImgObj = getGrayCardImg(item.showCards, item.publicCards, item.handCards, item.handCardExtList)
		bottomRow.publicCardsImg = grayCardsImgObj.publicCardsImg
		bottomRow.handCardsImg = grayCardsImgObj.handCardsImg
		if (!bottomRow.hidden) {
			bottomList.push(bottomRow)
		}
	})
	const summary = {
		gameType: gameType,
		info: info,
		topInfo: topInfo,
		bottomList: bottomList,
		insurePoolDetail: insurePoolDetail
	}
	return summary
}

// 处理牌局回放列表数据【圈层数据】
export function formatGameReplayListRes(gameReplayListRes) {
	if (!gameReplayListRes?.length) {
		return []
	}
	const replayStepArr = []
	gameReplayListRes.forEach((items) => {
		// 牌局步骤标识: [PRE,FLOP,TURN,RIVER,SHOWDOWN]
		const isPre = items.roundCode === 'PRE'
		const isFlop = items.roundCode === 'FLOP'
		const isTurn = items.roundCode === 'TURN'
		const isRiver = items.roundCode === 'RIVER'
		const isShowDown = items.roundCode === 'SHOWDOWN'
		const stepName = stepNameMap.get(items.roundCode)
		// const gameType = items.gameType
		// 步骤顶部信息
		const topInfo = {
			stepName: stepName,
			stepShowIndex: stepName,
			stepItemMiddle: stepItemMiddleArr.indexOf(stepName) > -1,
			isShowPublicCards: true,
			isShowJoinCount: true,
			joinCount: items.joinCount,
			potAmount: items.potAmount,
			publicCardsImg: []
		}
		// pre步骤不展示公共牌和加入人数
		if (isPre) {
			// topInfo.isShowPublicCards = false
			topInfo.isShowJoinCount = false
		}
		// showdown步骤不展示加入人数
		if (isShowDown) {
			topInfo.isShowJoinCount = false
		}
		// 保险池
		const insurePoolDetail = {
			isInsured: 0,
			title: '保险池:',
			src: getOtherImg('insure'),
			insuredPool: items.insuredPool || 0
		}
		// 步骤底部列表
		const bottomList = []
		// 统计弃牌人数
		// let consecutiveFoldCount = 0
		// 数组长度判断是否为索引最后一条
		// const listLength = items?.gameRecordRoundDeatailList?.length
		const winSquidRoundList = [] // 获取鱿鱼的用户数
		items.gameRecordRoundDeatailList?.forEach((item, index) => {
			// 判断是否为连续弃牌折叠用户
			// const isFold = item?.consecutiveFold === true
			// if (isFold) {
			// 	consecutiveFoldCount++
			// }
			item.publicCards = item?.publicCards?.replace(/[\[\]]/g, '')
			item.handCards = item?.handCards?.replace(/[\[\]]/g, '')
			if (topInfo.publicCardsImg.length === 0 && item.publicCards) {
				topInfo.publicCardsImg = getTopInfoPublicCardsImg(isPre ? item.handCards.split(',') : item.publicCards.split(','))
			}
			// 如果是R并且raiseCount>10，直接展示
			item.isPlusBetAction = false
			if (item.betAction === 'R' && item.raiseCount > 10) {
				item.isPlusBetAction = true
			}
			// 如果是R并且raiseCount>2，需要在前面加上raiseCount+B
			if (item.betAction === 'R' && item.raiseCount > 2) {
				item.betAction = item.raiseCount + 'B'
			}
			// betAction是下注指令, F为弃牌操作，需要特殊展示
			const bottomRow = {
				isShowCardEye: item?.showFlag === true, // 是否展示扑克眼睛图片
				fontGrayStyle: item.betAction === 'F' ? 'fontGrayStyle' : '', // 列表项文字置灰
				nickName: formatUserNickName(item.userName, item.nickName),
				isPlusBetAction: item.isPlusBetAction, // 超过10次标识
				betAction: item.betAction, // 玩家下注指令
				betAmount: item.betAmount, // 玩家下注金额
				betActionInsureName: insureNameMap.get(item.betAction) || '', // 玩家下注金额前缀,标识保险类型
				// betOpImg: item.betAction ? getBetOpImg(item.betAction) : getImageUrl(undefined), // 下注指令对应图片
				// seatPosImg: item.seatCode ? getSeatPosImg(item.seatCode, gameType) : getImageUrl(undefined), // 玩家位置对应图片
				seatCodeName: item.seatCodeName, // 玩家位置
				publicCardsImg: [], // 公共牌
				handCardsImg: [], // 手牌
				resultInfo: '-',
				rightValue: '-',
				isFTR: false, // 是否是Flop,Turn,River
				netAmount: item?.netAmount || 0, // 打牌盈亏
				isEVInsured: false, // 是否ev保险
				isInsured: item?.isInsured, // 是否投保:0否,1是
				insuredNetAmount: item?.insuredNetAmount || 0, // 保险池金额
				totalNetAmount: item?.totalNetAmount || 0, // 总盈亏
				isSelf: item?.isSelf, // 是否是自己
				insurePool: item.potAmount, // 投保池金额
				insureType: item.potNo === '0' ? '主池' : '边池' + item.potNo, // 投保池类型
				hidden: item.hidden, // 是否隐藏, 一般此项用于各玩家均能支付Ante的操作项
				isWinSquidRecord: false
			}
			// ANTE转换
			if (item.betAction === 'ANTE') {
				bottomRow.betAction = 'AN'
			}
			// 投保转换
			if (insureNameMap.get(item.betAction)) {
				bottomRow.betAction = 'Ins'
			}
			// EVTB转换
			if (item.betAction === 'EVTB') {
				bottomRow.betAction = 'EV'
				bottomRow.isEVInsured = true
			}
			// 玩家行为对应背景色
			bottomRow.betActionColor = betOpColorMap.get(bottomRow.betAction) || '#d64f4f'

			// 列表中只要有一项购买了保险,则底部需要展示保险
			if (item?.isInsured === 1) {
				insurePoolDetail.isInsured = 1
			}
			// 不同的步骤，右侧获取的值不一样
			if (isPre) {
				// 获取值为afterChip,展示玩家剩余筹码数量
				// 获取值为leijiPotAmount 展示玩家每一次操作后，对应的底池数量
				bottomRow.afterChip = item?.afterChip
				bottomRow.leijiPotAmount = item.leijiPotAmount
			}
			if (isFlop || isTurn || isRiver) {
				bottomRow.afterChip = item?.afterChip
				bottomRow.leijiPotAmount = item.leijiPotAmount
				bottomRow.isFTR = true
			}
			if (isShowDown) {
				// ShowDown 获取值为netAmount,展示输赢筹码
				bottomRow.resultInfo = item.betAction === 'F' ? 'Fold' : item.resultInfo // 牌型

				// 只有showDown步骤才会展示手牌和公共牌
				const publicCardsArr = item.publicCards ? item.publicCards.split(',') : []
				const handCardsArr = item.handCards ? item.handCards.split(',') : []

				// 获取置灰的图片
				const grayCardsImgObj = getGrayCardImg(item.showCards, publicCardsArr, handCardsArr)
				bottomRow.publicCardsImg = grayCardsImgObj.publicCardsImg
				bottomRow.handCardsImg = grayCardsImgObj.handCardsImg

				// ShowDown判断鱿鱼用户记录是否获得到鱿鱼(0否  1是 )
				if (item.winSquid === 1) {
					winSquidRoundList.push({
						isWinSquidRecord: true,
						seatCodeName: item.seatCodeName, // 玩家位置
						nickName: bottomRow.nickName, // 昵称
						handCardsImg: bottomRow.handCardsImg, // 手牌图片
						betOpImg: getOtherImg('squid_red'),
						squidNumber: item.squidNumber, // 鱿鱼数量
						squidMarkName: squidMarkMap.get(item.squidMark) || '' // 鱿鱼获取标记
					})
				}
			} else {
				// 获取其他圈层手牌图片
				bottomRow.handCardsImg = item.handCards ? item.handCards.split(',').map((handCard) => ({ src: getCardsImg(handCard) })) : []
			}
			// 连续弃牌人数大于2，最后一条或者下一条的fold为false，则将上一条的记录加入列表中
			// if (consecutiveFoldCount > 2) {
			// 	if (!isFold || index === listLength - 1) {
			// 		const foldRow = Object.assign({}, bottomRow)
			// 		foldRow.isShowFold = true
			// 		foldRow.showFoldTitle = consecutiveFoldCount + ' folds'
			// 		bottomList.push(foldRow)
			// 		consecutiveFoldCount = 0
			// 	}
			// }
			// 加入未连续弃牌的记录
			// if (!isFold) {
			// 	bottomRow.isShowFold = false
			// 	bottomRow.showFoldTitle = ''
			// 	if (!bottomRow.hidden) {
			// 		bottomList.push(bottomRow)
			// 	}
			// }
			bottomRow.isShowFold = false
			bottomRow.showFoldTitle = ''
			if (!bottomRow.hidden) {
				bottomList.push(bottomRow)
			}
		})
		const step = {
			topInfo: topInfo,
			bottomList: bottomList,
			insurePoolDetail: insurePoolDetail
		}
		replayStepArr.push(step)

		// 如果是showdown层下的用户获取鱿鱼记录则添加
		if (winSquidRoundList.length) {
			replayStepArr.push({
				topInfo: {
					stepName: '鱿鱼',
					stepShowIndex: 'squidRecord',
					stepItemMiddle: true
				},
				bottomList: winSquidRoundList
			})
		}
	})
	return replayStepArr
}

// 格式化账号+昵称
function formatUserNickName(userName, nickName) {
	let first = ''
	let second = ' (-)'
	if (userName) {
		first = userName
	}
	if (nickName) {
		second = ` (${nickName})`
	}
	return first + second
}
// 大盲,小盲,前注数字格式化
function formatScoreNumber(val) {
	const num = Number(val) || 0
	const kNum = 1000 // K单位
	const mNum = 1000000 // M单位
	let newVal = val
	if (num >= mNum) {
		newVal = num / mNum + 'M'
	} else if (num >= kNum) {
		newVal = num / kNum + 'K'
	}
	return newVal
}
// 获取牌局头部公牌图片
function getTopInfoPublicCardsImg(publicCardsNum, markCardArr = []) {
	if (!publicCardsNum) return []
	// 获取改牌标识
	const markCardsArr = markCardArr?.length > 0
		? markCardArr.filter((item) => item.manipulateMark === true).map((item) => parseInt(item.cardNum))
		: []

	return publicCardsNum.map((publicCard) => (
		{
			isShowCardMark: markCardsArr.indexOf(parseInt(publicCard)) > -1,
			src: getCardsImg(publicCard)
		}
	))
}

// 获取置灰标识牌谱图片
function getGrayCardImg(showGrayCardsArr, publicCardsArr, handCardsArr, markCardArr = []) {
	// 获取改牌标识
	const markCardsArr = markCardArr?.length > 0
		? markCardArr.filter((item) => item.manipulateMark === true).map((item) => parseInt(item.cardNum))
		: []

	// 牌置灰标识
	const grayCardsArr = showGrayCardsArr
		.filter((item) => Object.values(item)[0] === true)
		.map((item) => parseInt(Object.keys(item)[0]))

	// 判断公牌是否在置灰标识里
	const publicCardsImg = publicCardsArr.map((publicCard) => (
		{
			isShowCardGray: grayCardsArr.indexOf(parseInt(publicCard)) > -1,
			src: getCardsImg(publicCard)
		}
	))
	// 判断手牌是否在置灰标识里
	const handCardsImg = handCardsArr.map((handCard) => (
		{
			isShowCardMark: markCardsArr.indexOf(parseInt(handCard)) > -1,
			isShowCardGray: grayCardsArr.indexOf(parseInt(handCard)) > -1,
			src: getCardsImg(handCard)
		}
	))
	return { publicCardsImg, handCardsImg }
}

// 获取图片地址
function getImageUrl(imgName) {
	if (!imgName) {
		imgName = 'icon-fail.png'
	}
	return require('@/assets/images/game/' + imgName)
}

// 获取游戏名称
function getGameTypeName(gameTypeId) {
	let gameTypeName = gameTypeMap.get(gameTypeId)
	if (!gameTypeName) {
		gameTypeName = 'Game'
	}
	return gameTypeName
}

// 根据扑克数字获取映射的图片地址
export function getCardsImgArr(cardsNumArr) {
	const imgPathArr = []
	cardsNumArr.forEach((item) => {
		const imgName = cardsMap.get(parseInt(item))
		imgPathArr.push(getImageUrl(imgName))
	})
	return imgPathArr
}

// 根据数字获取扑克图片
export function getCardsImg(cardsNum) {
	const imgName = cardsMap.get(parseInt(cardsNum))
	return getImageUrl(imgName)
}

// 获取根据gameType获取对应的位置图片
// function getSeatPosImg(level, gameType) {
// 	// 德州长牌和短牌根据gameType映射的图片不一样
// 	let imgName = null
// 	// 德州
// 	if (gameType === 2001) {
// 		imgName = texasSeatPosMap.get(parseInt(level))
// 	}
// 	// 短牌
// 	if (gameType === 2002) {
// 		imgName = shortSeatPosMap.get(parseInt(level))
// 	}
// 	return getImageUrl(imgName)
// }

// 获取操作动作图片
// function getBetOpImg(level) {
// 	const imgName = betOpMap.get(level)
// 	return getImageUrl(imgName)
// }

// 获取其他图片
function getOtherImg(imgCode) {
	const imgName = otherImgMap.get(imgCode)
	return getImageUrl(imgName)
}

// 参考app端数字扑克映射
const otherImgMap = new Map([
	['eye', 'invalidName.png'],
	['insure', 'baoxian.png'],
	['abnormal', 'abnormal.png'],
	['squid_red', 'gameReviewOp_YR.png'] // 鱿鱼红色图标
])

// 游戏类型
const gameTypeMap = new Map([
	[2001, '德州扑克'],
	[2002, '短牌'],
	[2003, 'AOF德州'],
	[2004, 'AOF短牌']
])

// 数字对应扑克
const cardsMap = new Map([
	[0, 'diamond_a_card.png'], // 方片1
	[1, 'club_a_card.png'], // 梅花1
	[2, 'heart_a_card.png'], // 红桃1
	[3, 'spade_a_card.png'], // 黑桃1

	[4, 'diamond_2_card.png'], // 方片2
	[5, 'club_2_card.png'], // 梅花2
	[6, 'heart_2_card.png'], // 红桃2
	[7, 'spade_2_card.png'], // 黑桃2

	[8, 'diamond_3_card.png'], // 方片3
	[9, 'club_3_card.png'], // 梅花3
	[10, 'heart_3_card.png'], // 红桃3
	[11, 'spade_3_card.png'], // 黑桃3

	[12, 'diamond_4_card.png'], // 方片4
	[13, 'club_4_card.png'], // 梅花4
	[14, 'heart_4_card.png'], // 红桃4
	[15, 'spade_4_card.png'], // 黑桃4

	[16, 'diamond_5_card.png'], // 方片5
	[17, 'club_5_card.png'], // 梅花5
	[18, 'heart_5_card.png'], // 红桃5
	[19, 'spade_5_card.png'], // 黑桃5

	[20, 'diamond_6_card.png'], // 方片6
	[21, 'club_6_card.png'], // 梅花6
	[22, 'heart_6_card.png'], // 红桃6
	[23, 'spade_6_card.png'], // 黑桃6

	[24, 'diamond_7_card.png'], // 方片7
	[25, 'club_7_card.png'], // 梅花7
	[26, 'heart_7_card.png'], // 红桃7
	[27, 'spade_7_card.png'], // 黑桃7

	[28, 'diamond_8_card.png'], // 方片8
	[29, 'club_8_card.png'], // 梅花8
	[30, 'heart_8_card.png'], // 红桃8
	[31, 'spade_8_card.png'], // 黑桃8

	[32, 'diamond_9_card.png'], // 方片9
	[33, 'club_9_card.png'], // 梅花9
	[34, 'heart_9_card.png'], // 红桃9
	[35, 'spade_9_card.png'], // 黑桃9

	[36, 'diamond_10_card.png'], // 方片10
	[37, 'club_10_card.png'], // 梅花10
	[38, 'heart_10_card.png'], // 红桃10
	[39, 'spade_10_card.png'], // 黑桃10

	[40, 'diamond_j_card.png'], // 方片J
	[41, 'club_j_card.png'], // 梅花J
	[42, 'heart_j_card.png'], // 红桃J
	[43, 'spade_j_card.png'], // 黑桃J

	[44, 'diamond_q_card.png'], // 方片Q
	[45, 'club_q_card.png'], // 梅花Q
	[46, 'heart_q_card.png'], // 红桃Q
	[47, 'spade_q_card.png'], // 黑桃Q

	[48, 'diamond_k_card.png'], // 方片K
	[49, 'club_k_card.png'], // 梅花K
	[50, 'heart_k_card.png'], // 红桃K
	[51, 'spade_k_card.png'] // 黑桃K
])

// 德州长牌位置gameType: 2001
// const texasSeatPosMap = new Map([
// 	[1, 'gameReviewPos_utg.png'],
// 	[2, 'gameReviewPos_utg1.png'],
// 	[3, 'gameReviewPos_mp1.png'],
// 	[4, 'gameReviewPos_mp2.png'],
// 	[5, 'gameReviewPos_ht.png'],
// 	[6, 'gameReviewPos_co.png'],
// 	[7, 'gameReviewPos_btn.png'],
// 	[8, 'gameReviewPos_sb.png'],
// 	[9, 'gameReviewPos_bb.png']
// ])

// 短牌位置 gameType:2002
// const shortSeatPosMap = new Map([
// 	[1, 'gameReviewPos_utg.png'],
// 	[2, 'gameReviewPos_utg1.png'],
// 	[3, 'gameReviewPos_utg2.png'],
// 	[4, 'gameReviewPos_mp.png'],
// 	[5, 'gameReviewPos_mp1.png'],
// 	[6, 'gameReviewPos_mp2.png'],
// 	[7, 'gameReviewPos_ht.png'],
// 	[8, 'gameReviewPos_co.png'],
// 	[9, 'gameReviewPos_btn.png']
// ])

// 动作
// const betOpMap = new Map([
// 	['2B', 'gameReviewOp_2b.png'],
// 	['3B', 'gameReviewOp_3b.png'],
// 	['4B', 'gameReviewOp_4b.png'],
// 	['5B', 'gameReviewOp_5b.png'],
// 	['6B', 'gameReviewOp_6b.png'],
// 	['7B', 'gameReviewOp_7b.png'],
// 	['8B', 'gameReviewOp_8b.png'],
// 	['9B', 'gameReviewOp_9b.png'],
// 	['10B', 'gameReviewOp_10b.png'],
// 	['A', 'gameReviewOp_A.png'],
// 	['B', 'gameReviewOp_B.png'],
// 	['BB', 'gameReviewOp_BB.png'],
// 	['C', 'gameReviewOp_C.png'],
// 	['F', 'gameReviewOp_F.png'],
// 	['R', 'gameReviewOp_R.png'],
// 	['RB', 'gameReviewOp_RB.png'],
// 	['SB', 'gameReviewOp_SB.png'],
// 	['X', 'gameReviewOp_X.png'],
// 	['TB', 'gameReviewOp_INS.png'],
// 	['PF', 'gameReviewOp_INS.png'],
// 	['BH', 'gameReviewOp_INS.png'],
// 	['ANTE', 'gameReviewOp_ANTE.png']
// ])

// 行为数据颜色
const betOpColorMap = new Map([
	['2B', '#d64f4f'],
	['3B', '#d64f4f'],
	['4B', '#d64f4f'],
	['5B', '#d64f4f'],
	['6B', '#d64f4f'],
	['7B', '#d64f4f'],
	['8B', '#d64f4f'],
	['9B', '#d64f4f'],
	['10B', '#d64f4f'],
	['A', '#d64f4f'],
	['B', '#d64f4f'],
	['BB', '#3eb76a'],
	['C', '#3eb76a'],
	['F', '#7e7e7e'],
	['R', '#d64f4f'],
	['RB', '#ff6e00'],
	['SB', '#3eb76a'],
	['X', '#3eb76a'],
	['TB', '#ffcd01'],
	['PF', '#ffcd01'],
	['BH', '#ffcd01'],
	['Ins', '#ffcd01'],
	['EV', '#ffcd01'],
	['ANTE', '#3eb76a'],
	['AN', '#3eb76a'],
	['S', '#d64f4f']
])
