import emojiRegex from 'emoji-regex/RGI_Emoji.js'

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
	return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
	const valid_map = ['admin', 'editor', 'howard']
	return valid_map.indexOf(str.trim()) >= 0
}

export function notSpecial2(str) {
	const specialKey =
		'#$&@/&*!~~!%^()_\\-+=<>?:"{}|,./;\'\\\\[\\]·~！@#￥%……&*（）——\\-+={}|《》？：“'
	for (let i = 0; i < str.length; i++) {
		if (specialKey.indexOf(str.substr(i, 1)) !== -1) {
			return false
		}
	}
	return true
}

export function isHaveEmoji(str) {
	const regex = emojiRegex()
	return !!regex.exec(str)
}

// 获取字符串截取指定长度后新的字符串和长度
export function getNewSubStrAndLen(str, maxLen = 12) {
	let newStr = str
	let newLen = 0
	let charCode = -1
	let wordLen = 0
	const strLen = str.length
	for (let i = 0; i < strLen; i++) {
		charCode = str.charCodeAt(i)
		wordLen = 2
		if (charCode >= 0 && charCode <= 128) {
			wordLen = 1
		}
		newLen += wordLen
		if (newLen > maxLen) {
			newStr = str.substr(0, i)
			newLen -= wordLen
			break
		}
	}
	return { newStr, newLen }
}
