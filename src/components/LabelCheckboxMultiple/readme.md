<LabelCheckboxMultiple
:dataList="dataList"
@getSelectData="getSelectData(\$event)"
:showCheckComponent="true" ></LabelCheckboxMultiple>

getSelectData：接收子组件传递选中的参数
clearAllSelect: 重置清除所选
dataList：传给子组件的数据
showCheckComponent：四否显示该组件
showColon：是否显示第一个标题后边的冒号

父组件数据传递格式：
dataList: [
{
label: '场馆一',
isCheck: false,
showColon: false,
checkedData: [],
dataChildList: [
{
label: '1111',
text: '德州 1'
},
{
label: '2222',
text: '德州 2'
}
]
},
{
label: '场馆二',
isCheck: false,
showColon: false,
checkedData: [],
dataChildList: [
{
label: '3333',
text: '德州 3'
},
{
label: '44444',
text: '德州 4'
}
]
}
]
