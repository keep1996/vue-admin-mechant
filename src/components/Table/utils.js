import Sortable from 'sortablejs'

export const sortTable = ({
    start,
    onChange = () => {}
}) => {
    if (start !== true) return

    const wrapperTr = document.querySelector(
        '.dx-table .el-table__header-wrapper tr'
    )
    Sortable.create(wrapperTr, {
        animation: 180,
        delay: 0,
        onEnd: (evt) => {
            onChange()
        }
    })
}

export const isFunction = (val) => Object.prototype.toString.call(val) === '[object Function]'
