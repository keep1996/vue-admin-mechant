import { Decimal } from 'decimal.js'
import './style.css'

const isFun = (val) => typeof val === 'function'

// 序号 TODO
export const order = () => {}

// 显示多行
export const multiLine = ({ prop, label, ...otherProps }) => ({
	prop: prop?.join(''),
	fixed: 'right',
	header: () => {
		return label?.map((subLable, index) => (
			<div key={prop[index]}>{subLable}</div>
		))
	},
	customRender: ({ row }) => {
		return prop?.map((key) => <div key={key}>{row[key]}</div>)
	},
	...otherProps
})

// 状态显示
export const status = ({
	prop = 'status',
	label = '状态',
	texts = {},
	status = {
		0: 'info',
		1: 'success',
		2: 'danger'
	}
}) => ({
	prop,
	label,
	customRender: ({ row }) => {
		const val = row[prop]
		return <el-tag type={status?.[val]}>{texts?.[val]}</el-tag>
	}
})

// 显示额度和货币
export const balance = ({
	prop,
	label,
	digit = 4 // 保留几位小数
}) => ({
	prop,
	label,
	customRender: (row) => {
		const num = row[prop]
		const currency = this.handleCurrency() // 暂时写死美元
		if (typeof num === 'number') {
			const data = new Decimal(
				new Decimal(num).toFixed(digit, Decimal.ROUND_DOWN)
			).toNumber()

			return [
				currency,
				data.toLocaleString('en-us', {
					minimumFractionDigits: data === 0 ? 0 : digit,
					useGrouping: true
				})
			]
		}
		return [currency, ' 0']
	}
})

// edit
export const edit = ({
	prop,
	label,
	isEdit = false,
	customRender,
	editRender,
	...otherProps
}) => ({
	prop,
	label,
	customRender: (columnProps) => {
		const { row } = columnProps
		if (isEdit || row?.isEdit) {
			return editRender(columnProps)
		}

		if (customRender) {
			return customRender(columnProps)
		}
		if (row[prop] === undefined) {
			return '-'
		}
		return row[prop]
	},
	...otherProps
})

export const actions = ({
	prop = 'actions',
	label = '操作',
	btns,
	editBtns,
	...otherProps
}) => ({
	prop,
	label,
	customRender: (columnProps) => {
		const { row } = columnProps
		let btnDoms = null
		if (row.isEdit) {
			const [cancelProps, okProps] = editBtns
			const onCancel = isFun(cancelProps) ? cancelProps : () => {}
			const onOk = isFun(okProps) ? okProps : () => {}
			btnDoms = [
				<el-button size='medium' onClick={() => onCancel(columnProps)}>
					取消
				</el-button>,
				<el-button
					type='primary'
					size='medium'
					onClick={() => onOk(columnProps)}
				>
					保存
				</el-button>
			]
		} else if (btns?.length > 0) {
			btnDoms = btns.map((btnProps) => {
				const { type, text, onClick = () => {}, isShow, disabled, ...otherProps } = btnProps
				const _disabled = typeof disabled === 'function' ? disabled(columnProps) : disabled
				if (isShow !== undefined) {
					const _isShow = typeof isShow === 'function' ? isShow(columnProps) : isShow

					if (_isShow === false) {
						return null
					}
				}

				return (
					<el-button
						type={_disabled ? '' : (type || 'primary')}
						onClick={() => onClick(columnProps)}
						disabled={_disabled}
						{...otherProps}
					>
						{text}
					</el-button>
				)
			})
		}

		return <div class='column-actions'>{btnDoms}</div>
	},
	...otherProps
})
const ratio = ({ label, prop, mult = 1, fixed = 2, ...otherProps }) => ({
	label,
	prop,
	customRender: ({ row, column }) => {
		const val = row[column.property]
		if (isNaN(val) || val === null || val === undefined) {
			return '-'
		}
		const ret = new Decimal(val).mul(mult).toFixed(fixed)
		return ret + '%'
	},
	...otherProps
})

export default {
	multiLine,
	status,
	balance,
	actions,
	edit,
	ratio
}
