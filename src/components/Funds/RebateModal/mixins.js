export default {
	data() {
		return {
			isShowOtherRebateDialog: false,
			isShowTexasRebateDialog: false,
			rebateModalParams: {},
			venueType: null
		}
	},
	components: {
		OtherRebateModal: () =>
			import('@/components/Funds/RebateModal/otherRebateModal.vue'),
		TexasRebateModal: () =>
			import('@/components/Funds/RebateModal/texasRebateModal.vue')
	},
	methods: {
		showOtherRebateDialog(rowData, venueType) {
			console.log(rowData, 'show qt modal')
			this.venueType = venueType
			this._getModalParams(rowData)
			this.isShowOtherRebateDialog = true
		},
		showTexasRebateDialog(rowData) {
			console.log(rowData, 'show dz modal')
			this._getModalParams(rowData)
			this.isShowTexasRebateDialog = true
		},
		_getModalParams(rowData) {
			const { reportDate, memberName, cycleStartDate, cycleEndDate, proxyName, commissionMode } = rowData || {}
			this.rebateModalParams = {
                memberName,
                reportDate,
				cycleStartDate: cycleStartDate || reportDate,
				cycleEndDate: cycleEndDate || reportDate,
				venueType: this.venueType,
				proxyName,
				commissionMode
			}
		}
	}
}
