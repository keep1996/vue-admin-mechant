import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case '有效投注':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case '分成金额':
						case '返点金额':
						case this.$t(
							'funds.proxy_member_funds_record.rebate_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount || totalSummary.personalRebateAmount || totalSummary.personRebateAmountRebate || totalSummary.personRebateAmountCommission
											)}
										</span>
									</p>
								</div>
							)
							break
						case '服务费':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.serviceCharge || totalSummary.serviceChargeAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case '服务费贡献':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.serviceContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case '投注金额':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case '会员输赢':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
