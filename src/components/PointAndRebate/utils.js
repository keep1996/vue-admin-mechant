export const rebateRender = ({ row, column }) => {
    const num = row[column.property]
    if (row.status === 3 && num < 0) {
        return '-'
    }
    if (num === undefined || num === null) return '-'
    return `${num}%`
}

// 团队返点keys
export const rebateKeys = ['texasRebate', 'texasInsuranceRebate', 'actualPersonRebate', 'sportsRebate', 'lotteryTicketRebate', 'chessRebate', 'esportsRebate']
