import Vue from 'vue'
import ImageViewer from './ImageViewer'
import MerchantSelect from './MerchantSelect'
import CardDescriptionItem from './CardDescriptionItem'
import LabelCheckboxMultiple from './LabelCheckboxMultiple'

Vue.component('ImageViewer', ImageViewer)
Vue.component('MerchantSelect', MerchantSelect)
Vue.component('CardDescriptionItem', CardDescriptionItem)
Vue.component('LabelCheckboxMultiple', LabelCheckboxMultiple)
