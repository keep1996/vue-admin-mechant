import i18n from '@/locales'

export default {
	1: {
		formName: 'commissionContractList',
		columns: [
			{
				label: i18n.t('common.components.win'),
				suffix: '¥',
				prop: 'netAmount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			// {
			// 	label: i18n.t('common.components.template_type'),
			// 	suffix: i18n.t('common.components.template_type'),
			// 	prop: 'validBetAmount',
			// 	input: {
			// 		min: 0,
			// 		max: 9999999999,
			// 		precision: 0
			// 	}
			// },
			{
				label: i18n.t('common.components.activity_number'),
				suffix: i18n.t('common.components.people'),
				prop: 'activeUsers',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			// {
			// 	label: i18n.t('common.components.template_type'),
			// 	suffix: i18n.t('common.components.template_type'),
			// 	prop: 'effecactiveUsers',
			// 	input: {
			// 		min: 0,
			// 		max: 9999999999,
			// 		precision: 0
			// 	}
			// },
			{
				label: i18n.t('common.components.new_activity'),
				suffix: i18n.t('common.components.people'),
				prop: 'monthAddActiveCount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			}
		]
	},
	2: {
		formName: 'rebateContractList',
		columns: [
			{
				label: i18n.t('common.components.flow'),
				suffix: i18n.t('common.components.money'),
				prop: 'validBetAmount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			{
				label: i18n.t('common.components.activity_person'),
				suffix: i18n.t('common.components.people'),
				prop: 'activeUsers',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			{
				label: i18n.t('common.components.true_activity_person'),
				suffix: i18n.t('common.components.people'),
				prop: 'effecactiveUsers',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			}
		]
	}
}
