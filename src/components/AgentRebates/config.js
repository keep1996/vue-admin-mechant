// 代理
// 平台默认会员返水：userType =  2 ,  ( platformFlag =1  )
// 代理返点         userType = 1 ,   zhanchengfalg  = 0  ( platformFlag = 0 )
// 代理占成         userType = 1 ,   zhanchengfalg  = 1  (platformFlag = 0 )

// 会员
// 平台发放会员返水：userType =  0 , platformFlag =1
// 代理发放返水  userType = 0 , platformFlag = 0

// 通用配置
const texasRebate = {
	prop: 'texasRebate',
	label: '俱乐部'
}
const texasInsuranceRebate = {
	prop: 'texasInsuranceRebate',
	label: '保险'
}
const valueAddedServiceRebate = {
	prop: 'valueAddedServiceRebate',
	label: '增值服务'
}
const actualPersonRebate = {
	prop: 'actualPersonRebate',
	label: '真人'
}
const sportsRebate = {
	prop: 'sportsRebate',
	label: '体育'
}

const lotteryTicketRebate = {
	prop: 'lotteryTicketRebate',
	label: '彩票'
}

const chessRebate = {
	prop: 'chessRebate',
	label: '棋牌'
}

const esportsRebate = {
	prop: 'esportsRebate',
	label: '电竞'
}

const electronicRebate = {
	prop: 'electronicRebate',
	label: '电子'
}

export const sportsRebateKeys = {
	1: 'sportsARebate',
	2: 'sportsBRebate',
	3: 'sportsCRebate',
	4: 'sportsDRebate'
}

// 体育盘口
const sportsRebateCols = {
	prop: 'sportsRebates',
	label: '体育',
	columns: [
		{
			prop: 'sportsARebate',
			label: 'A盘'
		},
		{
			prop: 'sportsBRebate',
			label: 'B盘'
		},
		{
			prop: 'sportsCRebate',
			label: 'C盘'
		},
		{
			prop: 'sportsDRebate',
			label: 'D盘',
			tip: 'D盘会员赔率最好，同时D盘的注单不会产生任何返点和返水。'
		}
	]
}


// 体育盘口下拉
const sportsRebateSelect = {
	label: '体育',
	prop: 'sportsRebate',
	slot: 'sportsRebateSelect',
	name: 'handicapType',
	width: '220px',
	opts: [
		{
			value: 1,
			label: 'A盘'
		},
		{
			value: 2,
			label: 'B盘'
		},
		{
			value: 3,
			label: 'C盘'
		},
		{
			value: 4,
			label: 'D盘',
			
		}
	]
}

export const baseColumns = [
	texasRebate,
	texasInsuranceRebate,
	valueAddedServiceRebate,
	actualPersonRebate,
	sportsRebate,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]

export const initRow = (columns) => {
	const row = {}

	const _cols = columns || baseColumns
	_cols.forEach((col) => {
		
		if (col.columns) {
			col.columns.forEach(subCol => {
				row[subCol.prop] = subCol.prop === 'sportsDRebate' ? 0 : undefined
			})
			
		} else {
			if(col.name) {
				row[col.name] = col.opts[0].value || undefined
			}
			row[col.prop] = undefined
		}
	})

	return row
}

const dzRebatesColumns = [
	texasRebate,
	texasInsuranceRebate,
	valueAddedServiceRebate
]
export const dzConfig = {
	label: '德州场馆',
	subLabel: '分成比例',
	name: 'dzRebates',
	id: 'dzConfig',
	query: {
		platformFlag: 0,
		userType: 1,
		zhanchengFlag: 0
	},
	columns: dzRebatesColumns,
	value: {
		...initRow(dzRebatesColumns)
	},
	maxValueRange: {}
}

const teamRebatesColumns = [
	actualPersonRebate,
	sportsRebateCols,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]
export const teamRebatesConfig = {
	label: '其他场馆',
	subLabel: '团队返点比例',
	name: 'teamRebates',
	id: 'teamRebatesConfig',
	query: {
		platformFlag: 0,
		userType: 1,
		zhanchengFlag: 0
	},
	columns: teamRebatesColumns,
	value: {
		...initRow(teamRebatesColumns)
	},
	maxValueRange: {}
}

const teamContributionColumns = [
	actualPersonRebate,
	sportsRebate,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]
export const teamContributionConfig = {
	label: '其他场馆',
	subLabel: '团队占成',
	name: 'teamContribution',
	id: 'teamContributionConfig',
	query: {
		platformFlag: 0,
		userType: 1,
		zhanchengFlag: 1
	},
	columns: teamContributionColumns,
	value: {
		...initRow(teamContributionColumns)
	},
	maxValueRange: {}
}

// 平台默认会员返水
const memberPlatformColumns = [
	texasRebate,
	actualPersonRebate,
	sportsRebate,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]
export const memberPlatformConfig = {
	label: '平台默认会员返水',
	subLabel: '返水比例',
	name: 'memberPlatform',
	id: 'memberPlatformConfig',
	query: {
		platformFlag: 1,
		// 代理详情 取 userType 2 会员取userType 0
		userType: 2,
		zhanchengFlag: 0
	},
	columns: memberPlatformColumns,
	value: {
		...initRow(memberPlatformColumns)
	},
	maxValueRange: {}
}

// 代理发放返水比例
const agentPointColumns = [
	texasRebate,
	actualPersonRebate,
	sportsRebate,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]
export const agentPointConfig = {
	label: '代理发放返水比例',
	subLabel: '个人返水',
	name: 'agentPoint',
	id: 'agentPointConfig',
	query: {
		platformFlag: 0,
		userType: 0,
		zhanchengFlag: 0
	},
	columns: agentPointColumns,
	value: {
		...initRow(agentPointColumns)
	},
	maxValueRange: {}
}

const commissionDzColumns = [
	{
		prop: 'texasCommission',
		label: '俱乐部'
	},
	{
		prop: 'texasInsuranceCommission',
		label: '保险'
	},
	{
		prop: 'valueAddedServiceCommission',
		label: '增值服务'
	}
]

export const commissionDzConfig = {
	label: '德州场馆',
	subLabel: '分成比例',
	name: 'commissionDzRebates',
	id: 'commissionDzConfig',
	query: {
	},
	columns: commissionDzColumns,
	value: {
		...initRow(commissionDzColumns),
		serialNo: 1
		// validBetAmount: 0,
		// activeUsers: 0,
		// addActiveUsers: 0
	},
	maxValueRange: {}
}

const otherCommissionColumns = [
	{
		prop: 'otherCommission',
		label: '团队佣金比例'
	}
]
export const otherCommission = {
	label: '其他场馆',
	subLabel: '',
	name: 'otherCommission',
	id: 'otherCommission',
	query: {
	},
	columns: otherCommissionColumns,
	value: {
		...initRow(otherCommissionColumns)
	},
	maxValueRange: {}
}

// 平台默认会员返水
const commissionMemberPlatformColumns = [
	actualPersonRebate,
	sportsRebateSelect,
	lotteryTicketRebate,
	chessRebate,
	esportsRebate,
	electronicRebate
]
export const commissionMemberPlatformConfig = {
	label: '平台默认会员返水',
	subLabel: '返水比例',
	name: 'memberPlatform',
	id: 'commissionMemberPlatformConfig',
	query: {
		platformFlag: 1,
		userType: 2
	},
	columns: commissionMemberPlatformColumns,
	value: {
		...initRow(commissionMemberPlatformColumns)
	},
	maxValueRange: {}
}

// 代理发放返水比例
const commissionAgentPointColumns = [
	{
		prop: 'texasRebate',
		label: '德州-俱乐部'
	}
]
export const commissionAgentPointConfig = {
	label: '代理发放返水比例',
	subLabel: '个人返水',
	name: 'agentPoint',
	id: 'commissionAgentPointConfig',
	query: {
		platformFlag: 0,
		userType: 0,
		zhanchengFlag: 0
	},
	columns: commissionAgentPointColumns,
	value: {
		...initRow(commissionAgentPointColumns)
	},
	maxValueRange: {}
}

export const rebateRender = ({ row, column }) => {
	const num = row[column.property]
	if (row.status === 3 && num < 0) {
		return '-'
	}
	if (num === undefined || num === null) return '-'
	return `${num}%`
}

export const pointConfigs = ({
	commissionModel
}) => {
	if (commissionModel === 1) {
		return [
			dzConfig, // 德州场馆
			teamRebatesConfig, // 团队返点
			teamContributionConfig, // 占成返点
			// memberPlatformConfig,
			agentPointConfig
		]
	}
	return [
		commissionDzConfig, // 德州场馆
		otherCommission, // 其他场馆
		commissionMemberPlatformConfig, // 平台默认返水比例
		commissionAgentPointConfig
	]
}
