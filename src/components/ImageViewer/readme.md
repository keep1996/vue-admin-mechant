- `image-viewer 图片预览`

```vue
<image-viewer :visible.sync="imageViewerVisible" :url-list="[imageViewerSrc]" />

visible: 是否显示
url-list: 图片地址列表

```