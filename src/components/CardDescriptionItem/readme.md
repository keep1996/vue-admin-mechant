CardDescriptionItem 卡片式列表形式多个字段数据展示
<CardDescriptionItem
					:title="titles"
					:titleColor="'#fc0'"
					:columns="3"
					:isShowComponent="true"
					:size="'medium'"
					:colon="false"
					:isShowBorder="true"
					:directions="'horizontal'"
					:descriptionItemList="lists"
				></CardDescriptionItem>
title: 卡片式列表标题
titleColor：卡片式列表标题颜色
columns：展示多少列字段数据
isShowComponent: 是否显示该卡片列表组件
size：列表的尺寸，可选值有"medium","small","mini"
colon: 列表项的 label 是否要显示冒号
isShowBorder：卡片列表是否需要显示边框
directions：卡片列表每一项的排列方式，可选值有"horizontal","vertical"
descriptionItemList: 卡片列表数组，里边包含每一项的值，包括，每一项的 label 值，每一项 label 所对应的 value 值，以及
每一项 value 的颜色
