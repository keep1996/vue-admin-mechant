import i18n from '@/locales'

export const BUSINESS_MODELS = [
	{ value: '0', label: i18n.t('common.business_model_both') },
	{ value: '1', label: i18n.t('common.business_model_cash') },
	{ value: '2', label: i18n.t('common.business_model_credit') }
]
