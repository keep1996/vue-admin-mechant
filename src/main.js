import Vue from 'vue'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
import '@/assets/js/gt4'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueLazyload from 'vue-lazyload'
import '@/styles/index.scss' // global css
import store from './store'
import i18n from './locales'
import '@/icons' // icon
import '@/permission' // permission control
import apiInstall from '@/utils/apiInstall'
import { hasPermission } from '@/mixins/methods'
import VueClipboard from 'vue-clipboard2' // 引入复制到剪切板插件
import { supportBrower } from './utils/judgeBrower'
import App from './App'
import router from './router'
import 'default-passive-events'
import './directives' // 全局注册自定义指令
// 开发环境mock
if (process.env.VUE_APP_MOCK === true) {
	require('@/mock')
	const xhr = new window._XMLHttpRequest()
	window.XMLHttpRequest.prototype.upload = xhr.upload
}
supportBrower()

import '@/components'

Vue.use(VueClipboard) // 挂载复制到剪切板插件

Vue.use(VueLazyload, {
	preLoad: 1,
	attempt: 1
})

// 多语言支持
Vue.use(ElementUI, {
	i18n: (key, value) => i18n.t(key, value)
})

// 加入api插件
Vue.use(apiInstall)

Vue.prototype.$initGeetest4 = window.initGeetest4
Vue.config.productionTip = false
Vue.mixin({
	methods: {
		hasPermission
	}
})

const vm = new Vue({
	el: '#app',
	i18n,
	router,
	store,
	render: (h) => h(App)
})

window.h = vm.$createElement
// 禁用禁用Promise reject错误输出控制台
window.addEventListener('unhandledrejection', function browserRejectionHandler(
	event
) {
	event && event.preventDefault()
})
