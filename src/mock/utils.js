export const getParams = (url, name) => {
	const index = url.indexOf('?')
	if (index !== -1) {
		const newArr = url.substring(index + 1).split('&')
		for (var i = 0; i < newArr.length; i++) {
			const itemArr = newArr[i].split('=')
			if (itemArr[0] === name) {
				return itemArr[1]
			}
		}
	}
	return null
}
