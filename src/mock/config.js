import dxn from '@/mock/modules/dxn'
import memberFundsRecord from '@/mock/modules/funds/memberFundsRecord'
// import betSlip from '@//mock/modules/betSlip'

import cardTablesList from '@/mock/modules/dxn/gameManager/cardTablesList'
import gameList from '@/mock/modules/dxn/gameManager/gameList'
import gameRuleManager from '@/mock/modules/dxn/gameManager/gameRuleManager'
import user from '@/mock/modules/user'
export default {
	dxn: {
		mockOpen: false,
		mockInterface: dxn
	},
	memberFundsRecord: {
		mockOpen: false,
		mockInterface: memberFundsRecord
	},
	// betSlip: {
	// 	mockOpen: false,
	// 	mockInterface: betSlip
	// },
	cardTablesList: {
		mockOpen: false,
		mockInterface: cardTablesList
	},
	gameList: {
		mockOpen: false,
		mockInterface: gameList
	},
	gameRuleManager: {
		mockOpen: false,
		mockInterface: gameRuleManager
	},
	user: {
		mockOpen: true,
		mockInterface: user
	}
}
