import Mock from 'mockjs'

// 配置信息
import configInfo from '@/mock/config'
// NOTE 全局设置：设置所有ajax请求的超时时间，模拟网络传输耗时
Mock.setup({
	timeout: 400 // 延时400s请求到数据
})

Object.keys(configInfo).forEach((item) => {
	if (configInfo[item].mockOpen) {
		configInfo[item].mockInterface.install()
	}
})
