import Mock from 'mockjs'

const queryList = () => {
    Mock.mock(`${process.env.VUE_APP_BASE_API}gameRecords/listPage`, 'post', () => {
        // TODO 数据处理
        return {
             code: 200,
            'data': Mock.mock({
                'record|20-50': [
                    {
                        'accountType': 0, // 账号类型(1-试玩，2-商务，3-正式，4-测试，5-置换)
                        'betAmount': 0,
                        'betContent': '',
                        'createAt': 0,
                        'createAtString': '',
                        'gameTypeId': '',
                        'gameTypeName': '',
                        'generatedId': '',
                        'id': '@id',
                        'loginIp': '',
                        'memberCurrency': '',
                        'memberName': '',
                        'merchantId': 0,
                        'merchantName': '',
                        'netAmount': 0,
                        'netAt': '@date',
                        'netAtString': '',
                        'obBetStatus': 0,
                        'obDeviceType': 0,
                        'parentProxyId': 0,
                        'parentProxyName': '',
                        'playerName': '',
                        'rebateAmount': 0,
                        'topProxyId': '@id',
                        'topProxyName': '',
                        'validBetAmount': 0,
                        'venueCode': '',
                        'venueName': '',
                        'venueTypeCode': '',
                        'venueTypeName': '',
                        'vipLevel': 0,
                        'vipRebateRate': 0
                    }
                ],
                'pageNum': 0,
                'pageSize': 0,
                'totalPage': 0,
                'totalRecord': 0
            })
        }
    })
}

export default {
	install: () => {
        queryList()
    }
}
