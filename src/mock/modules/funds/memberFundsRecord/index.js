import Mock from 'mockjs'
const optionList = Mock.mock({
	data: [
		{
			code: 1,
			description: '代理上分'
		},
		{
			code: 2,
			description: '代理下分'
		},
		{
			code: 3,
			description: '带入牌桌'
		},
		{
			code: 4,
			description: '带出牌桌'
		},
		{
			code: 5,
			description: '佣金转回'
		}
	]
})
/**
 * 请求的地址 - url
 */
const regUrl = (url) => {
	return RegExp(url + '.*')
}
/**
 * 请求的地址 - url
 * 要获取的url中的参数key名 - key
 */
const getUrlParam = (url, key) => {
	const urls = url.split('?')[1]
	const searchParams = new URLSearchParams('?' + urls)
	return searchParams.get(key)
}
/**
 *  全局的请求地址前缀 - baseUrl
 */
const baseUrl = process.env.VUE_APP_BASE_API

// 账变类型查询列表
const getMemberWalletChangeDic = (params) => {
	Mock.mock(
		regUrl(`${baseUrl}member/wallet/change/walletChangeDic`),
		'get',
		(options) => {
			const bizCode = getUrlParam(options.url, 'bizCode')
			const transType = getUrlParam(options.url, 'type')
			let arryList = []
			for (const items of optionList.data) {
				arryList.push(items)
			}
			if (transType === 'transType' && bizCode !== '0') {
				if (bizCode === '1') {
					arryList = arryList.filter(
						(item) => item.code !== 2 && item.code !== 3
					)
					return {
						code: 200,
						msg: 'success',
						data: arryList
					}
				} else if (bizCode === '2') {
					arryList = arryList.filter(
						(item) =>
							item.code !== 1 &&
							item.code !== 4 &&
							item.code !== 5
					)
					return {
						code: 200,
						msg: 'success',
						data: arryList
					}
				}
			} else {
				if (bizCode === '0') {
					return {
						code: 200,
						msg: 'success',
						data: arryList
					}
				} else if (bizCode === '1') {
					return {
						code: 200,
						msg: 'success',
						data: arryList.splice(0, 2)
					}
				} else if (bizCode === '2') {
					return {
						code: 200,
						msg: 'success',
						data: arryList.splice(2, 2)
					}
				} else if (bizCode === '3') {
					return {
						code: 200,
						msg: 'success',
						data: arryList.splice(4, 3)
					}
				}
			}
		}
	)
}
const install = () => {
	getMemberWalletChangeDic()
}

export default {
	install
}
