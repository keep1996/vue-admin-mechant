import Mock from 'mockjs'
const tableList = Mock.mock({
	'data|20-50': [
		{
			tableId: '@id', // 牌桌ID
			tableName: '@name',
			account: '@name',
			clubId: '@id', // 俱乐部ID,
			date: '@date',
			title: '@ctitle',
			'tableGameNumber|0-100': 0, // 牌桌局数
			'time|0-24': 0, // 时长（H）
			'totalCommission|0-100.0-10': 0 // 打牌总抽水
		}
	]
})

// NOTE 查询列表
const queryList = () => {
	Mock.mock(`${process.env.VUE_APP_BASE_API}getTableList`, 'post', () => {
		// TODO 数据处理
		return {
			code: 200,
			msg: 'success',
			data: tableList.data
		}
	})
}
// NOTE 增加数据
const addData = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}addTableList`,
		'post',
		(options) => {
			// TODO 数据处理
			const params = JSON.parse(options.body)
			tableList.data.push(params)
			return {
				code: 200,
				msg: 'success'
			}
		}
	)
}
// NOTE 删除数据
const delData = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}delTableList`,
		'post',
		(options) => {
			// TODO 数据处理
			const params = JSON.parse(options.body)
			tableList.data = tableList.data.filter((item) => {
				return item.tableId !== params.tableId
			})
			return {
				code: 200,
				msg: 'success'
			}
		}
	)
}
// NOTE 获取详情post
const postDetail = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}getTableDetail`,
		'post',
		(options) => {
			// TODO 数据处理
			const params = JSON.parse(options.body)
			const detailInfo = tableList.data.find((item) => {
				item.tableId === params.tableId
			})
			return {
				code: 200,
				msg: 'success',
				data: detailInfo
			}
		}
	)
}
// NOTE 获取详情get
const getDetail = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}getTableDetail.*`),
		'get',
		(options) => {
			// TODO 数据处理
			const params = getParams(options.url)
			const detailInfo = tableList.data.find((item) => {
				item.tableId === params.tableId
			})
			return {
				code: 200,
				msg: 'success',
				data: detailInfo
			}
		}
	)
}
const getParams = (url, name) => {
	const index = url.indexOf('?')
	if (index !== -1) {
		const newArr = url.substring(index + 1).split('&')
		for (var i = 0; i < newArr.length; i++) {
			const itemArr = newArr[i].split('=')
			if (itemArr[0] === name) {
				return itemArr[1]
			}
		}
	}
	return null
}

const install = () => {
	queryList()
	addData()
	delData()
	getDetail()
	postDetail()
}

export default {
	install
}
