import Mock from 'mockjs'
import { getParams } from '@/mock/utils'
const tableList = Mock.mock({
	'data|20-50': [
		{
			tableId: '@id', // 牌桌ID
			'status|1': [0, 1, 2], // 状态
			createdAt: '@datetime', // 创建时间
			tableName: '@ctitle', // 牌桌名称
			'tableType|1': [2001, 2002],
			'gameName|1': ['德州扑克', '短牌'], // 游戏名称
			clubId: '@id', // 俱乐部ID,
			start_time: '@datetime', // 开始时间
			end_time: '@datetime', // 结束时间
			blind_injection: '@integer(1, 100)', // 盲注
			bbBlindScore: '@integer(1, 100)', // 大盲注
			sbBlindScore: '@integer(1, 100)', // 小盲注
			num_cards_hand: '@integer(1, 100)', // 牌桌手牌数
			tableHandNumber: '@integer(1, 100)', // 牌桌局数
			bringChipScore: '@integer(1, 100)',
			bringOutChipScore: '@integer(1, 100)',
			players_bring_in: '@integer(100, 10000)', // 玩家总带出
			players_bring_out: '@integer(100, 1000)', // 玩家总带出
			total_card_pumping: '@integer(100, 10000)', // 总打牌抽水
			summary_of_member_wins_and_losses: '@integer(-1000, 1000)', // 会员输赢汇总
			total_card_cost: '@integer(100, 10000)', // 牌桌总费用
			accum_part: '@integer(1, 100)',
			member_name: '@cname',
			total_win_loss: '@integer(100, 10000)',
			total_carried_in: '@integer(100, 10000)',
			number_times_in: '@integer(1, 10)',
			hand_brand: '@id',
			hand_id: '@id',
			effective_bottom_pool: '@integer(100, 10000)',
			play_cards_pump_water: '@integer(100, 10000)',
			'public_brand_information|1': [
				'梅花A',
				'梅花5',
				'黑桃3',
				'红桃4',
				'方块5'
			], // 公牌信息
			member_total_winorloss: '@integer(100, 10000)', // 会员总输赢
			members_always_bring_in: '@integer(100, 10000)', // 会员总带入
			number_of_times_members_have_brought_in: '@integer(1, 10)', // 会员带入次数
			nickName: '@cname',
			userNames: '@cname', // 会员名称
			beginDate: '@datetime', // 开始时间
			endDate: '@datetime', // 结束时间
			netChipSum: '@integer(100, 1000)',
			bringMidwayChip: '@integer(100, 1000)',
			bringCount: '@integer(100, 1000)',
			gameTypeId: '@id',
			tableBeginTime: '@datetime',
			tableEndTime: '@datetime',
			rake: '@integer(0, 1)',
			handCount: '@integer(0, 100)',
			pumpingAmountTotal: '@integer(100, 10000)',
			costTotal: '@integer(100, 10000)' // 牌桌总费用
		}
	]
})

// NOTE 牌桌列表
const getDxtableList = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}dxtable/list`,
		'post',
		(options) => {
			const { pageNum = 1, pageSize = 100 } = JSON.parse(options.body)
			const totalRecord = tableList.data.length
			const record = tableList.data.filter(
				(item, index) =>
					index < Math.min(pageSize * pageNum, totalRecord) &&
					index >= pageSize * (pageNum - 1)
			)
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {
					record,
					totalRecord
				}
			}
		}
	)
}

// NOTE 牌桌详情-基础信息
const getDxtableDetail = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}dxtable/detail.*`),
		'get',
		(options) => {
			// TODO: 此处待完善
			const id = getParams(options.url, 'id')
			const detailInfo = tableList.data.find((item) => {
				return item.tableId === id
			})
			return {
				code: 200,
				msg: 'success',
				data: detailInfo
			}
		}
	)
}

// NOTE 牌桌详情-基础信息
const getDxtableBaseDetail = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}dxtable/base/detail.*`),
		'get',
		(options) => {
			// TODO: 此处待完善
			const id = getParams(options.url, 'id')
			const detailInfo = tableList.data.find((item) => {
				return item.tableId === id
			})
			return {
				code: 200,
				msg: 'success',
				data: detailInfo
			}
		}
	)
}

// NOTE 牌桌列表-结算详情
const getGameOrderUserSettlementDetail = () => {
	Mock.mock(
		`${
			process.env.VUE_APP_BASE_API
		}dxGameRecords/gameOrderUserSettlementDetail`,
		'post',
		(options) => {
			// TODO: 此处待完善
			const tableId = JSON.parse(options.body).tableId
			const detailInfo = tableList.data.find((item) => {
				return item.tableId === tableId
			})
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: detailInfo
			}
		}
	)
}

// NOTE 牌桌列表-基础信息-牌桌收入
const getCardTableBaseGameTableList = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}getCardTableBaseGameTableList`,
		'post',
		(options) => {
			const { pageNum = 1, pageSize = 100 } = JSON.parse(options.body)
			const totalRecord = tableList.data.length
			const record = tableList.data.filter(
				(item, index) =>
					index < Math.min(pageSize * pageNum, totalRecord) &&
					index >= pageSize * (pageNum - 1)
			)
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {
					record,
					totalRecord
				}
			}
		}
	)
}

// NOTE 牌桌列表-手牌数据
const getCardTableHandDataTableList = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}getCardTableHandDataTableList`,
		'post',
		(options) => {
			const { pageNum = 1, pageSize = 100 } = JSON.parse(options.body)
			const totalRecord = tableList.data.length
			const record = tableList.data.filter(
				(item, index) =>
					index < Math.min(pageSize * pageNum, totalRecord) &&
					index >= pageSize * (pageNum - 1)
			)
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {
					record,
					totalRecord
				}
			}
		}
	)
}

// NOTE 会员管理-会员详情-牌桌信息
const getMemberDetailTableList = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}getMemberDetailTableList`,
		'post',
		(options) => {
			const { pageNum = 1, pageSize = 100 } = JSON.parse(options.body)
			const totalRecord = tableList.data.length
			const record = tableList.data.filter(
				(item, index) =>
					index < Math.min(pageSize * pageNum, totalRecord) &&
					index >= pageSize * (pageNum - 1)
			)
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {
					record,
					totalRecord
				}
			}
		}
	)
}

// NOTE 德州-游戏管理-牌桌详情
const dxtableGameDissolve = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}dxtable/game/dissolve.*`,
		'get',
		() => {
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {}
			}
		}
	)
}

const install = () => {
	getDxtableList()
	getDxtableDetail()
	getGameOrderUserSettlementDetail()
	getCardTableBaseGameTableList()
	getCardTableHandDataTableList()
	getMemberDetailTableList()
	dxtableGameDissolve()
	getDxtableBaseDetail()
}

export default {
	install
}
