import Mock from 'mockjs'
// import { getParams } from '@/mock/utils'
const ruleObj = Mock.mock({
	data: {
		'gameTypeId|1': [1, 2],
		'gameName|1': ['德州扑克', '短牌'],
		'status|1': [0, 1], // 游戏总开关 0开，1关
		gameOvertime: 30, // 超时自动解散房间（min）
		bbSetting: '10,20,50,100,200,400,1000..', // 大盲BB设值
		bbDefaultCarry: '50,100,150,200,300,500', // 默认带入值（倍BB）
		gameTimes: '0.5,1,1.5,2,2.5,3,4,5,6,7,8', // 游戏时长（H）
		gamePeopleNumbers: '2,3,4,5,6,7,8,9', // 游戏人数
		autoStartNumbers: '2,3,4,5,6,7,8,9', // 自动开始人数
		complementLowerLimit: 100, // 补码下限
		complementUpperLimit: 2000, // 补码上限
		rake: 0.3 // 手牌有效底池抽水比例
	}
})

// NOTE 游戏配置查询
const getDxgameConfigDdetail = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}dxgame/config/detail.*`),
		'get',
		(options) => {
			// TODO 数据处理
			// const id = getParams(options.url, 'id')
			return {
				code: 200,
				msg: 'success',
				data: {
					...ruleObj.data
					// id,
					// gameName: id === '1' ? '德州扑克' : '短牌'
				}
			}
		}
	)
}
// NOTE 更新游戏配置
const dxgameConfigUpdate = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}dxgame/config/update`,
		'post',
		() => {
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {}
			}
		}
	)
}
// NOTE 修改游戏开关
const dxgameConfigSwitch = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}dxgame/config/switch`,
		'post',
		() => {
			// TODO 数据处理
			return {
				code: 200,
				msg: 'success',
				data: {}
			}
		}
	)
}

const install = () => {
	getDxgameConfigDdetail()
	dxgameConfigUpdate()
	dxgameConfigSwitch()
}

export default {
	install
}
