import Mock from 'mockjs'
import { getParams } from '@/mock/utils'
const tableList = Mock.mock({
	'data|20-50': [
		{
			handId: '@id', // 手牌ID
			'handStatus|1': [0, 1], // 状态
			tableId: '@id', // 牌桌ID
			tableName: '@ctitle', // 牌桌名称
			gameName: '@ctitle', // 游戏名称
			clubId: '@id', // 俱乐部ID,
			clubName: '@ctitle', // 俱乐部名称
			handBeginTime: '@datetime', // 牌局开始时间
			handEndTime: '@datetime', // 牌局结束时间
			handCode: '@integer(1, 100)', // 手牌号
			beginMount: '@integer(100, 10000)', // 期初金额汇总
			bringMidwayAmount: '@integer(100, 1000)', // 中途带入金额汇总
			endAmount: '@integer(100, 10000)', // 期末余额汇总
			validPot: '@integer(100, 10000)', // 有效底池
			play_cards_pump_this_bureau: '@integer(100, 10000)', // 打牌抽水
			summary_of_member_wins_and_losses: '@integer(100, 10000)',
			play_cards_pump_water: '@integer(100, 1000)', // 牌桌打牌抽水
			'publicCards|1': ['梅花A', '梅花5', '黑桃3', '红桃4', '方块5'], // 公牌信息
			member_name: '@cname',
			'start_hand_info|1': ['梅花A', '梅花5', '黑桃3', '红桃4', '方块5'], // 起手牌信息
			settlement_information: '@integer(100, 1000)'
		}
	]
})

// NOTE 手牌列表
const getDxhandLlist = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}dxhand/list`,
		'post',
		(options) => {
			const { pageNum = 1, pageSize = 100 } = JSON.parse(options.body)
			const totalRecord = tableList.data.length
			const record = tableList.data.filter(
				(item, index) =>
					index < Math.min(pageSize * pageNum, totalRecord) &&
					index >= pageSize * (pageNum - 1)
			)
			// TODO 此处待完善
			return {
				code: 200,
				msg: 'success',
				data: {
					record,
					totalRecord
				}
			}
		}
	)
}

// NOTE 手牌详情
const getDxhandDetail = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}dxhand/detail.*`),
		'get',
		(options) => {
			// TODO: 此处待完善,缺失手牌id字段导致跳转到详情手牌数无参数字段,暂时只取第一条数据mock
			const id = getParams(options.url, 'id')
			const detailInfo = tableList.data.find((item) => {
				return item.handId === id
			})
			return {
				code: 200,
				msg: 'success',
				data: detailInfo || tableList.data[0]
			}
		}
	)
}

// 获取游戏列表
const getDxnGameManagerList = () => {
	Mock.mock(
		RegExp(`${process.env.VUE_APP_BASE_API}game/list.*`),
		'get',
		(options) => {
			// TODO: 此处待完善,缺失手牌id字段导致跳转到详情手牌数无参数字段,暂时只取第一条数据mock
			return {
				code: 200,
				msg: 'success',
				data: tableList
			}
		}
	)
}

const install = () => {
	getDxhandLlist()
	getDxhandDetail()
	getDxnGameManagerList()
}

export default {
	install
}
