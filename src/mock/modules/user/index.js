import Mock from 'mockjs'

// 审核代办数量
const getUpcomingSelect = () => {
	Mock.mock(
		`${process.env.VUE_APP_BASE_API}upcoming/selectUpcoming1`,
		'post',
		() => {
			console.error('111')
			// TODO 数据处理
			return {
				// code: 200,
				// msg: 'success',
				// data: {
				// 	auditProxyAdd: 6,
				// 	auditMerchantAdd: 0,
				// 	memberWithdrawToBeAuditCount: 0,
				// 	memberActivityBonusToBeAuditCount: 0,
				// 	proxyWithdrawToBeAuditCount: 0,
				// 	auditNewUser: 1,
				// 	auditUpdateInfoUser: 1,
				// 	memberArtificialAccountAddCount: 0,
				// 	proxyArtificialAccountAddCount: 7,
				// 	proxyCommissionAudit: 0,
				// 	proxyRebateAudit: 0,
				// 	memberTransferProxyCount: 0,
				// 	auditUpdateInfoAgent: 1
				// }，

				code: 4091,
				msg: '服务器维护，请联系客服处理'
			}
		}
	)
}

const install = () => {
	getUpcomingSelect()
}

export default {
	install
}
