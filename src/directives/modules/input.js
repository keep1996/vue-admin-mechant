import { ONLY_INPUT_ZH, NO_SPECIAL_CHAR, SPACE_PATTERN, HAVE_SPECIAL_CHAR, VALID_TOP_PROXY_BELONG } from '@/utils/pattern'
// 数字
function onlyNum(input) {
	input.value = input.value.replace(/\D+/g, '')
}
// 整数(0+正整数)
function onlyInt(input) {
	let value = input.value
	value = value.replace(/\D+/g, '')
	input.value = value ? Number(value).toString() : value // 去掉开头多个0
}
// 正整数
function onlyIntp(input) {
	if (!/^[1-9][0-9]*$/.test(input.value)) {
		let value = input.value.replace(/\D+/g, '')
		if (value && value.substring(0, 1) === '0') {
			// 开头去除0
			value = value.substring(1)
		}

		input.value = value
	}
}

// 数字+小数点
function onlyNumPoint(input) {
	input.value = input.value.replace(/[^\d.]/g, '')
}

// 校验版本号
function validateVersionNum(input) {
	input.value = input.value.replace(/^[1-9]\d?(\.([1-9]?\d)){2}$/, '')
}

// 浮点型
// eslint-disable-next-line no-unused-vars
function onlyFloat(input, n) {
	let value = input.value
	value = value.replace(/[^\d.]/g, '')
	value = value.replace(/^\./g, '')
	value = value
		.replace('.', '$#$')
		.replace(/\./g, '')
		.replace('$#$', '.')
	if (n && Number(n) > 0) {
		// 限制n位
		var d = new Array(Number(n)).fill(`\\d`).join('')
		// eslint-disable-next-line no-useless-escape
		var reg = new RegExp(`^(\\-)*(\\d+)\\.(${d}).*$`, 'ig')
		value = value.replace(reg, '$1$2.$3')
	}
	if (value && !value.includes('.')) {
		value = Number(value).toString() // 去掉开头多个0
	}
	input.value = value
}

// 验证只能输入正负数，以及保留两位小数
function onlyInputFloat(input) {
	let num = input.value
	// 得到第一个字符是否为负号
	var getNum = num.charAt(0)
	// 先把非数字的都替换掉，除了数字和.
	num = num.replace(/[^\d\.]/g, '')
	// 必须保证第一个为数字而不是.
	num = num.replace(/^\./g, '')
	// 保证只有出现一个.而没有多个.
	num = num.replace(/\.{2,}/g, '.')
	// 保证.只出现一次，而不能出现两次以上
	num = num
		.replace('.', '$#$')
		.replace(/\./g, '')
		.replace('$#$', '.')
	// 保留小数
	// if(len < 0) len = 0;
	// num = Number(num).toFixed(len);
	if (num.indexOf('.') !== -1) {
		if (num.toString().split('.')[1].length >= 3) {
			num = Number(num).toFixed(2)
		}
	}
	if (num.indexOf('.') < 0 && num !== '') {
		// 以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
		num = parseFloat(num)
	}
	// 如果第一位是负号，则允许添加
	if (getNum === '-') {
		num = '-' + num
	}
	input.value = num
}

// 验证限制位数及保留两位小数
function limitOnlyInputFloat(input, binding) {
	// 默认限制10个字符
	const defaultLimit = binding.value.limit
	let num = input.value
	// 先把非数字的都替换掉，除了数字和.
	num = num.replace(/[^\d\.]/g, '')
	// 必须保证第一个为数字而不是.
	num = num.replace(/^\./g, '')
	// 保证只有出现一个.而没有多个.
	num = num.replace(/\.{2,}/g, '.')
	// 保证.只出现一次，而不能出现两次以上
	num = num
		.replace('.', '$#$')
		.replace(/\./g, '')
		.replace('$#$', '.')
	// 保留小数
	// if(len < 0) len = 0;
	// num = Number(num).toFixed(len);
	if (num.indexOf('.') !== -1) {
		if (num.toString().split('.')[1].length >= 3) {
			num = Number(num).toFixed(2)
		}
	}
	if (num.indexOf('.') < 0 && num !== '') {
		// 以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
		num = parseFloat(num)
	}
	if (defaultLimit) {
		num = String(num)
		const limit = num.indexOf('.') !== -1 ? (defaultLimit + (binding.value.dotLimit ? (binding.value.dotLimit + 1) : 0)) : defaultLimit
		while (num.length > limit) {
			num = num.slice(0, -1)
		}
	}

	input.value = num
}

// 字母
function onlyAlp(input) {
	input.value = input.value.replace(/[^A-Za-z]/g, '')
}
// 数字+字母
function onlyNumAlp(input) {
	input.value = input.value.replace(/[^A-Za-z0-9]/g, '')
}

// 四则运算+-*/()数字
function onlyArith(input) {
	const value = input.value
	if (value) {
		input.value = value.split('').reduce((prev, cur) => {
			// eslint-disable-next-line no-useless-escape
			if (/^[\d|\-|\+|\*|\/|\.|\(|\)]+$/.test(cur)) {
				return prev + cur
			}
			return prev
		}, '')
	}
}

// 只能输入中文
function onlyZh(input) {
	input.value = input.value.replace(ONLY_INPUT_ZH, '')
}

// 不能输入空格
function noEmpty(input) {
	input.value = input.value.replace(SPACE_PATTERN, '')
}

// 不能输入空格和数字
function noEmptyAndNumber(input) {
	input.value = input.value.replace(/^(\d+|\s)$/, '')
}

// 只能输入非特殊字符
function onlySymbols(input) {
	input.value = input.value.replace(NO_SPECIAL_CHAR, '')
}

// 只能输入中文，字母数字和空格
function inclueSpaceSymbols(input) {
	input.value = input.value.replace(HAVE_SPECIAL_CHAR, '')
}

// 限制输入多少字符
function onlyLimit(input, binding) {
	// 默认限制10个字符
	const defaultLimit = 10
	while (
		input.value.length >
		(binding.value.limit || (binding.value.limit !== 0 && defaultLimit))
	) {
		input.value = input.value.slice(0, -1)
	}
}

// 限制输入多少数字
function onlyLimitNum(input, binding) {
	// 默认限制10个数字
	const defaultLimit = 10
	input.value = input.value.replace(/\D+/g, '')
	while (
		input.value.length >
		(binding.value.limit || (binding.value.limit !== 0 && defaultLimit))
	) {
		input.value = input.value.slice(0, -1)
	}
}

// 仅能输入字母，数字和汉字字符
function onlyInputNumZh(input) {
	input.value = input.value.replace(VALID_TOP_PROXY_BELONG, '')
}

function judgeAction(input, binding, vnode) {
	if (vnode.locking) {
		return
	}
	// v-input="{name: 'num'}"
	if (binding.value.name === 'num') {
		// 只能输入数字（开头可以多个0）
		onlyNum(input)
	} else if (binding.value.name === 'num_point') {
		// v-input="{name: 'num_point}"
		// 只能输入数字+小数点（可以多个小数点）
		onlyNumPoint(input)
	} else if (binding.value.name === 'float') {
		// v-input="{name: 'float', num:'2'}"
		// 只能输入浮点型（只能一个小数点）
		onlyFloat(input, binding.value.num)
	} else if (binding.value.name === 'int') {
		//  v-input="{name: 'int'}"
		// 只能输入整数（0+正整数）（开头不能多个0）
		onlyInt(input)
	} else if (binding.value.name === 'intp') {
		// v-input="{name: 'intp'}"
		// 只能输入正整数
		onlyIntp(input)
	} else if (binding.value.name === 'alp') {
		// v-input="{name: 'alp'}"
		// 只能输入字母
		onlyAlp(input)
	} else if (binding.value.name === 'num_alp') {
		// v-input="{name: 'num_alp'}"
		// 只能输入数字+字母
		onlyNumAlp(input)
	} else if (binding.value.name === 'arith') {
		// v-input="{name: 'arith'}"
		// 四则运算符+数字
		onlyArith(input)
	} else if (binding.value.name === 'zh') {
		// v-input="{name: 'zh'}"
		// 只能输入中文
		onlyZh(input)
	} else if (binding.value.name === 'symbols') {
		// v-input="{name: 'symbols'}"
		// 只能输入非特殊字符
		onlySymbols(input)
	} else if (binding.value.name === 'limit') {
		// v-input="{name: 'limit', limit: 10}"
		// 限制输入多少字符
		onlyLimit(input, binding)
	} else if (binding.value.name === 'limitNum') {
		// v-input="{name: 'limitNum', limit: 10}"
		// 限制输入多少数字
		onlyLimitNum(input, binding)
	} else if (binding.value.name === 'versionNum') {
		// v-input="{name: 'versionNum'}"
		// 校验版本号
		validateVersionNum(input)
	} else if (binding.value.name === 'noEmpty') {
		// v-input="{name: 'noEmpty'}"
		// 非空校验
		noEmpty(input)
	} else if (binding.value.name === 'noNumEmpty') {
		// v-input="{name: 'noNumEmpty'}"
		// 非空和非数字校验
		noEmptyAndNumber(input)
	} else if (binding.value.name === 'onlyTwoNum') {
		// v-input="{name: 'onlyTwoNum'}"
		// 仅能输入正负数以及小数保留两位
		onlyInputFloat(input)
	} else if (binding.value.name === 'includeSpaceValidate') {
		// v-input="{name: 'includeSpaceValidate'}"
		// 可以输入中文，字母数字和空格
		inclueSpaceSymbols(input)
	} else if (binding.value.name == 'limitOnlyTwoNum') {
		// v-input="{name: 'limitOnlyTwoNum'}"
		// 限制位数及小数保留两位
		limitOnlyInputFloat(input, binding)
	} else if (binding.value.name == 'onlyInputNumZh') {
		// v-input="{name: 'onlyInputNumZh'}"
		// 可以输入中文，数字以及汉字字符
		onlyInputNumZh(input)
	}
	input.dispatchEvent(new Event('input'))
}
export default {
	bind(el, binding, vnode) {
		const input =
			el.querySelector('.el-input__inner') ||
			el.querySelector('.el-textarea__inner') ||
			el
		input.addEventListener('compositionstart', () => {
			vnode.locking = true // 解决中文输入双向绑定失效
		})
		input.addEventListener('compositionend', () => {
			vnode.locking = false // 解决中文输入双向绑定失效
			input.dispatchEvent(new Event('input'))
		})
		// 输入监听处理
		input.addEventListener('keyup', () => {
			judgeAction(input, binding, vnode)
		})
		// 失焦事件
		input.addEventListener('blur', () => {
			judgeAction(input, binding, vnode)
		})
	},
	unbind(el, binding, vnode) {
		const input =
			el.querySelector('.el-input__inner') ||
			el.querySelector('.el-textarea__inner') ||
			el
		input.removeEventListener('compositionstart', () => {
			vnode.locking = true // 解决中文输入双向绑定失效
		})
		input.removeEventListener('compositionend', () => {
			vnode.locking = false // 解决中文输入双向绑定失效
			input.dispatchEvent(new Event('input'))
		})
		// 输入监听处理
		input.removeEventListener('keyup', () => {
			judgeAction(input, binding, vnode)
		})
		// 失焦事件
		input.removeEventListener('blur', () => {
			judgeAction(input, binding, vnode)
		})
	}
}
