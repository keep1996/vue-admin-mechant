import { NO_SPECIAL_CHAR } from '@/utils/pattern'
function judgeAction(input, binding, vnode) {
	if (vnode.locking) {
		return
	}
	if (binding.name === 'symbols') {
		// v-symbols
		// 只能输入非特殊字符
		onlySymbols(input)
	}
	input.dispatchEvent(new Event('input'))
}

// 只能输入非特殊字符
function onlySymbols(input) {
	input.value = input.value.replace(NO_SPECIAL_CHAR, '')
}
export default {
	bind(el, binding, vnode) {
		const input =
			el.querySelector('.el-input__inner') ||
			el.querySelector('.el-textarea__inner') ||
			el
		input.addEventListener('compositionstart', () => {
			vnode.locking = true // 解决中文输入双向绑定失效
		})
		input.addEventListener('compositionend', () => {
			vnode.locking = false // 解决中文输入双向绑定失效
			input.dispatchEvent(new Event('input'))
		})
		// 输入监听处理
		input.addEventListener('keyup', () => {
			judgeAction(input, binding, vnode)
		})
		// 失焦事件
		input.addEventListener('blur', () => {
			judgeAction(input, binding, vnode)
		})
	},
	unbind(el, binding, vnode) {
		const input =
			el.querySelector('.el-input__inner') ||
			el.querySelector('.el-textarea__inner') ||
			el
		input.removeEventListener('compositionstart', () => {
			vnode.locking = true // 解决中文输入双向绑定失效
		})
		input.removeEventListener('compositionend', () => {
			vnode.locking = false // 解决中文输入双向绑定失效
			input.dispatchEvent(new Event('input'))
		})
		// 输入监听处理
		input.removeEventListener('keyup', () => {
			judgeAction(input, binding, vnode)
		})
		// 失焦事件
		input.removeEventListener('blur', () => {
			judgeAction(input, binding, vnode)
		})
	}
}
