import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '@/layout'
import Layout2 from '@/layout2'
import i18n from '@/locales'

Vue.use(Router)

export const constantRoutes = [
	{
		path: '/',
		name: 'home',
		redirect: '/dashboard'
	},
	{
		path: '/dashboard',
		component: Layout,
		redirect: '/dashboard/index',
		children: [
			{
				path: 'index',
				component: Layout2,
				name: 'Dashboard',
				children: [
					{
						path: '/dashboard/index',
						component: () => import('@/views/index'),
						name: 'DashboardIndex',
						meta: {
							title: i18n.t('routes.route_home'),
							icon: 'bb_home',
							affix: true
						}
					}
				]
			}
		]
	},
	{
		path: '/personalCenter',
		component: Layout,
		hidden: true,
		children: [
			{
				path: '/accountSettings',
				component: Layout2,
				name: 'accountSettings',
				meta: {
					icon: 'user',
					noCache: false
				},
				children: [
					{
						path: '/system/personalCenter/accountSettings',
						component: () =>
							import('@/views/system/personalCenter/accountSettings/index'),
						name: 'accountSettings',
						meta: {
							title: i18n.t('common.center'),
							icon: 'user',
							noCache: false
						}
					}
				]
			}
		]
	},
	{
		path: '/authorityManage',
		component: Layout,
		hidden: true,
		children: [
			{
				path: '/menuManage',
				component: Layout2,
				name: 'menuManage',
				meta: {
					icon: 'user',
					noCache: false
				},
				children: [
					{
						path: '/system/authorityManage/menuManage',
						component: () =>
							import('@/views/system/authorityManage/menuManage/index'),
						name: 'menuManage',
						meta: {
							title: i18n.t('routes.system.menu'),
							icon: 'user',
							noCache: false
						}
					}
				]
			}
		]
	},
	{
		path: '/login',
		name: 'login',
		component: () => import('@/views/login/index'),
		hidden: true
	},
	{
		path: '/404',
		component: () => import('@/views/404'),
		hidden: true
	},
	{
		path: '/403',
		hidden: true,
		name: i18n.t('routes.403'),
		component: Layout,
		children: [
			{
				path: '/403',
				component: Layout2,
				name: i18n.t('routes.403'),
				children: [
					{
						path: '/403',
						component: () => import('@/views/403'),
						name: i18n.t('routes.403'),
						meta: {
							title: i18n.t('routes.403')
						}
					}
				]
			}
		]
	},
	{
		path: '/demo',
		name: 'demo',
		component: () => import('@/views/demo/index'),
		hidden: true
	},
	{
		path: '*',
		// redirect: '/403',
		component: () => import('@/views/404'),
		hidden: true
	}
]
const createRouter = () =>
	new Router({
		mode: 'history', // require service support
		scrollBehavior: () => ({
			y: 0
		}),
		routes: constantRoutes
	})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
	const newRouter = createRouter()
	router.matcher = newRouter.matcher // reset router
}

export default router
