import member from './member'
import game from './game'
import agent from './agent'
import control from './risk'
import funds from './funds'
import operation from './operation'
import report from './report'
import system from './system'
import merchant from './merchant'
import dxn from './dxn'
export default [
	...member,
	...agent,
	...game,
	...funds,
	...operation,
	...control,
	...report,
	...merchant,
	...system,
	...dxn
]
