import i18n from '@/locales'

const operation = [
	// 第一级菜单
	{
		id: '500000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.operation.operation')
	},
	// 第二级菜单
	{
		id: '501000',
		parentId: '500000',
		level: '2',
		icon: 'bb_kehuduanpeizhiguanli',
		title: i18n.t('routes.operation.client_config')
	},
	{
		id: '502000',
		parentId: '500000',
		level: '2',
		icon: 'bb_infopeizhiguanli',
		title: i18n.t('routes.operation.information_config')
	},

	{
		id: '503000',
		parentId: '500000',
		level: '2',
		icon: 'bb_huodongpeizhiguanli',
		title: i18n.t('routes.operation.activity_manage')
	},

	{
		id: '504000',
		parentId: '500000',
		level: '2',
		icon: 'bb_huodongpeizhiguanli',
		title: i18n.t('routes.operation.recharge_manage')
	},
	{
		id: '505000',
		parentId: '500000',
		level: '2',
		icon: 'bb_huodongpeizhiguanli',
		title: i18n.t('routes.operation.manage')
	},

	// 第三级菜单->客户端配置管理
	{
		id: '501010',
		parentId: '501000',
		path: '/operation/clientConfig/clientBanner',
		title: i18n.t('routes.operation.banner_manage')
	},
	{
		id: '501020',
		parentId: '501000',
		path: '/operation/clientConfig/venueImage',
		title: i18n.t('routes.operation.venue_config')
	},
	{
		id: '501030',
		parentId: '501000',
		path: '/operation/clientConfig/clientStart',
		title: i18n.t('routes.operation.app_config')
	},
	{
		id: '501050',
		parentId: '501000',
		path: '/operation/clientConfig/promotionPageConfig',
		title: i18n.t('routes.operation.tag_config')
	},
	// {
	// 	id: '501040',
	// 	parentId: '501000',
	// 	path: '/operation/clientConfig/agentApplyImage',
	// 	title: i18n.t('routes.operation.funds')
	// },

	// 第三级菜单->信息配置管理
	{
		id: '502010',
		parentId: '502000',
		path: '/operation/infoConfig/announcement',
		title: i18n.t('routes.operation.config')
	},
	{
		id: '502020',
		parentId: '502000',
		path: '/operation/infoConfig/activity',
		title: i18n.t('routes.operation.message_config')
	},
	{
		id: '502030',
		parentId: '502000',
		path: '/operation/infoConfig/notice',
		title: i18n.t('routes.operation.notice_config')
	},
	{
		id: '502040',
		parentId: '502000',
		path: '/operation/infoConfig/feedBack',
		title: i18n.t('routes.operation.feedback')
	},

	// 第三级菜单->活动管理
	{
		id: '503010',
		parentId: '503000',
		path: '/operation/activityManage/publicRulesConfig',
		title: i18n.t('routes.operation.rule_config')
	},
	{
		id: '503020',
		parentId: '503000',
		path: '/operation/activityManage/activityList',
		title: i18n.t('routes.operation.list')
	},
	{
		id: '5030201',
		parentId: '503000',
		path: '/operation/activityManage/activityList/components/addRow',
		title: i18n.t('routes.operation.add')
	},
	{
		id: '5030202',
		parentId: '503000',
		path: '/operation/activityManage/activityList/components/editRow',
		title: i18n.t('routes.operation.edit')
	},
	{
		id: '5030203',
		parentId: '503000',
		path: '/operation/activityManage/activityList/components/editData',
		title: i18n.t('routes.operation.data')
	},
	// 充提优惠配置
	{
		id: '504010',
		parentId: '504000',
		path: '/operation/depositWithdrawDiscountManage/memberDiscountConfig',
		title: i18n.t('routes.operation.deposit_config')
	},
	// 分享
	{
		id: '505010',
		parentId: '505000',
		path: '/operation/shareManage/memberShareDomainManage',
		title: i18n.t('routes.operation.domain_manage')
	}
	// {
	// 	id: '876',
	// 	parentId: '505000',
	// 	path: '/operation/shareManage/memberShareDomainOperationRecord',
	// 	title: i18n.t('routes.operation.funds')
	// }
]
export default operation
