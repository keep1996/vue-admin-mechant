import i18n from '@/locales'

const merchant = [
	// 第一级菜单
	{
		id: '800000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.merchant.merchant')
	},
	// 第二级菜单
	{
		id: '801000',
		parentId: '800000',
		level: '2',
		icon: 'bb_usercenter',
		title: i18n.t('routes.merchant.merchant_manage')
	},
	{
		id: '802000',
		parentId: '800000',
		level: '2',
		icon: 'bb_usercenter',
		title: i18n.t('routes.merchant.merchant_audit')
	},
	{
		id: '803000',
		parentId: '800000',
		level: '2',
		icon: 'bb_usercenter',
		title: i18n.t('routes.merchant.merchant_config')
	},

	// 第三级菜单->商户管理
	{
		id: '801010',
		parentId: '801000',
		path: '/merchant/merchantManage/merchantList',
		title: i18n.t('routes.merchant.merchant_list')
	},
	{
		id: '801020',
		parentId: '801000',
		path: '/merchant/merchantManage/merchantDetails',
		title: i18n.t('routes.merchant.detail')
	},
	{
		id: '801030',
		parentId: '801000',
		path: '/merchant/merchantManage/addMerchant',
		title: i18n.t('routes.merchant.add')
	},
	// 第三级菜单->商户审核
	{
		id: '802010',
		parentId: '802000',
		path: '/merchant/merchantReview/addMerchantReview',
		title: i18n.t('routes.merchant.add_aduit')
	},
	// 第三级菜单->商户配置
	{
		id: '803010',
		parentId: '803000',
		path: '/merchant/merchantConfig/levelConfig',
		title: i18n.t('routes.merchant.level_config')
	},
	{
		id: '803020',
		parentId: '803000',
		path: '/merchant/merchantConfig/initialAgentConfig',
		title: i18n.t('routes.merchant.initial_config')
	},
	{
		id: '803030',
		parentId: '803000',
		path: '/merchant/merchantConfig/venueConfig',
		title: i18n.t('routes.merchant.venue_config')
	},
	{
		id: '803040',
		parentId: '803000',
		path: '/merchant/merchantConfig/paymentChannelConfig',
		title: i18n.t('routes.merchant.channel_config')
	},
	{
		id: '803060',
		parentId: '803000',
		path: '/merchant/merchantConfig/messageConfig',
		title: i18n.t('routes.merchant.message_config')
	},
	{
		id: '8030601',
		parentId: '803000',
		path: '/merchant/merchantConfig/messageChannelManage',
		title: i18n.t('routes.merchant.channel_manage'),
		hidden: true
	},
	{
		id: '803070',
		parentId: '803000',
		path: '/merchant/merchantConfig/customerServiceConfig',
		title: i18n.t('routes.merchant.service_config')
	},
	{
		id: '803050',
		parentId: '803000',
		path: '/merchant/merchantConfig/gameLinkConfig',
		title: i18n.t('routes.merchant.link_config')
	},
	{
		id: '803051',
		parentId: '803000',
		path: '/merchant/merchantConfig/cashinandout',
		title: i18n.t('routes.merchant.link_config')
	}
	// 第三级菜单->操作记录
	// {
	// 	id: '140401',
	// 	parentId: '1404',
	// 	path: '/merchant/merchantOperationRecord/merchantOperationRecord',
	// 	title: i18n.t('routes.merchant.funds')
	// }
]
export default merchant
