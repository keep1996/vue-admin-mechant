import i18n from '@/locales'

const member = [
	// 第一级菜单
	{
		id: '100000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.member.member')
	},
	// 第二级菜单
	{
		id: '101000',
		parentId: '100000',
		level: '2',
		icon: 'bb_accountInfo',
		title: i18n.t('routes.member.member_manage')
	},
	{
		id: '102000',
		parentId: '100000',
		level: '2',
		icon: 'bb_bankcard',
		title: i18n.t('routes.member.bank_coin_manage')
	},
	{
		id: '103000',
		parentId: '100000',
		level: '2',
		icon: 'bb_accounttransaction',
		title: i18n.t('routes.member.audit')
	},
	{
		id: '104000',
		parentId: '100000',
		level: '2',
		icon: 'bb_vipguanli',
		title: i18n.t('routes.member.vip_manage')
	},
	// 第三级菜单
	{
		id: '101010',
		parentId: '101000',
		path: '/member/memberManage/memberList',
		title: i18n.t('routes.member.list'),
		name: 'memberList'
	},
	{
		id: '101020',
		parentId: '101000',
		path: '/member/memberManage/memberDetails',
		title: i18n.t('routes.member.detail'),
		name: 'memberDetails'
	},
	// end
	{
		id: '101030',
		parentId: '101000',
		path: '/member/memberManage/addMember',
		title: i18n.t('routes.member.add'),
		name: 'addMember'
	},
	{
		id: '101040',
		parentId: '101000',
		path: '/member/memberManage/memberLabelConfig',
		title: i18n.t('routes.member.tag'),
		name: 'memberLabelConfig'
	},

	{
		id: '101060',
		parentId: '101000',
		path: '/member/memberManage/memberLogin',
		title: i18n.t('routes.member.login'),
		name: 'memberLogin'
	},
	{
		id: '101050',
		parentId: '101000',
		path: '/member/memberManage/inforLevelConfiguration',
		title: i18n.t('routes.member.config'),
		name: 'inforLevelConfiguration'
	},
	{
		id: '102010',
		parentId: '102000',
		path: '/member/bankAndVirtual/memberBankManage',
		title: i18n.t('routes.member.bank_manage'),
		name: 'memberBankManagement'
	},
	{
		id: '102020',
		parentId: '102000',
		path: '/member/bankAndVirtual/memberVirtualManage',
		title: i18n.t('routes.member.coin_manage'),
		name: 'memberVirtualManagement'
	},
	// {
	// 	id: '102030',
	// 	parentId: '102000',
	// 	path: '/member/bankAndVirtual/bankRecord',
	// 	title: '会员银行卡记录',
	// 	name: 'bankRecord'
	// },
	// {
	// 	id: '102040',
	// 	parentId: '102000',
	// 	path: '/member/bankAndVirtual/virtualRecord',
	// 	title: '会员虚拟币账号记录',
	// 	name: 'virtualRecord'
	// },
	{
		id: '103010',
		parentId: '103000',
		path: '/member/memberReview/memberChange',
		title: i18n.t('routes.member.modify'),
		name: 'memberChange'
	},
	{
		id: '103020',
		parentId: '103000',
		path: '/member/memberReview/addMemberCheck',
		title: i18n.t('routes.member.add_aduit'),
		name: 'addMemberCheck'
	},
	{
		id: '104010',
		parentId: '104000',
		path: '/member/memberVipConfig/vipLevelConfig',
		title: i18n.t('routes.member.vip_config'),
		name: 'vipLevelConfig'
	},
	{
		id: '104020',
		parentId: '104000',
		path: '/member/memberVipConfig/vipRightConfig',
		title: i18n.t('routes.member.vip_rights_config'),
		name: 'vipRightConfig'
	},
	{
		id: '104030',
		parentId: '104000',
		path: '/member/memberVipConfig/vipDiscountConfig',
		title: i18n.t('routes.member.vip_grade_config'),
		name: 'vipDiscountConfig'
	},
	{
		id: '104040',
		parentId: '104000',
		path: '/member/memberVipConfig/vipRebateConfig',
		title: i18n.t('routes.member.vip_rebate_config'),
		name: 'vipRebateConfig'
	},
	{
		id: '104050',
		parentId: '104000',
		path: '/member/memberVipConfig/vipChangeRecord',
		title: i18n.t('routes.member.vip_record'),
		name: 'vipChangeRecord'
	}
]
export default member
