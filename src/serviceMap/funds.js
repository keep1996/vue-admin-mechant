import i18n from '@/locales'

const funds = [
	// 第一级菜单
	{
		id: '400000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.funds.funds')
	},
	// 第二级菜单
	{
		id: '401000',
		parentId: '400000',
		level: '2',
		icon: 'bb_tikuanset',
		title: i18n.t('routes.funds.manage')
	},
	{
		id: '402000',
		parentId: '400000',
		level: '2',
		icon: 'bb_zijinshenhejilu',
		title: i18n.t('routes.funds.setting')
	},
	{
		id: '403000',
		parentId: '400000',
		level: '2',
		icon: 'bb_zijinshenhejilu',
		title: i18n.t('routes.funds.recharge_manage')
	},
	{
		id: '404000',
		parentId: '400000',
		level: '2',
		icon: 'bb_vipzijinjilu',
		title: i18n.t('routes.funds.record')
	},
	{
		id: '405000',
		parentId: '400000',
		level: '2',
		icon: 'bb_dailizijinjilu',
		title: i18n.t('routes.funds.agent_record')
	},
	{
		id: '406000',
		parentId: '400000',
		level: '2',
		icon: 'bb_zijintiaozheng',
		title: i18n.t('routes.funds.adjust')
	},
	{
		id: '407000',
		parentId: '400000',
		level: '2',
		icon: 'bb_zijinshenhe',
		title: i18n.t('routes.funds.audit')
	},
	{
		id: '408000',
		parentId: '400000',
		level: '2',
		icon: 'bb_zijinshenhejilu',
		title: i18n.t('routes.funds.audit_record')
	},

	// 第三级菜单->出入款管理
	{
		id: '401010',
		parentId: '401000',
		path: '/funds/thirdPeymentChannelConfig/thirdDepositConfig',
		title: i18n.t('routes.funds.funds_manage')
	},
	{
		id: '401020',
		parentId: '401000',
		path: '/funds/thirdPeymentChannelConfig/thirdWithdrawConfig',
		title: i18n.t('routes.funds.funds_out_manage')
	},
	{
		id: '401030',
		parentId: '401000',
		path: '/funds/thirdPeymentChannelConfig/paymentManufacturer',
		title: i18n.t('routes.funds.funds_pay_manage')
	},
	// 第三级菜单->虚拟币汇率设置
	{
		id: '402010',
		parentId: '402000',
		path: '/funds/virtualExchangeRateConfig/virtualExchangeRateConfig',
		title: i18n.t('routes.funds.exchange_manage')
	},
	// 第三级菜单->充提管理
	{
		id: '403010',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/memberDepositChannelConfig',
		title: i18n.t('routes.funds.channel_config')
	},
	{
		id: '403020',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/memberWithdrawChannelConfig',
		title: i18n.t('routes.funds.withdraw_channel_config')
	},
	{
		id: '403030',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/memberWithdrawConfig',
		title: i18n.t('routes.funds.withdraw_config')
	},
	{
		id: '403040',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/agentDepositChannelConfig',
		title: i18n.t('routes.funds.recharge_config')
	},
	{
		id: '403050',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/agentWithdrawChannelConfig',
		title: i18n.t('routes.funds.agent_withdraw_channel_config')
	},
	{
		id: '403060',
		parentId: '403000',
		path: '/funds/depositAndWithdrawConfig/agentWithdrawConfig',
		title: i18n.t('routes.funds.agent_withdraw_config')
	},
	// 第三级菜单->会员资金记录
	{
		id: '404010',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberAccountChangeRecord',
		title: i18n.t('routes.funds.adjust_record')
	},
	{
		id: '404020',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberDepositRecord',
		title: i18n.t('routes.funds.recharge_record')
	},
	{
		id: '404030',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberWithdrawalRecord',
		title: i18n.t('routes.funds.withdraw_record')
	},
	{
		id: '404040',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberTransferRecord',
		title: i18n.t('routes.funds.member_adjust')
	},
	{
		id: '404080',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberDepositpreferenceRecord',
		title: i18n.t('routes.funds.member_deposit_record')
	},
	{
		id: '404090',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberTopupRecord',
		title: i18n.t('routes.funds.member_topup_record')
	},
	{
		id: '404100',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberDeductionRecord',
		title: i18n.t('routes.funds.member_deduction_record')
	},
	{
		id: '404050',
		parentId: '404000',
		path: '/funds/memberFundsRecord/memberRebateRecord',
		title: i18n.t('routes.funds.member_rebate_record')
	},
	{
		id: '404070',
		parentId: '404000',
		path: '/funds/memberFundsRecord/JackpotRecord',
		title: i18n.t('routes.funds.member_activity_record')
	},
	{
		id: '404060',
		parentId: '404000',
		path: '/funds/memberFundsRecord/IssueRecord',
		title: i18n.t('routes.funds.member_funds_record')
	},
	// 第三级菜单->代理资金记录
	{
		id: '405010',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyAccountChangeRecord',
		title: i18n.t('routes.funds.agent_change_record')
	},
	{
		id: '405020',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyDepositRecord',
		title: i18n.t('routes.funds.agent_deposit_record')
	},
	{
		id: '405030',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyWithdrawalRecord',
		title: i18n.t('routes.funds.agent_withdrawal_record')
	},
	{
		id: '405060',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/transferRecord',
		title: i18n.t('routes.funds.transfer_record')
	},
	{
		id: '405040',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyTransferRecord',
		title: i18n.t('routes.funds.agent_transfer_record')
	},
	{
		id: '405070',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/commissionPayment',
		title: i18n.t('routes.funds.commission_payment')
	},
	{
		id: '405080',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/superAgentCommissionRecord',
		title: i18n.t('routes.funds.super_agent_record')
	},
	{
		id: '405090',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/agentCommissionRecord',
		title: i18n.t('routes.funds.agent_commission_record')
	},
	{
		id: '405130',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/commissionControlRecord',
		title: i18n.t('routes.funds.commission_control_record')
	},
	{
		id: '405100',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyRebateRecord',
		title: i18n.t('routes.funds.agent_rebate_record')
	},
	{
		id: '406060',
		parentId: '406000',
		path: '/funds/capitalAdjustment/agentCommissionCheck',
		title: i18n.t('routes.funds.agent_commission_manage')
	},
	{
		id: '407070',
		parentId: '407000',
		path: '/funds/fundReview/rebateCheck',
		title: i18n.t('routes.funds.rebate_check')
	},

	{
		id: '405050',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/substitutionRecord',
		title: i18n.t('routes.funds.substitution_record')
	},
	{
		id: '405110',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyTopupRecord',
		title: i18n.t('routes.funds.agent_topup_record')
	},
	{
		id: '405120',
		parentId: '405000',
		path: '/funds/proxyFundsRecord/proxyDeductionRecord',
		title: i18n.t('routes.funds.agent_dedution_record')
	},

	{
		id: '406010',
		parentId: '406000',
		path: '/funds/capitalAdjustment/membershipIncrease',
		title: i18n.t('routes.funds.membership_increase')
	},
	{
		id: '406020',
		parentId: '406000',
		path: '/funds/capitalAdjustment/memberDeduction',
		title: i18n.t('routes.funds.member_deduction')
	},
	{
		id: '406040',
		parentId: '406000',
		path: '/funds/capitalAdjustment/agencyIncrease',
		title: i18n.t('routes.funds.agency_increase')
	},
	{
		id: '406050',
		parentId: '406000',
		path: '/funds/capitalAdjustment/agencyDeduction',
		title: i18n.t('routes.funds.agency_deduction')
	},
	{
		id: '407010',
		parentId: '407000',
		path: '/funds/fundReview/memberWithdrawalReview',
		title: i18n.t('routes.funds.member_withdrawal_audit')
	},
	{
		id: '407020',
		parentId: '407000',
		path: '/funds/fundReview/memberTopupReview',
		title: i18n.t('routes.funds.member_topup')
	},
	{
		id: '407080',
		parentId: '407000',
		path: '/funds/fundReview/memberBillWithdrawalSubReview',
		title: i18n.t('routes.funds.member_bill_withdrawal_sub_audit')
	},
	{
		id: '407040',
		parentId: '407000',
		path: '/funds/fundReview/agentWithdrawalReview',
		title: i18n.t('routes.funds.agent_withdrawal_audit')
	},
	{
		id: '407050',
		parentId: '407000',
		path: '/funds/fundReview/agentTopupReview',
		title: i18n.t('routes.funds.agent_topup_audit')
	},
	{
		id: '407060',
		parentId: '407000',
		path: '/funds/fundReview/commissionReview',
		title: i18n.t('routes.funds.commission_audit')
	},
	{
		id: '408010',
		parentId: '408000',
		path: '/funds/fundAuditRecord/memberWithdrawalReviewRecord',
		title: i18n.t('routes.funds.member_withdrawal_audit_record')
	},
	{
		id: '408020',
		parentId: '408000',
		path: '/funds/fundAuditRecord/memberTopupReviewRecord',
		title: i18n.t('routes.funds.member_topup_aduit_record')
	},
	{
		id: '408040',
		parentId: '408000',
		path: '/funds/fundAuditRecord/agentWithdrawalReviewRecord',
		title: i18n.t('routes.funds.agent_withdrawal_audit_record')
	},
	{
		id: '408050',
		parentId: '408000',
		path: '/funds/fundAuditRecord/agentTopupReviewRecord',
		title: i18n.t('routes.funds.agent_topup_audit_record')
	},
	{
		id: '408060',
		parentId: '408000',
		path: '/funds/fundAuditRecord/commissionReviewRecord',
		title: i18n.t('routes.funds.commission_audit_record')
	},
	{
		id: '408070',
		parentId: '408000',
		path: '/funds/fundAuditRecord/rebateCheckRecord',
		title: i18n.t('routes.funds.rebate_audit_record')
	},
	{
		id: '406030',
		parentId: '406000',
		path: '/funds/capitalAdjustment/bonusDistribution',
		title: i18n.t('routes.funds.bonus_distribution')
	},
	{
		id: '407030',
		parentId: '407000',
		path: '/funds/fundReview/bonusReview',
		title: i18n.t('routes.funds.bonus_audit')
	},
	{
		id: '408030',
		parentId: '408000',
		path: '/funds/fundAuditRecord/bonusReviewRecord',
		title: i18n.t('routes.funds.bonus_audit_record')
	}
]

// 上面有多级代的假路由，到时候需要去掉下
export default funds
