import i18n from '@/locales'

const game = [
	// 第一级菜单
	{
		id: '300000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.game.game')
	},
	// 第二级菜单
	{
		id: '301000',
		parentId: '300000',
		level: '2',
		icon: 'bb_gamepeizhi',
		title: i18n.t('routes.game.game_venue_manage')
	},
	{
		id: '302000',
		parentId: '300000',
		level: '2',
		icon: 'bb_gamezhudan',
		title: i18n.t('routes.game.order')
	},
	// {
	// 	id: '1020400',
	// 	parentId: '300000',
	// 	icon: 'bb_domain',
	// 	title: i18n.t('routes.game.funds')
	// },

	// 第三级菜单->游戏场馆管理
	{
		id: '301020',
		parentId: '301000',
		path: '/game/gameVenueManage/gameManage',
		title: i18n.t('routes.game.game_manage')
	},
	{
		id: '301010',
		parentId: '301000',
		path: '/game/gameVenueManage/venueManage',
		title: i18n.t('routes.game.venue_manage')
	},
	{
		id: '301030',
		parentId: '301000',
		path: '/game/gameVenueManage/venueAccessManage',
		title: i18n.t('routes.game.venue_access_manage')
	},
	// 第三级菜单->注单管理
	{
		id: '302010',
		parentId: '302000',
		path: '/game/gameBetslip/gameBetslipTable',
		title: i18n.t('routes.game.game_order')
	}
	// 第三级菜单->操作记录
	// {
	// 	id: '1020403',
	// 	parentId: '1020400',
	// 	path: '/game/gameOperationRecord/gameOperationRecord',
	// 	title: i18n.t('routes.game.funds')
	// }
]
export default game
