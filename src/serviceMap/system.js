import i18n from '@/locales'

const system = [
	// 第一级菜单
	{
		id: '900000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.system.system')
	},
	{
		id: '901000',
		parentId: '900000',
		level: '2',
		icon: 'bb_quanxianguanli',
		title: i18n.t('routes.system.manage')
	},
	{
		id: '902000',
		parentId: '900000',
		level: '2',
		icon: 'bb_canshupeizhi',
		title: i18n.t('routes.system.config')
	},
	{
		id: '903000',
		parentId: '900000',
		level: '2',
		icon: 'bb_tuiguangguanli',
		title: i18n.t('routes.system.config_manage')
	},
	{
		id: '904000',
		parentId: '900000',
		level: '2',
		icon: 'bb_tuiguangguanli',
		title: i18n.t('routes.system.record')
	},
	// 第三级菜单
	{
		id: '901010',
		parentId: '901000',
		path: '/system/authorityManage/roleManage',
		title: i18n.t('routes.system.role')
	},
	{
		id: '901020',
		parentId: '901000',
		path: '/system/authorityManage/staffManage',
		title: i18n.t('routes.system.account')
	},
	{
		id: '901030',
		parentId: '901000',
		path: '/system/authorityManage/menuManage',
		title: i18n.t('routes.system.menu'),
		hidden: false
	},

	{
		id: '905010',
		parentId: '905000',
		path: '/system/personalCenter/accountSettings',
		title: i18n.t('routes.system.menu'),
		hidden: false
	},
	{
		id: '902010',
		parentId: '902000',
		path: '/system/parameterConfiguration/parameterDictionary',
		title: i18n.t('routes.system.dict')
	},
	{
		id: '903010',
		parentId: '903000',
		path: '/system/configurationManagement/domainNameConfiguration',
		title: i18n.t('routes.system.app_config')
	},
	{
		id: '903020',
		parentId: '903000',
		path: '/system/versionnumberConfiguration/versionnumberlist',
		title: i18n.t('routes.system.version')
	},
	{
		id: '904020',
		parentId: '904000',
		path: '/system/systemoOperationRecord/staffLoginLog',
		title: i18n.t('routes.system.login')
	}
]
export default system
