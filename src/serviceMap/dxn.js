import i18n from '@/locales'

const dxn = [
	// 第一级菜单
	{
		id: '90000022',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.dxn.dxn')
	},
	// 第二级菜单
	{
		id: '90000023',
		parentId: '90000022',
		level: '2',
		title: i18n.t('routes.dxn.game_manager')
	}
]

export default dxn
