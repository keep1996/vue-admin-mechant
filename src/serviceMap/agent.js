import i18n from '@/locales'

const agent = [
	// 第一级菜单
	{
		id: '200000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.agent.agent')
	},
	// 第二级菜单
	{
		id: '201000',
		parentId: '200000',
		level: '2',
		icon: 'bb_dailiguanli',
		title: i18n.t('routes.agent.agent_manage')
	},
	{
		id: '202000',
		parentId: '200000',
		level: '2',
		icon: 'bb_dailishenhe',
		title: i18n.t('routes.agent.agent_aduit')
	},
	{
		id: '203000',
		parentId: '200000',
		level: '2',
		icon: 'bb_tuiguangguanli',
		title: i18n.t('routes.agent.promote_manage')
	},
	{
		id: '204000',
		parentId: '200000',
		level: '2',
		icon: 'bb_dailipeizhi',
		title: i18n.t('routes.agent.config')
	},
	// 第三级菜单
	{
		id: '201010',
		parentId: '201000',
		path: '/agent/agentManage/agentList',
		title: i18n.t('routes.agent.list')
	},
	{
		id: '201020',
		parentId: '201000',
		path: '/agent/agentManage/agentDetails',
		title: i18n.t('routes.agent.detail')
	},
	{
		id: '201030',
		parentId: '201000',
		path: '/agent/agentManage/addAgent',
		title: i18n.t('routes.agent.add')
	},
	{
		id: '201050',
		parentId: '201000',
		path: '/agent/agentManage/memberTransfer',
		title: i18n.t('routes.agent.member')
	},
	{
		id: '201060',
		parentId: '201000',
		path: '/agent/agentManage/agentLogin',
		title: i18n.t('routes.agent.login')
	},
	// {
	// 	id: '201070',
	// 	parentId: '201000',
	// 	path: '/agent/agentManage/agentBankCard',
	// 	title: '代理银行卡记录'
	// },
	{
		id: '201040',
		parentId: '201000',
		path: '/agent/agentManage/contract',
		title: i18n.t('routes.agent.manage')
	},

	// {
	// 	id: '201080',
	// 	parentId: '201000',
	// 	path: '/agent/agentManage/agentVirtualRecord',
	// 	title: '代理虚拟币账号记录'
	// },

	// 三级菜单->代理审核
	{
		id: '202020',
		parentId: '202000',
		path: '/agent/agencyReview/agencyEdit',
		title: i18n.t('routes.agent.aduit')
	},
	{
		id: '202010',
		parentId: '202000',
		path: '/agent/agencyReview/addReview',
		title: i18n.t('routes.agent.add_aduit')
	},
	{
		id: '202030',
		parentId: '202000',
		path: '/agent/agencyReview/transformationReview',
		title: i18n.t('routes.agent.member_aduit')
	},

	// 三级菜单->推广管理
	{
		id: '203020',
		parentId: '203000',
		path: '/agent/promotionManage/exclusiveDomain',
		title: i18n.t('routes.agent.domain_manage')
	},
	{
		id: '203010',
		parentId: '203000',
		path: '/agent/promotionManage/domainNameManagement',
		title: i18n.t('routes.agent.promote_domain_manage')
	},
	{
		id: '203030',
		parentId: '203000',
		path: '/agent/promotionManage/pictureManage',
		title: i18n.t('routes.agent.promote_picture_manage')
	},

	// 第三级菜单->代理配置
	{
		id: '204010',
		parentId: '204000',
		path: '/agent/agentConfig/contractTemplate',
		title: i18n.t('routes.agent.template_config')
	},
	{
		id: '204020',
		parentId: '204000',
		path: '/agent/agentConfig/agentLevel',
		title: i18n.t('routes.agent.agent_config')
	},
	{
		id: '204030',
		parentId: '204000',
		path: '/agent/agentConfig/agentLabelConfig',
		title: i18n.t('routes.agent.tag_config')
	},
	{
		id: '204040',
		parentId: '204000',
		path: '/agent/agentConfig/AgentConfiguration',
		title: i18n.t('routes.agent.credit_config')
	},
	{
		id: '204050',
		parentId: '201000',
		path: '/agent/agentConfig/contractMinConfig',
		title: i18n.t('routes.agent.policy_config')
	}
]

export default agent
