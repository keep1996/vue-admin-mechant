import i18n from '@/locales'

const control = [
	// 第一级菜单
	{
		id: '600000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.risk.risk')
	},
	// 第二级菜单
	{
		id: '601000',
		parentId: '600000',
		level: '2',
		icon: 'bb_fengkongguanli',
		title: i18n.t('routes.risk.manage')
	},
	{
		id: '602000',
		parentId: '600000',
		level: '2',
		icon: 'bb_fengkongxinxibiangengjilu',
		title: i18n.t('routes.risk.black_list')
	},
	// 第三级菜单->风控管理
	{
		id: '601010',
		parentId: '601000',
		path: '/risk/riskManage/createRiskRank',
		title: i18n.t('routes.risk.add')
	},
	{
		id: '601020',
		parentId: '601000',
		path: '/risk/riskManage/editRiskRank',
		title: i18n.t('routes.risk.edit')
	},
	// 第三级->黑名单管理
	{
		id: '602010',
		parentId: '602000',
		path: '/risk/riskBlacklist/loginIPBlacklist',
		title: i18n.t('routes.risk.ip')
	},
	{
		id: '602020',
		parentId: '602000',
		path: '/risk/riskBlacklist/registerIPBlacklist',
		title: i18n.t('routes.risk.register')
	},
	{
		id: '602030',
		parentId: '602000',
		path: '/risk/riskBlacklist/logindeviceNumberBlacklist',
		title: i18n.t('routes.risk.login')
	},
	{
		id: '602040',
		parentId: '602000',
		path: '/risk/riskBlacklist/blacklistRegisteredEquipment',
		title: i18n.t('routes.risk.register_device')
	}
]
export default control
