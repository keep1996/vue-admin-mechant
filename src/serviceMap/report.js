import i18n from '@/locales'

const report = [
	// 第一级菜单
	{
		id: '700000',
		parentId: '0',
		level: '1',
		title: i18n.t('routes.report.report')
	},
	// 第二级菜单
	{
		id: '701000',
		parentId: '700000',
		level: '2',
		icon: 'bb_yingkuibaobiao',
		title: i18n.t('routes.report.profit_report')
	},
	{
		id: '702000',
		parentId: '700000',
		level: '2',
		icon: 'bb_yewubaobiao',
		title: i18n.t('routes.report.service_report')
	},
	// 第三级菜单
	{
		id: '701010',
		parentId: '701000',
		path: '/report/profitAndLossReport/memberProfitAndLoss',
		title: i18n.t('routes.report.member')
	},
	{
		id: '701020',
		parentId: '701000',
		path: '/report/profitAndLossReport/proxyProfitAndLoss',
		title: i18n.t('routes.report.agent')
	},

	{
		id: '701030',
		parentId: '701000',
		path: '/report/profitAndLossReport/venueProfitAndLoss',
		title: i18n.t('routes.report.venue')
	},
	{
		id: '701040',
		parentId: '701000',
		path: '/report/profitAndLossReport/gameProfitAndLoss',
		title: i18n.t('routes.report.game')
	},
	// {
	// 	id: '100',
	// 	parentId: '701000',
	// 	path: '/report/profitAndLossReport/clientProfitAndLoss',
	// 	title: i18n.t('routes.report.funds')
	// },
	{
		id: '701050',
		parentId: '701000',
		path: '/report/profitAndLossReport/dailyProfitAndLoss',
		title: i18n.t('routes.report.day')
	},
	// 第三级->业务报表
	{
		id: '702010',
		parentId: '702000',
		path: '/report/businessReport/memberReport',
		title: i18n.t('routes.report.member_report')
	},
	{
		id: '702020',
		parentId: '702000',
		path: '/report/businessReport/proxyReport',
		title: i18n.t('routes.report.agent_report')
	},
	// {
	// 	id: '104',
	// 	parentId: '702000',
	// 	path: '/report/businessReport/platformDepositAndWithdraw',
	// 	title: i18n.t('routes.report.funds')
	// },
	// {
	// 	id: '105',
	// 	parentId: '702000',
	// 	path: '/report/businessReport/comprehensiveReport',
	// 	title: i18n.t('routes.report.funds')
	// },
	{
		id: '702060',
		parentId: '702000',
		path: '/report/businessReport/commissionReport',
		title: i18n.t('routes.report.commssition_report')
	},
	{
		id: '702050',
		parentId: '702000',
		path: '/report/businessReport/activityOffersReport',
		title: i18n.t('routes.report.member_discount_report')
	},
	{
		id: '702040',
		parentId: '702000',
		path: '/report/businessReport/rebateReport',
		title: i18n.t('routes.report.rebate_report')
	},
	{
		id: '702030',
		parentId: '702000',
		path: '/report/businessReport/platformDepositAndWithdraw',
		title: i18n.t('routes.report.platform_report')
	},
	// {
	// 	id: '194',
	// 	parentId: '702000',
	// 	path: '/report/businessReport/newMemberReport',
	// 	title: i18n.t('routes.report.funds')
	// },
	{
		id: '702070',
		parentId: '702000',
		path: '/report/businessReport/commissionReceiptRaymentReport',
		title: i18n.t('routes.report.receipt_report')
	},
	{
		id: '702080',
		parentId: '702000',
		path: '/report/businessReport/rebatePeriodReport',
		title: i18n.t('routes.report.agent_rebate_report')
	}
]
export default report
