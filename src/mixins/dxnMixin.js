export default {
    data() {
        return {
            colSettings: {
                visible: false
            },
            columnSetKey: Math.random()
        }
    },
    computed: {
        activityListField() {
            return this.formatTableColumn(true)
        }
    },
    methods: {
        // 全选全部选
        choiceColumnSet(flag = true) {
            this.dialogColumnData = this.formatTableColumn(flag)
        },
        // 提交列设置数据
        submitColumnSet() {
            this.colSettings.visible = false
            this.updateIndexDB()
            setTimeout(() => {
                this.columnSetKey++
            }, 200)
        },
        // 展示列设置弹窗
        showColumnSet() {
            this.getIndexDB(this.activityListField)
            this.colSettings.visible = true
        },
        // 列头数据格式化
        formatTableColumn(flag = true) {
            const activityListColumn = {}
            Object.values(this.tableColumns).forEach((item) => {
                activityListColumn[item] = flag
            })
            return activityListColumn
        },
        // 列表排序
        handleTableSort({ prop, order }) {
            this.queryData.orderByField = prop
            // 升序
            if (order === 'ascending') {
                this.queryData.orderByMode = 'asc'
            } else if (order === 'descending') {
                // 降序
                this.queryData.orderByMode = 'desc'
            } else {
                this.queryData.orderByField = undefined
                this.queryData.orderByMode = undefined
            }
            this.loadData()
        },
        // 清空排序
        clearTableSort() {
            this.queryData.orderByField = undefined
            this.queryData.orderByMode = undefined
            this.$refs?.refTable?.clearSort()
        },
        // 删除导出不用条件
        deleteExportParams(params) {
            params.orderByField = undefined
            params.orderByMode = undefined
            params.pageNum = undefined
            params.pageSize = undefined
        },
        // 重置搜索框数据
        resetQueryData() {
            Object.keys(this.queryData).forEach((key) => {
                if (Array.isArray(this.queryData[key])) {
                    this.queryData[key] = []
                } else {
                    this.queryData[key] = undefined
                }
            })
            this.clearTableSort()
        }
    }
}
