import i18n from '@/locales'

export default {
	data() {
		const validatePassword = (rule, value, callback) => {
			if (value.length < 6) {
				callback(new Error(i18n.t('common.password.less')))
			} else {
				callback()
			}
		}

		const validatePass = (rule, value, callback) => {
			if (value === '') {
				callback(new Error(i18n.t('common.password.input')))
			} else {
				if (this.form.rePwd !== '') {
					this.$refs.form.validateField('rePwd')
				}
				callback()
			}
		}
		const validatePass2 = (rule, value, callback) => {
			if (value === '') {
				callback(new Error(i18n.t('common.password.input_again')))
			} else if (value !== this.form.pwd) {
				callback(new Error(i18n.t('common.no_match')))
			} else {
				callback()
			}
		}

		return {
			rules: {
				password: [
					{
						type: 'number',
						message: i18n.t('common.password.input_number')
					},
					{
						required: true,
						trigger: 'blur',
						validator: validatePassword
					},
					{ required: true, validator: validatePass, trigger: 'blur' }
				],
				checkPass: [
					{
						type: 'number',
						message: i18n.t('common.password.input_number')
					},
					{
						required: true,
						trigger: 'blur',
						validator: validatePassword
					},
					{
						required: true,
						validator: validatePass2,
						trigger: 'blur'
					}
				]
			}
		}
	},
	created() {
		const option = this.$options.doNotInit
		if (!option) {
			this.initList()
		}
	},
	watch: {
		page: 'loadData'
	},
	methods: {
		getSummaries(param) {
			const { columns, data } = param
			const sums = []
			columns.forEach((column, index) => {
				if (index === 0) {
					sums[index] = i18n.t('common.password.all')
					return
				}
				const values = data.map((item) => Number(item[column.property]))
				if (!values.every((value) => isNaN(value))) {
					sums[index] = values.reduce((prev, curr) => {
						const value = Number(curr)
						if (!isNaN(value)) {
							return prev + curr
						} else {
							return prev
						}
					}, 0)
					sums[index] += ` ${i18n.t('common.components.money')}`
				} else {
					sums[index] = 'N/A'
				}
			})

			return sums
		},
		getRowClass({ row, column, rowIndex, columnIndex }) {
			if (rowIndex === 0) {
				return 'background:#EFEFEF'
			} else {
				return ''
			}
		},
		/**
		 * 获取请求参数 默认只传递index(页码) pageSize(每页条数) 可以由调用方传递指定对象合并(或者覆盖)原参数
		 * @param params
		 * @returns {*}
		 */
		getParams(params = {}) {
			this.pageNum = params.pageNum ? params.pageNum : this.pageNum
			return Object.assign({
				pageNum: this.pageNum,
				pageSize: this.pageSize,
				...params
			})
		},
		/**
		 * 加载更多
		 */
		loadMore() {
			this.pageNum++
		},
		/**
		 * 推送到list中 因为vue的监听特性 只能用push进行数据的添加 如果有特殊处理 通过传递一个filter来过滤数据
		 * @param list
		 * @param filter
		 */
		pushToList(list, filter) {
			list.forEach((item) => {
				if (typeof filter === 'function') {
					this.list.push(filter(item))
				} else {
					this.list.push(item)
				}
			})
		},
		/**
		 * 初始化列表
		 */
		initList() {
			this.pageNum = 1
			this.list = []
			this.loadData()
		},
		/**
		 * @overwrite
		 * 加载数据方法 用到该mixin的都应该重写该方法 否则无法实现加载数据
		 */
		loadData() {
			// 每个列表自己的获取数据的方法需要重写
		}
	}
}
