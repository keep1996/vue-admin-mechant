// 会员模块
import member from './member'
// 代理模块
import agen from './agent'
// 游戏模块
import game from './game'
// 资金模块
import funds from './funds'
// 运营模块
import operation from './operation'
// 好友邀请
import friendInvitation from './friendInvitation'
// 风控模块
import risk from './risk'
// 商户模块
import merchant from './merchant'
// 报表模块
import report from './report'
// 系统模块
import system from './system'
// 公共
import common from './common'
// 路由
import routes from './routes'
// 枚举
import dict from './dict'
// 德州
import dxn from './dxn'

export default {
	...member,
	...agen,
	...game,
	...funds,
	...operation,
	...friendInvitation,
	...risk,
	...report,
	...merchant,
	...system,
	...merchant,
	...common,
	...routes,
	...dict,
	...dxn
}
