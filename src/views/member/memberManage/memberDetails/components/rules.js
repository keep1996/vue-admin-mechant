import i18n from '@/locales'
import { isHaveEmoji } from '@/utils/validate'

export const rules = () => {
	const testNickName = (rule, value, callback) => {
		const isRmoji = isHaveEmoji(String(value))
		if (isRmoji) {
			callback(
				new Error(i18n.t('member.expression_is_not_supported'))
			)
		} else {
			callback()
		}
	}
	const accountStatus = [
		{
			required: false,
			message: i18n.t('member.select_account_status'),
			trigger: 'change'
		}
	]
	const windControlId = [
		{
			required: false,
			message: i18n.t('member.select_wind_control_id'),
			trigger: 'change'
		}
	]
	const labelId = [
		{
			required: false,
			message: i18n.t('member.select_label_name'),
			trigger: 'change'
		}
	]
	const birthday = [
		{
			required: false,
			message: i18n.t('member.select_date_of_birth'),
			trigger: 'change'
		}
	]
	const areaCode = [
		{
			required: true,
			message: i18n.t('member.select_area_code'),
			trigger: 'change'
		}
	]
	const mobile = [
		{
			required: true,
			message: i18n.t('member.select_valid_number'),
			trigger: 'blur'
		}
	]
	const nickName = [
		{
			required: true,
			validator: testNickName,
			trigger: 'blur'
		}
	]
	const realName = [
		{
			required: true,
			message: i18n.t('member.Please_enter_your_name'),
			trigger: 'blur'
		}
	]
	const gender = [
		{
			required: false,
			message: i18n.t('member.select_gender'),
			trigger: 'change'
		}
	]
	const email = [
		{
			required: false,
			message: i18n.t('member.please_enter_your_email_address'),
			trigger: 'blur'
		}
	]
	const remark = [
		{
			min: 2,
			max: 500,
			required: true,
			message: i18n.t('member.length_2-500'),
			trigger: 'blur'
		}
	]
	const creditLevel = [
		{
			required: true,
			message: i18n.t('member.select_credit_level'),
			trigger: 'change'
		}
	]
	const checkType = [
		{
			required: true,
			message: i18n.t('member.select_check_type')
		}
	]
	return {
		accountStatus,
		windControlId,
		labelId,
		birthday,
		areaCode,
		mobile,
		nickName,
		realName,
		gender,
		email,
		remark,
		creditLevel,
		checkType
	}
}
