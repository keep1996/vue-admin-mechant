import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'firstDeposit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.firstDeposit
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.firstDeposit
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'balance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.balance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.balance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'creditAvailable':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.creditAvailable
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.creditAvailable
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'creditBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.creditBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.creditBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'creditShouldRepayment':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.creditShouldRepayment
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.creditShouldRepayment
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalDeposit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.totalDeposit,
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.totalDepositNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.totalDeposit,
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												totalSummary.totalDepositNum,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalWithdraw':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.totalWithdraw,
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.totalWithdrawNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.totalWithdraw,
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												totalSummary.totalWithdrawNum,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
