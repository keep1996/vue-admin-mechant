import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, summary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'withdrawalSuccessNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												summary.withdrawalSuccessNum,
												0
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												summary.withdrawalFailNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												summary.withdrawalSuccessNumAll,
												0
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												summary.withdrawalFailNumAll,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawalTotalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												summary.withdrawalTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												summary.withdrawalTotalAmountAll
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyWithdrawalSuccessNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												summary.proxyWithdrawalSuccessNum,
												0
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												summary.proxyWithdrawalFailNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												summary.proxyWithdrawalSuccessNumAll,
												0
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												summary.proxyWithdrawalFailNumAll,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyTotalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												summary.proxyTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												summary.proxyTotalAmountAll
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
