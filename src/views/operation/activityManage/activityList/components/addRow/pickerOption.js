import dayjs from 'dayjs'
import i18n from '@/locales'

export const pickerOption = {
	shortcuts: [
		{
			text: i18n.t('common.utils.today'),
			onClick(picker) {
				// const start = dayjs().startOf('day')
				const start = new Date().getTime()
				picker.$emit('pick', +start)
			}
		},
		{
			text: i18n.t('common.utils.yesterday'),
			onClick(picker) {
				const start = dayjs()
					.startOf('day')
					.subtract(1, 'd')
				picker.$emit('pick', +start)
			}
		},
		{
			text: i18n.t('common.utils.week_before'),
			onClick(picker) {
				// eslint-disable-next-line no-unused-vars
				let start, end
				const weekNum = dayjs().day()
				if (weekNum === 0) {
					end = dayjs().endOf('day')
					start = dayjs()
						.subtract(1, 'd')
						.startOf('week')
						.startOf('day')
						.add(1, 'd')
				} else {
					end = dayjs().endOf('day')
					start = dayjs()
						.startOf('day')
						.startOf('week')
						.add(1, 'd')
				}
				picker.$emit('pick', +start)
			}
		},
		// 当月
		{
			text: i18n.t('common.utils.month_now'),
			onClick(picker) {
				// const end = dayjs().endOf('day')
				const start = dayjs().startOf('month')
				picker.$emit('pick', +start)
			}
		},
		// 上月
		{
			text: i18n.t('common.utils.last_month'),
			onClick(picker) {
				// const end = dayjs()
				//     .subtract(1, 'month')
				//     .endOf('month')
				const start = dayjs()
					.startOf('month')
					.subtract(1, 'month')
				picker.$emit('pick', +start)
			}
		},
		// 前3个月
		{
			text: i18n.t('common.utils.month_three'),
			onClick(picker) {
				// const end = dayjs().endOf('day')
				const start = dayjs()
					.startOf('month')
					.subtract(3, 'month')
				picker.$emit('pick', +start)
				// const end = new Date()
				// const start = new Date()
				// start.setTime(start.getTime() - 3600 * 1000 * 24 * 91)
				// picker.$emit('pick', [start, end])
			}
		}
	]
}
