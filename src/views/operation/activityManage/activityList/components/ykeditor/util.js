import i18n from '@/locales'

export const customLang = {
	wangEditor: {
		插入: i18n.t('common.components.wangEditor.insert'),
		默认: i18n.t('common.components.wangEditor.default'),
		创建: i18n.t('common.components.wangEditor.create'),
		修改: i18n.t('common.components.wangEditor.edit'),
		如: i18n.t('common.components.wangEditor.like'),
		请输入正文: i18n.t('common.components.wangEditor.enter_text'),
		menus: {
			title: {
				标题: i18n.t('common.components.wangEditor.menus.title.title'),
				加粗: i18n.t('common.components.wangEditor.menus.title.bold'),
				字号: i18n.t('common.components.wangEditor.menus.title.size'),
				字体: i18n.t('common.components.wangEditor.menus.title.family'),
				斜体: i18n.t('common.components.wangEditor.menus.title.itatic'),
				下划线: i18n.t(
					'common.components.wangEditor.menus.title.underline'
				),
				删除线: i18n.t(
					'common.components.wangEditor.menus.title.delete_line'
				),
				缩进: i18n.t('common.components.wangEditor.menus.title.indent'),
				行高: i18n.t(
					'common.components.wangEditor.menus.title.lineheight'
				),
				文字颜色: i18n.t(
					'common.components.wangEditor.menus.title.color'
				),
				背景色: i18n.t(
					'common.components.wangEditor.menus.title.background'
				),
				链接: i18n.t('common.components.wangEditor.menus.title.link'),
				序列: i18n.t('common.components.wangEditor.menus.title.order'),
				对齐: i18n.t('common.components.wangEditor.menus.title.align'),
				引用: i18n.t('common.components.wangEditor.menus.title.import'),
				表情: i18n.t('common.components.wangEditor.menus.title.emoji'),
				图片: i18n.t('common.components.wangEditor.menus.title.image'),
				视频: i18n.t('common.components.wangEditor.menus.title.video'),
				表格: i18n.t('common.components.wangEditor.menus.title.table'),
				代码: i18n.t('common.components.wangEditor.menus.title.code'),
				分割线: i18n.t(
					'common.components.wangEditor.menus.title.split_line'
				),
				恢复: i18n.t(
					'common.components.wangEditor.menus.title.recover'
				),
				撤销: i18n.t('common.components.wangEditor.menus.title.back'),
				全屏: i18n.t(
					'common.components.wangEditor.menus.title.fullsceen'
				),
				代办事项: i18n.t(
					'common.components.wangEditor.menus.title.event'
				)
			},
			dropListMenu: {
				设置标题: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.set_title'
				),
				背景颜色: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.background'
				),
				文字颜色: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.color'
				),
				设置字号: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.size'
				),
				设置字体: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.set_font'
				),
				设置缩进: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.set_indet'
				),
				对齐方式: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.align'
				),
				设置行高: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.line_heihgt'
				),
				序列: i18n.t(
					'common.components.wangEditor.menus.dropListMenu.order'
				),
				head: {
					正文: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.head.text'
					)
				},
				indent: {
					增加缩进: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.indent.indent'
					),
					减少缩进: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.indent.outdent'
					)
				},
				justify: {
					靠左: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.justify.left'
					),
					居中: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.justify.center'
					),
					靠右: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.justify.right'
					),
					两端: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.justify.justify'
					)
				},
				list: {
					无序列表: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.list.unordered'
					),
					有序列表: i18n.t(
						'common.components.wangEditor.menus.dropListMenu.list.ordered'
					)
				}
			},
			panelMenus: {
				emoticon: {
					默认: i18n.t(
						'common.components.wangEditor.menus.panelMenus.emoticon.default'
					),
					新浪: i18n.t(
						'common.components.wangEditor.menus.panelMenus.emoticon.sina'
					),
					表情: i18n.t(
						'common.components.wangEditor.menus.panelMenus.emoticon.emoji'
					),
					手势: i18n.t(
						'common.components.wangEditor.menus.panelMenus.emoticon.gesture'
					)
				},
				image: {
					图片链接: i18n.t(
						'common.components.wangEditor.menus.panelMenus.image.image'
					),
					上传图片: i18n.t(
						'common.components.wangEditor.menus.panelMenus.image.upload'
					),
					网络图片: i18n.t(
						'common.components.wangEditor.menus.panelMenus.image.network'
					)
				},
				link: {
					链接: i18n.t(
						'common.components.wangEditor.menus.panelMenus.link.link'
					),
					链接文字: i18n.t(
						'common.components.wangEditor.menus.panelMenus.link.link_text'
					),
					删除链接: i18n.t(
						'common.components.wangEditor.menus.panelMenus.link.delete'
					),
					查看链接: i18n.t(
						'common.components.wangEditor.menus.panelMenus.link.view'
					)
				},
				video: {
					插入视频: '插入视频'
				},
				table: {
					行: 'row',
					列: 'column',
					的: ' ',
					表格: 'table',
					添加行: 'add row',
					删除行: 'delete row',
					添加列: 'add column',
					删除列: 'delete column',
					设置表头: 'set header',
					取消表头: 'cancel header',
					插入表格: 'insert table',
					删除表格: 'delete table'
				},
				code: {
					删除代码: i18n.t(
						'common.components.wangEditor.menus.panelMenus.code.delete'
					),
					修改代码: i18n.t(
						'common.components.wangEditor.menus.panelMenus.code.edit'
					),
					插入代码: i18n.t(
						'common.components.wangEditor.menus.panelMenus.code.insert'
					)
				}
			}
		},
		validate: {
			张图片: 'images',
			大于: 'greater than',
			图片链接: 'image link',
			不是图片: 'is not image',
			返回结果: 'return results',
			上传图片超时: 'upload image timeout',
			上传图片错误: 'upload image error',
			上传图片失败: 'upload image failed',
			插入图片错误: 'insert image error',
			一次最多上传: 'once most at upload',
			下载链接失败: 'download link failed',
			图片验证未通过: 'image validate failed',
			服务器返回状态: 'server return status',
			上传图片返回结果错误: 'upload image return results error',
			请替换为支持的图片类型:
				'please replace with a supported image type',
			您插入的网络图片无法识别:
				'the network picture you inserted is not recognized',
			您刚才插入的图片链接未通过编辑器校验:
				'the image link you just inserted did not pass the editor verification'
		}
	}
}
