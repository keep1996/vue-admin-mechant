export const allConfigList = [
	{
		title: '代理列表',
		code: 'agencyList',
		iconName: 'dz_delegate_my_list',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '创建代理',
		code: 'createAgency',
		iconName: 'dz_delegate_my_add',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '代理收益比例',
		code: 'agencyIncomeRadio',
		iconName: 'dz_delegate_back_pot',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '代理权限设置',
		code: 'agencyPowerManager',
		iconName: 'dz_delegate_back_pot_set',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '帮代理还款',
		code: 'helpAgencyBack',
		iconName: 'dz_delegate_help_agency_back',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '给代理上下分',
		code: 'helpAgencyScore',
		iconName: 'dz_delegate_help_agency_score',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '修改代理密码',
		code: 'replacePwd',
		iconName: 'dz_delegate_general_agent',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '代理账变明细',
		code: 'agencyAccount',
		iconName: 'dz_delegate_center_account_money',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '会员列表',
		code: 'memberList',
		iconName: 'dz_delegate_user_list',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '创建会员',
		code: 'createMember',
		iconName: 'dz_delegate_my_member_add',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '会员返水比例',
		code: 'memberBackRadio',
		iconName: 'dz_delegate_fanshui',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '会员提现审核',
		code: 'memberCashOut',
		iconName: 'dz_delegate_center_cash_out',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '帮会员还款',
		code: 'helpMemberBack',
		iconName: 'dz_delegate_help_delegate',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '给会员上下分',
		code: 'helpMemberScore',
		iconName: 'dz_delegate_help_member',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '修改会员密码',
		code: 'replaceMemberPwd',
		iconName: 'dz_delegate_change_member_pwd',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '会员注单记录',
		code: 'memberGameRecord',
		iconName: 'dz_delegate_game_record',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '代理收益统计',
		code: 'agencyIncome',
		iconName: 'dz_delegate_profit',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '代理信用账单',
		code: 'agencyBilling',
		iconName: 'dz_delegate_agency_bill_list',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '会员信用账单',
		code: 'memberBilling',
		iconName: 'dz_delegate_member_bill_list',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '代理团队账单',
		code: 'agencyTeamBilling',
		iconName: 'dz_delegate_team_list',
		hasCredit: true,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '代理数据统计',
		code: 'agencyData',
		iconName: 'dz_delegate_delegate',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '会员数据统计',
		code: 'memberData',
		iconName: 'dz_delegate_member',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '代理盈亏',
		code: 'agencyProfit',
		iconName: 'dz_delegate_my_profess',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '会员盈亏',
		code: 'memberProfit',
		iconName: 'dz_delegate_my_profess_dl',
		hasCredit: true,
		hasCash: true,
		hasRebate: false,
		hasZhancheng: true
	},
	{
		title: '历史返点统计',
		code: 'historyBackPoint',
		iconName: 'dz_delegate_fandian',
		hasCredit: false,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '牌局记录',
		code: 'pokerRecord',
		iconName: 'dz_delegate_pork_record',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '成员列表',
		code: 'pokerMemberList',
		iconName: 'dz_delegate_member_list',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '编辑俱乐部/创建俱乐部',
		code: 'pokerClubEdit',
		iconName: 'dz_delegate_edit',
		hasCredit: false,
		hasCash: false,
		hasRebate: false,
		hasZhancheng: false
	},
	{
		title: '德州牌桌',
		code: 'dzPoker',
		iconName: 'dz_delegate_center_table_texas',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '短牌牌桌',
		code: 'shortPoker',
		iconName: 'dz_delegate_center_table_short',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '真人限红配置',
		code: 'realManConfig',
		iconName: 'dz_delegate_people_config',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '订单列表',
		code: 'orderList',
		iconName: 'dz_delegate_order_list',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	// {
	// 	title: '代理充值提现',
	// 	code: 'agencyRechargeOrOut',
	// 	iconName: 'dz_delegate_recharge_out',
	// 	hasCredit: true,
	// 	hasCash: false
	// },
	// {
	// 	title: '会员充值提现',
	// 	code: 'memberRechargeOrOut',
	// 	iconName: 'dz_delegate_member_recharge_out',
	// 	hasCredit: true,
	// 	hasCash: false
	// },
	{
		title: '体育限红配置',
		code: 'sportLimitConfig',
		iconName: 'dz_delegate_sport_config',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: 'AOF德州',
		code: 'aofDz',
		iconName: 'club_afo_dz',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: 'AOF短牌',
		code: 'aofPoker',
		iconName: 'club_afo_dp',
		hasCredit: true,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	},
	{
		title: '代会员存款',
		code: 'helpMemberDeposit',
		iconName: 'dz_member_despot',
		hasCredit: false,
		hasCash: true,
		hasRebate: true,
		hasZhancheng: true
	}
	// {
	// 	title: '代理转账',
	// 	code: 'proxyTransfer',
	// 	iconName: 'dz_delegate_transfer',
	// 	hasCredit: false,
	// 	hasCash: true
	// }
]
