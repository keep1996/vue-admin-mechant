import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('agent.total_agent_account'),
			prop: 'proxyUsername',
			minWidth: '180',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.belong_merchant'),
			prop: 'merchantName',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.business_model'),
			prop: 'businessModel',
			minWidth: '120',
			align: 'center',
			showTip: true,
			accountType: 'businessModelTypeArr',
			renderType: 'commonTypeSolt'
		},
		{
			label: i18n.t('operation.login_state_vaid_time'),
			prop: 'loginValidTime',
			minWidth: '160',
			align: 'center',
			showTip: true,
			renderType: 'num'
		},
		{
			label: i18n.t('agent.last_modified_time'),
			prop: 'updatedAt',
			minWidth: '160',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('friendInvitation.modify_person'),
			prop: 'updatedBy',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.actions'),
			prop: 'operate',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'slot',
			slotName: 'actionSlot'
		}
	]
}
