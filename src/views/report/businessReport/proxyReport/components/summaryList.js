import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, currency, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'memberFirstDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.memberFirstDepositAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRegisterAndFirstDepositNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.memberRegisterAndFirstDepositNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'memberDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.memberDepositAmount
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.memberDepositTimes,
												0
											)}
											{this.$t('report.ci')}
										</span>
									</p>
								</div>
							)
							break
						case 'memberWithdrawAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.memberWithdrawAmount
											)}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.memberWithdrawTimes,
												0
											)}
											{this.$t('report.ci')}
										</span>
									</p>
								</div>
							)
							break
						case 'memberDepositWithdrawDifferAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.memberDepositWithdrawDifferAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											<span
												style={this.handleNumberColor(
													subSummary.memberNetAmount
												)}
											>
												{this.handleTotalNumber(
													currency,
													subSummary.memberNetAmount
												)}
											</span>
										</span>
									</p>
								</div>
							)
							break
						case 'underProxyNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.underProxyNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.underProxyNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'directlyUnderProxyNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.directlyUnderProxyNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.underProxyNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'underMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.underMemberNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.underMemberNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'directlyUnderMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.directlyUnderMemberNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.directlyUnderMemberNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'transferInNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.transferInNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.transferInNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break
						case 'transferOutNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.transferOutNum,
												0
											)}
											{this.$t('report.people')}
										</span>
										<span>
											{this.handleNumber(
												'',
												subSummary.transferOutNum,
												0
											)}
											{this.$t('report.people')}
										</span>
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'totalReceiveRebateInTHB':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalReceiveRebateInTHB
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalReceiveRebateInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalReceiveRebateInVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalReceiveRebateInTHB
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalReceiveRebateInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalReceiveRebateInVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'otherAdjustAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherAdjustAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAdjustAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'commissionAdjustAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.commissionAdjustAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.commissionAdjustAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'commissionAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.commissionAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.commissionAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalRebateCommissionAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalRebateCommissionAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalRebateCommissionAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalReceivePersonBrokerage':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalReceivePersonBrokerage
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalReceivePersonBrokerage
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'rebateOtherAdjustAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateOtherAdjustAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateOtherAdjustAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'firstDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.firstDepositAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.firstDepositAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.depositAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.depositAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.depositTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.depositTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.withdrawAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.withdrawAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'rebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.withdrawTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.withdrawTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositWithdrawDifferAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.depositWithdrawDifferAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.depositWithdrawDifferAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'officerProxyCashUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.officerProxyCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.officerProxyCashUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.officerProxyCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.officerProxyCashUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyBeCashUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyBeCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyBeCashUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyBeCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyBeCashUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'firstDeposit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.firstDeposit
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.firstDepositDatetime,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.firstDeposit
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.firstDepositDatetime,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToChildCashUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToChildCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToChildCashUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToChildCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToChildCashUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToMemCashUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToMemCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToMemCashUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToMemCashUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToMemCashUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'officerProxyCashDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.officerProxyCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.officerProxyCashDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.officerProxyCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.officerProxyCashDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyBeCashDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyBeCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyBeCashDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyBeCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyBeCashDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToChildCashDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToChildCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToChildCashDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToChildCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToChildCashDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToMemCashDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToMemCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToMemCashDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToMemCashDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToMemCashDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'officerProxyCreditUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.officerProxyCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.officerProxyCreditUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.officerProxyCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.officerProxyCreditUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyBeCreditUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyBeCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyBeCreditUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyBeCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyBeCreditUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToChildCreditUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToChildCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToChildCreditUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToChildCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToChildCreditUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToMemCreditUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToMemCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToMemCreditUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToMemCreditUpAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToMemCreditUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'officerProxyCreditDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.officerProxyCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.officerProxyCreditDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.officerProxyCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.officerProxyCreditDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyBeCreditDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyBeCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyBeCreditDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyBeCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyBeCreditDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToChildCreditDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToChildCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToChildCreditDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToChildCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToChildCreditDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyToMemCreditDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyToMemCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyToMemCreditDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyToMemCreditDownAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyToMemCreditDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'officialHelpProxyCreditRepayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.officialHelpProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.officialHelpProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.officialHelpProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.officialHelpProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'parentProxyHelpProxyCreditRepayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.parentProxyHelpProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.parentProxyHelpProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.parentProxyHelpProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.parentProxyHelpProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyHelpSubProxyCreditRepayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyHelpSubProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyHelpSubProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyHelpSubProxyCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyHelpSubProxyCreditRepayTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyHelpSubMemberCreditRepayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyHelpSubMemberCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.proxyHelpSubMemberCreditRepayTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyHelpSubMemberCreditRepayAmount
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.proxyHelpSubMemberCreditRepayTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'rebateAdjustAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAdjustAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAdjustAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'walletBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.walletBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.walletBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'creditBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.creditBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.creditBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'creditQuota':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.creditQuota
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.creditQuota
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'returnLoan':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.returnLoan
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.returnLoan
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directlyUnderProxyNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.directlyUnderProxyNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.underProxyNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.directlyUnderProxyNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.underProxyNum,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directlyUnderMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.directlyUnderMemberNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.underMemberNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.directlyUnderMemberNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.underMemberNum,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'transferOutNum':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.transferOutNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												subSummary.transferInNum,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.transferOutNum,
												0
											)}
											<br />
											{this.handleNumber(
												'',
												totalSummary.transferInNum,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportProxyReportExport(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
