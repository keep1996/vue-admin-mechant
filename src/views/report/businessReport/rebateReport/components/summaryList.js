import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'amountIn':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.amountInCNY
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												subSummary.amountInCNY
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.amountInTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountInTHB
											)}
										</span>

										<span
											style={this.handleNumberColor(
												subSummary.amountInVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'',
											subSummary.memberCount,
											0
										)}
										{this.$t('report.people')}
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 列表小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'amountIn':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.amountInCNY
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												subSummary.amountInCNY
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.amountInTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountInTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.amountInVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.amountInCNY
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.amountInCNY
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.amountInTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.amountInTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.amountInVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.amountInVND
											)}
										</span>
									</p>
								</div>
							)
							break

						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'',
											subSummary.memberCount,
											0
										)}
										{this.$t('report.people')}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleNumber(
											'',
											totalSummary.memberCount,
											0
										)}
										{this.$t('report.people')}
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportRebateExport(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
