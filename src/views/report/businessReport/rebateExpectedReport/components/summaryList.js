import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'personRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.personRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.personRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'childRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.childRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.childRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'rebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'clubRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.clubRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.clubRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'insuranceRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insuranceRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insuranceRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'liveRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.liveRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.liveRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'sportRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.sportRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.sportRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'lotteryRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lotteryRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lotteryRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'chessRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.chessRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.chessRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						case 'esportsRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												subSummary.esportsRebateAmount
											)}
										</label>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.esportsRebateAmount
											)}
										</label>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportDaynetamountExportExcel(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
