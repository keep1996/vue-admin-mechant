import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '返点周期',
			prop: 'cycleStartDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		// {
		// 	label: '所属商户',
		// 	prop: 'merchantName',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'defaultSolt',
		// 	isSortColumn: false
		// },
		{
			label: '个人返点',
			prop: 'personRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '下级返点',
			prop: 'childRebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '团队返点',
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'popopSolt',
			isSortColumn: false
		},
		{
			label: '德州-俱乐部服务费团队返点',
			prop: 'clubRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '德州-保险团队分成',
			prop: 'insuranceRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '真人团队返点',
			prop: 'liveRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '体育团队返点',
			prop: 'sportRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '彩票团队返点',
			prop: 'lotteryRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '棋牌团队返点',
			prop: 'chessRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '电竞团队返点',
			prop: 'esportsRebateAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		}
	]
}
