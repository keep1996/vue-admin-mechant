import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'payoutStatus':
						case 'proxyName':
						case 'proxyLevel':
						case 'seePopup':
						case 'parentProxyName':
						case 'topProxyName':
						case 'dzRebateRate':
						case 'valueAddedZhanchengRate':
						case 'insuredZhanchengRate':
						case 'tyRebateRate':
						case 'tyZhanchengRate':
						case 'zrRebateRate':
						case 'zrZhanchengRate':
						case 'cpRebateRate':
						case 'cpZhanchengRate':
						case 'djRebateRate':
						case 'djZhanchengRate':
						case 'qpRebateRate':
						case 'qpZhanchengRate':
						case 'tyNetZhanchengRate':
						case 'zrNetZhanchengRate':
						case 'cpNetZhanchengRate':
						case 'djNetZhanchengRate':
						case 'qpNetZhanchengRate':
						case 'businessModel':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>-</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary[column.property]
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
		// 导出
	}
}
