import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '结算状态',
			id: 2,
			show: true,
			prop: 'payoutStatus',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'statusSolt',
			isSortColumn: false
		},
		{
			label: '代理账号',
			id: 3,
			show: true,
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			isSortColumn: false
		},
		{
			label: '业务模式',
			id: 4,
			show: true,
			prop: 'businessModel',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'businessModelSolt',
			isSortColumn: false
		},
		{
			label: '代理层级',
			id: 5,
			show: true,
			prop: 'proxyLevel',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '收益比例',
			id: 6,
			show: true,
			prop: 'seePopup',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'popupSolt',
			isSortColumn: false
		},
		{
			label: '直属上级',
			id: 7,
			show: true,
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			id: 8,
			show: true,
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			isSortColumn: false
		},
		{
			label: '本期应发',
			id: 9,
			show: true,
			prop: 'incomeAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		},
		{
			label: '调整收益',
			id: 10,
			show: true,
			prop: 'ajustAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		},
		{
			label: '调整后收益',
			id: 11,
			show: true,
			prop: 'realRebateAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			itemTipContent:
				'如果实发收益为负数，则记账的时候该代理的现金余额累加该数值，如果累加后现金余额为负数，则现金余额记录为负数；',
			isSortColumn: false
		},
		{
			label: '德州俱乐部',
			show: true,
			id: 12,
			data: [
				{
					label: '服务费贡献',
					id: 1201,
					prop: 'dzPumpContribution',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'popupSolt',
					itemTipContent: '服务费贡献=手牌服务费贡献+局服务费贡献',
					isSortColumn: false
				},
				{
					label: '团队分成金额',
					id: 1203,
					prop: 'dzRebateAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '直属代理团队分成金额',
					id: 1204,
					prop: 'dzChildRebateAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1205,
					prop: 'dzMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发分成金额',
					id: 1206,
					prop: 'dzShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发分成金额=团队分成金额-直属代理团队分成金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '德州保险',
			id: 13,
			show: true,
			data: [
				{
					label: '会员游戏输赢',
					id: 1301,
					prop: 'insuredNetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '团队分成比例',
					id: 1302,
					prop: 'insuredZhanchengRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				},
				{
					label: '团队分成金额',
					id: 1303,
					prop: 'insuredRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '直属代理团队分成金额',
					id: 1304,
					prop: 'insuredChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发分成金额',
					id: 1305,
					prop: 'insuredShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发占成金额=团队占成金额-直属代理团队占成金额',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '德州增值服务',
			id: 88,
			show: true,
			data: [
				{
					label: '增值服务纯利',
					id: 8801,
					prop: 'valueAddedNetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '团队分成比例',
					id: 8802,
					prop: 'valueAddedZhanchengRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				},
				{
					label: '团队分成金额',
					id: 8803,
					prop: 'valueAddedRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '直属代理团队分成金额',
					id: 8804,
					prop: 'valueAddedChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发分成金额',
					id: 8805,
					prop: 'valueAddedShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发分成金额=团队分成金额-直属代理团队分成金额',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '体育返点分成',
			id: 14,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1401,
					prop: 'tyValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1402,
				// 	prop: 'tyRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1403,
					prop: 'tyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1404,
				// 	prop: 'tyZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'tyPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1405,
					prop: 'tyChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1406,
					prop: 'tyMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金额',
					id: 1407,
					prop: 'tyShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '真人返点分成',
			id: 15,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1501,
					prop: 'zrValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1502,
				// 	prop: 'zrRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1503,
					prop: 'zrRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1504,
				// 	prop: 'zrZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'zrPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1505,
					prop: 'zrChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1506,
					prop: 'zrMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金额',
					id: 1507,
					prop: 'zrShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '彩票返点分成',
			id: 16,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1601,
					prop: 'cpValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1602,
				// 	prop: 'cpRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1603,
					prop: 'cpRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1604,
				// 	prop: 'cpZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'cpPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1605,
					prop: 'cpChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1606,
					prop: 'cpMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金额',
					id: 1607,
					prop: 'cpShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},

		{
			label: '棋牌返点分成',
			id: 18,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1801,
					prop: 'qpValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1802,
				// 	prop: 'qpRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1803,
					prop: 'qpRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1804,
				// 	prop: 'qpZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'qpPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1805,
					prop: 'qpChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1806,
					prop: 'qpMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金',
					id: 1807,
					prop: 'qpShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '电竞返点分成',
			id: 17,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1701,
					prop: 'djValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1702,
				// 	prop: 'djRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1703,
					prop: 'djRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1704,
				// 	prop: 'djZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'djPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1705,
					prop: 'djChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1706,
					prop: 'djMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金额',
					id: 1707,
					prop: 'djShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '电子返点分成',
			id: 17,
			show: true,
			data: [
				{
					label: '会员有效投注',
					id: 1701,
					prop: 'dyValidBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队返点比例',
				// 	id: 1702,
				// 	prop: 'dyRebateRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },
				{
					label: '团队返点金额',
					id: 1703,
					prop: 'dyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				// {
				// 	label: '团队占成比例',
				// 	id: 1704,
				// 	prop: 'dyZhanchengRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// },

				// {
				// 	label: '返点成本分摊',
				// 	id: 1407,
				// 	prop: 'dyPartRebateAmount',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	itemTipContent:
				// 		'返点成本分摊=团队返点成本分摊-下级返点成本分摊',
				// 	solt: 'amountSolt'
				// },
				{
					label: '直属代理团队返点金额',
					id: 1705,
					prop: 'dyChildProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '会员代理返水',
					id: 1706,
					prop: 'dyMemberProxyRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '实际应发返点金额',
					id: 1707,
					prop: 'dyShouldRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					itemTipContent:
						'实际应发返点金额=团队返点金额-直属代理团队返点金额-会员代理返水',
					solt: 'amountSolt'
				}
			]
		}
		// {
		// 	label: '体育占成',
		// 	id: 19,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 1901,
		// 			prop: 'tyNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 1902,
		// 			prop: 'tyNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 1903,
		// 			prop: 'tyNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 1904,
		// 			prop: 'tyNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 1905,
		// 			prop: 'tyNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// },
		// {
		// 	label: '真人占成',
		// 	id: 20,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 2001,
		// 			prop: 'zrNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 2002,
		// 			prop: 'zrNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 2003,
		// 			prop: 'zrNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 2004,
		// 			prop: 'zrNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 2005,
		// 			prop: 'zrNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// },
		// {
		// 	label: '彩票占成',
		// 	id: 21,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 2101,
		// 			prop: 'cpNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 2102,
		// 			prop: 'cpNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 2103,
		// 			prop: 'cpNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 2104,
		// 			prop: 'cpNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 2105,
		// 			prop: 'cpNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// },
		// {
		// 	label: '棋牌占成',
		// 	id: 23,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 2301,
		// 			prop: 'qpNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 2302,
		// 			prop: 'qpNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 2303,
		// 			prop: 'qpNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 2304,
		// 			prop: 'qpNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 2305,
		// 			prop: 'qpNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// },
		// {
		// 	label: '电竞占成',
		// 	id: 22,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 2201,
		// 			prop: 'djNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 2202,
		// 			prop: 'djNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 2203,
		// 			prop: 'djNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 2204,
		// 			prop: 'djNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 2205,
		// 			prop: 'djNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// },
		// {
		// 	label: '电子占成',
		// 	id: 22,
		// 	show: true,
		// 	data: [
		// 		{
		// 			label: '累计会员游戏输赢',
		// 			id: 2201,
		// 			prop: 'dyNetAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '团队占成比例',
		// 			id: 2202,
		// 			prop: 'dyNetZhanchengRate',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'percentage'
		// 		},
		// 		{
		// 			label: '团队占成金额',
		// 			id: 2203,
		// 			prop: 'dyNetRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '直属代理团队占成金额',
		// 			id: 2204,
		// 			prop: 'dyNetChildProxyRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			solt: 'amountSolt'
		// 		},
		// 		{
		// 			label: '实际应发占成金额',
		// 			id: 2205,
		// 			prop: 'dyNetShouldRebateAmount',
		// 			alignCenter: 'center',
		// 			minWidth: '180',
		// 			isFixed: false,
		// 			isShow: true,
		// 			isShowTip: true,
		// 			itemTipContent:
		// 				'实际应发占成金额=团队占成金额-直属代理团队占成金额',
		// 			solt: 'amountSolt'
		// 		}
		// 	]
		// }
	]
}
