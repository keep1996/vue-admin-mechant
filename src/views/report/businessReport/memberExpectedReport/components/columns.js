// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '返水周期',
			prop: 'staticsDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '会员账号',
			prop: 'memberAccount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '直属代理账号',
			prop: 'proxyAccount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			prop: 'topProxyAccount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		// {
		// 	label: '所属商户',
		// 	prop: 'merchantName',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'defaultSolt',
		// 	isSortColumn: false
		// },
		{
			label: '个人返水',
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'popopSolt',
			isSortColumn: false
		},
		{
			label: '德州-俱乐部服务费个人返水',
			prop: 'rebateAmountDxClub',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '真人个人返水',
			prop: 'rebateAmountZr',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '体育个人返水',
			prop: 'rebateAmountTy',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '彩票个人返水',
			prop: 'rebateAmountCp',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '棋牌个人返水',
			prop: 'rebateAmountQp',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '电竞个人返水',
			prop: 'rebateAmountDj',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '电子个人返水',
			prop: 'rebateAmountDy',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		}
	]
}
