import i18n from '@/locales'

export const getColumnsNew = () => {
	return [
		{
			gameCode: 'TY',
			gameName: '体育场馆',
			list: [
				{
					label: '投注盈亏重算调整',
					prop: 'tyNetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '有效投注重算调整',
					prop: 'tyValidBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '返点重算调整',
					prop: 'tyRebateRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		},
		{
			gameCode: 'ZR',
			gameName: '真人场馆',
			list: [
				{
					label: '投注盈亏重算调整',
					prop: 'zrNetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '有效投注重算调整',
					prop: 'zrValidBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '返点重算调整',
					prop: 'zrRebateRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		},
		{
			gameCode: 'DJ',
			gameName: '电竞场馆',
			list: [
				{
					label: '投注盈亏重算调整',
					prop: 'djNetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '有效投注重算调整',
					prop: 'djValidBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '返点重算调整',
					prop: 'djRebateRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		},
		{
			gameCode: 'CP',
			gameName: '彩票场馆',
			list: [
				{
					label: '投注盈亏重算调整',
					prop: 'cpNetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '有效投注重算调整',
					prop: 'cpValidBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '返点重算调整',
					prop: 'cpRebateRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		}
	]
}
