import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计
		handleDialogRow(params, currency, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'payment':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.payable
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realPCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.payment
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'notPayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style='color:#D9001B'>
											{this.handleTotalNumber(
												currency,
												subSummary.unpay
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'paymentCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.payable
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realRCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.payment
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalChildCommissionAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style='color:#D9001B'>
											{this.handleTotalNumber(
												currency,
												subSummary.unpaid
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'payment':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.paymentInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.paymentInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.paymentInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.paymentInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.paymentInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.paymentInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realPCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.realPCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.realPCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.realPCommissionInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.realPCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.realPCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.realPCommissionInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'notPayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.notPayAmountInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.notPayAmountInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.notPayAmountInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.notPayAmountInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.notPayAmountInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.notPayAmountInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'paymentCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.paymentCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.paymentCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.paymentCommissionInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.paymentCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.paymentCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.paymentCommissionInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realRCommission':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.realRCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.realRCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.realRCommissionInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.realRCommissionInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.realRCommissionInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.realRCommissionInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalChildCommissionAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalChildCommissionAmountInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalChildCommissionAmountInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalChildCommissionAmountInVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalChildCommissionAmountInCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalChildCommissionAmountInTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalChildCommissionAmountInVND
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportProxyCommissionExport(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
