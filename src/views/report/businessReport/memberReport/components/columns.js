import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('common.member_number'),
			prop: 'playerName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			userColor: true
		},
		{
			label: i18n.t('funds.capital_adjustment.member_nickname'),
			prop: 'nickName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('common.number_type'),
			prop: 'accountType',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'filterType'
		},
		{
			label: i18n.t('common.top_agent'),
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copyDefaultSolt'
		},
		{
			label: i18n.t('funds.fund_audit.belong_general_agent'),
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('common.account_status'),
			prop: 'accountLockStatus',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'accountStatusSolt'
		},
		{
			label: i18n.t('member.label_name'),
			prop: 'labelName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('common.risk_control_level'),
			prop: 'windControlName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('report.register_dates'),
			prop: 'registerAt',
			alignCenter: 'center',
			minWidth: '190',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('report.Wallet_info'),
			prop: 'walletBalance',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true
		},
		{
			label: i18n.t('report.first_deposit_amount'),
			subLabel: i18n.t('report.first_deposit_time'),
			prop: 'firstDepositAmount',
			subProp: 'firstDepositTime',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			soltHeraer: true,
			solt: 'amountTimeSolt'
		},
		{
			label: i18n.t('report.bet_count'),
			prop: 'betCount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.bet_amount'),
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.valid_bet_amount'),
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.net_amount'),
			prop: 'betNetAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt',
			soltColor: true
		},
		{
			label: '代理发放会员返水',
			prop: 'memberRebateAmountByProxy',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: '平台发放会员返水',
			prop: 'memberRebateAmountByPlat',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.artificial_patch_amount'),
			prop: 'otherAdjustAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.net_profit'),
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt',
			soltColor: true
		},
		{
			label: i18n.t('report.proxy_score'),
			prop: 'proxyCashUpAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.proxy_score_times'),
			prop: 'proxyCashUpTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.proxy_points'),
			prop: 'proxyCashDownAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.proxy_points_times'),
			prop: 'proxyCashDownTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.proxy_credit_up_points'),
			prop: 'proxyCreditUpAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.proxy_credit_up_points_times'),
			prop: 'proxyCreditUpTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.proxy_credit_down_points'),
			prop: 'proxyCreditDownAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.proxy_credit_down_points_times'),
			prop: 'proxyCreditDownTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: '代理帮信用还款',
			prop: 'proxyHelpCreditRepayAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: '代理帮信用还款次数',
			prop: 'proxyHelpCreditRepayTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.deposit_amount'),
			prop: 'depositAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.deposit_times'),
			prop: 'depositTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.withdraw_amount'),
			prop: 'withdrawAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.deposit_withdraw_differ_amount'),
			prop: 'depositWithdrawDifferAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.withdraw_times'),
			prop: 'withdrawTimes',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			isSortColumn: true,
			solt: 'numberSolt'
		}
		// {
		// 	label: i18n.t('report.premium_deposit_amount'),
		// 	prop: 'premiumDepositAmount',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	isSortColumn: true,
		// 	solt: 'amountSolt'
		// },
		// {
		// 	label: i18n.t('report.premium_deposit_times'),
		// 	prop: 'premiumDepositTimes',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	isSortColumn: true,
		// 	solt: 'numberSolt'
		// }
	]
}
