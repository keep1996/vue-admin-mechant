import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, currency, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											<span
												style={this.handleNumberColor(
													subSummary.betNetAmount
												)}
											>
												{this.handleTotalNumber(
													currency,
													subSummary.betNetAmount
												)}
											</span>
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											<span
												style={this.handleNumberColor(
													subSummary.netAmount
												)}
											>
												{this.handleTotalNumber(
													currency,
													subSummary.netAmount
												)}
											</span>
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.betCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betAmount
											)}
										</span>
										{/* <span>
												{this.handleTotalNumber(
													'THB',
													subSummary.betAmountTHB
												)}
											</span>
											<span>
												{this.handleTotalNumber(
													'VND',
													subSummary.betAmountVND
												)}
											</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
										{/* <span>
												{this.handleTotalNumber(
													'THB',
													totalSummary.betAmountTHB
												)}
											</span>
											<span>
												{this.handleTotalNumber(
													'VND',
													totalSummary.betAmountVND
												)}
											</span> */}
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
										{/* <span>
												{this.handleTotalNumber(
													'THB',
													subSummary.validBetAmountTHB
												)}
											</span>
											<span>
												{this.handleTotalNumber(
													'VND',
													subSummary.validBetAmountVND
												)}
											</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
										{/* <span>
												{this.handleTotalNumber(
													'THB',
													totalSummary.validBetAmountTHB
												)}
											</span>
											<span>
												{this.handleTotalNumber(
													'VND',
													totalSummary.validBetAmountVND
												)}
											</span> */}
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</span>
										{/* <span
												style={this.handleNumberColor(
													subSummary.netAmountTHB
												)}
											>
												{this.handleTotalNumber(
													'THB',
													subSummary.netAmountTHB
												)}
											</span>
											<span
												style={this.handleNumberColor(
													subSummary.netAmountVND
												)}
											>
												{this.handleTotalNumber(
													'VND',
													subSummary.netAmountVND
												)}
											</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
										{/* <span
												style={this.handleNumberColor(
													totalSummary.netAmountTHB
												)}
											>
												{this.handleTotalNumber(
													'THB',
													totalSummary.netAmountTHB
												)}
											</span>
											<span
												style={this.handleNumberColor(
													totalSummary.netAmountVND
												)}
											>
												{this.handleTotalNumber(
													'VND',
													totalSummary.netAmountVND
												)}
											</span> */}
									</p>
								</div>
							)
							break
						case 'rebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyHelpCreditRepayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyHelpCreditRepayAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyHelpCreditRepayAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyHelpCreditRepayTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.proxyHelpCreditRepayTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.proxyHelpCreditRepayTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'otherAdjustAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherAdjustAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAdjustAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.betNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.netProfitTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.netProfitTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.netProfitVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.netProfitVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.betNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												totalSummary.netProfitTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.netProfitTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.netProfitVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.netProfitVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'proxyCashUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyCashUpAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyCashUpAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCashUpTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.proxyCashUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.proxyCashUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCashDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyCashDownAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyCashDownAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCashDownTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.proxyCashDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.proxyCashDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCreditUpAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyCreditUpAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyCreditUpAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCreditUpTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.proxyCreditUpTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.proxyCreditUpTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByProxy':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByPlat':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCreditDownAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyCreditDownAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyCreditDownAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyCreditDownTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.proxyCreditDownTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.proxyCreditDownTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalDeposit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalDeposit
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalDeposit
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.depositTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.depositTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'totalWithdrawal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalWithdrawal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalWithdrawal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositWithdrawDiffer':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.depositWithdrawDiffer
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.depositWithdrawDiffer
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'premiumDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.premiumDepositAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.premiumDepositAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'premiumDepositTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.premiumDepositTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.premiumDepositTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.withdrawAmount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.withdrawAmount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawTimes':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.withdrawTimes,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.withdrawTimes,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.depositAmount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.depositAmount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositWithdrawDifferAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary.depositWithdrawDifferAmount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary.depositWithdrawDifferAmount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'firstDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.firstDepositAmount
											)}
											<br />-
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.firstDepositAmount
											)}
											<br />-
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params, httpStr) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api[httpStr](params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
