import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'depositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.depositAmountCNY
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.depositAmountTHB
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.depositAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.depositAmountCNY
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.depositAmountTHB
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.depositAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'backDepositAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.backDepositAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.backDepositAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.backDepositAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.backDepositAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.backDepositAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.backDepositAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.withdrawalAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.withdrawalAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.withdrawalAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.withdrawalAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.withdrawalAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.withdrawalAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'backWithdrawalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.backWithdrawalAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.backWithdrawalAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.backWithdrawalAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.backWithdrawalAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.backWithdrawalAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.backWithdrawalAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'depositAmountTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.depositAmountTotalCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.depositAmountTotalTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.depositAmountTotalVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.depositAmountTotalCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.depositAmountTotalTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.depositAmountTotalVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'withdrawalAmountTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.withdrawalAmountTotalCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.withdrawalAmountTotalTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.withdrawalAmountTotalVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.withdrawalAmountTotalCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.withdrawalAmountTotalTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.withdrawalAmountTotalVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'accessAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.accessAmountCNY
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												subSummary.accessAmountCNY
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.accessAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.accessAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.accessAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.accessAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.accessAmountCNY
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.accessAmountCNY
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.accessAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.accessAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.accessAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.accessAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportAccessAmountDayExport(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
