import i18n from '@/locales'

export const getColumnsNew = () => {
	return [
		{
			gameCode: 'CK',
			gameName: '存款',
			list: [
				{
					label: '易币付充值',
					prop: 'yibiDepositAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '白马会充值',
					prop: 'bmhDepositAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '三方掉单补分',
					prop: 'thirdOrderLostCompensateAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '线下代存',
					prop: 'offlineDepositAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '代理代存',
					prop: 'proxyDepositForMemberAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '实际总存款',
					prop: 'totalDepositAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		},
		{
			gameCode: 'QK',
			gameName: '取款',
			list: [
				{
					label: '客户取款',
					prop: 'onlineWithdrawAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '手动下分出款',
					prop: 'offlineWithdrawAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '实际总提款',
					prop: 'totalWithdrawAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				}
			]
		}
	]
}
