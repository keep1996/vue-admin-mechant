import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '总代',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '业务模式',
			prop: 'businessModel',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '平台占比',
			prop: 'platformRate',
			alignCenter: 'platformRate',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '体育有效流水',
			prop: 'tyValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '电竞有效流水',
			prop: 'djValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '真人有效流水',
			prop: 'zrValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '棋牌有效流水',
			prop: 'qpValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '彩票有效流水',
			prop: 'cpValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '电子有效流水',
			prop: 'dzValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '总返水',
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '总输赢',
			prop: 'otherNetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '净输赢',
			prop: 'netProfitAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '真人打赏',
			prop: 'zrRewardAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '电子活动奖金',
			prop: 'dzActivityRewardAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '德州水保',
			prop: 'dxServiceContribuInsureWinorloss',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '局服务费',
			prop: 'serviceChargeRoundAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '德州增值服务费',
			prop: 'dxValueAddedTotalRebateAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		}
	]
}
