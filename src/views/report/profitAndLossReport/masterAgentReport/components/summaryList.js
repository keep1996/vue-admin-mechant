import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		handleRow(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'staticsDate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property] ||
												'-'}
										</span>
									</p>
								</div>
							)
							break
						case 'topProxyName':
						case 'netRate':
								sums[index] = (
									<div>
										<p class='footer_count_row  footer_count_row_border'>
											<span></span>
										</p>
									</div>
								)
								break

						case 'newMemberNum':
						case 'validUserCount':
						case 'activeMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{ totalSummary[column.property] }
										</span>
									</p>
								</div>
							)
							break
						case 'averageAgentSharingRate':
								sums[index] = (
									<div>
										<p class='footer_count_row  footer_count_row_border'>
											<span>
												{(totalSummary[column.property] ||
													0) + '%'}
											</span>
										</p>
									</div>
								)
								break
						case 'betAmount':
						case 'validBetAmount':
						case 'rebateSharingModelAmount':
						case 'bonusModelAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
