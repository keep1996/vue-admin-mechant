import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('report.date'),
			prop: 'staticsDate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('report.master_agent_account'),
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('report.new_member'),
			prop: 'newMemberNum',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.new_valid_member'),
			prop: 'validUserCount',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.daily_active_member'),
			prop: 'activeMemberNum',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: i18n.t('report.betting_amount'),
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.valid_bet_amount1'),
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.member_wins_and_losses'),
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: i18n.t('report.valid_profit_ratio'),
			prop: 'netRate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: i18n.t('report.agent_sharing_user_win_or_losses'),
			prop: 'agentSharingAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: i18n.t('report.agent_bonus_base_on_user_win_or_losses'),
			prop: 'agentBonusAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: i18n.t('report.rebate_of_sharing_model_agent'),
			prop: 'rebateSharingModelAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.bonus_of_bonuses_model_agent'),
			prop: 'bonusModelAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.average_agent_sharing_ratio'),
			prop: 'averageAgentSharingRate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: i18n.t('report.account_adjustment'),
			prop: 'accountAdjustmentAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t('report.agent_bonuses_withdrawal'),
			prop: 'agentBonusWithdrawAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		}
	]
}
