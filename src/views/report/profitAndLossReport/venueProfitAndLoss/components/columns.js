import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '场馆信息',
			prop: 'venueName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: '场馆名称',
					prop: 'venueName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: true,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt',
					isAddColor: true
				},
				{
					label: '场馆项目',
					prop: 'venueTypeName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt'
				}
			]
		},
		{
			label: '盈亏数据',
			prop: 'venueName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: i18n.t('report.member_count'),
					prop: 'memberCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_count'),
					prop: 'betCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_amount'),
					prop: 'betAmount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '服务费贡献',
					prop: 'serviceAmountContribution',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.valid_bet_amount'),
					prop: 'validBetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注重算调整',
					prop: 'validBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际有效投注',
					prop: 'realValidBetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.net_amount'),
					prop: 'netAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},
				{
					label: '投注盈亏重算调整',
					prop: 'netRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际投注盈亏',
					prop: 'realNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},

				{
					label: '实际有效盈利率',
					prop: 'realNetRate',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					soltColor: true,
					isSortColumn: true,
					itemTipContent:
						'实际有效盈利率=实际投注盈亏/实际有效投注*100%'
				}
			]
		}
	]
}

export const getColumnsNew = () => {
	return [
		{
			gameCode: 'DZ',
			gameName: 'DX德州',
			list: [
				{
					label: '投注人数',
					prop: 'dxMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'dxBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'dxBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'dxValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'dxNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '会员服务费贡献',
					prop: 'dxMemberPumpPercentageAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentageAmount',
					isSortColumn: true,
					soltColor: true,
					itemTipContent:
						'会员被收取的手牌服务费贡献+局服务费贡献之和'
				},
				{
					label: '代理服务费分成',
					prop: 'dxProxyPumpPercentageAmountRebate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				// {
				// 	label: '代理服务费分成(返佣)',
				// 	prop: 'dxProxyPumpPercentageAmountCommission',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'amountSolt',
				// 	isSortColumn: true,
				// 	soltColor: true
				// },
				{
					label: '平台服务费分成',
					prop: 'dxCompanyPumpPercentageAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '服务费盈利率',
					prop: 'dxPumpPercentageNetRate',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true,
					itemTipContent: '平台服务费分成/俱乐部投注金额*100%'
				},
				{
					label: '会员保险盈亏',
					prop: 'dxMemberInsuredNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '代理保险盈亏',
					prop: 'dxProxyInsuredNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '平台保险盈亏',
					prop: 'dxCompanyInsuredNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '保险盈利率',
					prop: 'dxInsuredNetRate',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true,
					itemTipContent: '平台保险盈亏/保险投注金额*100%'
				},
				{
					label: '会员增值服务费消费',
					prop: 'memberValueAddedAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '代理增值服务消费分成',
					prop: 'proxyValueAddedPercentageAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '平台增值服务消费分成',
					prop: 'companyValueAddedPercentageAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '增值服务利润率',
					prop: 'valueAddedAmountRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: true,
					soltColor: true,
					itemTipContent: '平台增值服务消费分成/增值服务纯利*100%'
				}
			]
		},
		{
			gameCode: 'ty',
			gameName: 'PM体育',
			list: [
				{
					label: '投注人数',
					prop: 'pmtyMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'pmtyBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'pmtyBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'pmtyValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'pmtyNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'pmtyNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'zr',
			gameName: 'PM真人',
			list: [
				{
					label: '投注人数',
					prop: 'pmzrMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'pmzrBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'pmzrBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'pmzrValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'pmzrNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'pmzrNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'byqp',
			gameName: '博雅棋牌',
			list: [
				{
					label: '投注人数',
					prop: 'byqpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'byqpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'byqpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'byqpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'byqpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'pmqpNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'cp',
			gameName: 'PM彩票',
			list: [
				{
					label: '投注人数',
					prop: 'pmcpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'pmcpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'pmcpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'pmcpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'pmcpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'pmcpNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'hccp',
			gameName: 'HC彩票',
			list: [
				{
					label: '投注人数',
					prop: 'hccpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'hccpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'hccpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'hccpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'hccpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'hccpNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'dj',
			gameName: 'PM电竞',
			list: [
				{
					label: '投注人数',
					prop: 'pmdjMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'pmdjBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'pmdjBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'pmdjValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'pmdjNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'pmdjNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'dy',
			gameName: 'PM电子',
			list: [
				{
					label: '投注人数',
					prop: 'dyMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'dyBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'dyBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'dyValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'dyNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'dyNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		}
	]
}
