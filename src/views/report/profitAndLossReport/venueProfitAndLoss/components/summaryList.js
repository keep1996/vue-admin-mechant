import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		// 列表小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceAmountContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceAmountContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.realValidBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netRate
											)}
										>
											{(totalSummary.netRate || 0) + '%'}
										</span>
									</p>
								</div>
							)
							break

						case 'realNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.realNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.realNetRate
											)}
										>
											{(totalSummary.realNetRate || 0) +
												'%'}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleBetDetailRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.betCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.betAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'realValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.realValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.realValidBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.netRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceAmountContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.serviceAmountContribution
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceAmountContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break

						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												subSummary.netAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleRowNew(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'dxMemberCount':
						case 'pmtyMemberCount':
						case 'pmzrMemberCount':
						case 'byqpMemberCount':
						case 'pmcpMemberCount':
						case 'hccpMemberCount':
						case 'dyMemberCount':
						case 'pmdjMemberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'dxPumpPercentageNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsuredNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'pmdjNetRate':
						case 'pmcpNetRate':
						case 'hccpNetRate':
						case 'pmqpNetRate':
						case 'pmzrNetRate':
						case 'dyNetRate':
						case 'pmtyNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxBetCount':
						case 'pmtyBetCount':
						case 'pmzrBetCount':
						case 'byqpBetCount':
						case 'pmcpBetCount':
						case 'hccpBetCount':
						case 'pmdjBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property]}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedAmountRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property]}%
										</span>
									</p>
								</div>
							)
							break
						case 'dxMemberPumpPercentageAmount':
						case 'dxProxyPumpPercentageAmountRebate':
						case 'dxProxyPumpPercentageAmountCommission':
						case 'dxCompanyPumpPercentageAmount':
						case 'dxMemberInsuredNetAmount':
						case 'dxProxyInsuredNetAmount':
						case 'dxCompanyInsuredNetAmount':
						case 'pmtyNetAmount':
						case 'pmzrNetAmount':
						case 'byqpNetAmount':
						case 'pmcpNetAmount':
						case 'hccpNetAmount':
						case 'pmdjNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
