import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, currency, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												currency,
												subSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyRebatePoint':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.proxyRebatePoint
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 场馆费弹框小计行
		handleVenueRow(params, currency, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'totalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.totalNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'platformAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.platformAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'totalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.totalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.totalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherAdjustmentTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherAdjustmentTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherAdjustmentTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherAdjustmentTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAdjustmentTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.qpValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.betAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.betAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.betAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.betAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.validBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.validBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.validBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.validBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{/* <span>{subSummary.memberCount}</span> */}
										<span>-</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{/* <span>{totalSummary.memberCount}</span> */}
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>{subSummary.betCount}</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.netAmountTHB
											)}
										</span>

										<span
											style={this.handleNumberColor(
												subSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.netAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>{totalSummary.betCount}</span>

										{/* <span
											style={this.handleNumberColor(
												totalSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.netAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.netAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.proxyRebatePointTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.proxyRebatePointVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.proxyRebatePointTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.proxyRebatePointVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'proxyArtificialPatchAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyArtificialPatchAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.artificialPatchAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.artificialPatchAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyArtificialPatchAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.artificialPatchAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.artificialPatchAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'memberRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyRebatePoint':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRebatePoint
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRebatePoint
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRebatePoint
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRebatePoint
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberArtificialPatchAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberArtificialPatchAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberArtificialPatchAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberNetwinAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberNetwinAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberNetwinAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedTotalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'previousPeriodBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.previousPeriodBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.previousPeriodBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.previousPeriodBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.previousPeriodBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'venueFee':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.venueFee
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.venueFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.venueFee
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.venueFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByProxy':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByPlat':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directRebateAmountByProxy':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directMemberRebateAmountByProxy':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directMemberRebateAmountByPlat':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directRebateAmountByPlat':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyNetwinAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyNetwinAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyNetwinAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lastMonthBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.lastMonthBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lastMonthBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.lastMonthBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lastMonthBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyRushNet':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRushNet
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRushNet
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRushNet
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRushNet
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'previousReversingNetLosses':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.previousReversingNetLosses
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.previousReversingNetLosses
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.previousReversingNetLosses
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.previousReversingNetLosses
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.directBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.directBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directValidBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.directValidBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.directValidBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directValidBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directValidBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directValidBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.directNetAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.directNetAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.directNetAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.directNetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												totalSummary.directNetAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directNetAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directNetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeRound':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.serviceChargeRound
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.serviceChargeRound
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directArtificialPatchAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directArtificialPatchAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directArtificialPatchAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directNetwinAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.directNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directNetwinAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.directProfitAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.directProfitAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.directProfitAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.directProfitAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directNetwinAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												totalSummary.directProfitAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directProfitAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.directProfitAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directProfitAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'dxClubNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxSquidNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxSquidNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxSquidNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxSquidNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxSquidNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxClubServiceChargeRoundAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeRoundAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeRoundAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeRoundAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeRoundAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxClubServiceChargeContributionRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeContributionRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeContributionRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeContributionRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeContributionRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsureNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxInsureNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxInsureNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsureNetRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxInsureNetRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureNetRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxInsureNetRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureNetRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxValueAddedTotalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxValueAddedTotalAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxValueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxValueAddedTotalAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxValueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxValueAddedTotalRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxValueAddedTotalRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxValueAddedTotalRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxValueAddedTotalRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxValueAddedTotalRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherRebateRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherRebateRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartRebateRecalculationAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartRebateRecalculationAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'memberRebateAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'memberAccountAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.memberAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberAccountAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberAccountAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'proxyRebateAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRebateAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRebateAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'proxyAccountAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.proxyAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyAccountAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyAccountAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubServiceChargeHandBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.dxClubServiceChargeHandBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dxClubServiceChargeHandBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.dxInsureBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dxInsureBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubTotalServiceChargeContributionAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubTotalServiceChargeContributionAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubTotalServiceChargeContributionAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubServiceChargeHandAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeHandAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeHandAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeHandAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeHandAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.otherTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.otherBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.otherBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.tyBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.tyBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.tyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.tyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.tyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.tyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.tyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.zrBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.zrBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.zrNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.zrTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.zrRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.zrNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.zrPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.cpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.cpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.hccpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.hccpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyActivityBonus':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyActivityBonus
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyActivityBonus
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.cpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.hccpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.cpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.hccpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break

						case 'cpRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.cpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.hccpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break

						case 'cpPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.cpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.hccpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRewardAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRewardAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRewardAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.djBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.djBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.djNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.djTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.djRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.djNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.qpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.djPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.dyBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dyBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.qpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.qpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpPartRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.qpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.qpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.dyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 日弹窗，总计行
		handleDayRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>总计</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'totalBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.totalBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.betAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.betAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.betAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.betAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'totalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.totalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalNetAmount
											)}
										</span>
										{/* <span>
												{this.handleTotalNumber(
													'THB',
													totalSummary.betAmountTHB
												)}
											</span>
											<span>
												{this.handleTotalNumber(
													'VND',
													totalSummary.betAmountVND
												)}
											</span> */}
									</p>
								</div>
							)
							break
						case 'otherAdjustmentTotalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherAdjustmentTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAdjustmentTotalNetAmount
											)}
										</span>
										{/* <span>
													{this.handleTotalNumber(
														'THB',
														totalSummary.betAmountTHB
													)}
												</span>
												<span>
													{this.handleTotalNumber(
														'VND',
														totalSummary.betAmountVND
													)}
												</span> */}
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.validBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.validBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.validBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.validBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										{/* <span>{subSummary.memberCount}</span> */}
										<span>-</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{/* <span>{totalSummary.memberCount}</span> */}
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'betCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>{subSummary.betCount}</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.netAmountTHB
											)}
										</span>

										<span
											style={this.handleNumberColor(
												subSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.netAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>{totalSummary.betCount}</span>

										{/* <span
											style={this.handleNumberColor(
												totalSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.netAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.netAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.proxyRebatePointTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.proxyRebatePointVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.proxyRebatePointTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.proxyRebatePointVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'proxyArtificialPatchAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyArtificialPatchAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.artificialPatchAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.artificialPatchAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyArtificialPatchAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.artificialPatchAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.artificialPatchAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'memberRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyRebatePoint':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRebatePoint
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRebatePoint
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRebatePoint
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRebatePoint
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberArtificialPatchAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberArtificialPatchAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberArtificialPatchAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberNetwinAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberNetwinAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberNetwinAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedTotalAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'previousPeriodBalance':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.previousPeriodBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.previousPeriodBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.previousPeriodBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.previousPeriodBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'venueFee':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.venueFee
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.venueFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.venueFee
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.venueFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByProxy':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberRebateAmountByPlat':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directRebateAmountByProxy':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directMemberRebateAmountByProxy':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByProxy
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByProxy
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directMemberRebateAmountByPlat':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directRebateAmountByPlat':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directMemberRebateAmountByPlat
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directMemberRebateAmountByPlat
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyNetwinAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyNetwinAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyNetwinAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lastMonthBalance':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.lastMonthBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lastMonthBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.lastMonthBalance
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lastMonthBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'proxyRushNet':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRushNet
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRushNet
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRushNet
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRushNet
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'previousReversingNetLosses':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.previousReversingNetLosses
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.previousReversingNetLosses
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.previousReversingNetLosses
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.previousReversingNetLosses
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.directBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.directBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directValidBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.directValidBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.directValidBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directValidBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directValidBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directValidBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.directNetAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.directNetAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.directNetAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.directNetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directNetAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												totalSummary.directNetAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directNetAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directNetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'directRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeRound':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.serviceChargeRound
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.serviceChargeRound
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directArtificialPatchAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directArtificialPatchAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directArtificialPatchAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directArtificialPatchAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'directNetwinAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.directNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.directNetwinAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												subSummary.directProfitAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.directProfitAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												subSummary.directProfitAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.directProfitAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.directNetwinAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.directNetwinAmount
											)}
										</span>
										{/* <span
											style={this.handleNumberColor(
												totalSummary.directProfitAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.directProfitAmountTHB
											)}
										</span>
										<span
											style={this.handleNumberColor(
												totalSummary.directProfitAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.directProfitAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'dxClubNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxSquidNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxSquidNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxSquidNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxClubServiceChargeRoundAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeRoundAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeRoundAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeRoundAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeRoundAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxClubServiceChargeContributionRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeContributionRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeContributionRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeContributionRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeContributionRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsureNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxInsureNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxInsureNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsureNetRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxInsureNetRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureNetRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxInsureNetRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureNetRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxValueAddedTotalAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxValueAddedTotalAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxValueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxValueAddedTotalAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxValueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxValueAddedTotalRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxValueAddedTotalRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxValueAddedTotalRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxValueAddedTotalRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxValueAddedTotalRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherRebateRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherRebateRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherPartRebateRecalculationAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherPartRebateRecalculationAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'memberRebateAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberRebateAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberRebateAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'memberAccountAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.memberAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.memberAccountAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.memberAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.memberAccountAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'proxyRebateAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyRebateAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyRebateAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyRebateAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'proxyAccountAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.proxyAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.proxyAccountAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.proxyAccountAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.proxyAccountAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubServiceChargeHandBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.dxClubServiceChargeHandBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dxClubServiceChargeHandBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.dxInsureBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dxInsureBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubTotalServiceChargeContributionAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubTotalServiceChargeContributionAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubTotalServiceChargeContributionAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxInsureValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxInsureValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxInsureValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dxClubServiceChargeHandAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dxClubServiceChargeHandAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dxClubServiceChargeHandAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dxClubServiceChargeHandAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dxClubServiceChargeHandAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.otherTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.otherTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.otherBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.otherBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'otherValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.otherValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.otherValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.tyBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.tyBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.tyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.tyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.tyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.tyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'tyPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.tyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tyPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.tyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tyPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.zrBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.zrBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.zrNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.zrTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.zrRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.zrNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.zrPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.zrPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.cpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.cpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.hccpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.hccpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyActivityBonus':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyActivityBonus
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyActivityBonus
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.cpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.hccpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.cpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.hccpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.cpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.hccpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break

						case 'qpValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.qpValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpValidBetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.cpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.cpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.hccpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.hccpPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'zrRewardAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.zrRewardAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.zrRewardAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.djBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.djBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'cpNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.cpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.cpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'hccpNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.hccpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.hccpNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.djNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.djTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.djRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.djNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.qpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'djPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.djPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.djPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.djPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.djPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.dyBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.dyBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpBetCount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleNumber(
												'',
												subSummary.qpBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.qpBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyValidBetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyValidBetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyRebateRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyRebateRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartRebateRecalculationAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartRebateRecalculationAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartRebateRecalculationAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpPartRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpPartRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpPartRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.qpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'qpTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.qpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.qpTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.qpTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.qpTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyTotalNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyTotalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyTotalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyTotalNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyRebateAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyRebateAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyRebateAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyNetRecalculationAdjustmentAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyNetRecalculationAdjustmentAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyNetRecalculationAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						case 'dyPartNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										<span
											style={this.handleNumberColor(
												subSummary.dyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.dyPartNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.dyPartNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.dyPartNetAmount
											)}
										</span>
									</p>
								</div>
							)

							break
						default:
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style='display: none;'
									>
										-
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params, httpStr) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api[httpStr](params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
