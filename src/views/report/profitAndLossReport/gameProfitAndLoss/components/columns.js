import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '游戏信息',
			prop: 'gameTypeName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: '游戏名称',
					prop: 'gameTypeName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: true,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt',
					isAddColor: true
				},
				{
					label: '所属场馆',
					prop: 'venueName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt'
				}
			]
		},
		{
			label: '盈亏数据',
			prop: 'gameTypeName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: i18n.t('report.member_count'),
					prop: 'memberCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_count'),
					prop: 'betCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_amount'),
					prop: 'betAmount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '服务费贡献',
					prop: 'serviceAmountContribution',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.valid_bet_amount'),
					prop: 'validBetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注重算调整',
					prop: 'validBetRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际有效投注',
					prop: 'realValidBetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.net_amount'),
					prop: 'netAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},
				{
					label: '投注盈亏重算调整',
					prop: 'netRecalculationAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际投注盈亏',
					prop: 'realNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				}
			]
		}
	]
}
