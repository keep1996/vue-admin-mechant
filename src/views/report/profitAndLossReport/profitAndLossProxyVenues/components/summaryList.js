import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		// 列表小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceChargeContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountReal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmountReal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netRate
											)}
										>
											{(totalSummary.netRate || 0) + '%'}
										</span>
									</p>
								</div>
							)
							break

						case 'netAmountReal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmountReal
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmountReal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netPercentage':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netPercentage
											)}
										>
											{(totalSummary.netPercentage || 0) +
												'%'}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleRowDay(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceAmountContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountReal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceChargeContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'memberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netRate
											)}
										>
											{(totalSummary.netRate || 0) + '%'}
										</span>
									</p>
								</div>
							)
							break

						case 'netAmountReal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmountReal
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmountReal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netPercentage':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netPercentage
											)}
										>
											{(totalSummary.netPercentage || 0) +
												'%'}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleBetDetailRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.betCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.betAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountReal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetAmountReal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmountReal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetAmountRecal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmountRecal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.netAmountRecal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.serviceChargeContribution
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.serviceChargeContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.validBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												subSummary.netAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleRowNew(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					console.log(column.property, 'column.property')
					switch (column.property) {
						case 'proxyName':
						case 'dxClubMemberCount':
						case 'tyMemberCount':
						case 'zrMemberCount':
						case 'qpMemberCount':
						case 'cpMemberCount':
						case 'hccpMemberCount':
						case 'dyMemberCount':
						case 'djMemberCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxInsureNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'djValidNetRate':
						case 'cpValidNetRate':
						case 'hccpValidNetRate':
						case 'qpValidNetRate':
						case 'zrValidNetRate':
						case 'dxValueAddedTotalNetRate':
						case 'dyValidNetRate':
						case 'tyValidNetRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handlePercentage(
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'dxClubBetCount':
						case 'tyBetCount':
						case 'zrBetCount':
						case 'qpBetCount':
						case 'dyBetCount':
						case 'cpBetCount':
						case 'hccpBetCount':
						case 'djBetCount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property]}
										</span>
									</p>
								</div>
							)
							break
						case 'newMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary.newMemberNum || 0}
										</span>
									</p>
								</div>
							)
							break
						case 'newEffecactiveMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary.newEffecactiveMemberNum ||
												0}
										</span>
									</p>
								</div>
							)
							break
						case 'activeMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary.activeMemberNum || 0}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedAmountRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property]}%
										</span>
									</p>
								</div>
							)
							break
						case 'dxMemberPumpPercentageAmount':
						case 'dxProxyPumpPercentageAmountRebate':
						case 'dxProxyPumpPercentageAmountCommission':
						case 'dxCompanyPumpPercentageAmount':
						case 'dxMemberInsuredNetAmount':
						case 'dxProxyInsuredNetAmount':
						case 'dxCompanyInsuredNetAmount':
						case 'tyNetAmount':
						case 'zrNetAmount':
						case 'byqpNetAmount':
						case 'cpNetAmount':
						case 'hccpNetAmount':
						case 'djNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p
										style={this.handleNumberColor(
											totalSummary[column.property]
										)}
										class='footer_count_row  footer_count_row_border'
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
