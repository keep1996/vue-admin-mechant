import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '场馆信息',
			prop: 'venueName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: '场馆名称',
					prop: 'venueName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: true,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt',
					isAddColor: true
				},
				{
					label: '场馆项目',
					prop: 'venueTypeName',
					alignCenter: 'center',
					minWidth: '100',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt'
				}
			]
		},
		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true
		},
		{
			label: '盈亏数据',
			prop: 'venueName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isAddColor: true,
			children: [
				{
					label: i18n.t('report.member_count'),
					prop: 'memberCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_count'),
					prop: 'betCount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.bet_amount'),
					prop: 'betAmount',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '服务费贡献',
					prop: 'serviceChargeContribution',
					alignCenter: 'center',
					minWidth: '200',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.valid_bet_amount'),
					prop: 'validBetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注重算调整',
					prop: 'validBetAmountRecal',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际有效投注',
					prop: 'validBetAmountReal',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: i18n.t('report.net_amount'),
					prop: 'netAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},
				{
					label: '投注盈亏重算调整',
					prop: 'netAmountRecal',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '实际投注盈亏',
					prop: 'netAmountReal',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},

				{
					label: '实际有效盈利率',
					prop: 'netPercentage',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					soltColor: true,
					isSortColumn: true,
					itemTipContent:
						'实际有效盈利率=实际投注盈亏/实际有效投注*100%'
				}
			]
		}
	]
}

export const getColumnsNew = () => {
	return [
		{
			gameCode: 'DZ',
			gameName: 'DX德州',
			list: [
				{
					label: '投注人数',
					prop: 'dxClubMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'dxClubBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'dxClubBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'dxClubValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'dxNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					soltColor: true,
					isSortColumn: true
				},
				{
					label: '会员服务费贡献',
					prop: 'dxClubTotalServiceChargeContributionAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentageAmount',
					isSortColumn: true,
					soltColor: true,
					itemTipContent:
						'会员被收取的手牌服务费贡献+局服务费贡献之和'
				},
				{
					label: '代理服务费分成',
					prop: 'dxClubProxyServiceChargeRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				// {
				// 	label: '代理服务费分成(返佣)',
				// 	prop: 'dxProxyPumpPercentageAmountCommission',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'amountSolt',
				// 	isSortColumn: true,
				// 	soltColor: true
				// },
				{
					label: '平台服务费分成',
					prop: 'dxClubPlaformServiceChargeRebateAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '服务费盈利率',
					prop: 'serviceChargeNetRate',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true,
					itemTipContent: '平台服务费分成/俱乐部有效投注金额*100%'
				},
				{
					label: '会员保险盈亏',
					prop: 'dxInsureNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '代理保险盈亏',
					prop: 'dxInsureProxyRebateNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '平台保险盈亏',
					prop: 'dxInsurePlaformRebateNetAmount',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '保险盈利率',
					prop: 'dxInsureNetRate',
					alignCenter: 'center',
					minWidth: '150',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true,
					itemTipContent: '平台保险盈亏/保险有效投注金额*100%'
				},
				{
					label: '会员增值服务费消费',
					prop: 'dxValueAddedTotalAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '代理增值服务消费分成',
					prop: 'dxProxyValueAddedTotalRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '平台增值服务消费分成',
					prop: 'dxPlaformValueAddedTotalRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '增值服务利润率',
					prop: 'dxValueAddedTotalNetRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: true,
					soltColor: true,
					itemTipContent: '平台增值服务消费分成/增值服务纯利*100%'
				}
			]
		},
		{
			gameCode: 'ty',
			gameName: 'PM体育',
			list: [
				{
					label: '投注人数',
					prop: 'tyMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'tyBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'tyBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'tyValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'tyNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'tyValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'zr',
			gameName: 'PM真人',
			list: [
				{
					label: '投注人数',
					prop: 'zrMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'zrBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'zrBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'zrValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'zrNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'zrValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'byqp',
			gameName: '博雅棋牌',
			list: [
				{
					label: '投注人数',
					prop: 'qpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'qpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'qpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'qpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'qpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'qpValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'cp',
			gameName: 'PM彩票',
			list: [
				{
					label: '投注人数',
					prop: 'cpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'cpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'cpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'cpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'cpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'cpValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'hccp',
			gameName: 'HC彩票',
			list: [
				{
					label: '投注人数',
					prop: 'hccpMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'hccpBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'hccpBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'hccpValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'hccpNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'hccpValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'dj',
			gameName: 'PM电竞',
			list: [
				{
					label: '投注人数',
					prop: 'djMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'djBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'djBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'djValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'djNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'djValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		},
		{
			gameCode: 'dy',
			gameName: 'PM电子',
			list: [
				{
					label: '投注人数',
					prop: 'dyMemberCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '注单量',
					prop: 'dyBetCount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt',
					isSortColumn: true
				},
				{
					label: '投注金额',
					prop: 'dyBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '有效投注',
					prop: 'dyValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true
				},
				{
					label: '投注盈亏',
					prop: 'dyNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: true,
					soltColor: true
				},
				{
					label: '有效盈利率',
					prop: 'dyValidNetRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage',
					isSortColumn: false,
					soltColor: true
				}
			]
		}
	]
}
