import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('report.date'),
			prop: 'staticsDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('report.member_count'),
			prop: 'memberCount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: true
		},
		{
			label: i18n.t('report.bet_count'),
			prop: 'betCount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: true
		},
		{
			label: i18n.t('report.bet_amount'),
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: true
		},
		{
			label: '有效投注',
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: true
		},
		{
			label: i18n.t('report.rebate_amount'),
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: true
		},
		{
			label: '局服务费',
			prop: 'serviceChargeRound',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage',
			isSortColumn: true,
			itemTipContent: '统计会员被收取的局服务费'
		},
		{
			label: '增值服务消费',
			prop: 'valueAddedTotalAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage',
			isSortColumn: true,
			itemTipContent:
				'会员在德州场馆内因增值服务、商城、返利等功能引起的资金变动。'
		},
		{
			label: i18n.t('report.artificial_patch_amount'),
			prop: 'artificialPatchAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: true
		},
		{
			label: i18n.t('report.net_amount'),
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true,
			isSortColumn: true
		},
		{
			label: i18n.t('report.net_profit'),
			prop: 'netProfit',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage',
			soltColor: true,
			isSortColumn: true,
			itemTipContent: '投注盈亏+返水金额-局服务费-增值服务消费+其他调整'
		}
	]
}
