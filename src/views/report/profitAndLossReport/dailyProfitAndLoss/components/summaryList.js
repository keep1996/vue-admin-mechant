import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.betCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.betCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.betAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.betAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.betAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.validBetAmountVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.validBetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.validBetAmountVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case 'rebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedTotalAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.valueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'artificialPatchAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.artificialPatchAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.artificialPatchAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeRound':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.serviceChargeRound
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.serviceChargeRound
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label
											style={this.handleNumberColor(
												subSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</label>
										{/* <label
											style={this.handleNumberColor(
												subSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.netAmountTHB
											)}
										</label>
										<label
											style={this.handleNumberColor(
												subSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.netAmountVND
											)}
										</label> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label
											style={this.handleNumberColor(
												totalSummary.netAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</label>
										{/* <label
											style={this.handleNumberColor(
												totalSummary.netAmountTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.netAmountTHB
											)}
										</label>
										<label
											style={this.handleNumberColor(
												totalSummary.netAmountVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.netAmountVND
											)}
										</label> */}
									</p>
								</div>
							)
							break
						case 'netProfit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<label
											style={this.handleNumberColor(
												subSummary.netProfit
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netProfit
											)}
										</label>
										{/* <label
											style={this.handleNumberColor(
												subSummary.netProfitTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												subSummary.netProfitTHB
											)}
										</label>
										<label
											style={this.handleNumberColor(
												subSummary.netProfitVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												subSummary.netProfitVND
											)}
										</label> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<label
											style={this.handleNumberColor(
												totalSummary.netProfit
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netProfit
											)}
										</label>
										{/* <label
											style={this.handleNumberColor(
												totalSummary.netProfitTHB
											)}
										>
											{this.handleTotalNumber(
												'THB',
												totalSummary.netProfitTHB
											)}
										</label>
										<label
											style={this.handleNumberColor(
												totalSummary.netProfitVND
											)}
										>
											{this.handleTotalNumber(
												'VND',
												totalSummary.netProfitVND
											)}
										</label> */}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				`${this.$t('common.success_tip')}`,
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getReportDaynetamountExportExcel(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
