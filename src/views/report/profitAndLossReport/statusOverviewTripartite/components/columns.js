import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			prop: 'staticsDate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: true
		},
		{
			label: '新增',
			prop: 'newMemberNum',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: '有效新增',
			prop: 'validUserCount',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: '日活',
			prop: 'activeMemberNum',
			alignCenter: 'center',
			minWidth: '80',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt'
		},
		{
			label: '投注额',
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '有效投注额',
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '会员输赢',
			prop: 'netAmount',
			alignCenter: 'center',

			minWidth: '180',

			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '有效盈余比例',
			prop: 'netRate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '平台输赢',
			prop: 'platformNetAmount',
			alignCenter: 'center',
			minWidth: '180',
			link: true,
			isFixed: false,
			dialogName: 'netAmountDialog',
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '招商收益',
			prop: 'businessProfit',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '代理占成输赢',
			prop: 'commissionNetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		// {
		// 	label: '代理返佣输赢',
		// 	prop: 'fanyongNetAmount',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'profit'
		// },
		{
			label: '平台返水',
			prop: 'platformRebateAmount',
			alignCenter: 'center',
			link: true,
			dialogName: 'platformDialog',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '代理占成返水',
			prop: 'commissionRebateAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		// {
		// 	label: '代理返佣返水',
		// 	prop: 'fanyongRebateAmount',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'amountSolt'
		// },
		{
			label: '代理平均占成比例',
			prop: 'commissionRebate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '账户调整',
			prop: 'totalAcctadjustAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '佣金提款',
			prop: 'fanyongWithdrawAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '分红',
			prop: 'bonusAmount',
			alignCenter: 'center',
			link: true,
			dialogName: 'bonusAmountDialog',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			itemTipContent: '仅计算信用线总代和现金占成总代的分红，不计算现金返佣总代和现金返点总代的分红。'
		},
		{
			label: '增值服务纯利',
			prop: 'valueAddedNetProfitAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '增值服务平台分成',
			prop: 'valueAddedPlatformPercentageAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '增值服务代理分成',
			prop: 'valueAddedProxyPercentageAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '增值服务平均分成比例',
			prop: 'valueAddedAvgPercentageRate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '平台收入',
			prop: 'platformIncomeAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '台底',
			prop: 'platformBottomAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '平台收入(含台底)',
			prop: 'platformAllIncomeAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'profit'
		},
		{
			label: '操作',
			prop: 'operate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'actionSolt',
			permissionId: '90660082'
		}
	]
}
