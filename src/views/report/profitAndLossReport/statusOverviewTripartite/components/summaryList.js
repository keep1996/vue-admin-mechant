import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							{/* <p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p> */}
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'staticsDate':
							sums[index] = (
								<div>
									{/* <p class='footer_count_row'>
										<span>
											{subSummary[column.property] || '-'}
										</span>
									</p> */}
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property] ||
												'-'}
										</span>
									</p>
								</div>
							)
							break
						case 'newMemberNum':
							sums[index] = (
								<div>
									{/* <p class='footer_count_row'>
										<span>
											{subSummary[column.property] || '-'}
										</span>
									</p> */}
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary[column.property],
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validUserCount':
							sums[index] = (
								<div>
									{/* <p class='footer_count_row'>
										<span>
											{subSummary[column.property] || '-'}
										</span>
									</p> */}
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary[column.property],
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'activeMemberNum':
							sums[index] = (
								<div>
									{/* <p class='footer_count_row'>
										<span>
											{subSummary[column.property] || '-'}
										</span>
									</p> */}
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary[column.property],
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
						case 'platformNetAmount':
						case 'businessProfit':
						case 'commissionNetAmount':
						case 'fanyongNetAmount':
						case 'totalAcctadjustAmount':
						case 'platformIncomeAmount':
						case 'fanyongWithdrawAmount':
						case 'platformBottomAmount':
						case 'platformAllIncomeAmount':
						case 'commissionRebateAmount':
						// case 'fanyongRebateAmount':
						case 'platformRebateAmount':
						case 'bonusAmount':
						case 'valueAddedNetProfitAmount':
						case 'valueAddedPlatformPercentageAmount':
						case 'valueAddedProxyPercentageAmount':
						case 'betAmount':
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRate':
						case 'commissionRebate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handlePercentage((totalSummary[column.property] ||
												0))}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedAvgPercentageRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{(totalSummary[column.property] ||
												0) + '%'}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleRowPlatformBottom(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							{/* <p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p> */}
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary.netAmountSum
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary.betAmountSum
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netwinAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary.netwinAmountSum
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'platformBottomAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleCurrency('$')}
											{this.handleNumber(
												'$',
												totalSummary.platformBottomAmountSum
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netRate':
						case 'commissionRebate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handlePercentage((totalSummary[column.property] ||
												0))}
										</span>
									</p>
								</div>
							)
							break
						case 'valueAddedAvgPercentageRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{(totalSummary[column.property] ||
												0) + '%'}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
		// 导出
	}
}
