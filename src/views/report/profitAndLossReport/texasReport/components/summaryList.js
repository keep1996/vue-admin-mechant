import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'proxyPumpRatio':
						case 'proxyServiceChargeRoundContributionRate':
						case 'insureWinRatio':
						case 'platformValueAddedTotalAmountAvgRate':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
						case 'serviceChargeRoundContribution':
						case 'platformServiceChargeRoundContribution':
						case 'proxyServiceChargeRoundContribution':
						case 'adjustAmount':
						case 'platformIncomeAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
		// 导出
	}
}
