import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			id: 1,
			show: true,
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '德州投注总额',
			id: 2,
			show: true,
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		},
		{
			label: '德州有效投注',
			id: 3,
			show: true,
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		},
		{
			label: '手抽数据',
			show: true,
			id: 12,
			data: [
				{
					label: '手牌抽水总额',
					id: 1201,
					prop: 'pumpAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '手牌平台抽水',
					id: 1202,
					prop: 'platformPumpAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '手牌代理抽水',
					id: 1203,
					prop: 'proxyPumpAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '手牌代理抽水比例',
					id: 1204,
					prop: 'proxyPumpRatio',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				}
			]
		},

		{
			label: '局抽数据',
			show: true,
			id: 22,
			data: [
				{
					label: '局抽水总额',
					id: 2201,
					prop: 'serviceChargeRoundContribution',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					soltColor: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '局平台抽水',
					id: 2202,
					prop: 'platformServiceChargeRoundContribution',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					soltColor: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '局代理抽水',
					id: 2203,
					prop: 'proxyServiceChargeRoundContribution',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					soltColor: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '局代理抽水比例',
					id: 2204,
					prop: 'proxyServiceChargeRoundContributionRate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				}
			]
		},

		{
			label: '保险数据',
			show: true,
			id: 32,
			data: [
				{
					label: '保险平台投注',
					id: 3201,
					prop: 'insureAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '保险平台有效投注',
					id: 3202,
					prop: 'insureValidBetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '保险会员输赢',
					id: 3203,
					prop: 'insureMemberNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '保险平台输赢',
					id: 3204,
					prop: 'insurePlatformNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '保险代理占成输赢',
					id: 3205,
					prop: 'insureProxyProportion',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '有效保险盈余比例',
					id: 3206,
					prop: 'insureWinRatio',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				}
			]
		},

		{
			label: '增值数据',
			show: true,
			id: 42,
			data: [
				{
					label: '增值服务会员消费',
					id: 4201,
					prop: 'valueAddedTotalAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt',
					isSortColumn: false
				},
				{
					label: '增值服务平台分成',
					id: 4202,
					prop: 'platformValueAddedTotalAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '增值服务代理分成',
					id: 4203,
					prop: 'proxyValueAddedTotalAmount',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '增值服务平均占成比例',
					id: 4204,
					prop: 'platformValueAddedTotalAmountAvgRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				}
			]
		},

		{
			label: '德州账户调整',
			id: 4,
			show: true,
			prop: 'adjustAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			soltColor: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		},

		{
			label: '德州场馆平台收入',
			id: 5,
			show: true,
			prop: 'platformIncomeAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			soltColor: true,
			isShowTip: true,
			solt: 'amountSolt',
			isSortColumn: false
		}
	]
}
