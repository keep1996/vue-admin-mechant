import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: true
		},
		{
			label: '德州俱乐部投注额',
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '德州俱乐部有效投注',
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '德州抽水总额',
			prop: 'pumpAmount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '代理抽水总额',
			prop: 'proxyPumpAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '代理抽水比例',
			prop: 'proxyPumpRatio',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '保险投注额',
			prop: 'insureAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '保险有效投注额',
			prop: 'insureValidBetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '保险会员输赢',
			prop: 'insureMemberNetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '保险平台输赢',
			prop: 'insurePlatformNetAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '保险代理占成输赢',
			prop: 'insureProxyProportion',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '保险盈余比例',
			prop: 'insureWinRatio',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage'
		},
		{
			label: '德州账户调整',
			prop: 'adjustAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '德州场馆平台收入',
			prop: 'platformIncomeAmount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		}
	]
}
