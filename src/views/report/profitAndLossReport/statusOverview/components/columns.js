import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '用户数据',
			show: true,
			data: [
				{
					label: '日期',
					prop: 'reportDate',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'defaultSolt',
					isSortColumn: true
				},
				{
					label: '新增',
					prop: 'newMemberNum',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt'
				},
				{
					label: '有效新增',
					prop: 'newEffecactiveMemberNum',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt'
				},
				{
					label: '日活',
					prop: 'activeMemberNum',
					alignCenter: 'center',
					minWidth: '120',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'numberSolt'
				}
			]
		},
		{
			label: '平台运营数据',
			show: true,
			data: [
				{
					label: '德州保险',
					prop: 'dx21Amount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '德州服务费',
					prop: 'dx11Amount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '投注额',
					prop: 'betAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '有效投注额',
					prop: 'validBetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '平台输赢',
					prop: 'netAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '返点线输赢',
					prop: 'rebateNetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '占成线输赢',
					prop: 'commissionNetAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '盈利率',
					prop: 'netRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				}
			]
		},
		{
			label: '平台运营成本',
			show: true,
			data: [
				{
					label: '德州保险',
					prop: 'dx22Amount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '德州服务费',
					prop: 'dx12Amount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '综合场馆',
					prop: 'multiVenueAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '占成返点',
					prop: 'payoutRebateAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '团队返点',
					prop: 'payoutCommissionAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				},
				{
					label: '返点率',
					prop: 'rebateRate',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'percentage'
				},
				{
					label: '预估场馆费用',
					prop: 'preVenueAmount',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				}
			]
		},
		{
			label: '收入数据',
			show: true,
			data: [
				{
					label: '净利润',
					prop: 'netProfit',
					alignCenter: 'center',
					minWidth: '180',
					isFixed: false,
					isShow: true,
					isShowTip: true,
					solt: 'amountSolt'
				}
				// {
				// 	label: '净利润率',
				// 	prop: 'netProfitRate',
				// 	alignCenter: 'center',
				// 	minWidth: '180',
				// 	isFixed: false,
				// 	isShow: true,
				// 	isShowTip: true,
				// 	solt: 'percentage'
				// }
			]
		}
	]
}
