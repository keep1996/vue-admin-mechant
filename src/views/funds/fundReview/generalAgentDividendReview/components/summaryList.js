import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		handleRow2(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>总计</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'amountTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.amountTotal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetTotal
											)}
										</span>
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
