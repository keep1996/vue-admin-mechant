// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			prop: 'date',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'dateSolt',
			slotColor: true,
			isSortColumn: false
		},
        {
			label: '会员账号',
			prop: 'userName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			slotColor: true,
			isSortColumn: false
		},
        {
			label: '现金钱包',
            alignCenter: 'center',
			minWidth: '200',
            columns: [
                {
                    label: '现金余额',
                    prop: 'walletBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '冻结金额',
                    prop: 'freezeBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
            ]
		},
        {
			label: '信用钱包',
			minWidth: '200',
            alignCenter: 'center',
            columns: [
                {
                    label: '可用余额',
                    prop: 'creditBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '应还借款',
                    prop: 'shouldRepay',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '信用额度',
                    prop: 'creditQuota',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
            ]
		},
        {
			label: '个人净资产',
			prop: 'netAssetsNew',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slotColor: true,
			isSortColumn: false,
            tipContent: '个人净资产=现金钱包现金余额+现金钱包冻结金额-信用钱包应还借款'
		}
        
	]
}
