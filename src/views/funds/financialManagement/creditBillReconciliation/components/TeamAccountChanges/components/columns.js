
export const getAgentAccountColumnsPlus = () => {
	return [
		{
			label: '代理给会员提现下分',
			prop: 'proxyToMemCashDown',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理给下级提现下分',
			prop: 'proxyToChildCashDown',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理被上级充值上分',
			prop: 'proxyBeUp',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '官方给代理充值上分',
			prop: 'officerProxyCashUp',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理存款(客户端)',
			prop: 'proxyDeposit',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理存款(后台)',
			prop: 'proxyDepositBack',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '官方帮代理信用还款',
			prop: 'officialHelpProxyCreditRepay',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '上级帮代理信用还款',
			prop: 'parentProxyHelpProxyCreditRepay',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '财务增加调整',
			prop: 'proxyFinanceAdjustAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '运营增加调整',
			prop: 'proxyOperatorAdjustAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '三方掉单补分',
			prop: 'proxyThirdOrderLostCompensate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '线下结算上分',
			prop: 'proxyOfflineSettleUpScore',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		// {
		// 	label: '其他调整',
		// 	prop: 'proxyOtherAdjustAdd',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'numberSolt',
		// 	isSortColumn: false
		// },
		{
			label: '代理收益发放',
			prop: 'proxyProfitPayByPlatform',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		}, {
			label: '收入加额调整',
			prop: 'proxyProfitAdjustAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '汇总',
			prop: 'totalProxyWalletAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false,
			noLink: true
		}

	]
}

export const getAgentAccountColumnsReduce = () => {
	return [
		{
			label: '代理给会员充值上分',
			prop: 'proxyToMemCashUp',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理给下级充值上分',
			prop: 'proxyToChildCashUp',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理被上级提现下分',
			prop: 'proxyBeDown',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '官方给代理提现下分',
			prop: 'officerProxyCashDown',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理取款(客户端)',
			prop: 'proxyWithdraw',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理取款(后台)',
			prop: 'proxyWithdrawBack',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理帮会员信用还款',
			prop: 'proxyHelpSubMemberCreditRepay',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '代理帮下级信用还款',
			prop: 'proxyHelpSubProxyCreditRepay',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '财务扣除调整',
			prop: 'proxyFinanceAdjustSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '运营扣除调整',
			prop: 'proxyOperatorAdjustSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '违规扣款',
			prop: 'proxyViolationFine',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '占成亏损分摊',
			prop: 'proxyLossSharingSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		// {
		// 	label: '其他调整',
		// 	prop: 'proxyOtherAdjustSub',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'numberSolt',
		// 	isSortColumn: false
		// },
		{
			label: '代理收益亏损',
			prop: 'proxyProfitLoss',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		}, {
			label: '收入扣除调整',
			prop: 'proxyProfitAdjustSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '汇总',
			prop: 'totalProxyWalletSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false,
			noLink: true
		}

	]
}

export const getMemberAccountColumnsPlus = () => {
	return [
		{
			label: '会员充值',
			prop: 'memberDeposit',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '会员存款(后台)',
			prop: 'memberDepositBack',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '财务增加调整',
			prop: 'memberFinanceAdjustAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '运营增加调整',
			prop: 'memberOperatorAdjustAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '三方掉单补分',
			prop: 'memberThirdOrderLostCompensate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '线下结算上分',
			prop: 'memberOfflineSettleUpScore',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		// {
		// 	label: '其他调整',
		// 	prop: 'memberOtherAdjustAdd',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'numberSolt',
		// 	isSortColumn: false
		// },
		{
			label: '真人撤单',
			prop: 'zrChedan',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人派彩增加',
			prop: 'zrPaicaiAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人活动增加',
			prop: 'zrActivityAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人费用回滚',
			prop: 'zrFeeRollback',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育拒单',
			prop: 'tyJudan',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育结算派彩',
			prop: 'tyPaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育注单取消',
			prop: 'tyChedan',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		}, {
			label: '体育提前结算取消回滚',
			prop: 'tyPaijiangAdvanceQuxiaoCancel',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育人工加款',
			prop: 'tyArtificialAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育用户预约下注取消',
			prop: 'tyAppointmentTouzhuCancel',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育提前部分结算',
			prop: 'tyPaijiangAdvancePart',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育提前全额结算',
			prop: 'tyPaijiangAdvanceAll',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '彩票撤单',
			prop: 'cpChedan',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '彩票派奖',
			prop: 'cpPaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '彩票二次派奖',
			prop: 'cpErcipaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电竞派彩',
			prop: 'djPaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电竞其他加款',
			prop: 'djOtherAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '棋牌结算',
			prop: 'qpSettlement',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电子取消投注',
			prop: 'dzChedan',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电子派彩',
			prop: 'dzPaicai',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电子活动奖金',
			prop: 'dzActivityReward',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '带出牌桌',
			prop: 'bringOutTable',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '帮信用还款',
			prop: 'proxyHelpCreditRepay',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '会员返水',
			prop: 'rebate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '汇总',
			prop: 'totalMemberWalletAdd',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false,
			noLink: true
		}
	]
}

export const getMemberAccountColumnsReduce = () => {
	return [
		{
			label: '会员提现',
			prop: 'memberWithdraw',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '会员取款(后台)',
			prop: 'memberWithdrawBack',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '财务扣除调整',
			prop: 'memberFinanceAdjustSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '运营扣除调整',
			prop: 'memberOperatorAdjustSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '违规扣款',
			prop: 'memberViolationFine',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		// {
		// 	label: '占成亏损分摊',
		// 	prop: 'proxyLossSharingSub',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'numberSolt',
		// 	isSortColumn: false
		// },
		// {
		// 	label: '其他调整',
		// 	prop: 'memberOtherAdjustSub',
		// 	alignCenter: 'center',
		// 	minWidth: '180',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'numberSolt',
		// 	isSortColumn: false
		// },
		{
			label: '真人投注',
			prop: 'zrTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人派彩减少',
			prop: 'zrPaicaiSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人活动减少',
			prop: 'zrActivitySub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '真人打赏',
			prop: 'zrDashang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育投注',
			prop: 'tyTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育结算回滚',
			prop: 'tyPaijiangCancel',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育注单取消回滚',
			prop: 'tyChedanCancel',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育提前结算取消',
			prop: 'tyPaijiangAdvanceQuxiao',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育人工扣款',
			prop: 'tyArtificialSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '体育用户预约下注',
			prop: 'tyAppointmentTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '彩票投注',
			prop: 'cpTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '彩票撤回派奖',
			prop: 'cpChehuipaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电竞撤回派彩',
			prop: 'djChehuipaijiang',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电竞其他扣款',
			prop: 'djOtherSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电竞投注',
			prop: 'djTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '棋牌投注',
			prop: 'qpTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '电子投注',
			prop: 'dzTouzhu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '带入牌桌',
			prop: 'bringToTable',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '局服务费',
			prop: 'tableServeFee',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '发弹幕',
			prop: 'texasZengzhiFatangmu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '发表情',
			prop: 'texasZengzhiFabiaoqing',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '发短语',
			prop: 'texasZengzhiFaduanyu',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '发语音',
			prop: 'texasZengzhiFayuyin',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '互动道具',
			prop: 'texasZengzhiHudongdaoju',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '看公牌',
			prop: 'texasZengzhiKangongpai',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '保险行动延时',
			prop: 'texasZengzhiBaoxianxingdongyanshi',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '下注行动延时',
			prop: 'texasZengzhiXiazhuxingdongyanshi',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '汇总',
			prop: 'totalMemberWalletSub',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false,
			noLink: true
		}
	]
}
