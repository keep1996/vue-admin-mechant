// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			slot: 'serialNumber',
			minWidth: '60',
			label: '#',
			alignCenter: 'center'
		},
		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理层级',
			prop: 'proxyLevel',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属商户',
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '直属上级',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账号类型',
			prop: 'accountType',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'accountTypeSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '周期应发返点',
			prop: 'shouldRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slottColor: true,
			isSortColumn: false
		},
		{
			label: '对账结果',
			prop: 'matchResult',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'matchResultSolot',
			slottColor: true,
			isSortColumn: false
		},
		{
			label: '团队周期实发返点',
			prop: 'realRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slotColor: true,
			isSortColumn: false
		},
		{
			label: '团队周期内未审核拒绝返点',
			prop: 'invalidRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slotColor: true,
			isSortColumn: false
		},
		{
			label: '对账时间',
			prop: 'createdTime',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			slotColor: true,
			isSortColumn: false
		}
	]
}
