
export const getColumns = () => {
	return [
		{
			slot: 'serialNumber',
			minWidth: '60',
			label: '#',
			alignCenter: 'center'
		},
		{
			label: '订单号',
			prop: 'orderNo',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			isSortColumn: false
		},
		{
			label: '结算周期',
			prop: 'payoutTime',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理账号',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '本期收益',
			prop: 'incomeAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '状态',
			prop: 'orderStatus',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'orderStatusSlot',
			soltColor: true,
			isSortColumn: false
		}
	]
}
