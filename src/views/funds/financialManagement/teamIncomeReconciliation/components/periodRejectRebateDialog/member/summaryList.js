import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'incomeAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'USDT',
												subSummary[column.property] ,
												2,
												true,
												true
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'USDT',
												totalSummary[column.property] ,
												2,
												true,
												true
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											-
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											-
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
		// 导出
	}
}
