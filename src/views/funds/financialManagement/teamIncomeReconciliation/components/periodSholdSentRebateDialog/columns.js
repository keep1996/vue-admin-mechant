
export const columns1 = [
		{
			label: '项目',
			prop: 'name',
			alignCenter: 'center',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '服务费贡献',
			prop: 'serviceContribution',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '实际应发分成金额',
			prop: 'clubRebateAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '查看',
			prop: 'viewDetail1',
			alignCenter: 'center',
			width: '140',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'viewDetail',
			isSortColumn: false
		}
	]

export const getColumns2 = () => {
	return [
		{
			label: '项目',
			prop: 'name',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '会员游戏输赢',
			prop: 'dxInsureNetAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '团队分成比例',
			prop: 'dxInsureRate',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage',
			isSortColumn: false
		},
		{
			label: '团队分成金额',
			prop: 'teamDxInsureRebateAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		}
	]
}

export const getColumns3 = () => {
	return [
		{
			label: '项目',
			prop: 'name',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '增值服务纯利',
			prop: 'valueAddedTotalAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '团队分成比例',
			prop: 'valueAddedRate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'percentage',
			isSortColumn: false
		},
		{
			label: '团队分成金额',
			prop: 'teamValueAddedRebateAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		}
	]
}

export const getColumns4 = () => {
	return [
		{
			label: '项目',
			prop: 'name',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '有效投注',
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '实际应发返点金额',
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '查看',
			prop: 'viewDetail2',
			alignCenter: 'center',
			width: '140',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'viewDetail',
			isSortColumn: false
		}
	]
}
