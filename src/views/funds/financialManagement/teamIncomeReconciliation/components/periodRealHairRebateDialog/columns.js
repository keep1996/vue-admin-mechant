
export const getColumns = () => {
	return [
		{
			slot: 'serialNumber',
			minWidth: '60',
			label: '#',
			alignCenter: 'center'
		},
		{
			label: '关联订单号',
			prop: 'orderNo',
			alignCenter: 'center',
			minWidth: '300',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账号',
			prop: 'userName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账号类型',
			prop: 'userType',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'userTypeSlot',
			isSortColumn: false
		},
		{
			label: '业务类型',
			prop: 'bizTypeName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账变类型',
			prop: 'changeTypeName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '收支类型',
			prop: 'transTypeName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变前余额',
			prop: 'changeBefore',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变金额',
			prop: 'amount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变后余额',
			prop: 'changeAfter',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变时间',
			prop: 'eventTime',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		}
	]
}
