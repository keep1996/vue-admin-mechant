
export const getColumns = () => {
	return [
		{
			label: '注单号',
			prop: 'betOrderId',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'betOrderIdSlot'
		},
		{
			label: '三方注单号',
			prop: 'generatedId',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '场馆类型',
			prop: 'venueTypeName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '场馆名称',
			prop: 'venueName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '游戏名称',
			prop: 'gameTypeName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '会员账号',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '游戏账号',
			prop: 'playerName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账号类型',
			prop: 'accountType',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'accountTypeSlot',
			isSortColumn: false
		},
		{
			label: '业务模式',
			prop: 'businessModel',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'businessModelSlot',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '所属商户',
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '上级代理',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '投注金额',
			prop: 'betAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '注单类型',
			prop: 'betType',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'betTypeSlot',
			isSortColumn: false
		},
		{
			label: '有效投注',
			prop: 'validBetAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'validBetAmountSlot',
			isSortColumn: false
		},
		{
			label: '手牌服务费',
			prop: 'userHandFee',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '会员输赢',
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '注单状态',
			prop: 'obBetStatus',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'obBetStatusSlot',
			isSortColumn: false
		},
		{
			label: '是否重算',
			prop: 'obSettleCount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'obSettleCountSlot',
			isSortColumn: false
		},
		{
			label: '投注时间',
			prop: 'createAtString',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '结算时间',
			prop: 'netAtString',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '赛事名称',
			prop: 'matchInfo',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '投注详情',
			prop: 'betContent',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'betContentSlot',
			isSortColumn: false
		},
		{
			label: '投注ip',
			prop: 'loginIp',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: 'ip归属地',
			prop: 'ipAttribution',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '投注终端',
			prop: 'obDeviceType',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'obDeviceTypeSlot',
			isSortColumn: false
		},
		{
			label: '账变订单号',
			prop: 'eventIds',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'eventIdsSolt',
			isSortColumn: false
		},
		// {
		// 	label: '会员账号',
		// 	prop: 'memberName',
		// 	alignCenter: 'center',
		// 	minWidth: '100',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'defaultSolt',
		// 	isSortColumn: false
		// },
		{
			label: '账变金额',
			prop: 'billAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '对账结果',
			prop: 'status',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'statusSlot',
			isSortColumn: false
		},
		{
			label: '对账时间',
			prop: 'billCreateAt',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '操作',
			prop: 'rebateAmountDj',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'operate',
			soltColor: true,
			isSortColumn: false
		}
	]
}

export const getDialogTableColumns = () => {
	return [
		{
			label: '账变订单号',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '会员账号',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属商户',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '总代账号',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '上级代理',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '风控层级',
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '业务模式',
			prop: 'adjustmentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账号状态',
			prop: 'lastPeriodShouldReceiptPaymentParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '钱包类型',
			prop: 'lastPeriodShouldReceiptPaymentParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '业务类型',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变类型',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '收支类型',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账变前余额',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '账变金额',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSlot',
			isSortColumn: false
		},
		{
			label: '账变后余额',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			isSortColumn: false
		},
		{
			label: '账变时间',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '备注',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		}
	]
}

