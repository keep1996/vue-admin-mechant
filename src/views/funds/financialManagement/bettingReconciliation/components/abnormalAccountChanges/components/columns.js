// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '关联订单号',
			prop: 'eventId',
			alignCenter: 'center',
			minWidth: '300',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理账号',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属商户',
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '风控层级',
			prop: 'windControlName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '账号状态',
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'accountStatusSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '代理钱包',
			prop: 'walletTypeStr',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '业务类型',
			prop: 'bizTypeStr',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变类型',
			prop: 'changeTypeStr',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '收支类型',
			prop: 'transTypeStr',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变前余额',
			prop: 'changeBefore',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变金额',
			prop: 'amount',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变后余额',
			prop: 'changeAfter',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'noColorNumberSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '账变时间',
			prop: 'createdAt',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '对账结果',
			prop: 'billStatus',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'billStatusSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '备注',
			prop: 'remark',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '对账时间',
			prop: 'billTime',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		}
	]
}
