// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '日期',
			prop: 'date',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'dateSolt',
			slotColor: true,
			isSortColumn: false
		},
        {
			label: '代理账号',
			prop: 'userName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'defaultSolt',
			slotColor: true,
			isSortColumn: false
		},
        {
			label: '现金钱包',
            alignCenter: 'center',
			minWidth: '200',
            columns: [
                {
                    label: '现金余额',
                    prop: 'walletBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '冻结金额',
                    prop: 'freezeBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
               
            ]
		},
        {
			label: '信用钱包',
			minWidth: '200',
            alignCenter: 'center',
            columns: [
                {
                    label: '可用余额',
                    prop: 'creditBalance',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '应还借款',
                    prop: 'shouldRepay',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '外放额度',
                    prop: 'outsideAmount',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
                {
                    label: '授信总额',
                    prop: 'teamCreditQuota',
                    alignCenter: 'center',
                    minWidth: '250',
                    isFixed: false,
                    isShow: true,
                    isShowTip: true,
                    slot: 'numberSolt',
                    slotColor: true,
                    isSortColumn: false
                },
            ]
		},
        {
			label: '个人净资产',
			prop: 'netAssetsNew',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slotColor: true,
			isSortColumn: false,
            tipContent: '个人净资产=现金钱包现金余额+现金钱包冻结金额-信用钱包应还借款'
		},
        {
			label: '团队净资产',
			prop: 'teamFundsNew',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			slot: 'numberSolt',
			slotColor: true,
			isSortColumn: false,
            tipContent: 'SUM[(该团队所有代理的净资产)+(该团队所有会员的净资产)]'
		}
        
	]
}
