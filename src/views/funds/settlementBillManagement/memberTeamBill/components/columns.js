// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '账单周期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '300',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSoltTime'
		},
		{
			label: '更新时间',
			prop: 'updatedAt',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'updatedAtSolt',
			isSortColumn: false
		},
		{
			label: '会员账号',
			prop: 'memberName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '上级代理',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属总代',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '本期盈亏',
			prop: 'netAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '汇总德州+体育+真人+电竞+棋牌+彩票+电子+其他八个项目的盈亏之和'
		},
		{
			label: '其他资金变动',
			prop: 'adjustmentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '汇总上级给会员的充值、提现、还款的总额; 公式：上级给会员充值+上级给会员提现+上级帮会员还款'
		},
		{
			label: '上期应收上级',
			prop: 'lastPeriodShouldReceiptPaymentParentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneAddLast',
			soltColor: true,
			isSortColumn: false,
		},
		{
			label: '上期应付上级',
			prop: 'lastPeriodShouldReceiptPaymentParentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneReduceLast',
			soltColor: true,
			isSortColumn: false,
		},
		{
			label: '本期应付上级',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneReduce',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '本期盈亏+其他资金变动+上期应收上级+上期应付上级<0的金额'
		},
		{
			label: '本期应收上级',
			prop: 'shouldPaymentReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneAdd',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '本期盈亏+其他资金变动+上期应收上级+上期应付上级>0的金额'
		},
		{
			label: '操作',
			prop: 'rebateAmountDj',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'operate',
			soltColor: true,
			isSortColumn: false
		}
	]
}
