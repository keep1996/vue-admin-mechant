// import i18n from '@/locales'

// shouldPaymentChildAmount	应付下级
// shouldReceiptChildAmount	应收下级
// shouldReceiptPaymentParentAmount	应收付上级   正值应收 负值应付
// personShouldReceiptPaymentAmount	本期个人应收付  正值应收 负值应付
export const getColumns = () => {
	return [
		{
			label: '账单周期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '400',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSoltTime'
		},

		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理层级',
			prop: 'proxyLevelName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'proxyLevelSolt',
			isSortColumn: false
		},
		{
			label: '直属上级',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属总代',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},

		{
			label: '团队盈亏',
			prop: 'teamNetAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountAbsSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '下级团队盈亏',
			prop: 'childTeamNetAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountAbsSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '个人盈亏',
			prop: 'personNetAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountAbsSolt',
			soltColor: true,
			isSortColumn: false
		},
		{
			label: '更新时间',
			prop: 'updatedAt',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '操作',
			prop: 'operate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: 'right',
			isShow: true,
			isShowTip: true,
			solt: 'operate',
			soltColor: true,
			isSortColumn: false
		}
	]
}

export const getDetail = () => {
	return [
		{
			title: '真人',
			top: [
				{
					label: '场馆盈亏',
					prop: 'zrVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'zrNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'zrZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'zrNetAmount'
				},
				{
					label: '输赢重算调整',
					prop: 'zrNetRecalculationAdjustmentAmount'
				},
				{
					label: '有效投注返点',
					prop: 'zrRebateAmount'
				},
				{
					label: '返点重算调整',
					prop: 'zrRebateRecalculationAdjustmentAmount'
				}
			]
		},
		{
			title: '体育',
			top: [
				{
					label: '场馆盈亏',
					prop: 'tyVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'tyNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'tyZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'tyNetAmount'
				},
				{
					label: '输赢重算调整',
					prop: 'tyNetRecalculationAdjustmentAmount'
				},
				{
					label: '有效投注返点',
					prop: 'tyRebateAmount'
				},
				{
					label: '返点重算调整',
					prop: 'tyRebateRecalculationAdjustmentAmount'
				}
			]
		},
		{
			title: '电竞',
			top: [
				{
					label: '场馆盈亏',
					prop: 'djVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'djNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'djZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'djNetAmount'
				},
				{
					label: '输赢重算调整',
					prop: 'djNetRecalculationAdjustmentAmount'
				},
				{
					label: '有效投注返点',
					prop: 'djRebateAmount'
				},
				{
					label: '返点重算调整',
					prop: 'djRebateRecalculationAdjustmentAmount'
				}
			]
		},
		{
			title: '棋牌',
			top: [
				{
					label: '场馆盈亏',
					prop: 'qpVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'qpNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'qpZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'qpNetAmount'
				},
				// {
				// 	label: '输赢重算调整',
				// 	prop: 'qpNetRecalculationAdjustmentAmount'
				// },
				{
					label: '有效投注返点',
					prop: 'qpRebateAmount'
				}
				// ,
				// {
				// 	label: '返点重算调整',
				// 	prop: 'qpRebateRecalculationAdjustmentAmount'
				// }
			]
		},
		{
			title: '彩票',
			top: [
				{
					label: '场馆盈亏',
					prop: 'cpVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'cpNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'cpZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'cpNetAmount'
				},
				{
					label: '输赢重算调整',
					prop: 'cpNetRecalculationAdjustmentAmount'
				},
				{
					label: '有效投注返点',
					prop: 'cpRebateAmount'
				},
				{
					label: '返点重算调整',
					prop: 'cpRebateRecalculationAdjustmentAmount'
				}
			]
		},
		{
			title: '电子',
			top: [
				{
					label: '场馆盈亏',
					prop: 'dyVenueNetAmount',
					showTips: true
				},
				{
					label: '代理净盈亏',
					prop: 'dyNetProfitAmount'
				},
				{
					label: '代理占成比例',
					prop: 'dyZhanchengRate'
				}
			],
			bottom: [
				{
					label: '会员游戏输赢',
					prop: 'dyNetAmount'
				},

				{
					label: '有效投注返点',
					prop: 'dyRebateAmount'
				}
			]
		}
	]
}
