import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'shouldReceiptChildAmount':
						case 'shouldPaymentChildAmount':
						case 'shouldReceiptParentAmount':
						case 'shouldPaymentParentAmount':
						case 'personShouldReceiptAmount':
						case 'personShouldPaymentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
												totalSummary[column.property]
											)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						// case 'shouldReceiptPaymentParentAmount':
						// 	sums[index] = (
						// 		<div>
						// 			<p class='footer_count_row  footer_count_row_border'>
						// 				<span
						// 					style={totalSummary.personShouldPaymentAmount > 0 ? this.handleNumberColor(
						// 						totalSummary.personShouldPaymentAmount
						// 					) : ''}
						// 				>
						// 					{totalSummary.personShouldPaymentAmount > 0 ? this.handleTotalNumber(
						// 						'USDT',
						// 						totalSummary.personShouldPaymentAmount
						// 					) : '$0.00'}
						// 				</span>
						// 			</p>
						// 		</div>
						// 	)
						// 	break
						// case 'shouldReceiptPaymentParentAmount':
						// 	sums[index] = (
						// 		<div>
						// 			<p class='footer_count_row  footer_count_row_border'>
						// 			<span
						// 					style={totalSummary.personShouldPaymentAmount < 0 ? this.handleNumberColor(
						// 						totalSummary.personShouldPaymentAmount
						// 					) : ''}
						// 				>
						// 					{totalSummary.personShouldPaymentAmount < 0 ? this.handleTotalNumber(
						// 						'USDT',
						// 						totalSummary.personShouldPaymentAmount
						// 					) : '$0.00'}
						// 				</span>
						// 			</p>
						// 		</div>
						// 	)
						// 	break
						// case 'personShouldPaymentAmount':
						// 	sums[index] = (
						// 		<div>
						// 			<p class='footer_count_row  footer_count_row_border'>
						// 			<span
						// 					style={totalSummary.personShouldPaymentAmount < 0 ? this.handleNumberColor(
						// 						totalSummary.personShouldPaymentAmount
						// 					) : ''}
						// 				>
						// 					{totalSummary.personShouldPaymentAmount < 0 ? this.handleTotalNumber(
						// 						'USDT',
						// 						totalSummary.personShouldPaymentAmount
						// 					) : '$0.00'}
						// 				</span>
						// 			</p>
						// 		</div>
						// 	)
						// 	break
						// case 'personShouldReceiptAmount':
						// 	sums[index] = (
						// 		<div>
						// 			<p class='footer_count_row  footer_count_row_border'>
						// 			<span
						// 					style={totalSummary.personShouldReceiptAmount > 0 ? this.handleNumberColor(
						// 						totalSummary.personShouldReceiptAmount
						// 					) : ''}
						// 				>
						// 					{totalSummary.personShouldReceiptAmount > 0 ? this.handleTotalNumber(
						// 						'USDT',
						// 						totalSummary.personShouldReceiptAmount
						// 					) : '$0.00'}
						// 				</span>
						// 			</p>
						// 		</div>
						// 	)
						// 	break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>-</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
		// 导出
	}
}
