// import i18n from '@/locales'

// shouldPaymentChildAmount	应付下级		
// shouldReceiptChildAmount	应收下级	
// shouldReceiptPaymentParentAmount	应收付上级   正值应收 负值应付
// personShouldReceiptPaymentAmount	本期个人应收付  正值应收 负值应付
export const getColumns = () => {
	return [
		{
			label: '账单周期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '300',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSoltTime'
		},
		{
			label: '更新时间',
			prop: 'updatedAt',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '代理层级',
			prop: 'proxyLevelName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'proxyLevelSolt',
			isSortColumn: false
		},
		{
			label: '直属上级',
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '所属总代',
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '应收上级',
			prop: 'shouldReceiptParentAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneAddLast',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '本期团队盈亏+其他资金变动+上期应收上级-上期应付上级>0的金额'
		},
		{
			label: '应付上级',
			prop: 'shouldPaymentParentAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneReduceLast',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '本期团队盈亏+其他资金变动+上期应收上级-上期应付上级<0的金额'
		},
		{
			label: '应收下级',
			prop: 'shouldReceiptChildAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountAbsSolt',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '汇总当期应收下级的总额'
		},
		{
			label: '应付下级',
			prop: 'shouldPaymentChildAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountAbsSolt',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '汇总当期应付下级的总额'
		},
		{
			label: '本期个人应付',
			prop: 'personShouldPaymentAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneReduce',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '当期应收总额减去应付总额的差值；差值为负数时表示亏损; 公式：应收上级+应收下级-应付上级-应付下级'
		},
		{
			label: '本期个人应收',
			prop: 'personShouldReceiptAmount',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'aloneAdd',
			soltColor: true,
			isSortColumn: false,
			itemTipContent: '当期应收总额减去应付总额的差值；差值为正数时表示盈利; 公式：应收上级+应收下级-应付上级-应付下级'
		},
		{
			label: '操作',
			prop: 'operate',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'operate',
			soltColor: true,
			isSortColumn: false
		}
	]
}
