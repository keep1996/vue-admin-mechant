export const rules = (that) => {
	const periodDayOfweek = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.bill_related_set_tip1'),
			trigger: 'change'
		}
	]
	const periodHour = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.bill_related_set_tip2'),
			trigger: 'change'
		}
	]
	const withdrawDailyMaxCount = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.bill_related_set_tip3'),
			trigger: 'blur'
		}
	]
	const withdrawDailyMaxAmount = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.bill_related_set_tip4'),
			trigger: 'blur'
		}
	]
	return {
		periodDayOfweek,
		periodHour,
		withdrawDailyMaxCount,
		withdrawDailyMaxAmount
	}
}
