import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t(
							'funds.settlement_bill_management.team_assets'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.subTeamFunds
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTeamFunds
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												subSummary.totalTeamFunds
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTeamFunds
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.team_payable_loan'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTeamReturnLoan
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTeamReturnLoan
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.team_withdrawable_limit'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTeamWithdrawable
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTeamWithdrawable
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.texas_insurance_team_share'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subInsuranceRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalInsuranceRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.texas_insurance_added'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subValueAddedTotalAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalValueAddedTotalAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.texas_club_service_charge_team_rebate'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subClubRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalClubRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.general_rebate'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMultipleRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMultipleRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.general_divide'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMultipleRebateAmountRate
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMultipleRebateAmountRate
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.general_share'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTeamRebateSharing
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTeamRebateSharing
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.rebate_income'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTeamIncomeAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTeamIncomeAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.bill_net_assets'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.subNetAssets
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subNetAssets
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												subSummary.totalNetAssets
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalNetAssets
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.payable_bill_loan'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subReturnLoan
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalReturnLoan
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.withdrawable_limit'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subWithdrawable
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalWithdrawable
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t('report.center_wallet_balance'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subWalletBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalWalletBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.credit_wallet_balance'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subCreditBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalCreditBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.should_repay'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subShouldRepay
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalShouldRepay
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.outside_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subOutsideAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalOutsideAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.total_credit_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTotalCreditAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTotalCreditAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t('funds.fund_audit.credit_limit'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subCreditQuota
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalCreditQuota
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.person_rebate_income'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.subAgencyPersonalIncome
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subAgencyPersonalIncome
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												subSummary.totalAgencyPersonalIncome
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalAgencyPersonalIncome
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.total_withdraw_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subTotalWithdrawAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalTotalWithdrawAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.bet_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subValidBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalValidBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.bet_count'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.subBetCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												subSummary.totalBetCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.net_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.subNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												subSummary.totalNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.member_rebate_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMemberRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMemberRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.member_deposit_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMemberDepositAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMemberDepositAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.member_withdraw_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMemberWithdrawAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMemberWithdrawAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.member_loan'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMemberLoan
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMemberLoan
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.settlement_bill_management.member_repayment'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.subMemberRepayment
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalMemberRepayment
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
