import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('funds.order_no'),
			prop: 'orderNo',
			alignCenter: 'center',
			minWidth: '280',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: i18n.t('funds.proxy_member_funds_record.rebate_time'),
			prop: 'cycleEndDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('funds.proxy_member_funds_record.client_state'),
			prop: 'payoutStatus',
			alignCenter: 'center',
			minWidth: '140',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'filterType'
		},
		{
			label: i18n.t('funds.proxy_name'),
			prop: 'proxyAccount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: i18n.t('funds.fund_audit.top_proxy_name'),
			prop: 'topProxyAccount',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: i18n.t('common.belong_merchant'),
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('funds.proxy_member_funds_record.proxy_level_name'),
			prop: 'proxyLevelName',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t(
				'funds.proxy_member_funds_record.parent_proxy_account'
			),
			prop: 'parentProxyAccount',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: i18n.t('common.account_status'),
			prop: 'accountLockStatus',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'accountStatusSolt'
		},
		{
			label: i18n.t('funds.wind_control_level'),
			prop: 'windControlName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t(
				'funds.proxy_member_funds_record.person_rebate_amount'
			),
			prop: 'personRebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t(
				'funds.proxy_member_funds_record.child_rebate_amount'
			),
			prop: 'childRebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: i18n.t(
				'funds.proxy_member_funds_record.rebate_amount_point'
			),
			prop: 'rebateAmount',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		// {
		// 	label: i18n.t('report.rebate_adjustment'),
		// 	prop: 'adjustOfRebate',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShowTip: true,
		// 	solt: 'amountSolt',
		// 	soltColor: true
		// },
		{
			label: i18n.t('funds.proxy_member_funds_record.issue_time'),
			prop: 'payoutTime',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('common.remark'),
			prop: 'audit1Desc',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'auditSolt'
		}
		// {
		// 	label: i18n.t('common.actions'),
		// 	prop: 'auditStep',
		// 	alignCenter: 'center',
		// 	minWidth: '150',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'actionSolt'
		// }
	]
}
