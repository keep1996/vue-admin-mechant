export const tableColumns = {
	cycleStartEnd: '结算周期',
	payoutStatus: '客户端状态',
	topProxyName: '总代账号',
	merchantName: '所属商户',
	accountStatus: '账号状态',
	windControlName: '风控层级',
	otherMemberNetAmount: '其他场馆会员游戏输赢',
	otherMemberRebateAmount: '其他场馆会员总返水',
	otherMemberAccountAdjustAmount: '会员账户调整',
	otherProxyNetAmount: '其他场馆代理净盈亏',
	otherCommissionRate: '其他场馆佣金比例',
	otherPartsAmount: '其他场馆佣金',
	clubPartsAmount: '德州俱乐部服务费分成',
	insurePartsAmount: '德州俱乐部保险分成',
	clubValueAddedPartsAmount: '德州增值服务消费分成',
	dividendAmount: '分红',
	lastMonthBalance: '上月结余',
	curMonthBalance: '本月结余',
	commissionAmount: '综合佣金',
	payoutTime: '发放时间',
	auditDesc: '备注'
}

export const activityListField = {
	[tableColumns.cycleStartEnd]: true,
	[tableColumns.payoutStatus]: true,
	[tableColumns.topProxyName]: true,
	[tableColumns.merchantName]: true,
	[tableColumns.accountStatus]: true,
	[tableColumns.windControlName]: true,
	[tableColumns.otherMemberNetAmount]: true,
	[tableColumns.otherMemberRebateAmount]: true,
	[tableColumns.otherMemberAccountAdjustAmount]: true,
	[tableColumns.otherProxyNetAmount]: true,
	[tableColumns.otherCommissionRate]: true,
	[tableColumns.otherPartsAmount]: true,
	[tableColumns.clubPartsAmount]: true,
	[tableColumns.insurePartsAmount]: true,
	[tableColumns.clubValueAddedPartsAmount]: true,
	[tableColumns.dividendAmount]: true,
	[tableColumns.lastMonthBalance]: true,
	[tableColumns.curMonthBalance]: true,
	[tableColumns.commissionAmount]: true,
	[tableColumns.payoutTime]: true,
	[tableColumns.auditDesc]: true

}

// 后端接口只返回1 3 5 6
export const payoutStatusArr = [
	{
		code: 0,
		description: '未结算'
	},
	{
		code: 1,
		description: '待发放'
	},
	{
		code: 2,
		description: '待领取'
	},
	{
		code: 3,
		description: '已发放'
	},
	{
		code: 4,
		description: '超时未领取'
	},
	{
		code: 5,
		description: '无佣金'
	},
	{
		code: 6,
		description: '已取消'
	}
]
