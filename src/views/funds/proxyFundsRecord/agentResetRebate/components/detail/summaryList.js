import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								总计
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case '体育团队返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.teamRebateAmountRecalTy)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.teamRebateAmountRecalTy
											)}
										</span>
									</p>
								</div>
							)
							break
						case '真人团队返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.teamRebateAmountRecalZr)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.teamRebateAmountRecalZr
											)}
										</span>
									</p>
								</div>
							)
							break
						case '电竞团队返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.teamRebateAmountRecalDj)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.teamRebateAmountRecalDj
											)}
										</span>
									</p>
								</div>
							)
							break
						case '彩票团队返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.teamRebateAmountRecalCp)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.teamRebateAmountRecalCp
											)}
										</span>
									</p>
								</div>
							)
							break

							case '体育个人返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.personRebateAmountRecalTy)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.personRebateAmountRecalTy
											)}
										</span>
									</p>
								</div>
							)
							break
						case '真人个人返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.personRebateAmountRecalZr)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.personRebateAmountRecalZr
											)}
										</span>
									</p>
								</div>
							)
							break
						case '电竞个人返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.personRebateAmountRecalDj)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.personRebateAmountRecalDj
											)}
										</span>
									</p>
								</div>
							)
							break
						case '彩票个人返点重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.personRebateAmountRecalCp)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.personRebateAmountRecalCp
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
