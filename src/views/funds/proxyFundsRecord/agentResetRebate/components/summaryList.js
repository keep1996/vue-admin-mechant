import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case '应发个人返点调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmountRecal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmountRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						case '实发个人返点调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.rebateAmountRealRecal)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmountRealRecal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(totalSummary.rebateAmountRealRecal)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmountRealRecal
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
