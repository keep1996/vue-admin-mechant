import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('funds.order_no'),
			prop: 'orderNo',
			alignCenter: 'center',
			minWidth: '280',
			isFixed: true,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: '结算周期',
			prop: 'cycleEndDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			formateDate: true
		},
		// {
		// 	label: i18n.t('funds.proxy_member_funds_record.client_state'),
		// 	prop: 'payoutStatus',
		// 	alignCenter: 'center',
		// 	minWidth: '140',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'filterType'
		// },
		{
			label: i18n.t('funds.proxy_name'),
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},

		{
			label: '业务模式',
			show: true,
			prop: 'businessModel',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'businessModelSolt',
			isSortColumn: false
		},
		{
			label: i18n.t('funds.proxy_member_funds_record.proxy_level_name'),
			prop: 'proxyLevel',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'proxyLevel'
		},
		{
			label: '收益比例',
			prop: 'incomerate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'viewSolt'
		},
		{
			label: i18n.t(
				'funds.proxy_member_funds_record.parent_proxy_account'
			),
			prop: 'parentProxyName',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		{
			label: i18n.t('funds.fund_audit.top_proxy_name'),
			prop: 'topProxyName',
			alignCenter: 'center',
			minWidth: '120',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt'
		},
		// {
		// 	label: i18n.t('common.belong_merchant'),
		// 	prop: 'merchantName',
		// 	alignCenter: 'center',
		// 	minWidth: '100',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'defaultSolt'
		// },

		// {
		// 	label: i18n.t('common.account_status'),
		// 	prop: 'accountLockStatus',
		// 	alignCenter: 'center',
		// 	minWidth: '100',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'accountStatusSolt'
		// },

		{
			label: '德州俱乐部服务费分成',
			prop: 'clubRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true,
			view: 1
		},
		{
			label: '德州保险盈亏分成',
			prop: 'insurePartsAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true,
			view: 2
		},
		{
			label: '德州增值服务分成',
			prop: 'valueAddedRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true,
			view: 5
		},
		{
			label: '其他场馆代理返点',
			prop: 'otherRebateAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true,
			view: 3
		},
		{
			label: '其他场馆盈亏占成',
			prop: 'otherPartsAmount',
			propIs: 'billFlag',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'otherPartsAmount',
			soltColor: true,
			view: 4
		},
		{
			label: '本期收益',
			prop: 'incomeAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt'
		},
		{
			label: '收益调整',
			prop: 'adjustIncomeAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true
		},
		{
			label: '调整后收益',
			prop: 'realIncomeAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'amountSolt',
			soltColor: true
		},
		{
			label: '客户端状态',
			prop: 'payoutStatus',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'clientStatusSolt'
		},

		// {
		// 	label: i18n.t('report.rebate_adjustment'),
		// 	prop: 'adjustOfRebate',
		// 	alignCenter: 'center',
		// 	minWidth: '200',
		// 	isFixed: false,
		// 	isShowTip: true,
		// 	solt: 'amountSolt',
		// 	soltColor: true
		// },
		{
			label: i18n.t('funds.proxy_member_funds_record.issue_time'),
			prop: 'payoutTime',
			alignCenter: 'center',
			minWidth: '180',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: i18n.t('common.remark'),
			prop: 'audit1Desc',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'auditSolt'
		}
		// {
		// 	label: i18n.t('common.actions'),
		// 	prop: 'auditStep',
		// 	alignCenter: 'center',
		// 	minWidth: '150',
		// 	isFixed: false,
		// 	isShow: true,
		// 	isShowTip: true,
		// 	solt: 'actionSolt'
		// }
	]
}
