// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '结算周期',
			prop: 'reportDate',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '代理账号',
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'copySolt',
			isSortColumn: false
		},
		{
			label: '业务模式',
			prop: 'businessModel',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'business',
			isSortColumn: false
		},
		{
			label: '所属商户',
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '收益加减金额',
			prop: 'adjustAmount',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'numberSolt',
			isSortColumn: false
		},
		{
			label: '操作人',
			prop: 'operatorName',
			alignCenter: 'center',
			minWidth: '200',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '操作时间',
			prop: 'operatorDate',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			isSortColumn: false
		},
		{
			label: '备注',
			prop: 'remark',
			alignCenter: 'center',
			minWidth: '230',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
			soltColor: true,
			isSortColumn: false
		}
	]
}
