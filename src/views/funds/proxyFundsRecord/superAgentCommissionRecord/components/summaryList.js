import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t(
							'funds.proxy_member_funds_record.total_person_commission_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalPersonCommissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalPersonCommissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalPersonCommissionAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalPersonCommissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalPersonCommissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalPersonCommissionAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.team_commission_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.commissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.commissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.commissionAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.commissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.commissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.commissionAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.commission_add_and_subtract'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.adjustCommissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.adjustCommissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.adjustCommissionAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.adjustCommissionAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.adjustCommissionAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.adjustCommissionAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.commission_only_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.commissionOnlyAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.commissionOnlyAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.commissionOnlyAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.commissionOnlyAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.commissionOnlyAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.commissionOnlyAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_rush_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalRushAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalRushAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalRushAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalRushAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalRushAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalRushAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_last_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalLastAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalLastAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalLastAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalLastAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalLastAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalLastAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_pure_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalPureAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalPureAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalPureAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalPureAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalPureAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalPureAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_net_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalNetAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalNetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalNetAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalNetAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalNetAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalNetAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_platform_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalPlatformAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalPlatformAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalPlatformAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalPlatformAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalPlatformAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalPlatformAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_rebate_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalRebateAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalRebateAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalRebateAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalRebateAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalRebateAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalRebateAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_discount_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalDiscountAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalDiscountAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalDiscountAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalDiscountAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalDiscountAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalDiscountAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_proxy_rebate_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalProxyRebateAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalProxyRebateAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalProxyRebateAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalProxyRebateAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalProxyRebateAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalProxyRebateAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.total_adjust_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.totalAdjustAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.totalAdjustAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.totalAdjustAmountVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.totalAdjustAmountCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												totalSummary.totalAdjustAmountTHB
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												totalSummary.totalAdjustAmountVND
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				this.$t('common.success_tip'),
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getGeneralProxyCommissionExport(params)
						.then((res) => {
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {})
				})
				.catch(() => {})
		}
	}
}
