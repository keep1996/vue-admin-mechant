import { isHaveEmoji, notSpecial2 } from '@/utils/validate'
import { checkProxyDetailAPI } from '@/api/agent'
import i18n from '@/locales'
export const rules = () => {
	// const reg1 = /^[A-Za-z]{1}(?=(.*[a-zA-Z]){0,})(?=(.*[0-9]){1,})[0-9A-Za-z]{3,10}$/
	const reg1 = /^[A-Za-z0-9]{4,11}$/
	const reg2 = /^([a-zA-Z0-9]*[a-zA-Z]+[0-9]+[a-zA-Z0-9]*|[a-zA-Z0-9]*[0-9]+[a-zA-Z]+[a-zA-Z0-9]*)$/

	const checkAgentInfo = async (val) => {
		return new Promise((resolve, reject) => {
			const params = { userName: val }
			checkProxyDetailAPI(params)
				.then(async (res) => {
					if (res.data?.length) {
						// if (res.data.entryAuthority == 0) {
						// 	resolve(['此帐号入口权限未打开,不允许添加', res])
						// } else if (res.data.maxLevel <= res.data.proxyLevel) {
						// 	resolve(['此帐号不允许发展下级', res])
						// } else {
						// 	resolve([null, res])
						// }
						resolve([null, res])
					} else {
						resolve([i18n.t('agent.rule_agent_tip'), res])
					}
				})
				.catch((err) => {
					resolve([err, null])
				})
		})
	}
	const testUserName = async (rule, value, callback) => {
		const isSpecial = !notSpecial2(String(value))
		const isRmoji = isHaveEmoji(String(value))
		if (isSpecial) {
			callback(new Error(i18n.t('agent.rule_other_tip2')))
		} else if (isRmoji) {
			callback(new Error(i18n.t('agent.rule_other_tip1')))
		} else if (!reg1.test(value)) {
			callback(new Error(i18n.t('agent.rule_input_tip4-11')))
		} else {
			callback()
		}
	}
	const testParentProxyName = async (rule, value, callback) => {
		const isSpecial = !notSpecial2(String(value))
		const isRmoji = isHaveEmoji(String(value))
		if (isSpecial) {
			callback(new Error(i18n.t('agent.rule_other_tip2')))
		} else if (isRmoji) {
			callback(new Error(i18n.t('agent.rule_other_tip1')))
		} else if (!reg1.test(value)) {
			callback(new Error(i18n.t('agent.rule_input_tip4-11')))
		} else {
			const [err] = await checkAgentInfo(value)
			if (err) {
				callback(err)
			} else {
				callback()
			}
		}
	}
	// eslint-disable-next-line no-unused-vars
	const testSw = (rule, value, callback) => {
		const isRmoji = isHaveEmoji(String(value))
		if (isRmoji) {
			callback(new Error(i18n.t('agent.rule_other_tip1')))
		} else if (value && !reg1.test(value)) {
			callback(new Error(i18n.t('agent.rule_input_tip4-11')))
		} else {
			callback()
		}
	}

	const testPassword = (rule, value, callback) => {
		const isSpecial = !notSpecial2(String(value))
		const isRmoji = isHaveEmoji(String(value))
		if (isSpecial) {
			callback(new Error(i18n.t('agent.rule_other_tip2')))
		} else if (isRmoji) {
			callback(new Error(i18n.t('agent.rule_other_tip1')))
		} else if (!reg2.test(value)) {
			callback(new Error(i18n.t('agent.rule_other_tip8')))
		} else {
			callback()
		}
	}
	const withdrawLimitType = [
		{
			required: true,
			message: '请选择取款类型限制',
			trigger: 'change'
		}
	]
	const maxLevels = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_agent_level'),
			trigger: 'change'
		}
	]
	const accountType = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_agent_type'),
			trigger: 'change'
		}
	]
	const contractModel = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_contract_mode'),
			trigger: 'change'
		}
	]
	const commissionPolicyTypeId = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_commission_type'),
			trigger: 'change'
		}
	]
	const rebatePolicyTypeId = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_rebate_policy'),
			trigger: 'change'
		}
	]
	const merchantId = [
		{
			required: true,
			message: i18n.t('agent.rule_select_tip_merchant'),
			trigger: 'change'
		}
	]

	const userName = [
		{
			required: true,
			validator: testUserName,
			trigger: 'blur'
		},
		{
			min: 4,
			max: 11,
			message: i18n.t('agent.rule_username_4_11_tip'),
			trigger: 'blur'
		}
	]

	const password = [
		{
			required: true,
			validator: testPassword,
			trigger: 'blur'
		},
		{
			min: 8,
			max: 12,
			message: i18n.t('agent.rule_other_tip8'),
			trigger: 'blur'
		}
	]
	const applyInfo = [
		{
			min: 2,
			max: 500,
			message: i18n.t('common.components.limit_500'),
			trigger: 'blur'
		}
	]
	const parentProxyName = [
		{
			required: true,
			validator: testParentProxyName,
			trigger: 'blur'
		}
	]
	const appId = [
		{
			required: true,
			message: i18n.t('agent.register_website_null'),
			trigger: 'change'
		}
	]
	const businessModel = [
		{
			required: true,
			message: i18n.t('common.business_model_null'),
			trigger: 'change'
		}
	]
	const sharingPolicyId = [
		{
			required: true,
			message: i18n.t('agent.rule_profit_ratio_tip'),
			trigger: 'change'
		}
	]
	return {
		appId,
		maxLevels,
		accountType,
		contractModel,
		merchantId,
		commissionPolicyTypeId,
		rebatePolicyTypeId,
		userName,
		businessModel,
		sharingPolicyId,
		password,
		parentProxyName,
		applyInfo,
		withdrawLimitType
	}
}

export function accountTypeArr() {
	const arr =
		(this.globalDics.accountType &&
			JSON.parse(JSON.stringify(this.globalDics.accountType))) ||
		[]
	// arr.length && arr.splice(-1)
	return arr
}
