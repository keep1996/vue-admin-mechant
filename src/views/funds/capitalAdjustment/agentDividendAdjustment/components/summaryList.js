import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 列表小计，总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>总计</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'winTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.winTotal
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.winTotal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span
											style={this.handleNumberColor(
												subSummary.validBetTotal
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetTotal
											)}
										</span>
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		handleRow2(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>总计</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'amountTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.amountTotal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetTotal
											)}
										</span>
									</p>
								</div>
							)
							break

						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
