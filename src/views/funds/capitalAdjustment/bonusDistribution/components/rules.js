export const rules = (that) => {
	const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,11}$/
	const testUserName = (rule, value, callback) => {
		if (value && reg.test(value)) {
			callback()
		} else {
			callback(that.$t('funds.capital_adjustment.form_rules_tip1'))
		}
	}
	const merchantId = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip2'),
			trigger: 'change'
		}
	]

	const activityTitle = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip11'),
			trigger: ['change', 'blur']
		}
	]
	const operationType = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip12'),
			trigger: 'blur'
		}
	]
	const location = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip13'),
			trigger: 'blur'
		}
	]
	const userName = [
		{
			required: true,
			validator: testUserName,
			trigger: 'blur'
		}
	]
	const bounsAmount = [
		{
			required: true,
			message: that.$t(
				'funds.capital_adjustment.bouns_amount_placeholder'
			),
			trigger: 'blur'
		}
	]
	const fileUrl = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip14'),
			trigger: 'blur'
		}
	]
	const billMultiple = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip9'),
			trigger: 'blur'
		}
	]
	const notification = [
		{
			required: true,
			message: that.$t('common.please_choose'),
			trigger: 'blur'
		}
	]
	const remark = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip15'),
			trigger: 'blur'
		},
		{
			min: 2,
			max: 500,
			message: that.$t('funds.capital_adjustment.form_rules_tip16'),
			trigger: 'blur'
		}
	]
	return {
		merchantId,
		activityTitle,
		operationType,
		location,
		userName,
		bounsAmount,
		fileUrl,
		billMultiple,
		notification,
		remark
	}
}
