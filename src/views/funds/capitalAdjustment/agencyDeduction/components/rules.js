export const rules = (that) => {
	// const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{4,11}$/
	const reg = /^[A-Za-z0-9]{4,11}$/
	const testUserName = (rule, value, callback) => {
		if (value && reg.test(value)) {
			callback()
		} else {
			callback(that.$t('funds.capital_adjustment.form_rules_tip1'))
		}
	}
	const merchantId = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip2'),
			trigger: 'change'
		}
	]
	const userName = [
		{
			required: true,
			validator: testUserName,
			trigger: 'blur'
		}
	]
	const realAccountType = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip17'),
			trigger: 'change'
		}
	]
	const adjustType = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip3'),
			trigger: 'change'
		}
	]
	const rechargeMode = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip18'),
			trigger: 'chnage'
		}
	]
	const lessMoney = [
		{
			required: true,
			message: that.$t('funds.capital_adjustment.form_rules_tip8'),
			trigger: 'blur'
		}
	]
	const remark = [
		{
			min: 2,
			max: 500,
			required: true,
			message: that.$t('funds.fund_audit.form_rules_tip2'),
			trigger: 'blur'
		}
	]
	return {
		merchantId,
		userName,
		realAccountType,
		adjustType,
		rechargeMode,
		lessMoney,
		remark
	}
}
