import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 小计，总计行
		handleSummary(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t(
							'funds.capital_adjustment.real_person_commission_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.actualAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.actualAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.capital_adjustment.total_child_commission_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.payableAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.payableAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.capital_adjustment.real_pay_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.actualPayAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.actualPayAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t('funds.capital_adjustment.lock_account'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.remainingDebtAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.remainingDebtAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.capital_adjustment.commission_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.receivableAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.receivableAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.capital_adjustment.real_receive_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.actualReceivableAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.actualReceivableAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.capital_adjustment.not_recvied_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.remainingUncollectedAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.remainingUncollectedAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
