export const tableColumns = {
	cycleStartEnd: '结算周期',
	proxyName: '代理账号',
	topProxyName: '总代账号',
	businessModel: '业务模式',
	proxyLevel: '代理层级',
	merchantName: '所属商户',
	windControlName: '风控层级',
	otherMemberNetAmount: '其他场馆会员游戏输赢',
	otherMemberRebateAmount: '其他场馆会员总返水',
	otherMemberAccountAdjustAmount: '会员账户调整',
	otherProxyNetAmount: '其他场馆代理净盈亏',
	otherCommissionRate: '其他场馆佣金比例',
	otherPartsAmount: '其他场馆佣金',
	clubPartsAmount: '德州俱乐部服务费分成',
	insurePartsAmount: '德州俱乐部保险分成',
	clubValueAddedPartsAmount: '德州增值服务消费分成',
	// lastMonthBalance: '上月结余',
	commissionAmount: '综合佣金',
	orderStatus: '订单状态',
	auditTime: '审核时间',
	auditCost: '审核用时',
	auditOperator: '审核人',
	auditDesc: '备注'
}

export const activityListField = {
	[tableColumns.cycleStartEnd]: true,
	[tableColumns.proxyName]: true,
	[tableColumns.topProxyName]: true,
	[tableColumns.businessModel]: true,
	[tableColumns.proxyLevel]: true,
	[tableColumns.merchantName]: true,
	[tableColumns.windControlName]: true,
	[tableColumns.otherMemberNetAmount]: true,
	[tableColumns.otherMemberRebateAmount]: true,
	[tableColumns.otherMemberAccountAdjustAmount]: true,
	[tableColumns.otherProxyNetAmount]: true,
	[tableColumns.otherCommissionRate]: true,
	[tableColumns.otherPartsAmount]: true,
	[tableColumns.clubPartsAmount]: true,
	[tableColumns.insurePartsAmount]: true,
	[tableColumns.clubValueAddedPartsAmount]: true,
	[tableColumns.lastMonthBalance]: true,
	[tableColumns.commissionAmount]: true,
	[tableColumns.orderStatus]: true,
	[tableColumns.auditTime]: true,
	[tableColumns.auditCost]: true,
	[tableColumns.auditOperator]: true,
	[tableColumns.auditDesc]: true,
}

export const payoutStatusArr = [
	{
		code: 0,
		description: '未结算'
	},
	{
		code: 1,
		description: '待审核'
	},
	{
		code: 2,
		description: '待领取'
	},
	{
		code: 3,
		description: '领取成功'
	},
	{
		code: 4,
		description: '超时未领取'
	},
	{
		code: 5,
		description: '无返点'
	},
	{
		code: 6,
		description: '已取消'
	}
]
