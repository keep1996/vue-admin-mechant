import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 小计，总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t('funds.bonus_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.amountPageSumCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountPageSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountPageSumVND
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												subSummary.amountTotalSumCNY
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountTotalSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountTotalSumVND
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
