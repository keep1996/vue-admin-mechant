import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								总计
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case '注单量':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{subSummary.betCountRecal || 0}
										</span>
									</p>
								</div>
							)
							break
						case '应发返水重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmountRecal
											)}
										</span>

									</p>

								</div>
							)
							break
						case '实发返水重算调整':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(subSummary.rebateRealAmount)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateRealAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
