import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 弹框小计行
		handleDialogRow(params, currency, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t(
							'funds.proxy_member_funds_record.valid_bet_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.rebate_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.rebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case this.$t('funds.fund_audit.actual_rebate_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												currency,
												subSummary.rebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
