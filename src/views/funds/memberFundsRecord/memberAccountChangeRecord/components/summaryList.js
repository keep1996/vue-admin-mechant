import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 小计，总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t(
							'funds.proxy_member_funds_record.change_before'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.changeBeforePageSum
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.changeBeforePageSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.changeBeforePageSumVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.changeBeforeTotalSum
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.changeBeforeTotalSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.changeBeforeTotalSumVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.change_amount'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.amountPageSum
											)}
										</span>

										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountPageSumTHR
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountPageSumVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.amountTotalSum
											)}
										</span>

										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.amountTotalSumTHR
											)}
										</span>

										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.amountTotalSumVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						case this.$t(
							'funds.proxy_member_funds_record.change_after'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.changeAfterPageSum
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.changeAfterPageSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.changeAfterPageSumVND
											)}
										</span> */}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.changeAfterTotalSum
											)}
										</span>
										{/* <span>
											{this.handleTotalNumber(
												'THB',
												subSummary.changeAfterTotalSumTHR
											)}
										</span>
										<span>
											{this.handleTotalNumber(
												'VND',
												subSummary.changeAfterTotalSumVND
											)}
										</span> */}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 导出
		exportExcel(params) {
			this.$confirm(
				`<strong>${this.$t(
					'common.is_export'
				)}</strong></br><span style='font-size:12px;color:#c1c1c1'>${this.$t(
					'common.excess_data'
				)}</span>`,
				this.$t('common.success_tip'),
				{
					dangerouslyUseHTMLString: true,
					confirmButtonText: this.$t('common.confirm'),
					cancelButtonText: this.$t('common.cancel'),
					type: 'warning'
				}
			)
				.then(() => {
					this.$api
						.getMemberWalletChangeExport(params)
						.then((res) => {
							this.loading = false
							const { data, status } = res
							if (res && status === 200) {
								const { type } = data
								if (type.includes('application/json')) {
									const reader = new FileReader()
									reader.onload = (evt) => {
										if (evt.target.readyState === 2) {
											const {
												target: { result }
											} = evt
											const ret = JSON.parse(result)
											if (ret.code !== 200) {
												this.$message({
													type: 'error',
													message: ret.msg,
													duration: 1500
												})
											}
										}
									}
									reader.readAsText(data)
								} else {
									const result = res.data
									const disposition =
										res.headers['content-disposition']
									const fileNames =
										disposition && disposition.split("''")
									let fileName = fileNames[1]
									fileName = decodeURIComponent(fileName)
									const blob = new Blob([result], {
										type: 'application/octet-stream'
									})
									if (
										'download' in
										document.createElement('a')
									) {
										const downloadLink = document.createElement(
											'a'
										)
										downloadLink.download = fileName || ''
										downloadLink.style.display = 'none'
										downloadLink.href = URL.createObjectURL(
											blob
										)
										document.body.appendChild(downloadLink)
										downloadLink.click()
										URL.revokeObjectURL(downloadLink.href)
										document.body.removeChild(downloadLink)
									} else {
										window.navigator.msSaveBlob(
											blob,
											fileName
										)
									}
									this.$message({
										type: 'success',
										message: this.$t(
											'common.export_success'
										),
										duration: 1500
									})
								}
							}
						})
						.catch(() => {
							this.loading = false
							// this.$message({
							//   type: "error",
							//   message: "导出失败",
							//   duration: 1500,
							// });
						})
				})
				.catch(() => {
					this.loading = false
				})
		}
	}
}
