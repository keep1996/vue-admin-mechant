export const basicColumns = [
	{
		valueAddedType: 1,
		label: '发弹幕'
	},
	{
		valueAddedType: 3,
		label: '发表情'
	},
	{
		valueAddedType: 2,
		label: '发短语'
	},
	{
		valueAddedType: 4,
		label: '发语音'
	},
	{
		valueAddedType: 5,
		label: '看公牌'
	},
	{
		valueAddedType: 15,
		label: '下注行动延时'
	},
	{
		valueAddedType: 16,
		label: '保险行动延时'
	}

]

export const interactiveColumns = [
	{
		valueAddedType: 19,
		columnWidthType: 5960,
		label: '互动道具-抓鸡'
	},
	{
		valueAddedType: 20,
		columnWidthType: 5960,
		label: '互动道具-丢大便'
	},
	{
		valueAddedType: 21,
		columnWidthType: 5960,
		label: '互动道具-碰杯'
	},
	{
		valueAddedType: 22,
		columnWidthType: 5960,
		label: '互动道具-点赞'
	},
	{
		valueAddedType: 33,
		columnWidthType: 5960,
		label: '互动道具-西红柿'
	},
	{
		valueAddedType: 24,
		columnWidthType: 5960,
		label: '互动道具-鼓掌'
	},
	{
		valueAddedType: 25,
		columnWidthType: 5960,
		label: '互动道具-炸弹'
	},
	{
		valueAddedType: 26,
		columnWidthType: 5960,
		label: '互动道具-鲜花'
	},
	{
		valueAddedType: 27,
		columnWidthType: 5960,
		label: '互动道具-飞吻'
	},
	{
		valueAddedType: 28,
		columnWidthType: 5960,
		label: '互动道具-鲨鱼'
	},
	{
		valueAddedType: 32,
		columnWidthType: 5960,
		label: '互动道具-拍桌子'
	},
	{
		valueAddedType: 29,
		columnWidthType: 5960,
		label: '互动道具-鲤鱼'
	},
	{
		valueAddedType: 30,
		columnWidthType: 5960,
		label: '互动道具-大炮'
	},
	{
		valueAddedType: 31,
		columnWidthType: 5960,
		label: '互动道具-抽烟'
	},
	{
		valueAddedType: 71,
		columnWidthType: 5960,
		label: '互动道具-拳头'
	}
]

export const lookHandColumns = [
	{
		valueAddedType: 65,
		label: '看手牌',
		minWidth: '200'
	}
]
