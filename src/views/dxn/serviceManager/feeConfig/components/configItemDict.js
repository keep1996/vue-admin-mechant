const configType = {
	texas: 17, // 德州-普通
	short: 18, // 短牌-普通
	texasHigh: 59, // 德州-高级
	shortHigh: 60 // 短牌-高级
}

const texasSubBillingTypes = {
	1: '$',
	2: '倍大盲'
}

const shortSubBillingTypes = {
	1: '$',
	2: '倍前注'
}

export const defaultBillingTypes = {
	1: '固定收费',
	2: '盲注倍数收费'
}
export const actionDelayBillingTypes = {
	1: '固定收费',
	2: '盲注倍数收费',
	3: '单次行动指数递增'
}
export const handCardBillingTypes = {
	1: '固定收费',
	4: '底池比例收费'
}
export const allBillingTypes = {
	1: '固定收费',
	2: '盲注倍数收费',
	3: '单次行动指数递增',
	4: '底池比例收费'
}

export const configItemTitle = {
	amount: '$/次',
	texasMultiple: '倍大盲',
	shortMultiple: '倍前注',
	start: '起始',
	incrStep: '倍增长',
	limit: '上限',
	pot: '%底池',
	rebate: '%返利',
	firstTimeFree: '首次免费',
	upper: '封顶'
}

// 是否是互动道具配置
export const isInteractiveConfigTypeFunc = (type) => {
	return [59, 60].includes(type)
}

// 是否是看手牌配置
export const isLookHandConfigTypeFunc = (type) => {
	return [65, 66].includes(type)
}

// 是否为单次行动指数递增
export const isActionDelayValueAddedTypeFunc = (valueAddedType) => {
	return [15, 16].includes(valueAddedType)
}

export const getBillingTypeInfoFunc = (configType, valueAddedType) => {
	let retObj = {
		style: 'width: 130px;',
		types: defaultBillingTypes
	}
	if (isActionDelayValueAddedTypeFunc(valueAddedType)) {
		retObj = {
			style: 'width: 160px;',
			types: actionDelayBillingTypes
		}
	}
	if (isLookHandConfigTypeFunc(configType)) {
		retObj = {
			style: 'width: 150px;',
			types: handCardBillingTypes
		}
	}
	return retObj
}
export const getSubBillingTypeInfoFunc = (type) => {
	return type === configType.texas || type === configType.texasHigh
		? {
			multipleTitle: configItemTitle.texasMultiple,
			subTypes: texasSubBillingTypes
		}
		: {
			multipleTitle: configItemTitle.shortMultiple,
			subTypes: shortSubBillingTypes
		}
}

