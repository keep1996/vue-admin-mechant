import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validBetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'grossProfit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.grossProfit
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.grossProfit
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'userHandFee2':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.userHandFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.userHandFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'pumpContribution':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.pumpContribution
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.pumpContribution
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
