
// 商品类型
export const productTypeArr = [
    {
        code: 1,
        description: '入场弹幕'
    },
    {
        code: 2,
        description: '入座特效'
    }
]

// 商品状态
export const productStatusArr = [
    {
        code: 2,
        description: '开启中'
    },
    {
        code: 1,
        description: '已禁用'
    }
]

// 上架状态
export const shelvesStatusStatusArr = [
    {
        code: 2,
        description: '上架'
    },
    {
        code: 1,
        description: '下架'
    }
]

// 会员商品列表-当前使用商品
export const currentProductUsedArr = [
    {
        code: 2,
        description: '是'
    },
    {
        code: 1,
        description: '否'
    }
]

// 会员商品列表-商品生效状态
export const productEffectiveStatusArr = [
    {
        code: 2,
        description: '生效中'
    },
    {
        code: 3,
        description: '已过期'
    }
]

// 商品购买记录-当次商品生效状态
export const theProductEffectiveStatusArr = [
    {
        code: 1,
        description: '未生效'
    },
    {
        code: 2,
        description: '生效中'
    },
    {
        code: 3,
        description: '已过期'
    }
]

// 标识商品时效为永久
export const durationPermanentNum = 99999
export const durationPermanentDesc = '永久'

export const getDurationNameFunc = (day) => {
    return day === durationPermanentNum
        ? durationPermanentDesc
        : day + '天'
}
