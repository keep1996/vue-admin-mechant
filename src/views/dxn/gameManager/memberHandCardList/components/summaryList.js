import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'beginAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.beginAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.beginAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringMidwayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringMidwayAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringMidwayAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'endAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.endAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.endAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validPot':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validPot
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validPot
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'userHandFee':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.userHandFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.userHandFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'evInsuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.evInsuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.evInsuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'handNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.handNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.handNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.handNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.handNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
