import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'beforeChip':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.beforeChip
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.beforeChip
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringMidwayChip':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringMidwayChip
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringMidwayChip
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'afterChip':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.afterChip
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.afterChip
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'pumpingAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.pumpingAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.pumpingAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'validPot':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.validPot
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.validPot
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'evInsuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.evInsuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.evInsuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'handNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.handNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.handNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.handNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.handNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
