export const tableColumns = {
	clubName: '俱乐部名称',
	statusName: '俱乐部白名单状态',
	createdBy: '添加人',
	createdAt: '白名单添加时间',
	updatedBy: '最近操作人',
	updatedAt: '最近操作时间',
	remark: '备注'
}
