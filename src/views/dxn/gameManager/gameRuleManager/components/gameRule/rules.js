export const rules = (that) => {
	const checkGameOvertime = (rule, value, callback) => {
		if (!that.maxLimit.gameOvertime) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (!value) {
			return callback(
				new Error(that.$t('dxn.game_rule.timeout_placeholder'))
			)
		} else if (value > that.maxLimit.gameOvertime) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.max_limit_tips') +
					that.maxLimit.gameOvertime
				)
			)
		} else {
			return callback()
		}
	}
	const checkLowerLimit = (rule, value, callback) => {
		if (!that.maxLimit.complementLimit) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (!value) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.lower_limit_placeholder')
				)
			)
		} else if (that.maxLimit.clubBringInLimit && value > that.maxLimit.clubBringInLimit) {
			that.$message.closeAll()
			that.$message.warning('补码上下限不能大于单牌桌累计带入上限: ' + that.maxLimit.clubBringInLimit)
			return callback(
				new Error('不能大于单牌桌累计带入上限')
			)
		} else if (value > that.maxLimit.complementLimit) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.max_limit_tips') +
					that.maxLimit.complementLimit
				)
			)
		} else if (value > that.ruleForm.complementUpperLimit) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.cannot_be_greater_than')
				)
			)
		} else {
			return callback()
		}
	}
	const checkUpperLimit = (rule, value, callback) => {
		if (!that.maxLimit.complementLimit) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (!value) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.upper_limit_placeholder')
				)
			)
		} else if (that.maxLimit.clubBringInLimit && value > that.maxLimit.clubBringInLimit) {
			that.$message.closeAll()
			that.$message.warning('补码上下限不能大于单牌桌累计带入上限:' + that.maxLimit.clubBringInLimit)
			return callback(
				new Error('不能大于单牌桌累计带入上限')
			)
		} else if (value > that.maxLimit.complementLimit) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.max_limit_tips') +
					that.maxLimit.complementLimit
				)
			)
		} else if (value < that.ruleForm.complementLowerLimit) {
			return callback(
				new Error(that.$t('dxn.game_rule.cannot_be_less_than'))
			)
		} else {
			return callback()
		}
	}
	const checkRake = (rule, value, callback) => {
		if (!that.maxLimit.rake) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (!value && parseInt(value) !== 0) {
			return callback(
				new Error(
					that.$t('dxn.game_rule.pool_pump_ratio_placeholder')
				)
			)
		} else if (parseFloat(value) > that.maxLimit.rake) {
			return callback(
				new Error('有效值为>=0且<=' + that.maxLimit.rake + '（支持2位小数点）')
			)
		} else {
			return callback()
		}
	}
	const checkInsuranceLower = (rule, value, callback) => {
		if (!value && parseInt(value) !== 0) {
			return callback(
				new Error('保险触发下限不能为空')
			)
		} else {
			return callback()
		}
	}
	const checkEvExtractionLowerLimit = (rule, value, callback) => {
		if (!value && parseInt(value) !== 0) {
			return callback(
				new Error('EV提取下限不能为空')
			)
		} else {
			return callback()
		}
	}
	const checkBottomPoolFee = (rule, value, callback) => {
		if (!that.maxLimit.bottomPoolFee) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (value === '') {
			return callback(
				new Error('请输入底池比例收费')
			)
		} else {
			return callback()
		}
	}
	const checkProfitFee = (rule, value, callback) => {
		if (!that.maxLimit.profitFee) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else if (value === '') {
			return callback(
				new Error('请输入盈利比例收费')
			)
		} else {
			return callback()
		}
	}
	const checkTableFeeScale = (rule, value, callback) => {
		if (value === '') {
			return callback(
				new Error('请输入局服务费比例')
			)
		} else {
			return callback()
		}
	}
	const checkLimitToLessThan = (rule, value, callback) => {
		if (that.ruleForm.gpsImposeSwitch === 2 && !value) {
			return callback(
				new Error('请输入GPS限制范围')
			)
		} else {
			return callback()
		}
	}
	const checkMinimumHandNumber = (rule, value, callback) => {
		if (!that.maxLimit.minimumHandNumber) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else {
			return callback()
		}
	}
	const checkMinimumBringPoolRate = (rule, value, callback) => {
		if (!that.maxLimit.minimumBringPoolRate) {
			return callback(
				new Error(that.$t('dxn.game_rule.get_max_limit_error'))
			)
		} else {
			return callback()
		}
	}

	return {
		gameTypeId: [
			{
				required: true,
				message: that.$t('dxn.game_rule.game_placeholder'),
				trigger: 'blur'
			}
		],
		gameName: [
			{
				required: true,
				message: that.$t('dxn.game_rule.game_rule_placeholder'),
				trigger: 'blur'
			}
		],
		status: [
			{
				required: true,
				message: that.$t('dxn.game_rule.game_master_switch'),
				trigger: 'change'
			}
		],
		gameOvertime: [
			{
				required: true,
				trigger: 'blur',
				validator: checkGameOvertime
			}
		],
		bbSetting: [
			{
				required: true,
				message: that.bbSettingLabel,
				trigger: 'change'
			}
		],
		bbDefaultCarry: [
			{
				required: true,
				message: that.bbDefaultCarryLabel,
				trigger: 'change'
			}
		],
		complementLowerLimit: [
			{
				required: true,
				trigger: 'blur',
				validator: checkLowerLimit
			}
		],
		complementUpperLimit: [
			{
				required: true,
				trigger: 'blur',
				validator: checkUpperLimit
			}
		],
		gameTimes: [
			{
				required: true,
				message: that.$t('dxn.game_rule.duration'),
				trigger: 'change'
			}
		],
		gamePeopleNumbers: [
			{
				required: true,
				message: that.$t('dxn.game_rule.people'),
				trigger: 'change'
			}
		],
		autoStartNumbers: [
			{
				required: true,
				message: that.$t('dxn.game_rule.people_auto_start'),
				trigger: 'change'
			}
		],
		rake: [
			{
				required: true,
				trigger: 'blur',
				validator: checkRake
			}
		],
		insuranceLowerMulti: [
			{
				required: true,
				trigger: 'blur',
				validator: checkInsuranceLower
			}
		],
		evExtractionLowerLimitMulti: [
			{
				required: true,
				trigger: 'blur',
				validator: checkEvExtractionLowerLimit
			}
		],
		insuranceTurn: [
			{
				required: true,
				message: that.$t('dxn.game_rule.turn_buy_insure_tips'),
				trigger: 'change'
			}
		],
		mustKeepChip: [
			{
				required: true,
				message: '请输入必要保留筹码',
				trigger: 'change'
			}
		],
		bottomPoolFee: [
			{
				required: true,
				trigger: 'change',
				validator: checkBottomPoolFee
			}
		],
		profitFee: [
			{
				required: true,
				trigger: 'change',
				validator: checkProfitFee
			}
		],
		tableFeeScale: [
			{
				required: true,
				trigger: 'change',
				validator: checkTableFeeScale
			}
		],
		limitToLessThan: [
			{
				required: true,
				trigger: 'blur',
				validator: checkLimitToLessThan
			}
		],
		minimumHandNumber: [
			{
				required: true,
				trigger: 'change',
				validator: checkMinimumHandNumber
			}
		],
		minimumBringPoolRate: [
			{
				required: true,
				trigger: 'change',
				validator: checkMinimumBringPoolRate
			}
		]
	}
}
