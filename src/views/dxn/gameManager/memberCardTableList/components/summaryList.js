import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'totalNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.totalNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.totalNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.totalNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.totalNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'tableHandCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.tableHandCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.tableHandCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringInChipScore':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringInChipScore
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringInChipScore
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringOutChipScore':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringOutChipScore
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringOutChipScore
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringInCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.bringInCount,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.bringInCount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'grossProfit':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.grossProfit
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.grossProfit
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.grossProfit
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.grossProfit
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.netAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.netAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.netAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.netAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'userHandFee':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.userHandFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.userHandFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'tableFee':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tableFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tableFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'evInsuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.evInsuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.evInsuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'squidNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.squidNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.squidNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.squidNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.squidNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
