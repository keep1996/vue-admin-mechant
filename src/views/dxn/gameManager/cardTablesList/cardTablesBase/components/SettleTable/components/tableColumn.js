export const tableColumns = {
	handBeginDate: '开始时间',
	handEndDate: '结束时间',
	tableCode: '牌桌ID',
	handCount: '手数',
	costTotal: '会员总输赢',
	bringOutChipScore: '牌桌带出',
	tableTransToSquidAmount: '牌桌划出',
	bringMidwayChip: '牌桌带入',
	bringCount: '带入次数',
	grossProfit: '未抽水打牌输赢',
	netAmount: '抽水后打牌输赢',
	userHandFee: '手牌服务费',
	tableFee: '局服务费',
	insuredNetAmount: '经典保险输赢',
	evInsuredNetAmount: 'EV保险输赢',
	sendBarrageAmount: '发弹幕',
	sendEmoteAmount: '发表情',
	sendPuraseAmount: '发短语',
	sendVoiceAmount: '发语音',
	interactPropAmount: '互动道具',
	lookPublicCardAmount: '看公牌',
	betActionDelayAmount: '下注行动延时',
	insureActionDelayAmount: '保险行动延时',
	lookHandAmount: '看手牌',
	giveRewardAmount: '打赏',
	interactivePropsRebateAmount: '互动道具返利',
	lookHandRebateAmount: '看手牌返利',
	giveRewardAcquireAmount: '打赏获得',
	interactivePropsProfitAmount: '互动道具盈利',
	lookHandProfitAmount: '看手牌盈利',
	giveRewardProfitAmount: '打赏盈利',
	pumpContribution: '手牌服务费贡献',
	tableFeeContribution: '局服务费贡献',
	contribution: '服务费贡献',
	squidBringOutAmount: '带出鱿鱼钱包',
	squidBringInAmount: '带入鱿鱼钱包',
	squidPayAmount: '鱿鱼钱包支付',
	squidTransInAmount: '牌桌划入',
	squidRound: '鱿鱼轮数',
	squidRewardsAmount: '鱿鱼模式总奖励',
	squidPayoutAmount: '鱿鱼模式总赔付',
	squidNetAmount: '鱿鱼输赢'
}
