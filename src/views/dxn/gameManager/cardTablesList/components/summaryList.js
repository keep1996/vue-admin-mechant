import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRow(params, subSummary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('report.subtotal')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'handCount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleNumber(
												'',
												subSummary.tableHandNumber,
												0
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'',
												totalSummary.tableHandNumber,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringInChipScore':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringInChipScore
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringInChipScore
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'bringOutChipScore':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.bringOutChipScore
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.bringOutChipScore
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'pumpingAmountTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.pumpAmountSum
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.pumpAmountSum
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'tableFee':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary.tableFee
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.tableFee
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.insuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'evInsuredNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.evInsuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.evInsuredNetAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.evInsuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'costTotal':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.costTotal
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.costTotal
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.costTotal
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.costTotal
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'sendBarrageAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.sendBarrageAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.sendBarrageAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.sendBarrageAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.sendBarrageAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'sendEmoteAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.sendEmoteAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.sendEmoteAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.sendEmoteAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.sendEmoteAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'sendPuraseAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.sendPuraseAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.sendPuraseAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.sendPuraseAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.sendPuraseAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'sendVoiceAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.sendVoiceAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.sendVoiceAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.sendVoiceAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.sendVoiceAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'interactPropAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.interactPropAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.interactPropAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.interactPropAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.interactPropAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lookPublicCardAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.lookPublicCardAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lookPublicCardAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.lookPublicCardAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lookPublicCardAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betActionDelayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.betActionDelayAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.betActionDelayAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.betActionDelayAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.betActionDelayAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insureActionDelayAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.insureActionDelayAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.insureActionDelayAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.insureActionDelayAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.insureActionDelayAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lookHandAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.lookHandAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lookHandAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.lookHandAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lookHandAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'giveRewardAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.giveRewardAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.giveRewardAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.giveRewardAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.giveRewardAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'interactivePropsRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.interactivePropsRebateAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.interactivePropsRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.interactivePropsRebateAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.interactivePropsRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lookHandRebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.lookHandRebateAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lookHandRebateAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.lookHandRebateAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lookHandRebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'giveRewardAcquireAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.giveRewardAcquireAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.giveRewardAcquireAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.giveRewardAcquireAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.giveRewardAcquireAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'interactivePropsProfitAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.interactivePropsProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.interactivePropsProfitAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.interactivePropsProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.interactivePropsProfitAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'lookHandProfitAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.lookHandProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.lookHandProfitAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.lookHandProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.lookHandProfitAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'giveRewardProfitAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span style={this.handleNumberColor(
											subSummary.giveRewardProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												subSummary.giveRewardProfitAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span style={this.handleNumberColor(
											totalSummary.giveRewardProfitAmount
										)}>
											{this.handleTotalNumber(
												'USDT',
												totalSummary.giveRewardProfitAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
