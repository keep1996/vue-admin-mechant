export const tableColumns = {
	direction: '收支类型',
	changeBefore: '账变前余额',
	changeAmount: '账变金额',
	changeAfter: '账变后余额',
	time: '账变时间',
	remark: '备注'
}
