import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 小计，总计行
		handleRow(params, subSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'potAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.potAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.potAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'betAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.betAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.betAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.insuredAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.insuredAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredNetAmount':
							sums[index] = (
								<div>
									<p
										class='footer_count_row'
										style={this.handleNumberColor(
											subSummary[0]?.insuredNetAmount
										)}
									>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.insuredNetAmount
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												subSummary[1]?.insuredNetAmount
											)}
										>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.insuredNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'insuredRewards':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.insuredRewards
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.insuredRewards
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'negativeBalance':
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[0]?.negativeBalance
											)}
										</span>
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'USDT',
												subSummary[1]?.negativeBalance
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
