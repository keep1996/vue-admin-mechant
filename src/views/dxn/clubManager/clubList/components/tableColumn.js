export const tableColumns = {
	clubName: '俱乐部名称',
	iconUrl: '俱乐部头像',
	introduction: '俱乐部介绍',
	membership: '俱乐部会员数量',
	proxyName: '代理账号',
	topProxyName: '所属总代账号',
	createdAt: '创建时间'
}
