import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		getCurrency(key) {
			const currencyMap = {
				thbSum: { id: 'th', currency: 'THB' },
				cnySum: { id: 'zh', currency: 'CNY' },
				vndSum: { id: 'vn', currency: 'VND' },
				usdtSum: { id: 'usd', currency: 'USD' }
			}
			const map = currencyMap[key]
			return map ? map.currency : ''
		},
		// 总计行
		handleRow(params, summary, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t('game.game_betslip.order_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.orderAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleNumber(
											'USDT',
											totalSummary.orderAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.discount_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.preferentialAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleNumber(
											'USDT',
											totalSummary.preferentialAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t(
							'game.game_betslip.contribution_contribution'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.serviceContribution
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleNumber(
											'USDT',
											totalSummary.serviceContribution
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.actual_payment_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.payAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleNumber(
											'USDT',
											totalSummary.payAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.member_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.memberRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.memberRebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.topRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.topRebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide1'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy1RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy1RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide2'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy2RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy2RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide3'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy3RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy3RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide4'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy4RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy4RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide5'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy5RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy5RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide6'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy6RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy6RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide7'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy7RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy7RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide8'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy8RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy8RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_divide9'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.proxy9RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.proxy9RebateAmount
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.time_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											'USDT',
											summary.teamRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency('USDT')}
										{this.handleNumber(
											'USDT',
											totalSummary.teamRebateAmount
										)}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
