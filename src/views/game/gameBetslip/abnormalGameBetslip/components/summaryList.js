import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		getCurrency(key) {
			const currencyMap = {
				thbSum: { id: 'th', currency: 'THB' },
				cnySum: { id: 'zh', currency: 'CNY' },
				vndSum: { id: 'vn', currency: 'VND' },
				usdtSum: { id: 'usd', currency: 'USD' }
			}
			const map = currencyMap[key]
			return map ? map.currency : ''
		},
		// 总计行
		handleRow(params, summary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t('game.game_betslip.bet_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.betAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.member_win'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageNetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.netAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.efficient_bet'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageValidBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.validBetAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.pump_contribution'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageUserHandFeeAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.userHandFeeAmount[
											'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.pump_contribution_contribution'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPagePumpContributionAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.pumpContributionAmount[
											'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
