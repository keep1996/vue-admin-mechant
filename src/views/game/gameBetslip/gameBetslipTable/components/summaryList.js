import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		getCurrency(key) {
			const currencyMap = {
				thbSum: { id: 'th', currency: 'THB' },
				cnySum: { id: 'zh', currency: 'CNY' },
				vndSum: { id: 'vn', currency: 'VND' },
				usdtSum: { id: 'usd', currency: 'USD' }
			}
			const map = currencyMap[key]
			return map ? map.currency : ''
		},
		// 总计行
		handleRow(params, summary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t('game.game_betslip.bet_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.betAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.member_win'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageNetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.netAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.efficient_bet'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageValidBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.validBetAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.pump_contribution'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageUserHandFeeAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.userHandFeeAmount[
												'usdtSum'
											] || 0
										)}
									</p>
								</div>
							)
							break
						case this.$t(
							'game.game_betslip.pump_contribution_contribution'
						):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPagePumpContributionAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.pumpContributionAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.member_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageMemberRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.memberRebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageTopRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.topRebateAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate1'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy1RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy1RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate2'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy2RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy2RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate3'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy3RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy3RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate4'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy4RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy4RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate5'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy5RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy5RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate6'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy6RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy6RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate7'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy7RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy7RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate8'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy8RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy8RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate9'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy9RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy9RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.team_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageTeamRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.teamRebateAmount &&
												summary.teamRebateAmount[
													'usdtSum'
												]
										)}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		// 重算总计行
		handleIsResetRow(params, summary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row'>
								{this.$t('funds.sub_total')}
							</p>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('funds.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.label) {
						case this.$t('game.game_betslip.bet_amount'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.betAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.member_win'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageNetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.netAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.efficient_bet'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageValidBetAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.validBetAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break

						case this.$t('game.game_betslip.member_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageMemberRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.memberRebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageTopRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.topRebateAmount['usdtSum']
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate1'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy1RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy1RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate2'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy2RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy2RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate3'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy3RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy3RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate4'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy4RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy4RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate5'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy5RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy5RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate6'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy6RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy6RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate7'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy7RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy7RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate8'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy8RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy8RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.personal_rebate9'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageProxy9RebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.proxy9RebateAmount[
												'usdtSum'
											]
										)}
									</p>
								</div>
							)
							break
						case this.$t('game.game_betslip.team_rebate'):
							sums[index] = (
								<div>
									<p class='footer_count_row'>
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.currPageTeamRebateAmount
										)}
									</p>
									<p class='footer_count_row  footer_count_row_border'>
										{this.handleCurrency(
											this.getCurrency('usdtSum')
										)}
										{this.handleNumber(
											this.getCurrency('usdtSum'),
											summary.teamRebateAmount &&
												summary.teamRebateAmount[
													'usdtSum'
												]
										)}
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row'>-</p>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
