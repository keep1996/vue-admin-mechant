export default {
    props: {
        isReset: {
            type: Boolean,
			default: false
        }
    },
    methods: {
        formatObSettleCount(obSettleCount) {
            if (!obSettleCount) return '未重算'
            return `重算过${obSettleCount}次`
        },
        formatObSettleTitle(obSettleCount) {
            const count = this.lookMsgList.obSettleCount > 0 ? obSettleCount + 1 : obSettleCount
            if (this.isReset) {
                return '重算后注单信息'
            }
            console.log(count, '-----------')
            if (count > 0) {
               const cn = this.$t('common.chineseNumber')[count]
                return `第${cn}次结算注单详情`
            }
            return this.$t('game.game_betslip.note_information')
        },
        markSettleCountRed(obSettleCount) {
            if (obSettleCount > 0) {
                return {
                    color: 'red'
                }
            }
        },

        fetchApi(params) {
            if (this.isReset) {
                return this.$api.getGameRecordResettlement(params)
            }
            return this.$api.getGameRecordNotes(params)
        },
        fetchDetailApi(params) {
            if (this.isReset) {
                return this.$api.gameRecordResettlementDetails(params)
            }
            return this.$api.getGameRecordDetail(params)
        },
        fetchExportExcel(params) {
            if (this.isReset) {
                return this.exportExcelPublic({
					api: 'gameRecordResettlementListExport',
					params
				})
            }
            return this.exportExcelPublic({
                api: 'gameRecordsListExport',
                params
            })
        }
    }
}
