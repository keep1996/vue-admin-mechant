export default [
	{
		id: '200000',
		parentId: 0,
		// 当设置 true 的时候该路由不会在侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
		visible: '1', // (默认 false)
		orderNum: 1,
		giveMerchant: '1',
		// 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
		redirect: 'noRedirect',
		perms: 'system:user:list',
		// 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
		// 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
		// 若你想不管路由下面的 children 声明的个数都显示你的根路由
		// 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
		alwaysShow: true,
		// 1: '目录', 2: '菜单', 3: '按钮'
		// 类型 0菜单 1按钮 2其他 3子按钮
		menuType: '0',
		// 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题 (可不填写，自动拼接处理)
		name: 'Menu',
		// 路由地址 （可选）
		path: '/game/gameVenueManage/venueManage',
		// vue组件路径
		component: 'system/user/index',
		// 菜单名称对应的国际化字段 (如果是按钮，则可以直接输入中文名称) 在侧边栏和面包屑中展示的名字
		menuName: 'member.member',
		icon: 'bb_accountInfo', // 设置该路由的图标，支持 svg-class，也支持 el-icon-x element-ui 的 icon
		isCache: '1', // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
		breadcrumb: true, //  如果设置为false，则不会在breadcrumb面包屑中显示(默认 true)
		affix: false, // 如果设置为true，它则会固定在tags-view中(默认 false)
		createdAt: '2022-09-08',
		// 当路由设置了该属性，则会高亮相对应的侧边栏。
		// 这在某些场景非常有用，比如：一个文章的列表页路由为：/article/list
		// 点击文章进入文章详情页，这时候路由为/article/1，但你想在侧边栏高亮文章列表的路由，就可以进行如下设置
		activeMenu: ''
	},
	{
		id: 2,
		parentId: 0,
		// 当设置 true 的时候该路由不会在侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
		visible: '1', // (默认 false)
		orderNum: 1,
		giveMerchant: '1',
		// 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
		redirect: 'noRedirect',
		perms: 'system:user:list',
		// 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
		// 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
		// 若你想不管路由下面的 children 声明的个数都显示你的根路由
		// 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
		alwaysShow: true,
		// 1: '目录', 2: '菜单', 3: '按钮'
		menuType: '0',
		// 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题 (可不填写，自动拼接处理)
		name: 'Menu',
		// 路由地址 （可选）
		path: '/game/gameVenueManage/venueManage',
		// vue组件路径
		component: '/game/gameVenueManage/venueManage',
		// 菜单名称对应的国际化字段  在侧边栏和面包屑中展示的名字
		menuName: 'agent.bind_agent',
		icon: 'bb_accountInfo', // 设置该路由的图标，支持 svg-class，也支持 el-icon-x element-ui 的 icon
		isCache: '1', // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
		breadcrumb: true, //  如果设置为false，则不会在breadcrumb面包屑中显示(默认 true)
		affix: false, // 如果设置为true，它则会固定在tags-view中(默认 false)
		createdAt: '2022-09-08',
		// 当路由设置了该属性，则会高亮相对应的侧边栏。
		// 这在某些场景非常有用，比如：一个文章的列表页路由为：/article/list
		// 点击文章进入文章详情页，这时候路由为/article/1，但你想在侧边栏高亮文章列表的路由，就可以进行如下设置
		activeMenu: ''
	},
	{
		id: 101,
		parentId: 1,
		visible: '0', // (默认 false)
		orderNum: 1,
		giveMerchant: '1',
		// 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
		redirect: 'noRedirect',
		menuType: '1',
		perms: 'system:user:list',
		// 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
		// 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
		// 若你想不管路由下面的 children 声明的个数都显示你的根路由
		// 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
		alwaysShow: true,
		component: 'system/user/index',
		name: null, // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
		path: null,
		menuName: '新增', // 设置该路由在侧边栏和面包屑中展示的名字
		icon: 'bb_accountInfo', // 设置该路由的图标，支持 svg-class，也支持 el-icon-x element-ui 的 icon
		isCache: '1', // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
		breadcrumb: true, //  如果设置为false，则不会在breadcrumb面包屑中显示(默认 true)
		affix: false, // 如果设置为true，它则会固定在tags-view中(默认 false)
		createdAt: '2022-09-08',
		// 当路由设置了该属性，则会高亮相对应的侧边栏。
		// 这在某些场景非常有用，比如：一个文章的列表页路由为：/article/list
		// 点击文章进入文章详情页，这时候路由为/article/1，但你想在侧边栏高亮文章列表的路由，就可以进行如下设置
		activeMenu: ''
	}
]
