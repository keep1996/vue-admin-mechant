// 验证最小最大整数
const testMinAndMaxNumber = (value, callback, min, max) => {
	const pattern = /^\d+$/
	const message = `有效值范围：${min}-${max}整数`
	if (!pattern.test(value)) {
		return callback(new Error(message))
	}
	if (Number(value) < min || Number(value) > max) {
		return callback(new Error(message))
	}
	return callback()
}

// 验证最小最大整数
const testMinMaxNumber = (value, callback, min, max) => {
	const pattern = /^\d+$/
	let message = '有效值为>0整数'
	if (!pattern.test(value)) {
		return callback(new Error(message))
	}
	if (Number(value) < min) {
		return callback(new Error(message))
	}
	// max为0 标识最大值没有限制，为数字就可以了
	if (max > 0) {
		message = `有效值范围：${min}-${max}整数`
		if (Number(value) > max) {
			return callback(new Error(message))
		}
	}
	return callback()
}

// 验证最小最大浮点数
const testMinMaxFloat = (value, callback) => {
	const message = '正整数，支持小数点两位'
	const pattern = /^\d+(\.\d{1,2})?$/
	if (!pattern.test(value)) {
		return callback(new Error(message))
	}
	return callback()
}

// 验证最小最大浮点数
const testRateFloat = (value, callback) => {
	const message = '数字有效值范围：0-100（支持小数点3位）'
	const pattern = /^\d+(\.\d{1,3})?$/
	if (pattern.test(value)) {
		const vals = Number(value)
		if (vals > 100) {
			return callback(new Error(message))
		}
	} else {
		return callback(new Error(message))
	}
	return callback()
}

// 验证true或false
// const testTrueOrFalse = (value, callback) => {
// 	const message = '有效值：布尔值true或false，True或False'
// 	const valueArr = ['true', 'false', 'True', 'False']
// 	if (valueArr.indexOf(value) > -1) {
// 		return callback()
// 	}
// 	return callback(new Error(message))
// }

// 验证字符串长度
const testStrMaxLength = (value, callback, maxLength = 200) => {
	const message = `字符串长度限定${maxLength}以内`
	if (value.length > maxLength) {
		return callback(new Error(message))
	}
	return callback()
}

// 验证0或1
const testZeroOrOne = (value, callback) => {
	const message = '有效值：0或1'
	const valueArr = ['0', '1']
	if (valueArr.indexOf(value.toString()) > -1) {
		return callback()
	}
	return callback(new Error(message))
}

// 验证逗号隔开字符串不能为空
const testCommaStrNoEmpty = (value, callback) => {
	const message = '不能为空，格式为：字符串逗号隔开'
	if (!value) {
		return callback(new Error(message))
	}
	return callback()
}

const testAdjustBillAmount = (value, callback) => {
	const message = '有效值≥0(支持4位小数)'
	const pattern = /^\d+(\.\d{1,4})?$/
	if (!pattern.test(value)) {
		return callback(new Error(message))
	}
	return callback()
}

// 参数字典配置项校验
export const valueRules = {
	// 1.login 参数配置功能开关0 [有效数字：0或1]
	'login.uncommonly.ipaddress.onoff': (rule, value, callback) => {
		return testZeroOrOne(value, callback)
	},
	// 2.member 每天头像替换频率(次数) [有效值：0-100整数]
	'member.headimage.chang.rate': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 100)
	},
	// 3.payment 支付密码连续错误(次) [有效值：0-100整数]
	'payment.paypassword.wrong.times': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 100)
	},
	// 4.log 日志上报url [字符串，不超过200字]
	'log.logreport.url': (rule, value, callback) => {
		return testStrMaxLength(value, callback)
	},
	// 5.log 日志上报开关(0关1开) [有效数字：0或1 ]
	'log.logreport.onoff': (rule, value, callback) => {
		return testZeroOrOne(value, callback)
	},
	// 6.export 每次导出后等待时间;单位秒 [有效数字：0-600整数]
	'export.wait.time': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 600)
	},
	// 7.funds 资金审核刷新时间 [数组，有效值为数字，多个逗号隔开；数字范围0-600整数]
	'funds.audit.pagerefresh.interval.config': (rule, value, callback) => {
		const message = '有效值为数字，逗号隔开，数字范围0-600整数'
		const valueArr = value.split(',')
		let isError = false
		for (const item of valueArr) {
			const itemNum = Number(item)
			if (isNaN(itemNum) || itemNum > 600) {
				isError = true
				break
			}
		}
		if (isError) {
			return callback(new Error(message))
		}
		return callback()
	},
	// 8.wallet 钱包显示顺序,按gameCode配置 [数组，有效值字符串，多个逗号隔开；]
	'wallet.display.order': (rule, value, callback) => {
		return testCommaStrNoEmpty(value, callback)
	},
	// 9.domain 参数配置域名类型 [数组，有效值字符串，多个逗号隔开；]
	'domain.type': (rule, value, callback) => {
		return testCommaStrNoEmpty(value, callback)
	},
	// 10.limit 待支付订单数峰值支付限制 [有效值：0-1000整数]
	'jav.pay.order.number': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 1000)
	},
	// 11.surrogate 流水返点结算周期；1=日结，2=周结，3=月结 [有效数字范围：1，2，3]
	'surrogate.settle.period.config': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 3)
	},
	// 12.surrogate 结算时间代理返点 [数组，有效值为数字，格式[每周几，每月几号] 周有效范围：1-7 日有效范围：1-31]
	'rebate.settle.default.config': (rule, value, callback) => {
		return callback()
		// const message = '有效值为数字，逗号隔开，周：1-7，日：1-31，如：1,1'
		// const valueArr = value.split(',')
		// const weekNum = Number(valueArr[0])
		// const dateNum = Number(valueArr[1])
		// if (valueArr.length !== 2 || isNaN(weekNum) || isNaN(dateNum)) {
		// 	return callback(new Error(message))
		// }
		// if (weekNum < 1 || weekNum > 7 || dateNum < 1 || dateNum > 31) {
		// 	return callback(new Error(message))
		// }
		// return callback()
	},
	// 13.currency 币种配置 通用 币种 [{"code":"USDT","desc":"美元","symbol":"$"}] [数组，字符串]
	'dx.currency': (rule, value, callback) => {
		return callback()
		// const message = '不能为空，配置格式为：[{"key":"value"},{"key":"value"}]'
		// if (!value) {
		// 	return callback(new Error(message))
		// }
		// return callback()
	},
	// 14.member 会员默认头像 个人资料 [数组，有效值最多8个]
	'member.default.top.image.url': (rule, value, callback) => {
		const message = '有效值逗号隔开，最多8个'
		if (value.split(',').length > 8) {
			return callback(new Error(message))
		}
		return callback()
	},
	// 15.member 德州返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'texas.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 16.member 真人返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'actual.person.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 17.member 体育返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'sports.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 18.member 彩票返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'lottery.ticket.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 19.member 棋牌返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'chess.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 20.member 电竞返点比例上限 [数字有效值范围：0-1（支持小数点2位）]
	'esports.rebate.rate.limit': (rule, value, callback) => {
		return testRateFloat(value, callback)
	},
	// 21.bill 账单周期每周几结算，1到7代表周日到周六 [有效值：1-7整数]
	'proxy.loan.bill.period.dayofweek': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 7)
	},
	// 22.bill 小时账单周期>账单相关设置 [有效值：0-23整数]
	'proxy.loan.bill.period.hour': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 23)
	},
	// 23.withdraw 配置账单设置中当日可提现的数次 [有效值：0-100整数]
	'account.withdraw.member.dailyMxCount': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 100)
	},
	// 24.withdraw 单笔可提现上限 [有效值：0-100000]
	'account.withdraw.member.maxAmount': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 100000)
	},
	// 25.common 代理后台地址配置 [字符串长度限定200以内]
	'merchant.manager.url': (rule, value, callback) => {
		return testStrMaxLength(value, callback, 200)
	},
	// 26.game 在线人数和桌台人数 PC/移动端>游戏大厅[10,10][10,10] [数组，有效值范围0-10000]
	'win.bulletin': (rule, value, callback) => {
		const message = '数字，格式如：[10,10][10,10]，有效值范围0-10000'
		const pattern = /^(\[([1-9]\d{0,3}|10000)\,([1-9]\d{0,3}|10000)\]){2,2}$/
		if (!pattern.test(value)) {
			return callback(new Error(message))
		}
		return callback()
	},
	// 27.game 全局金额的单位符号；中控后台生效； [字符串]
	'global.amount.limit': (rule, value, callback) => {
		return callback()
	},
	// 28.game 德州超时(min)自动解散房间 [有效值为>0且<实际值（720）]
	'texas.time.out': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 29.game 德州大盲BB [有效值为>0且<实际值（100000）]
	'texas.blind.bb': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 30.game 德州默认带入值（倍BB） [有效值为>0且<实际值（100000）]
	'texas.default.bb': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 31.game 德州补码上下限（倍BB） [有效值为>0且<实际值（100000）]
	'texas.lower.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 32.game 德州时长 [有效值为>0且<实际值（100）]
	'texas.duration': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 33.game 德州每手牌有效底池抽水比例(%) [有效值为>0且<实际值（支持2位小数点）（1）]
	'bottom.pool.ratio': (rule, value, callback) => {
		return testMinMaxFloat(value, callback)
	},
	// 34.game 短牌超时(min)自动解散房间 [有效值为>0且<实际值（100）]
	'short.card.timeout': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 35.game 短牌Ante [有效值为>0且<实际值（100000）]
	'short.card.ante': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 36.game 短牌默认带入值（倍Ante） [有效值为>0且<实际值（100000）]
	'short.card.default': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 37.game 短牌补码上下限 倍Ante） [有效值为>0且<实际值（100000）]
	'short.card.lower.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 38.game 短牌时长 [有效值为>0且<实际值（100）]
	'short.card.duration': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 39.game 短牌每手牌有效底池抽水比例(%) [有效值为>0且<实际值（支持2位小数点）（1）]
	'short.card.pool.ratio': (rule, value, callback) => {
		return testMinMaxFloat(value, callback)
	},
	// 40.game 俱乐部人数上限 [有效值为>0且<实际值；（10000）]
	'club.maximum.number': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 41.game 同时开牌桌上限 [有效值为>0且<实际值；（10000）]
	'maximum.simultaneous.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 42.game 单牌桌旁观人数上限 [有效值为>0且<实际值；（10000）]
	'maximum.onlookers.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 43.game 单牌桌可坐下人数上限 [有效值为>0且<实际值；（10000）]
	'maximum.sit.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 44.game 德州单牌桌累计带入上限（倍BB） [有效值为>0且<实际值（10000）]
	'texas.upper.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 45.game 短牌单牌桌累计带入上限（倍Ante） [有效值为>0且<实际值（10000）]
	'short.upper.limit': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 46.member 代理层级限定 代理>代理层级配置 防止配错或配置太多，限定值为3 [有效值：0-10整数]
	'proxy.level.limit': (rule, value, callback) => {
		return testMinAndMaxNumber(value, callback, 1, 10)
	},
	// 47.member 代理后台代理入口代理后台的入口地址 url [字符串长度限定200以内]
	'backend.proxy.entrance': (rule, value, callback) => {
		return testStrMaxLength(value, callback, 200)
	},
	// 48.member 代理后台链接 通用 PC端新增代理后台入口链接的调用 url [字符串长度限定200以内]
	'proxy.backend.link': (rule, value, callback) => {
		return testStrMaxLength(value, callback, 200)
	},
	// 49.轮播图轮播的间隔时间(秒)（无实际效果，下期优化删除！！！）
	'operation.banner.interval.time': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 50.APP-banner轮播时间(秒)（无实际效果，下期优化删除！！！）
	'resource.banner.interval.time': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 1, 0)
	},
	// 默认会员信用等级
	'member.default.credit.level': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 9)
	},
	// 默认代理信用等级
	'proxy.default.credit.level': (rule, value, callback) => {
		return testMinMaxNumber(value, callback, 0, 9)
	},
	// 会员提款流水要求支持运营配置舍弃值 [值栏位仅能输入正数，且最多为4位小数]
	'account.member.adjustBillAmount': (rule, value, callback) => {
		return testAdjustBillAmount(value, callback)
	}
}
