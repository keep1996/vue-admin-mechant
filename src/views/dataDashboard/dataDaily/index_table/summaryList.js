import list from '@/mixins/list'
export default {
	mixins: [list],
	methods: {
		// 总计行
		handleRowTableDataDaily(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								合计
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'newEffectiveNum':
						case 'activeNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property] ||
												'-'}
										</span>
									</p>
								</div>
							)
							break
						case 'netAmount':
						case 'platformIncomeAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'10k',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'10k',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		},
		tableDataComparison(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								合计
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'effecactiveMemberNum':
						case 'activeMemberNum':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{totalSummary[column.property] ||
												'-'}
										</span>
									</p>
								</div>
							)
							break
						case 'platformIncomeAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span
											style={this.handleNumberColor(
												totalSummary[column.property]
											)}
										>
											{this.handleTotalNumber(
												'10k',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'10k',
												totalSummary[column.property]
											)}
										</span>
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
