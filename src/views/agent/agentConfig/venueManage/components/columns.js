// import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: '所属商户',
			prop: 'merchantName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
        {
			label: '总代账号',
			prop: 'userName',
			alignCenter: 'center',
			minWidth: '150',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt'
		},
		{
			label: '合营码',
			prop: 'joinCode',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		},
		{
			label: '彩票场馆',
			prop: 'lotteryStadium',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		},
		{
			label: '最后操作人',
			prop: 'lastOperator',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		},
		{
			label: '最后操作时间',
			prop: 'lastOperateTime',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		},
		{
			label: '备注',
			prop: 'lotteryStadiumRemark',
			alignCenter: 'center',
			minWidth: '100',
			isFixed: false,
			isShow: true,
			isShowTip: true,
			solt: 'defaultSolt',
		},
		{
			label: '操作',
			prop: 'actions',
			alignCenter: 'center',
			minWidth: '130',
			isFixed: false,
			isShow: true,
			isShowTip: false,
			solt: 'actionsSolt',
		},
	]
}

