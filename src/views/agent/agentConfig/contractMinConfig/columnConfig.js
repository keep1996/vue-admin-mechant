import i18n from '@/locales'
export default {
	1: {
		formName: 'commissionContractList',
		columns: [
			{
				label: i18n.t('agent.agent_net_win_loss'),
				suffix: '¥',
				prop: 'gameLoss',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			// {
			// 	label: '游戏流水',
			// 	suffix: '元',
			// 	prop: 'gameFlow',
			// 	input: {
			// 		min: 0,
			// 		max: 9999999999,
			// 		precision: 0
			// 	}
			// },
			{
				label: i18n.t('agent.month_active_people_number'),
				suffix: i18n.t('agent.people'),
				prop: 'monthActiveCount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			// {
			// 	label: '本月有效活跃人数',
			// 	suffix: '人',
			// 	prop: 'monthEffectiveActiveCount',
			// 	input: {
			// 		min: 0,
			// 		max: 9999999999,
			// 		precision: 0
			// 	}
			// },
			{
				label: i18n.t('agent.month_active_people_new_add'),
				suffix: i18n.t('agent.people'),
				prop: 'monthAddActiveCount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			}
		]
	},
	2: {
		formName: 'rebateContractList',
		columns: [
			{
				label: i18n.t('agent.game_trade'),
				suffix: i18n.t('agent.yuan'),
				prop: 'gameFlow',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			{
				label: i18n.t('agent.active_people_number'),
				suffix: i18n.t('agent.people'),
				prop: 'monthActiveCount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			},
			{
				label: i18n.t('agent.active_people_new_add'),
				suffix: i18n.t('agent.people'),
				prop: 'monthEffectiveActiveCount',
				input: {
					min: 0,
					max: 9999999999,
					precision: 0
				}
			}
		]
	}
}
