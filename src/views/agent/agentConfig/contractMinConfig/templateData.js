export default {
	1: {
		rebateRatio: undefined,
		gameLoss: undefined,
		gameFlow: undefined,
		monthActiveCount: undefined,
		monthEffectiveActiveCount: undefined,
		monthAddActiveCount: undefined
	},
	2: {
		rebateRatio: undefined,
		gameFlow: undefined,
		monthActiveCount: undefined,
		monthEffectiveActiveCount: undefined
	}
}
