import list from '@/mixins/list'

export default {
	mixins: [list],
	methods: {
		// 列表小计，总计行
		handleRow(params, totalSummary) {
			const { columns } = params
			const sums = [columns]
			columns?.forEach((column, index) => {
				if (index === 0) {
					sums[index] = (
						<div>
							<p class='footer_count_row footer_count_row_border'>
								{this.$t('report.total')}
							</p>
						</div>
					)
					return
				} else {
					switch (column.property) {
						case 'tyValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleNumber(
												'CNY',
												totalSummary.tyValidBetAmount,
												0
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'zrValidBetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.zrValidBetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'otherNetAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.otherNetAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'rebateAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.rebateAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'netProfitAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.netProfitAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'accountAdjustmentAmount':
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										<span>
											{this.handleTotalNumber(
												'CNY',
												totalSummary.accountAdjustmentAmount
											)}
										</span>
									</p>
								</div>
							)
							break
						case 'platformUpAmount':
								sums[index] = (
									<div>
										<p class='footer_count_row  footer_count_row_border'>
											<span>
												{this.handleTotalNumber(
													'CNY',
													totalSummary.platformUpAmount
												)}
											</span>
										</p>
									</div>
								)
								break
							case 'platformDownAmount':
								sums[index] = (
									<div>
										<p class='footer_count_row  footer_count_row_border'>
											<span>
												{this.handleTotalNumber(
													'CNY',
													totalSummary.platformDownAmount
												)}
											</span>
										</p>
									</div>
								)
								break
							case 'lastPeriodBalanceAmount':
									sums[index] = (
										<div>
											<p class='footer_count_row  footer_count_row_border'>
												<span>
													{this.handleTotalNumber(
														'CNY',
														totalSummary.lastPeriodBalanceAmount
													)}
												</span>
											</p>
										</div>
									)
									break
								case 'platformShouldReceiptAmount':
									sums[index] = (
										<div>
											<p class='footer_count_row  footer_count_row_border'>
												<span>
													{this.handleTotalNumber(
														'CNY',
														totalSummary.platformShouldReceiptAmount
													)}
												</span>
											</p>
										</div>
									)
									break
								case 'platformShouldPaymentAmount':
									sums[index] = (
										<div>
											<p class='footer_count_row  footer_count_row_border'>
												<span>
													{this.handleTotalNumber(
														'CNY',
														totalSummary.platformShouldPaymentAmount
													)}
												</span>
											</p>
										</div>
									)
									break
								case 'dxAmount':
									sums[index] = (
										<div>
											<p class='footer_count_row  footer_count_row_border'>
												<span>
													{this.handleTotalNumber(
														'CNY',
														totalSummary.dxAmount
													)}
												</span>
											</p>
										</div>
									)
									break
						default:
							sums[index] = (
								<div>
									<p class='footer_count_row  footer_count_row_border'>
										-
									</p>
								</div>
							)
							break
					}
				}
			})
			return sums
		}
	}
}
