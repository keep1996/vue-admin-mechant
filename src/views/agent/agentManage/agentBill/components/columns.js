import dayjs from 'dayjs'
import i18n from '@/locales'

export const getColumns = (vm) => {
	const filter = ({ label }) => {
		return vm.tableColumnData[label] === true || vm.tableColumnData[label] === undefined
	}

	return [
		{
			label: i18n.t('agent.bill_cycle_statrt_date'),
			prop: 'cycleStatrtDate',
			alignCenter: 'center',
			minWidth: '280',
			isFixed: false,
			defaultSlot: ({ row }) => {
				if (row.cycleStatrtDate && row.cycleEndDate) {
					return `${row.cycleStatrtDate} 至 ${row.cycleEndDate}`
				}
				return `-`
			}
		},
		{
			label: i18n.t('agent.bill_proxy_name'),
			prop: 'proxyName',
			alignCenter: 'center',
			minWidth: '150',
			defaultSlot: ({ row }) => {
				return row.proxyName
			}
		},
		{
			gameCode: 'DZ',
			label: '三方场馆输赢',
			list: [
				{
					label: i18n.t('agent.bill_ty_valid_bet_amount'),
					prop: 'tyValidBetAmount',
					alignCenter: 'center',
					minWidth: '200',
					isShowTip: true,
					itemTipContent:
						'包含体育、电竞场馆的有效投注总额',
					defaultSlot: ({ row }) => {
						return vm.amount(row.tyValidBetAmount)
					}
				},
				{
					label: i18n.t('agent.bill_zr_valid_bet_amount'),
					prop: 'zrValidBetAmount',
					alignCenter: 'center',
					minWidth: '200',
					isShowTip: true,
					itemTipContent:
					'包含真人、彩票、棋牌场馆的有效投注总额',
					defaultSlot: ({ row }) => {
						return vm.amount(row.zrValidBetAmount)
					}
				},
				{
					label: i18n.t('agent.bill_other_net_amount'),
					prop: 'otherNetAmount',
					alignCenter: 'center',
					minWidth: '120',
					defaultSlot: ({ row }) => {
						return vm.amount(row.otherNetAmount)
					}
				},
				{
					label: i18n.t('agent.bill_rebate_amount'),
					prop: 'rebateAmount',
					alignCenter: 'center',
					minWidth: '120',
					defaultSlot: ({ row }) => {
						return vm.amount(row.rebateAmount)
					}
				},
				{
					label: i18n.t('agent.bill_net_profit_amount'),
					prop: 'netProfitAmount',
					alignCenter: 'center',
					minWidth: '120',
					defaultSlot: ({ row }) => {
						return vm.amount(row.netProfitAmount)
					}
				},
				{
					label: i18n.t('agent.bill_platform_zhangcheng_rate'),
					prop: 'platformZhangchengRate',
					alignCenter: 'center',
					minWidth: '150',
					defaultSlot: ({ row }) => {
						return `${row.platformZhangchengRate}%`
					}
				}
			]
		},
		{
			label: '资金变动',
			list: [
				{
					label: i18n.t('agent.bill_account_adjustment_amount'),
					prop: 'accountAdjustmentAmount',
					alignCenter: 'center',
					minWidth: '200',
					defaultSlot: ({ row }) => {
						return vm.amount(row.accountAdjustmentAmount)
					}
				},
				{
					label: i18n.t('agent.bill_platform_up_amount'),
					prop: 'platformUpAmount',
					alignCenter: 'center',
					minWidth: '200',
					defaultSlot: ({ row }) => {
						return vm.amount(row.platformUpAmount)
					}
				},
				{
					label: i18n.t('agent.bill_platform_down_amount'),
					prop: 'platformDownAmount',
					alignCenter: 'center',
					minWidth: '200',
					defaultSlot: ({ row }) => {
						return vm.amount(row.platformDownAmount)
					}
				},
				{
					label: i18n.t('agent.bill_last_period_balance_amount'),
					prop: 'lastPeriodBalanceAmount',
					alignCenter: 'center',
					minWidth: '200',
					defaultSlot: ({ row }) => {
						return vm.amount(row.lastPeriodBalanceAmount)
					}
				}
			]
		},
		{
			label: i18n.t('agent.bill_platform_should_receipt_amount'),
			prop: 'platformShouldReceiptAmount',
			alignCenter: 'center',
			minWidth: '200',
			defaultSlot: ({ row }) => {
				return vm.amount(row.platformShouldReceiptAmount)
			}
		},
		{
			label: i18n.t('agent.bill_platform_should_payment_amount'),
			prop: 'platformShouldPaymentAmount',
			alignCenter: 'center',
			minWidth: '200',
			defaultSlot: ({ row }) => {
				return vm.amount(row.platformShouldPaymentAmount)
			}
		},
		{
			label: '德州',
			alignCenter: 'center',
			list: [
				{
					label: i18n.t('agent.bill_dx_amount'),
					prop: 'dxAmount',
					alignCenter: 'center',
					minWidth: '200',
					isShowTip: true,
					itemTipContent:'德州俱乐部服务费贡献分成+德州保险盈亏分成',
					defaultSlot: ({ row }) => {
						return vm.amount(row.dxAmount)
					}
				}
			]
		}
	].filter(filter)
}
