import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('agent.liquidation_agent_account'),
			prop: 'userName',
			minWidth: '180',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('agent.agent_level'),
			prop: 'proxyLevel',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'num'
		},
		{
			label: i18n.t('common.direct_superior'),
			prop: 'parentProxyName',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('funds.fund_audit.belong_general_agent'),
			prop: 'topProxyName',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('agent.recycle_model'),
			prop: 'recycleMode',
			minWidth: '120',
			align: 'center',
			showTip: true,
			accountType: 'recycleModelTypeArr',
			renderType: 'commonTypeSolt'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_amout_quota'),
			prop: 'beforeResetTeamCashBalanceAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_available_quota'),
			prop: 'beforeResetTeamCanUseQuotaAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_repaid_quota'),
			prop: 'beforeResetTeamShouldPayLoanAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_credit_quota'),
			prop: 'beforeResetTeamCreditQuotaAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_External_quota'),
			prop: 'beforeResetTeamExternalQuotaAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_before_teams_net_quota'),
			prop: 'beforeResetTeamNetAssetsAmount',
			minWidth: '140',
			align: 'center',
			showTip: true,
			soltColor: true,
			renderType: 'amount'
		},
		{
			label: i18n.t('agent.liquidation_time'),
			prop: 'resetCreatedAt',
			minWidth: '160',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.updateBy'),
			prop: 'operatorUser',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.actions'),
			prop: 'operate',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'slot',
			slotName: 'actionSlot'
		}
	]
}
