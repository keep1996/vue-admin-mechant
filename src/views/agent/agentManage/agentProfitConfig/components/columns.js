import i18n from '@/locales'
import { TableColumns } from '@/components/TableContainer'
const columnsElements = [
	[
		{
			prefix: i18n.t('common.strict_provide'),
			suffix: 'RebatePlatform'
		},
		{
			prefix: i18n.t('common.agent_provide'),
			suffix: 'Rebate'
		}
	],
	[
		{
			prefix: i18n.t('common.components.rebate'),
			suffix: 'Rebate'
		},
		{
			prefix: i18n.t('common.components.share'),
			suffix: 'Zhancheng'
		}
	]
]
const filterSummary4 = (value) => {
	if (typeof value !== 'number') return '-'
	if( value < 0 ) {
		return '-' 
	}
	const valueStr = '0'
	const dotIndex = valueStr.indexOf('.')
	const finalValue =
		dotIndex !== -1
			? valueStr.substring(0, valueStr.indexOf('.') + 5) - 0
			: value
	return `${finalValue.toLocaleString('en-us', {
		maximumFractionDigits: 4
	})}%`
}
const lineRender = (prefix, value, key) => (
	<div key={key}>
		{prefix}: {value < 0 ? '-' : filterSummary4(value)}
	</div>
)
const multiAgentRender = ({ row, column }) =>
	row.status === 0
		? '-'
		: columnsElements[1].map((item, index) => {
				return lineRender(
					item.prefix,
					row[`${column.property}${item.suffix}`],
					index
				)
		  })

const multiMemberRender = ({ row, column }) => {
	return filterSummary4(row[`${column.property}Rebate`]) 
}

	
const sportsKey = {
	sportsARebate: 'A盘返点',
	sportsBRebate: 'B盘返点',
	sportsCRebate: 'C盘返点',
	sportsDRebate: 'D盘返点',
}

const sportsRebateKeys = {
	1: 'sportsARebate',
	2: 'sportsBRebate',
	3: 'sportsCRebate',
	4: 'sportsDRebate'
}


const handicapTypes = {
	1: "A盘",
	2: "B盘",
	3: "C盘",
	4: "D盘"
}

const agentSportsRebate = ({ row, column }) => {
	return <div>
		{Object.keys(sportsKey).map(k => {
			return <div>
				{ sportsKey[k] }:
				 { filterSummary4(row[k])  }
			</div>
		})}

		<div>
			{i18n.t('common.components.share')}: {filterSummary4(row.sportsZhancheng)}	
		</div>

	</div>
}

const header = (title, isMember) => {
	return <div>
		{title}
		{isMember ? <p>个人返水</p> : null}
	</div>
}
	
// 公共column
export const columns = (isMember, isExpiredLogs) => {
	return [
		{
			label: i18n.t('common.serial_number'),
			width: '60px',
			customRender: ({ $index }) => {
				return $index + 1
			}
		},
		{
			label: i18n.t('agent.agent_account'),
			isShow: !isMember,
			prop: 'userName'
		},
		{
			label: '会员账号',
			isShow: isMember,
			prop: 'userName'
		},
		{
			label: i18n.t('agent.agent_level'),
			prop: 'proxyLevel',
			customRender: ({ row }) => {
				if (row.userType === 0 || isMember)
					return i18n.t('common.member')
				return row.proxyLevel.toString()
			}
		},
		{
			label: i18n.t('common.belong_merchant'),
			prop: 'merchantName'
		},
		{
			label: i18n.t('agent.total_agent_account'),
			prop: 'topProxyName'
		},
		{
			label: i18n.t('common.top_agent'),
			prop: 'parentProxyName'
		},
		{
			prop: 'texas',
			header: () => header('德州俱乐部', isMember),
			customRender: isMember
				? multiMemberRender
				: ({ row, column }) =>
						row.status === 0
							? '-'
							: lineRender(
									'分成',
									row[`${column.property}Rebate`],
									`${column.property}Rebate`
							  )
		},
		{
			prop: 'texasInsurance',
			header: () => '德州保险',
			isShow: !isMember,
			customRender: isMember
				? multiMemberRender
				: ({ row, column }) =>
						row.status === 0
							? '-'
							: lineRender(
									'分成',
									row[`${column.property}Rebate`],
									`${column.property}Rebate`
							  )
		},
		{
			prop: 'valueAddedService',
			header: () => '德州增值服务',
			isShow: !isMember,
			customRender: isMember
				? multiMemberRender
				: ({ row, column }) =>
						row.status === 0
							? '-'
							: lineRender(
									'分成',
									row[`${column.property}Rebate`],
									`${column.property}Rebate`
							  )
		},
		{
			header: () => header(i18n.t('common.game_video'), isMember),
			prop: 'actualPerson',
			customRender: isMember ? multiMemberRender : multiAgentRender
		},
		{
			header: () => header(i18n.t('common.game_sports'), isMember),
			prop: 'sports',
			customRender: isMember ?  ({ row, column }) => {
				return <div>{handicapTypes[row.proxyHandicapType] || ''} {filterSummary4(row[sportsRebateKeys[row.proxyHandicapType]])} </div>
			}  : agentSportsRebate
		},
		{
			header: () => header(i18n.t('common.game_lottery'), isMember),
			prop: 'lotteryTicket',
			customRender: isMember ? multiMemberRender : multiAgentRender
		},
		{
			// header: () => i18n.t('common.game_poker'),
			header: () => header(i18n.t('common.game_poker'), isMember),
			prop: 'chess',
			customRender: isMember ? multiMemberRender : multiAgentRender
		},
		{
			// header: () => i18n.t('common.game_e_sports'),
			header: () => header(i18n.t('common.game_e_sports'), isMember),
			prop: 'esports',
			customRender: isMember ? multiMemberRender : multiAgentRender
		},
		{
			// header: () => i18n.t('common.game_electronic'),
			header: () => header(i18n.t('common.game_electronic'), isMember),
			prop: 'electronic',
			customRender: isMember ? multiMemberRender : multiAgentRender
		},

		TableColumns.status({
			minWidth: 80,
			texts: {
				0: '未设置',
				1: '已生效',
				2: '已失效',
				3: '平台调整'
			}
		}),
		{
			label: i18n.t('friendInvitation.modify_time'),
			prop: 'updatedAt',
			minWidth: 170
		},
		{
			label: i18n.t('agent.modified_account'),
			prop: 'updatedBy'
		},
		{
			label: '失效时间',
			prop: 'lostEffectDt'
		}
	]
}
