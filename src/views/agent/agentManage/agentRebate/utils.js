export const commissionRender = (min, max) => {
    if (!min && !max) return '-'

    return `${min}% ~ ${max}%`
}
