import i18n from '@/locales'

export const getColumns = () => {
	return [
		{
			label: i18n.t('common.updateAt'),
			prop: 'createdAt',
			minWidth: '180',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.belong_merchant'),
			prop: 'merchantName',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('agent.agent_account'),
			prop: 'operationTarget',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.number_type'),
			prop: 'operatorType',
			minWidth: '120',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('member.vip_change_type'),
			prop: 'operationFunc',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('member.change_before_info'),
			prop: 'beforeModify',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('member.change_after_info'),
			prop: 'afterModify',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.updateBy'),
			prop: 'operator',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		},
		{
			label: i18n.t('common.remark'),
			prop: 'remark',
			minWidth: '140',
			align: 'center',
			showTip: true,
			renderType: 'defautlSolt'
		}
	]
}
