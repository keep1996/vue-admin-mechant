export const commissionRender = (min, max) => {
    if (!min && !max) return '-'

    return `${min}% ~ ${max}%`
}

export const rebateRender = ({ row, column }) => {
    const num = row[column.property]

    if (num !== 0 && !num) return '-'
    return `${num}%`
}
