export default function(roles) {
	// 活动列表自定义路由
	roles.push({
		id: '14',
		parentId: '0',
		permissionName: '商户',
		remark: null,
		path: '/14',
		isExist: null,
		type: '0',
		orderNum: '11',
		children: [
			{
				id: '1401',
				parentId: '14',
				permissionName: '商户管理',
				remark: null,
				path: '/14/1401',
				isExist: null,
				type: '0',
				orderNum: '1',
				children: [
					{
						id: '140101',
						parentId: '1401',
						permissionName: '商户列表',
						remark: null,
						path: '/14/1401/140101',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140102',
						parentId: '1401',
						permissionName: '盘单操作记录',
						remark: null,
						path: '/14/1401/140102',
						isExist: null,
						type: '0',
						orderNum: '2',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140103',
						parentId: '1401',
						permissionName: '新增商户',
						remark: null,
						path: '/14/1401/140103',
						isExist: null,
						type: '0',
						orderNum: '2',
						children: null,
						level: 3,
						bottom: []
					}
				],
				level: 2,
				bottom: null
			},
			{
				id: '1402',
				parentId: '14',
				permissionName: '商户审核',
				remark: null,
				path: '/14/1402',
				isExist: null,
				type: '0',
				orderNum: '1',
				children: [
					{
						id: '140201',
						parentId: '1402',
						permissionName: '新增商户审核',
						remark: null,
						path: '/14/1402/140201',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					}
				],
				level: 2,
				bottom: null
			},
			{
				id: '1403',
				parentId: '14',
				permissionName: '商户配置',
				remark: null,
				path: '/14/1403',
				isExist: null,
				type: '0',
				orderNum: '1',
				children: [
					{
						id: '140301',
						parentId: '1403',
						permissionName: '层级配置',
						remark: null,
						path: '/14/1403/140301',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140302',
						parentId: '1403',
						permissionName: '初代配置',
						remark: null,
						path: '/14/1403/140302',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140303',
						parentId: '1403',
						permissionName: '场馆配置',
						remark: null,
						path: '/14/1403/140303',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140304',
						parentId: '1403',
						permissionName: '支付通道配置',
						remark: null,
						path: '/14/1403/140304',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140305',
						parentId: '1403',
						permissionName: '游戏地址配置',
						remark: null,
						path: '/14/1403/140305',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140306',
						parentId: '1403',
						permissionName: '短信配置',
						remark: null,
						path: '/14/1403/140306',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140307',
						parentId: '1403',
						permissionName: '客服配置',
						remark: null,
						path: '/14/1403/140307',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '140308',
						parentId: '1403',
						permissionName: '短信渠道管理',
						remark: null,
						path: '/14/1403/140308',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					}
				],
				level: 2,
				bottom: null
			},
			{
				id: '1404',
				parentId: '14',
				permissionName: '操作记录',
				remark: null,
				path: '/14/1404',
				isExist: null,
				type: '0',
				orderNum: '1',
				children: [
					{
						id: '140401',
						parentId: '1404',
						permissionName: '操作记录',
						remark: null,
						path: '/14/1404/140401',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					}
				],
				level: 2,
				bottom: null
			}
		],
		level: 1,
		bottom: null
	})
}
