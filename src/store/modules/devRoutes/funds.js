/**
 * 前端自定义菜单路由
 * 不要随便更改
 *
 */
export default function(routes) {
	// 活动列表自定义路由
	routes.forEach((route) => {
		if (route.id === '150') {
			route.children.push({
				id: '1501',
				parentId: '14',
				permissionName: '充提管理',
				remark: null,
				path: '/14/1501',
				isExist: null,
				type: '0',
				orderNum: '3',
				children: [
					{
						id: '150101',
						parentId: '1401',
						permissionName: '会员充值渠道配置',
						remark: null,
						path: '/14/1501/150101',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '150102',
						parentId: '1401',
						permissionName: '会员提款渠道配置',
						remark: null,
						path: '/14/1501/150102',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '150103',
						parentId: '1401',
						permissionName: '会员提款配置',
						remark: null,
						path: '/14/1501/150103',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '150104',
						parentId: '1401',
						permissionName: '代理充值渠道配置',
						remark: null,
						path: '/14/1501/150104',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '150105',
						parentId: '1401',
						permissionName: '代理提款渠道配置',
						remark: null,
						path: '/14/1501/150105',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					},
					{
						id: '150106',
						parentId: '1401',
						permissionName: '代理提款配置',
						remark: null,
						path: '/14/1501/150106',
						isExist: null,
						type: '0',
						orderNum: '1',
						children: null,
						level: 3,
						bottom: []
					}
				],
				level: 2,
				bottom: null
			})
			route.children?.forEach((child) => {
				if (child.id === '62') {
					child.children?.push(
						{
							children: null,
							id: '866',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '62',
							path: '/58/62/866',
							permissionName: '活动列表-编辑',
							remark: '',
							type: '0'
						},
						{
							children: null,
							id: '864',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '62',
							path: '/58/62/864',
							permissionName: '活动列表-新增',
							remark: '',
							type: '0'
						},
						{
							children: null,
							id: '865',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '62',
							path: '/58/62/865',
							permissionName: '活动列表-数据',
							remark: '',
							type: '0'
						}
					)
				}
			})
		}
	})
}
