/**
 * 前端自定义菜单路由
 * 不要随便更改
 *
 */
export default function(routes) {
	// 活动列表自定义路由
	routes.forEach((route) => {
		if (route.id === '500000') {
			route.children?.forEach((child) => {
				if (child.id === '503000') {
					child.children?.push(
						{
							children: null,
							id: '5030201',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '503000',
							path: '/500000/503000/866',
							permissionName: '活动列表-编辑',
							remark: '',
							type: '0'
						},
						{
							children: null,
							id: '5030202',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '503000',
							path: '/500000/503000/5030202',
							permissionName: '活动列表-新增',
							remark: '',
							type: '0'
						},
						{
							children: null,
							id: '5030203',
							isExist: null,
							level: 3,
							orderNum: '2',
							parentId: '503000',
							path: '/500000/503000/5030203',
							permissionName: '活动列表-数据',
							remark: '',
							type: '0'
						}
					)
				}
			})
		}
		if (route.id === '800000') {
			route.children?.forEach((child) => {
				if (child.id === '803000') {
					child.children?.push({
						children: null,
						id: '8030601',
						isExist: null,
						level: 3,
						orderNum: '2',
						parentId: '803000',
						path: '/800000/803000/8030601',
						permissionName: '短信渠道管理',
						remark: '',
						type: '0'
					})
				}
			})
		}
	})
}
