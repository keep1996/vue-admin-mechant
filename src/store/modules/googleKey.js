import { validateGoogleKeyApi } from '@/api/secret'
const state = {
	googleKeyResult: ''
}

const mutations = {
	SET_GOOGLE_KEY_RESULT: (state, googleKeyResult) => {
		state.googleKeyResult = googleKeyResult
	}
}

const actions = {
	// 验证googleKey
	async getGoogleKeyInfo({ commit, state }) {
		const { data } = await validateGoogleKeyApi({
			googleKey: state.googleKey
		})
		commit('SET_GOOGLE_KEY_RESULT', data || {})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
