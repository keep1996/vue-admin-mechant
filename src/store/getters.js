const getters = {
	routes: (state) => state.permission.routes,
	language: (state) => state.app.language,
	langList: (state) => state.app.langList,
	sidebar: (state) => state.app.sidebar,
	device: (state) => state.app.device,
	visitedViews: (state) => state.tagsView.visitedViews,
	cachedViews: (state) => state.tagsView.cachedViews,
	lookUpData: (state) => state.app.lookUpData,
	token: (state) => state.user.token,
	avatar: (state) => state.user.avatar,
	nowRoute: (state) => state.permission.nowRoute,
	globalDics: (state) => state.user.globalDics,
	username: (state) => state.user.username,
	userId: (state) => state.user.id,
	userInfo: (state) => state.user.userInfo,
	vipDict: (state) => state.user.vipDict,
	userLabel: (state) => state.user.userLabel,
	// 按钮权限
	merchantList: (state) => state.user.merchantList,
	merchantInfo: (state) => state.user.merchantInfo,
	pageSizes: () => [10, 20, 50, 100, 200],
	// 是否是总控
	// isZk: (state) => !state.user.merchantInfo.id,
	isZk: (state) => state.user.userInfo.merchantId === '0',
	finger: (state) => state.app.finger,
	googleKeyResult: (state) => state.googleKey.googleKeyResult
}
export default getters
