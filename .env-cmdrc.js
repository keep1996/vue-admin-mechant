//  --------------------参数说明--------------------
//  VUE_APP_BASE_API：base api
//	VUE_APP_B_END_BASE_API 德州模块几个url需要请求B端api
//  VUE_APP_KEY：密钥key  dev不加密
//  VUE_APP_SECRET_VERSION：密钥版本 test 1  pre 1  release 2
//  VUE_APP_ENCRYPTED： 是否开启加密 1加密 0不加密  预发和生产默认加密
//  VUE_APP_ENV_FLAG： 环境标识

// 生产环境公共配置
const prodCommonConfig = {
	VUE_APP_BASE_API: '/dx-merchant-manager/',
	VUE_APP_B_END_BASE_API: '/dx-merchant-gateway/',
	VUE_APP_KEY: 'f7050f03f3ae43ab',
	VUE_APP_CAPTCHA_ID: '85ed79a1f82245de02ae8a091c39ff7d',
	VUE_APP_SECRET_VERSION: 'v1.0.0',
	VUE_APP_ENCRYPTED: '1',
	VUE_APP_ENV_FLAG: 'release',
	VUE_APP_MOCK: false
}

// 多环境配置
module.exports = {
	// dev环境
	dev: {
		// VUE_APP_BASE_API: 'http://172.31.24.20:8010/dx-merchant-manager/',
		VUE_APP_BASE_API: 'https://merchant-gateway-dev.dx252.com/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://merchant-gateway-dev.dx252.com/dx-merchant-gateway/',
		// // 本地联调打开
		// VUE_APP_BASE_API: '/api',
		// VUE_APP_B_END_BASE_API: '/Bapi',
		VUE_APp_DX_BASE_API: 'http://172.31.24.20:8115',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_CAPTCHA_ID: 'ad1a9cc36a0d06be02f4c898794fcddf',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '0',
		VUE_APP_ENV_FLAG: 'dev',
		VUE_APP_MOCK: false,
		VUE_APP_MOCK_BASE_API: '/mock/'
	},
	dev1: {
		// 本地联调注释
		VUE_APP_BASE_API:
			'https://merchant-gateway-dev1.dx252.com/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://merchant-gateway-dev1.dx252.com/dx-merchant-gateway/',
		// 本地联调打开
		// VUE_APP_BASE_API: '/api',
		// VUE_APP_B_END_BASE_API: '/Bapi',
		VUE_APp_DX_BASE_API: 'http://172.31.24.20:8115',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_CAPTCHA_ID: 'ad1a9cc36a0d06be02f4c898794fcddf',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '0',
		VUE_APP_ENV_FLAG: 'dev',
		VUE_APP_MOCK: true,
		VUE_APP_MOCK_BASE_API: '/mock/'
	},
	// qa环境
	test: {
		VUE_APP_BASE_API: 'https://dxmerchant-fat.ak12.cc/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://dxmerchant-fat.ak12.cc/dx-merchant-gateway/',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_CAPTCHA_ID: 'ad1a9cc36a0d06be02f4c898794fcddf',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '0',
		VUE_APP_ENV_FLAG: 'test',
		VUE_APP_MOCK: false
	},
	// qa1环境
	test1: {
		VUE_APP_BASE_API: 'https://dxmerchant-fat1.ak12.cc/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://dxmerchant-fat1.ak12.cc/dx-merchant-gateway/',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_CAPTCHA_ID: 'ad1a9cc36a0d06be02f4c898794fcddf',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '0',
		VUE_APP_ENV_FLAG: 'test1',
		VUE_APP_MOCK: false
	},
	// 越南团队 qa2
	test2: {
		VUE_APP_BASE_API: 'https://dxmerchant-fat2.ak12.cc/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://dxmerchant-fat2.ak12.cc/dx-merchant-gateway/',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_CAPTCHA_ID: 'ad1a9cc36a0d06be02f4c898794fcddf',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '0',
		VUE_APP_ENV_FLAG: 'test2',
		VUE_APP_MOCK: false
	},
	// 预发环境
	pre: {
		VUE_APP_BASE_API:
			'https://dxmerchant-gateway.unlfy.com/dx-merchant-manager/',
		VUE_APP_B_END_BASE_API:
			'https://dxmerchant-gateway.unlfy.com/dx-merchant-gateway/',
		VUE_APP_KEY: 'f7050f03f3ae43ab',
		VUE_APP_SECRET_VERSION: 'v1.0.0',
		VUE_APP_ENCRYPTED: '1',
		VUE_APP_ENV_FLAG: 'pre'
	},
	// 生产环境
	release: {
		...prodCommonConfig
	},
	// 商户1
	merchant_1: {
		...prodCommonConfig
	}
	// 商户n....
}
